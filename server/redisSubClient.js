//var loopback = require('loopback');
var redis = require('redis');

var redisHost = process.env.REDIS_HOST || "localhost";
var redisPort = process.env.REDIS_PORT || 6379;
var redisAuth = process.env.REDIS_AUTH || "$%^g*d33p&*";

var subClient = redis.createClient(
    redisPort, // port
    redisHost, // host
    {
        auth_pass: redisAuth,
        return_buffers: true
    }); // options hash

subClient.on('connect', function() {
    console.log('redis subscriber: redis connected');
});

subClient.on('close', function() {
    console.log('redis subscriber: redis connection closed');
});

subClient.on('ready', function() {
    console.log('redis subscriber: redis ready');
});

subClient.on('error', function(err) {
    console.log('redis subscriber: redis err ', err);
});

module.exports = subClient;
