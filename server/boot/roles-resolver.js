module.exports = function(app) {
	var Role = app.models.UserRoleType
	  , roleResolver;

	roleResolver = function(permission, context, cb) {
		var matchingRoles    = []
		  , accessToken      = context.accessToken
		  , userPermissions  = accessToken ? accessToken.userPermissions : []
		  , isSuperUser      = accessToken.superUser
		  , modelName        = context.modelName;

		if ((!accessToken.teamAccount || !accessToken.teamAccount.canEvaluate) && (
			modelName.indexOf('Scouting') !== -1 ||
			modelName.indexOf('Target') !== -1 ||
			modelName.indexOf('Prospect') !== -1)) {

			// the user is trying to access evaluation pages when his team account is not allowed to do so
			//return cb(new Error('User\'s team account is not allowed to perform evaluations.'));
			return cb(null, false);
		}

		if (accessToken.playerId && modelName === 'TeamPlayerSeasonStat') {
			// the user is a player trying to access the team-player-season-stat endpoints, proceed to check if his team account allows players to see ranks
			return cb(null, accessToken.teamAccount && accessToken.teamAccount.playersCanSeeRanks);
		}

    	cb(null, isSuperUser || (userPermissions && userPermissions.some(function (userPermission) {
			return userPermission === permission;
		})));
	};

	Role.registerResolver('TEAM-ADMIN', roleResolver);
	Role.registerResolver('GRADER', roleResolver);
	Role.registerResolver('PLAYER', roleResolver);
	Role.registerResolver('REPORT-VIEWER', roleResolver);
	Role.registerResolver('SCOUT', roleResolver);
};
