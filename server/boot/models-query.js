module.exports = function(app) {
  return; /*
  var modelKey
    , model
    , select
    , relatedModel
    , relatedModelTableName
    , relatedModelForeignKey
    , relation
    , key
    , modelName
    , connector
    , tableName
    , relations
    , from
    , where
    , relatedModelsKeys
    , withClauses
    , modelField
    , relatedModelField
    , modelFields
    , relatedModelFields
    , scope
    , settings
    , properties
    , definition
    , error
    , models             = app.models
    , getColumnsAndAlias = function(_model, fields, aliasPrefix) {
      var _modelName       = _model.modelName
        , tableName        = connector.tableEscaped(_modelName)
        , columns          = connector.getColumns(_modelName, fields)
        , columnsWithAlias = []
        , hiddenAttrs      = _model.definition.settings.hidden || []
        , columnAlias
        , field;

      aliasPrefix = aliasPrefix ? '__' + aliasPrefix + '__' : '';

      columns.replace(/"| /g, '').split(',').forEach(function(column, index) {
        field = fields[index];
        if (hiddenAttrs.indexOf(field) === -1) {

          columnAlias = aliasPrefix + field;

          if (aliasPrefix.length > 0) {
            console.log('  pushing %s into relatedModelsKeys', columnAlias);
            relatedModelsKeys.push(columnAlias);
          }

          columnsWithAlias.push(tableName + '.' + column + ' AS "' + columnAlias + '"');
        }
      });

      return columnsWithAlias.join(',');
    };

  for (modelKey in models) {
    model             = models[modelKey];

    definition        = model.definition;
    settings          = definition.settings;
    properties        = definition.properties;

    if (settings.base === 'BaseModel') {
      console.log('\nprocessing key %s, name %s',modelKey, model.modelName);

      connector         = model.getDataSource().connector;
      from              = '';
      where             = [];
      modelFields       = [];
      relatedModelsKeys = [];
      withClauses       = [];
      modelName         = model.modelName;
      tableName         = connector.tableEscaped(modelName);

      for (modelField in properties) {
        modelFields.push(modelField);
      }

      // handle special with statements that prefix entire query with a WITH clause
      // example: WITH offense as (select id from team_position_type where team_position_type.go_platoon_type_id = 1)
      for(key in settings['with[]']) {
        var withClause;
        try {
          withClause = settings['with[]'][key];
          withClauses.push(withClause);
        }
        catch (err) {
          var error = new Error("invalid with clause in with[] for %s.%s is %s" + modelKey, key, withClause);
          console.log("error found in %s, %o", modelKey, err);
          throw(error);
        }
      }

      // if we added any with clauses, prefix the query
      if (withClauses.length) {
        select = 'WITH ' + withClauses.join('\n,') + ' \n';
      } else {
        select = "";
      }

      select += 'SELECT ' + getColumnsAndAlias(model, modelFields);

      // handle loopback belongsTo relationships
      relations = model.relations;
      for(key in relations) {
        relation              = relations[key];
        relatedModel          = relation.modelTo;
        relatedModelTableName = connector.tableEscaped(relatedModel.modelName);
        relatedModelFields    = [];
        if (relation.type === 'belongsTo') {
          console.log('  creating belongsTo relationship %s for %s',key, modelKey );
          scope = relation.scope;

          if (scope && scope.fields && scope.fields.length > 0) {
            relatedModelFields = scope.fields;
          } else {
            for (relatedModelField in relatedModel.definition.properties) {
              relatedModelFields.push(relatedModelField);
            }
          }

          select  += (',' + getColumnsAndAlias(relatedModel, relatedModelFields, key));
          from   += (relatedModelTableName + ',');
          console.log('  pushing into where');
          where.push(tableName + '.' + relation.keyFrom + '=' + relatedModelTableName + '.' + relatedModel.getIdName());
        }
      }

      // handle special hasMany relation that returns results as an array of ids
      relations = settings['relations[]'];
      for(key in relations) {
        relation = relations[key];
        try {
          if (relation.model) {
            relatedModelTableName = connector.tableEscaped(relation.model);
          } else {
            relatedModelTableName = "";
          }
          if (relation.foreignKey) {
            relatedModelForeignKey = connector.columnEscaped(relation.model, relation.foreignKey);
          } else {
            relatedModelForeignKey = "";
          }
          if (relation.type === 'hasMany') {
            select += ('\n, array(select ' + relatedModelTableName + '.' + relatedModel.getIdName() +
            ' from ' + relatedModelTableName +
            ' where ' + relatedModelTableName + '.' + relatedModelForeignKey + ' = ' + tableName + '.' + model.getIdName() + ') as "' + key + '"'  );
          }
          else if (relation.type === 'hasOne') {
            select += (
            '\n,(select ' + relatedModel.getIdName() +
            ' from ' + relatedModelTableName +
            ' where ' + relatedModelTableName + '.' + relatedModelForeignKey + ' = ' + tableName + '.' + model.getIdName() +
            ') as "' + key + '"'
            );
          }
        } catch(err) {
          error = new Error('invalid relationship in relations[] for '+modelKey);
          console.log("error found in %s, %o", modelKey, err);
          throw(error);
        }
      }

      // handle special queryColumns that returns a custom query for a single
      // column in the select clause
      for(key in settings['queryColumns[]']) {
        var queryColumn;
        try {
          queryColumn = settings['queryColumns[]'][key];
          if (queryColumn.query) {
            select += ('\n, (' + queryColumn.query + ') as "' + key + '"'  );
          }
        } catch(err) {
          error = new Error("invalid queryColumn %s for %s", key, modelKey);
          console.log("error found in %s, %o", modelKey, err);
          throw(error);
        }
      }

      // Adding query parameters keys
      select += ('\n\nFROM ' + from + connector.tableEscaped(model.modelName) + ' \n\n');

      model['query']             = select;
      model['relatedModelsKeys'] = relatedModelsKeys;
      model['whereJoin']         = where.join(' AND ');
    }
  } */
};

