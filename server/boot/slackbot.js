var Slack = require('slack-client');

module.exports = function(app) {
	var _slack;
	// This code inits the slack bot in charge to send messages from the api to the team's slack, only if
	// GODEEPBOT_TOKEN env variable is provided, this in order to avoid notifications form development envs

	if (process.env.GODEEPBOT_TOKEN) {

		_slack = new Slack(process.env.GODEEPBOT_TOKEN, true, true);

		_slack.on('open', function() {
			//var channel;

			console.log(_slack.self.name + ' connected to ' + _slack.team.name + ' Slack');

			// channel id: C0FPD0SCR
			//channel = _slack.getChannelByName('logins');

			//channel.send("Ready to inform about user logins");

			// setting the slackbot as global attr to the app
			app['slackbot'] = {
				send : function(msg, channelName) {
					if (msg && channelName) {
						_slack.getChannelByName(channelName).send(msg);
						console.log('GODEEPBOT. Notification sent');
					}
				},

				sendLoginNotification: function(user) {
					console.log('GODEEPBOT. Sending login notification..');
					this.send(user.firstName + ' ' + user.lastName + ' (' + user.email + ') has logged into ' + process.env.GODEEP_ENV, 'logins');
				},

				sendNewOrganizationNotification: function(organization) {
					console.log('GODEEPBOT. Sending new organization notification..');
					this.send(
						organization.organizationType.name + ' ' +
						organization.name + ' (' +
						organization.conference.name + ') has been added'
					, 'new_organization');
				},

				sendNewUserNotification: function(user) {
					console.log('GODEEPBOT. Sending new user notification..');
					this.send('User ' + (user.username || user.email) + ' (' + user.firstName + ' ' + user.lastName + ' from ' + user.team + ') has been created', 'new_user');
				}
			};
		});

		_slack.on('error', function(err) {
			console.log(_slack.self.name + ' got an error:', err);
		});

		console.log('Connecting slackbot ...');
		_slack.login();
	} else {
		console.log('GODEEPBOT is not initialized!');
	}

};
