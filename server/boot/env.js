module.exports = function enableEnvCommand(server) {
    server.use('/env', function(req, res, next) {
        var env = process.env;
        var result = ["<ul>"];
        for (var key in env) {
            result.push("<li>" + key + ": " + env[key] + "</li>");
        }
        result.push("</ul>");
        res.send(result.join(""));
    });
};
