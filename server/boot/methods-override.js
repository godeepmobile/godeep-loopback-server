module.exports = function(app) {
	var pwd_encrypted = process.env.PWD_ENCRYPTED || true;
	var User = app.models.GoUser;

	if (!pwd_encrypted) {
		console.log('==== NOT USING HASHED PASSWORDS ====');
		User.prototype.hasPassword = function(plain, fn) {
			fn(null, (this.password === plain));
		};
	} else {
		console.log('==== USING HASHED PASSWORDS ====');
	}
};
