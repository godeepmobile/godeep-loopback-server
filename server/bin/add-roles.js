var path = require('path')
  , app  = require(path.resolve(__dirname, '../server'))
  , Role = app.models.lb_role;

Role.create([{
	name: 'Admin'
}, {
	name: 'Team-Admin'
}, {
	name: 'Grader'
}, {
	name: 'Player'
}], function(err) {
	if (err) throw err;
	console.log('Roles Created!');
});