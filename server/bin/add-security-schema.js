var path       = require('path')
  , app        = require(path.resolve(__dirname, '../server'))
  , dataSource = app.dataSources['loopback-demo'];

// Create the security models on the database
dataSource.automigrate(['lb_user', 'lb_role', 'lb_acl', 'lb_rolemapping', 'lb_accesstoken'], function(err){
	
	if (err) throw err;

	console.log('Security Models Created!');

});

