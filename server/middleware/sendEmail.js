/**
 * Send email.
 *
 * This middleware send email to the client.
 *
 * @return {Function}
 * @api public
 */

var path       = require('path');
var nodemailer = require('nodemailer');
var hbs        = require('express-handlebars');
var neh        = require('nodemailer-express-handlebars');

//TODO: implement this the Node way: add a callback that takes an err and response parameter. tell the caller if this succeeded or not.
module.exports = function sendEmail(to, subject, mailOptions) {
  var emailTo = process.env.EMAIL_TO_BY_DEFAULT || to;
  console.log('Mail service: sending email to: %s using template: %s', emailTo, mailOptions.template);
  var emailSenderPass = process.env.EMAIL_SENDER_PASS
    , emailSenderAddress = process.env.EMAIL_SENDER_ADDRESS;
  // Template engine setup
  //TODO: move these outside of the function into the module global space, only need to do this setup once.
  var viewsPath = path.join(__dirname, '../views')
    ,templatePath = path.join(viewsPath, 'email');
  var viewEngine = hbs.create({
    //Enable to use handlebars' layouts and partials
    /*layoutsDir: path.join(viewsPath, 'layouts'),
    partialsDir: path.join(viewsPath, 'partials'),
    defaultLayout: 'layout',
    extname: '.hbs'*/
  });
  var options = neh({
    viewEngine: viewEngine,
    viewPath: templatePath,
    extName: '.hbs'
  });

  // Transport to send email
  var transporter = nodemailer.createTransport({
    service: 'gmail',
    auth: {
        user: emailSenderAddress,
        pass: emailSenderPass
      }
  });
  transporter.use('compile', options);
  transporter.sendMail({
      from:  {
          name   : 'GoDeep Support',
          address: emailSenderAddress
      },
      to      : emailTo,
      subject : subject,
      template: mailOptions.template,
      context : mailOptions.context
  }, function (error, response) {
    if (error) {
      console.log('error sending email: ', error);
    }
 });
};
