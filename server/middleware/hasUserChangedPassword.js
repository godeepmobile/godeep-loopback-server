/**
 * Ensure that user changes the password.
 *
 * This middleware ensures that user changes the password.
 *
 * @return {Function}
 * @api public
 */
module.exports = function hasUserChangedPassword() {
    return function(req, res, next) {
        var token = req.session.accessToken;

        if (token && token.hasUserChangedPassword) {
            console.log('hasUserChangedPassword: user has changed password');
            return next();
        } else {
            console.log('hasUserChangedPassword: user has not changed password');
            return res.redirect('/change-password');
        }
    };
};
