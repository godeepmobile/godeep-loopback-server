/**
 * Check a user for superuser privileges. If true, then force superuser to
 * select a team/user before proceeding.
 *
 * This middleware ensures that a team user is logged in.  If a request is received
 * for a superuser, the request will be redirected to a team/user select page (by
 * default to `/selectteam`).
 *
 * Additionally, `returnTo` will be be set in the session to the URL of the
 * current request.  After selection, this value can be used to redirect
 * the user to the page that was originally requested.
 *
 * Options:
 *   - `redirectTo`   URL to redirect to for login, defaults to _/selectteam_
 *   - `setReturnTo`  set redirectTo in session, defaults to _true_
 *
 * Examples:
 *
 *     app.get('/profile',
 *       checkSuperUser(),
 *       function(req, res) { ... });
 *
 *     app.get('/profile',
 *       checkSuperUser('/signin'),
 *       function(req, res) { ... });
 *
 *     app.get('/profile',
 *       checkSuperUser({ redirectTo: '/session/new', setReturnTo: false }),
 *       function(req, res) { ... });
 *
 * @param {Object} options
 * @return {Function}
 * @api public
 */
module.exports = function checkSuperUser(options) {
  if (typeof options === 'string') {
    options = { redirectTo: options };
  }
  options = options || {};

  var url = options.redirectTo || '/superuser';
  return function(req, res, next) {
    var token = req.session.accessToken;

    if (token && token.superUser) {
      console.log('checkSuperUser: %s is superUser', token.username);
      req.user = token.user;
      return res.redirect(url);
    } else {
      console.log('checkSuperUser: not superUser');
    }
    next();
  };
};
