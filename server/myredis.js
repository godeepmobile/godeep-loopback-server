//var loopback = require('loopback');
var redis = require('redis');

var redisHost = process.env.REDIS_HOST || "localhost";
var redisPort = process.env.REDIS_PORT || 6379;
var redisAuth = process.env.REDIS_AUTH || "$%^g*d33p&*";

var client = redis.createClient(
    redisPort, // port
    redisHost, // host
    {
        auth_pass: redisAuth
    }); // options hash

client.on('connect', function() {
    console.log('myredis: redis connected');
});

client.on('close', function() {
    console.log('myredis: redis connection closed');
});

client.on('ready', function() {
    console.log('myredis: redis ready');
});

client.on('error', function(err) {
    console.log('myredis: redis err ', err);
});

module.exports = client;
