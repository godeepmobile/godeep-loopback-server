var envarr = [];
var env = process.env;
for (var key in env) {
    if (env.hasOwnProperty(key)) {
        if (key.toLowerCase().indexOf('password') === -1) {
            envarr.push(key + '=' + env[key]);
        } else {
            envarr.push(key + '=' + '********');
        }
    }
}
console.log(envarr.sort());
env = null;
envarr = null;

if (process.env.APP_NAME) {
  console.log('New Relic monitoring is enabled');
  require('newrelic');
}

var loopback = require('loopback');
var boot = require('loopback-boot');
var assert = require('assert');

var cliDeploy = require('./cli-deploy');
//var checkSuperUser = require('./middleware/checkSuperUser');
var isSuperUser = require('./middleware/isSuperUser');
var hasUserChangedPassword = require('./middleware/hasUserChangedPassword');
var sendEmail = require('./middleware/sendEmail');
var bodyParser = require('body-parser');
var path = require('path');
var flash = require('express-flash');
var exphbs  = require('express-handlebars');
//var WebSocketServer = require('ws').Server;
var ioRedis = require('socket.io-redis');

var session = require('express-session');
var RedisStore = require('connect-redis')(session);
var redisClient = require('./myredis');
var redisSubClient = require('./redisSubClient');
var redisPubClient = require('./redisPubClient');

var app = module.exports = loopback();

var WS_COMMANDS = {
  setToken    : 0,
  refreshPage : 1
};

var dumpState = function (msg, req, res) {
  if (req.session) {
    console.log('%s: session.accessToken is ', msg, req.session.accessToken);
  }
  console.log('%s: cookies is ', msg, req.cookies);
  console.log('%s: signed cookies is ', msg, req.signedCookies);
};

var noLocalStorageWarningDefault = 'GoDeep is unable to create a successful log in session on your machine.<br/>' +
  'To allow your browser to save your login credentials, please remove any restrictions on cookies or local data storage.';

var noLocalStorageWarningSafari = 'GoDeep is unable to create a successful log in session on your machine.<br/>' +
  'To allow your browser to save your login credentials, please disable Private Browsing or remove any restrictions on cookies or local data storage.<br/>' +
  'Please refer to <a href="https://support.apple.com/en-us/HT203036" target="_blank">these instructions</a> on how to disable Private Browsing.';

var noLocalStorageWarningMsg = 'GoDeep is unable to create a successful log in session on your machine.<br/>' +
  'To allow your browser to save your login credentials, please remove any restrictions on cookies or local data storage.<br/>' +
  'Please refer to <a href="__link__" target="_blank">these instructions</a> on how to do it.';

var helpLinkChrome = 'https://support.google.com/chrome/answer/95647?hl=en';

var helpLinkFirefox = 'https://support.mozilla.org/en-US/kb/enable-and-disable-cookies-website-preferences';

var helpLinkIE = 'http://windows.microsoft.com/en-US/internet-explorer/delete-manage-cookies#ie=ie-11';

//app.middleware('auth', isSuperUser);
//app.middleware('routes:before', checkSuperUser);

app.use(loopback.context());

// Set up the /favicon.ico
app.use(loopback.favicon());
app.use(loopback.compress());

// Settup to use jade template engine **************************
/*app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');
*/
// End - Setup to use java template engine ********************

// Setup to use express-handlebars template engine **************************
app.set('views', path.join(__dirname, 'views'));
var hbsConfig = {
    layoutsDir: path.join(app.settings.views, 'layouts'),
    partialsDir: path.join(app.settings.views, 'partials'),
    defaultLayout: 'layout',
    extname: '.hbs'
};

app.engine('.hbs', exphbs(hbsConfig));
app.set('view engine', '.hbs');
// End - Setup to use express-handlebars template engine ********************

// Setup to use static files
app.use(loopback.static(path.resolve(__dirname, '../public')));


// Bootstrap the application, configure models, datasources and middleware.
// Sub-apps like REST API are mounted via boot scripts.
boot(app, __dirname);

app.use(session({
  store: new RedisStore({
    client: redisClient
  }),
  saveUninitialized: true,
  resave: true,
  secret: app.get('cookieSecret')
}));

// to support JSON-encoded bodies
app.use(bodyParser.json());

// to support URL-encoded bodies
app.use(bodyParser.urlencoded({
  extended: true
}));

app.use(function noCachePlease(req, res, next) {
  if (req.url === '/coach' || req.url === '/player' || req.url === '/login' || req.url === '/auth/local') {
    res.header("Cache-Control", "no-cache, no-store, must-revalidate");
    res.header("Pragma", "no-cache");
    res.header("Expires", 0);
  }
  next();
});

// The access token is only available after boot
app.use(loopback.token({
  model: app.models.lb_accesstoken
}));

app.use(loopback.cookieParser(app.get('cookieSecret')));

/*
app.use(loopback.session({
  secret: app.get('cookieSecret'),
  saveUninitialized: true,
  resave: true
}));
*/

app.use(flash());

/**
 * Determine which client interface to load for the logged in user. It can use
 * either user permissions or the superUser flag to determine the correct client.
 *
 * @param {token} the accessToken for the logged in user
 * @param {cb} callback function that receives route for the client interface to redirect to
 */
var whichClientRoute = function (token, cb) {
  assert(token, 'token is not null');
  console.log('whichClientRoute');
  var permissions = token.userPermissions;

  function coachPermission(element, index, array) {
    return element === 'GRADER' || element === 'TEAM-ADMIN';
  }

  function playerPermission(element, index, array) {
    return element === 'PLAYER';
  }

//TODO: change hasUserChangedPassword into middleware
  /*  if (!token.hasUserChangedPassword) {
    cb(null, '/change-password/');
  } else */
  if (token.superUser) {
    return cb(null, '/superuser/');
  } else if (permissions.some(coachPermission)) {
    return cb(null, '/coach/');
  } else if (permissions.some(playerPermission)) {
    return cb(null, '/player/');
  } else {
    // signal error
    return cb(new Error('user has no valid client permissions'));
  }
};

/**
 * Login a specified user. Redirect to the correct client route upon successful
 * login.
 *
 * @param {user} user credentials to pass to login function
 * @param {req}
 * @param {res}
 */
var userLogin = function (user, req, res) {
  assert(user, 'user is defined');

console.log('userLogin');
  // make sure username is lower case before attempting to login
  if (user.username) {
    user.username = user.username.toLowerCase().trim();
  }
  app.models.GoUser.login(user, "user", function (err, accessToken) {
    var slackbot = app.slackbot
      , userObject;

    if (err) {
      console.log('userLogin: error logging in user %s, %s ', user.username, err.message);
      req.flash('error', err.message);
      return res.redirect('back');
    }

    userObject = accessToken.toJSON().user;

    
    if (slackbot) {
      slackbot.sendLoginNotification(userObject);
    }

    // keep the accessToken in the current session.
    req.session.accessToken = accessToken;

    // which client interface should be shown for this user?
    whichClientRoute(accessToken, function whichClientRouteRedirect(err, clientRoute){
        if (err) {
          req.flash('error', err.message);
          return res.redirect('/');
        } else {
          return res.redirect(clientRoute);
        }
    });
  });
};

/**
 * Main route for the server. Presents a simple login form allowing a user to login
 * to the server.
 *
 * @param {req}
 * @param {res}
 * @param {next}
 */
app.get('/', function (req, res) {
  var clientBrowser, warn = noLocalStorageWarningDefault, isUnsupportedDevice;

  if (req.session && req.session.accessToken) {
    delete req.session.accessToken;
  }

  if (req.headers && req.headers['user-agent']) {
    clientBrowser = req.headers['user-agent'].toLowerCase();

    if (clientBrowser.indexOf('safari') !== -1 && clientBrowser.indexOf('chrome') === -1) {
      console.log('client browser is Safari');
      warn = noLocalStorageWarningSafari;
    }

    if (clientBrowser.indexOf('chrome') !== -1) {
      console.log('client browser is Chrome');
      warn = noLocalStorageWarningMsg.replace('__link__', helpLinkChrome);
    }

    if (clientBrowser.indexOf('firefox') !== -1) {
      console.log('client browser is Firefox');
      warn = noLocalStorageWarningMsg.replace('__link__', helpLinkFirefox);
    }

    if (clientBrowser.indexOf('msie') !== -1) {
      console.log('client browser is IE');
      warn = noLocalStorageWarningMsg.replace('__link__', helpLinkIE);
    }

    isUnsupportedDevice = /iPhone|iPod|Android|webOS|Blackberry|MeeGo|BB|Windows Phone/i.test(clientBrowser);
    console.log('clientBrowser:', clientBrowser);
  }

  //dumpState('/', req, res);
  console.log('/: render login page');
  res.render('pages/login', {
    user: req.user,
    url: req.url,
    noLocalStorageWarning: warn,
    isUnsupportedDevice: isUnsupportedDevice
  });
});

var tests = [
// {d:"1",q:"select * from rpt_aggregate_grades('3b64ab51-f121-4197-ad3c-1dc05bdb4286', 'game-performance', 'player',   NULL, 'season', NULL)"}
//,{d:"2",q:"select * from rpt_aggregate_grades('3b64ab51-f121-4197-ad3c-1dc05bdb4286', 'game-performance', 'platoon',  NULL, 'season', NULL)"}
//,{d:"3",q:"select * from rpt_aggregate_grades('3b64ab51-f121-4197-ad3c-1dc05bdb4286', 'game-performance', 'position', NULL, 'season', NULL)"}
//,{d:"4",q:"select * from rpt_aggregate_grades('3b64ab51-f121-4197-ad3c-1dc05bdb4286', 'game-performance', 'player',   NULL, 'event',  NULL)"}
//,{d:"5",q:"select * from rpt_aggregate_grades('3b64ab51-f121-4197-ad3c-1dc05bdb4286', 'game-performance', 'platoon',  NULL, 'event',  NULL)"}
//,{d:"6",q:"select * from rpt_aggregate_grades('3b64ab51-f121-4197-ad3c-1dc05bdb4286', 'game-performance', 'position', NULL, 'event',  NULL)"}
//,{d:"7",q:"select * from rpt_aggregate_grades('3b64ab51-f121-4197-ad3c-1dc05bdb4286', 'game-performance', 'player',   NULL, 'season', 2015)"}
//,{d:"8",q:"select * from rpt_aggregate_grades('3b64ab51-f121-4197-ad3c-1dc05bdb4286', 'game-performance', 'platoon',  NULL, 'season', 2015)"}
//,{d:"9",q:"select * from rpt_aggregate_grades('3b64ab51-f121-4197-ad3c-1dc05bdb4286', 'game-performance', 'position', NULL, 'season', 2015)"}
//,{d:"10",q:"select * from rpt_aggregate_grades('3b64ab51-f121-4197-ad3c-1dc05bdb4286', 'game-performance', 'player',   NULL, 'event',  5020)"}
//,{d:"11",q:"select * from rpt_aggregate_grades('3b64ab51-f121-4197-ad3c-1dc05bdb4286', 'game-performance', 'platoon',  NULL, 'event',  5020)"}
//,{d:"12",q:"select * from rpt_aggregate_grades('3b64ab51-f121-4197-ad3c-1dc05bdb4286', 'game-performance', 'position', NULL, 'event',  5020)"}
{d:"13",q:"select * from rpt_aggregate_grades('3b64ab51-f121-4197-ad3c-1dc05bdb4286', 'game-performance', player',   '7a55bae0-f9d4-4add-af92-e7396c2d032c', 'season', NULL)"}
,{d:"14",q:"select * from rpt_aggregate_grades('3b64ab51-f121-4197-ad3c-1dc05bdb4286', 'game-performance', 'platoon',  '1', 'season', NULL)"}
,{d:"15",q:"select * from rpt_aggregate_grades('3b64ab51-f121-4197-ad3c-1dc05bdb4286', 'game-performance', 'position', '1', 'season', NULL)"}
,{d:"16",q:"select * from rpt_aggregate_grades('3b64ab51-f121-4197-ad3c-1dc05bdb4286', 'game-performance', 'player',   '7a55bae0-f9d4-4add-af92-e7396c2d032c', 'event',  NULL)"}
,{d:"17",q:"select * from rpt_aggregate_grades('3b64ab51-f121-4197-ad3c-1dc05bdb4286', 'game-performance', 'platoon',  '1', 'event',  NULL)"}
,{d:"18",q:"select * from rpt_aggregate_grades('3b64ab51-f121-4197-ad3c-1dc05bdb4286', 'game-performance', 'position', '1', 'event',  NULL)"}

,{d:"19",q:"select * from rpt_aggregate_grades('3b64ab51-f121-4197-ad3c-1dc05bdb4286', 'game-performance', 'player',   '7a55bae0-f9d4-4add-af92-e7396c2d032c', 'season', 2015)"}
,{d:"20",q:"select * from rpt_aggregate_grades('3b64ab51-f121-4197-ad3c-1dc05bdb4286', 'game-performance', 'platoon',  '1',    'season', 2015)"}
,{d:"21",q:"select * from rpt_aggregate_grades('3b64ab51-f121-4197-ad3c-1dc05bdb4286', 'game-performance', 'position', '1',    'season', 2015)"}
,{d:"22",q:"select * from rpt_aggregate_grades('3b64ab51-f121-4197-ad3c-1dc05bdb4286', 'game-performance', 'player',   '7a55bae0-f9d4-4add-af92-e7396c2d032c', 'event',  5020)"}
,{d:"23",q:"select * from rpt_aggregate_grades('3b64ab51-f121-4197-ad3c-1dc05bdb4286', 'game-performance', 'platoon',  '1',    'event',  5020)"}
,{d:"24",q:"select * from rpt_aggregate_grades('3b64ab51-f121-4197-ad3c-1dc05bdb4286', 'game-performance', 'position', '1',    'event',  5020)"}
];

var getTeamSeasonEventPlays = function (_teamId, cb) {
  assert(_teamId, 'team ID is defined');
  var connector = app.models.Team.getDataSource().connector;
//  var requestQuery = 'SELECT  * from godeepmobile.view_season_event_plays';
  var requestQuery = "select * from rpt_aggregate_grades('3b64ab51-f121-4197-ad3c-1dc05bdb4286', 'player',   NULL, 'event',  5020)";

  tests.forEach( function(test) {
    connector.executeSQL(test.q, [], function (err, records) {
      console.log("\n%s: %s", test.d, test.q);
      if (err) {
        console.log("error: ", err);
      } else {
        var results = records[0].rpt_aggregate_grades;
        console.log("results: ", JSON.stringify(results, null, 4));
      }
    });
  });
  return cb(null, []);
};

app.get('/plays', function (req, res) {
  //dumpState('/', req, res);
  getTeamSeasonEventPlays(1, function(err, plays){
    res.send('ok');
  });
});

/**
 * Main route post handler for the server. Accepts the user credentials in the
 * req bodyand attempts to login the user.
 *
 * @param {req}
 * @param {res}
 * @param {next}
 */
app.post('/', function (req, res) {
  var user = {
    username: req.body.username.trim(),
    password: req.body.password
  };
  console.log('/: attempt to login user');
  return userLogin(user, req, res);
});

/**
 * Coach client route(s) for the server. Sends the coach client to the user's browser.
 *
 * @param {req}
 * @param {res}
 * @param {next}
 */
app.get('/coach', hasUserChangedPassword(), function (req, res) {
  var token = req.session.accessToken;
//  dumpState('/coach/', req, res);
//  assert(token, 'accessToken is defined');
  console.log('/coach: call cliDeploy, query is ', req.query);
  req.session.client = 'coach';

  // this call is in charge to refresh some session attrs when the page refresh
  if (token && token.user) {
    app.models.GoUser.findById(token.user.id, function(err, user) {
      if (err) {
        console.log('/coach: the user on the request colud not be found');
        res.set('Content-Type', 'text/html');
        return res.send("<p>Could not find user profile</p>");
      } else {
        console.log('/coach: refreshing user object');
        // attrs which need to be refreshed
        req.session.accessToken.user.firstTimeLogin = user.firstTimeLogin;
        req.session.accessToken.user.displayWelcomeDialog = user.displayWelcomeDialog;
        return cliDeploy(req, res);
      }
    });
  } else {
    return cliDeploy(req, res);
  }
});

app.get('/coach/*', hasUserChangedPassword(), function (req, res) {
  var token = req.session.accessToken;
//  dumpState('/coach/', req, res);
//  assert(token, 'accessToken is defined');
  console.log('/coach/*: calling cliDeploy, query is ', req.query);
  req.session.client = 'coach';
  
  // this call is in charge to refresh some session attrs when the page refresh
  if (token && token.user) {
    app.models.GoUser.findById(token.user.id, function(err, user) {
      if (err) {
        console.log('/coach/*: the user on the request colud not be found');
        res.set('Content-Type', 'text/html');
        return res.send("<p>Could not find user profile</p>");
      } else {
        console.log('/coach/*: refreshing user object');
        req.session.accessToken.user.firstTimeLogin = user.firstTimeLogin;
        return cliDeploy(req, res);
      }
    });
  } else {
    return cliDeploy(req, res);
  }
});

/**
 * Player client route(s) for the server. Sends the player page to the user's browser.
 *
 * @param {req}
 * @param {res}
 * @param {next}
 */
app.get('/player', hasUserChangedPassword(), function (req, res) {
  var token = req.session.accessToken;
  //dumpState('/coach', req, res);
  assert(token, 'accessToken is defined');
  console.log('/player: calling cliDeploy, query is ', req.query);
  req.session.client = 'player';
  return cliDeploy(req, res);
});
app.get('/player/*', hasUserChangedPassword(), function (req, res) {
  var token = req.session.accessToken;
  //dumpState('/player/*', req, res);
  assert(token, 'accessToken is defined');
  console.log('/player/*: calling cliDeploy, query is ', req.query);
  req.session.client = 'player';
  return cliDeploy(req, res);
});

/**
 *
 *
 * @param {_teamId} id of team to search
 * @param {cb}  callback function that passes the found TeamUsers
 * @param {next}
 */
var getTeamUsers = function (_teamId, cb) {
  assert(_teamId, 'team ID is defined');
  var connector = app.models.Team.getDataSource().connector;
  var requestQuery = 'SELECT  * from godeepmobile.view_active_user_non_players where "go_team_id" = $1';

  connector.executeSQL(requestQuery, [_teamId], function (err, records) {
    if (err) {
      console.log("getTeamUsers: query %s \nhas error ", requestQuery, err);
      return cb(err);
    } else {
      return cb(null, records);
    }
  });
};

app.get('/superuser', isSuperUser(), hasUserChangedPassword(), function (req, res) {
  var token = req.session.accessToken;
  //dumpState('/superuser', req, res);
  assert(token, 'accessToken is defined');

  res.render('pages/superuser', {
    user: token.user,
    url: req.url
  });
});

app.get('/selectteam', isSuperUser(), hasUserChangedPassword(), function (req, res) {
  var Team = app.models.Team;
  var User = app.models.GoUser;
  var teamId = req.query.team;
  var userId = req.query.user;
  var users = null;
  var token = req.session.accessToken;
  //dumpState('/selectteam', req, res);
  assert(token, 'accessToken is defined');

  if (userId) {
    console.log('/selectteam: superuser selected user ', userId);

    User.findById(userId, function (err, user) {
      if (err) {
        console.log('/selectteam: error finding selected user ', err);
        req.flash('error', err.message);
        return res.redirect('back');
      }
      console.log('/selectteam: trying to login superuser as %s', user.username);

      var userParms = {
        username: user.username,
        password: user.password
      };
      return userLogin(userParms, req, res);
    });
  } else {
    console.log('/selectteam: looking for teams');
    Team.find(function (err, teams) {
      if (err) {
        console.log('/selectteam: error finding teams ', err);
        req.flash('error', err.message);
        return res.redirect('back');
      }
      console.log("/selectteam: found teams");
      if (teamId) {
        // find users for this team here
        console.log('/selectteam: looking for team users for team ', teamId);
        getTeamUsers(teamId, function (err, users) {
          if (err) {
            console.log('/selectteam: error finding users ', err);
            req.flash('error', err.message);
            return res.redirect('back');
          }
          console.log('/selectteam: rendering page with team selected');
          res.render('pages/selectteam', {
            teams: teams,
            team: teamId,
            users: users,
            url: req.url,
          });
        });
      } else {
        console.log('/selectteam: rendering page without team selected');
        res.render('pages/selectteam', {
          teams: teams,
          team: teamId,
          users: null,
          url: req.url,
        });
      }
    });
  }
});

app.get('/new-organization', isSuperUser(), hasUserChangedPassword(), function (req, res) {
  var token = req.session.accessToken;
  //dumpState('new-organization', req, res);
  assert(token, 'accessToken is defined');

  app.models.Conference.find({
    order: 'name ASC'
  }, function (err, conferences) {
    if (err) {
      console.log('/new-organization: error finding conferences ', err);
      req.flash('error', err.message);
      return res.redirect('back');
    } else {
      res.render('pages/new-organization', {
        user: req.user,
        conferences: conferences,
        url: req.url
      });
    }
  });
});

var findOrCreateUserTeamAssignment = function (req, team, user, cb) {
  assert(team, 'team is not null');
  assert(user, 'user is not null');
  console.log('findOrCreateUserTeamAssignment');

  // create admin user
  var UserTeamAssignment = app.models.UserTeamAssignment;
  UserTeamAssignment.find({
          where : {
              and : [
                  {go_team_id: team.id},
                  {go_user_id: user.id},
                  {endDate   : null}
              ]
          }
      }, function (err, assignments) {
    if (err) {
      console.log('findOrCreateUserTeamAssignment: error finding findOrCreateUserTeamAssignment by id ', err);
      return cb(err);
    } else {
      if (!assignments.length) {
        var newTeamAssignment = {
          startDate: new Date(),
          principalType: 'USER',
          go_user_role_type_id: 4, // admin / director of operations role
          go_team_id: team.id,
          go_user_id: user.id
        };
        UserTeamAssignment.create(newTeamAssignment, function (err, assignment) {
          if (err) {
            console.log('findOrCreateUserTeamAssignment: error creating new findOrCreateUserTeamAssignment ', err);
            return cb(err);
          } else {
            console.log('findOrCreateUserTeamAssignment: created findOrCreateUserTeamAssignment ', user);
            req.flash('info', '\rteam admin user assignment created');
            return cb(null, assignment);
          }
        });
      } else {
        console.log('findOrCreateUserTeamAssignment: findOrCreateUserTeamAssignment found: %o', assignments[0]);
        req.flash('info', '\rteam admin user assignment already exists');
        return cb(null, assignments[0]);
      }
    }
  });
};

var findOrCreateUser = function (req, org, cb) {
  assert(org, 'organization is not null');
  console.log('findOrCreateUser');

  // create admin user
  var userName = "admin_" + org.shortName.toLowerCase().replace(/ /g, "")
    , User     = app.models.GoUser;
  User.find({where : {username: userName}}, function (err, users) {
    if (err) {
      console.log('findOrCreateUser: error finding user by id ', err);
      return cb(err);
    } else {
      if (!users.length) {
        var newUser = {
          username: userName,
          password: 'r1dg3gam3',
          email: userName + "@godeepmobile.com",
          firstName: org.shortName,
          lastName: 'Admin',
          isSystemUser: true
        };
        User.create(newUser, function (err, user) {
          if (err) {
            console.log('findOrCreateUser: error creating new user ', err);
            return cb(err);
          } else {
            console.log('findOrCreateUser: created user ', user);
            req.flash('info', '\rteam admin user created');
            return cb(null, user);
          }
        });
      } else {
        console.log('findOrCreateUser: User found: %o', users[0]);
        req.flash('info', '\rteam admin user already exists');
        return cb(null, users[0]);
      }
    }
  });
};

var findOrCreateTeam = function (req, org, cb) {
  assert(org, 'organization is not null');
  console.log('findOrCreateTeam');
  var Team   = app.models.Team;
  org = JSON.parse(JSON.stringify(org));
  Team.find({where : {organization: org.id}}, function (err, teams) {
    if (err) {
      console.log('findOrCreateTeam: error finding Team by id ', err);
      return cb(err);
    } else {
      if (!teams.length) {
        var newTeam = {
          abbreviation: org.espnName,
          sport       : 1,
          organization: org.id,
          shortName   : org.shortName,
          mascot      : org.mascot,
          conference  : isNaN(org.conference) ? org.conference.id : org.conference,
          assetBaseUrl: "https://s3.amazonaws.com/godeeppublic/" + org.shortName.toLowerCase().replace(/ /g, "")
        };
        // create football team
        Team.create(newTeam, function (err, teamCreated) {
          console.log(teamCreated);
          if (err) {
            console.log('findOrCreateTeam: error creating new team ', err);
            return cb(err);
          } else {
            console.log('findOrCreateTeam: created team ', teamCreated);
            req.flash('info', '\rnew team created');
            return cb(null, teamCreated);
          }
        });
      } else {
        console.log('findOrCreateTeam: Team found: %o', teams[0]);
        req.flash('info', '\rteam already exists');
        return cb(null, teams[0]);
      }
    }
  });
};

var findOrCreateOrganization = function (req, cb) {
  assert(req, 'req is not null');
  console.log('findOrCreateOrganization by id: ', req.body.organizationId);
  var Organization   = app.models.Organization
    , organizationId = req.body.organizationId
    , connector      = Organization.getDataSource().connector
    , assetBaseUrl   = "https://s3.amazonaws.com/godeeppublic/" + req.body.shortName.toLowerCase().replace(/ /g, "");

  if (organizationId) {

    connector.executeSQL('UPDATE go_organization SET name=$1, short_name=$2, espn_name=$3, mascot=$4, go_conference_id=$5, go_organization_type_id=$6, asset_base_url=$7 WHERE id = $8;', [
      req.body.name,
      req.body.shortName,
      req.body.espnName,
      req.body.mascot,
      req.body.conference,
      req.body.organizationType,
      assetBaseUrl,
      organizationId
    ], function(err) {
      if (err) {
        console.log('findOrCreateOrganization: error updating organization ', err);
        return cb(err);
      } else {
        console.log('findOrCreateOrganization: updated organization ', req.body.name);
        req.flash('info', 'organization already exists (updated)');

        Organization.findById(organizationId, function (err, org) {
          if (err) {
            console.log('findOrCreateOrganization: error finding Organization by id ', err);
            return cb(err);
          } else {
            console.log('findOrCreateOrganization: finding Organization done');
            return cb(null, org);
          }
        });
      }
    });    
  } else {
    var newOrganization = {
      name            : req.body.name,
      shortName       : req.body.shortName,
      espnName        : req.body.espnName,
      mascot          : req.body.mascot,
      conference      : req.body.conference,
      organizationType: req.body.organizationType,
      assetBaseUrl    : assetBaseUrl
    };
    Organization.create(newOrganization, function (err, org) {
      if (err) {
        console.log('findOrCreateOrganization: error creating new organization ', err);
        return cb(err);
      } else {
        console.log('findOrCreateOrganization: created organization ', org);
        req.flash('info', 'new organization created');
        return cb(null, org);
      }
    });
  }
};

app.post('/new-organization', isSuperUser(), hasUserChangedPassword(), function (req, res) {
  var connector;
  var requestQuery;
  findOrCreateOrganization(req, function (err, org) {
    if (err) {
      console.log('/new-organization: error creating new organization ', err);
      req.flash('error', err.message);
      return res.redirect('back');
    } else {
      console.log('/new-organization: created organization ', org);
      // create football team
      findOrCreateTeam(req, org, function (err, team) {
        if (err) {
          console.log('/new-organization: error creating new team ', err);
          req.flash('error', err.message);
          return res.redirect('back');
        } else {
          console.log('/new-organization: created team %o', team);
          // create admin user
          findOrCreateUser(req, org, function (err, user) {
            if (err) {
              console.log('/new-organization: error creating new user ', err);
              req.flash('error', err.message);
              return res.redirect('back');
            } else {
              console.log('/new-organization: created user %o', user);
              // create user team assignment
              findOrCreateUserTeamAssignment(req, team, user, function (err, assignment) {
                if (err) {
                  console.log('/new-organization: error creating assignment for user ', err);
                  req.flash('error', err.message);
                  return res.redirect('back');
                } else {
                  console.log('/new-organization: created user assignment %o', assignment);
                  // create default team values
                  connector = app.models.Team.getDataSource().connector;
                  requestQuery = 'SELECT  godeepmobile.initialize_new_team($1)';
                  connector.executeSQL(requestQuery, [team.id], function (err, result) {
                    if (err) {
                      console.log("\n/new-organization: query: %s\nhas error ", requestQuery, err);
                      return res.redirect('back');
                    } else {
                      // success. we are done.
                      return res.redirect('/superuser');
                    }
                  });
                }
              });
            }
          });
        }
      });
    }
  });
});

app.get('/signup', function (req, res) {
  res.render('pages/signup', {
    user: req.user,
    url: req.url
  });
});

app.post('/signup', function (req, res) {
  //var User = app.models.user;
  var User = app.models.GoUser;

  var newUser = {};
  newUser.email = req.body.email.toLowerCase();
  newUser.username = req.body.username.trim();
  newUser.password = req.body.password;
  newUser.firstName = req.body.firstName;
  newUser.lastName = req.body.lastName;

  User.create(newUser, function (err, user) {
    if (err) {
      console.log('/signup: error creating new user ', err);
      req.flash('error', err.message);
      return res.redirect('back');
    } else {
      console.log('/signup: created user %o', user);
      // login user immediately
      return userLogin(user, req, res);
    }
  });
});

app.get('/auth/logout', function (req, res) {
  var token = req.session.accessToken;
  //dumpState('/auth/logout', req, res);

  if (token) {
    app.models.GoUser.logout(token.id, function (err) {
      //TODO delete session properly
      if (req.session.accessToken) {
        delete req.session.accessToken;
      }
      res.redirect('/');
    });
  } else {
    res.redirect('/');
  }
});

app.get('/change-password', function (req, res) {
  var token = req.session.accessToken;
  //dumpState('change-password', req, res);
  if (token) {
    res.render('pages/change-password');
  } else {
    res.redirect('/');
  }
});

app.post('/change-password', function (req, res) {
  var password        = req.body.password
    , confirmPassword = req.body.confirmPassword
    , userId          = req.session.accessToken.userId
    , User            = app.models.GoUser;

  if (password === confirmPassword) {
    User.findById(userId, function (err, user) {
      if (err) {
        console.log('/change-password: error finding user by id ', err);
        req.flash('error', err.message);
        return res.redirect('back');
      } else {
        var userAttrs = {
          email             : req.session.accessToken.user.email,
          password          : password,
          hasChangedPassword: true
        };
        user.updateAttributes(userAttrs, function (err, us) {
          if (err) {
            console.log("/change-password: error updating user's attributes ", err);
            req.flash('error', err.message);
            return res.redirect('back');
          } else {
            var userCrendentials = {
              username: us.username,
              password: password
            };
            console.log('/change-password: password changed. logging user');
            return userLogin(userCrendentials, req, res);
          }
        });
      }
    });
  } else {
    req.flash('error', "The new password entered does not match the confirmation password.");
    return res.redirect('back');
  }
});


app.get('/forgot-password', function (req, res) {
  //dumpState('forgot-password', req, res);
  if (req.query && req.query.return) {
    req.flash('return', req.query.return);
  }
  res.render('pages/forgot-password');
});

app.post('/forgot-password', function (req, res) {
  var User  = app.models.GoUser
    , email = req.body.email.toLowerCase()
    , error = 'We did not recognize the email address you entered. Please try again.';

  User.find({where : {email: email}}, function(err, users) {
    if (err) {
      console.log('/forgot-password: error finding user by email ', err);
      req.flash('error', error);
      return res.redirect('back');
    } else {
      if (users && users.length > 0) {
        var user = users[0];
        var password = user.firstName + (Math.floor(Math.random()*1000) + 1000);
        var encryptedPassword = User.hashPassword(password);
        var userAttrs = {
          email             : email,
          password          : encryptedPassword,
          hasChangedPassword: false
        };
        user.updateAttributes(userAttrs, function (err, us) {
          if (err) {
            console.log("/forgot-password: error updating user's attributes ", err);
            req.flash('error', err.message);
            return res.redirect('back');
          } else {
            var to      = user.email
              , url     = "http://app.godeepmobile.com" //req.protocol + '://' + req.get('host')
              , mUrl    = 'http://m.godeepmobile.com'
              , subject = user.firstName +", here is your new password!"
              , returnUrl = '/';
            var mailOptions = {
              template: 'new-password',
              context : {
                firstName: user.firstName,
                username : user.username,
                password : password,
                url      : url
              }
            };
            sendEmail(to, subject, mailOptions);
            //return res.redirect('/');

            req.flash('successEmail', to);

            if (req.body.returnToMobile === 'm') {
              returnUrl = mUrl;
            } else {
              if (req.body.returnToMobile === 'no') {
                returnUrl = null;
              }
            }
            req.flash('returnUrl', returnUrl);

            return res.redirect('back');
          }
        });
      } else {
        console.log('/forgot-password: error email not found ');
        req.flash('error', error);
        return res.redirect('back');
      }
    }
  });
});

// Requests that get this far won't be handled
// by any middleware. Convert them into a 404 error
// that will be handled later down the chain.
app.use(loopback.urlNotFound());

// The ultimate error handler.
app.use(loopback.errorHandler());

app.start = function () {
  // start the web server
  return app.listen(function () {
    app.emit('started');
    console.log('app.start: Web server listening at: %s', app.get('url'));
  });
};

// start the server if `$ node server.js`
if (require.main === module) {
  //app.start();

  // start the WebSocket
  app.io = require('socket.io')(app.start());

  // using redis to communicate the web sockets
  app.io.adapter(ioRedis({pubClient: redisPubClient, subClient: redisSubClient}));

  app.io.on('connection', function(socket) {
    console.log('WebSocket: Client connected:', socket.id);

    socket.on('disconnect', function() {
      console.log('WebSocket: Client disconnected:', socket.id);
    });
  });

  console.log('app.start: WebSocket ready!');
}
