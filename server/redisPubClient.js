//var loopback = require('loopback');
var redis = require('redis');

var redisHost = process.env.REDIS_HOST || "localhost";
var redisPort = process.env.REDIS_PORT || 6379;
var redisAuth = process.env.REDIS_AUTH || "$%^g*d33p&*";

var pubClient = redis.createClient(
    redisPort, // port
    redisHost, // host
    {
        auth_pass: redisAuth,
        return_buffers: true
    }); // options hash

pubClient.on('connect', function() {
    console.log('redis publisher: redis connected');
});

pubClient.on('close', function() {
    console.log('redis publisher: redis connection closed');
});

pubClient.on('ready', function() {
    console.log('redis publisher: redis ready');
});

pubClient.on('error', function(err) {
    console.log('redis publisher: redis err ', err);
});

module.exports = pubClient;
