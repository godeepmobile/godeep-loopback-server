
var postgresDS = {
  "name":      "postgresDS",
  "connector": "postgresql",
  "host":      process.env.DB_HOST     || "gd-app-dev2.cvm6ylk4oa38.us-east-1.rds.amazonaws.com",
  "port":      process.env.DB_PORT     || 5432,
  "database":  process.env.DB_DATABASE || "godeep",
  "username":  process.env.DB_USERNAME || "superuser",
  "password":  process.env.DB_PASSWORD || "321peedog",
  "debug":     process.env.DB_DEBUG    || false,
  "poolSize":  process.env.DB_POOL     || 20
};

// for some reason, a local install of pg does not like having the ssl flag set (either true or false)
// so make sure it is undefined for localhost
if (process.env.DB_HOST !== 'localhost') {
  postgresDS.ssl = process.env.DB_SSL      || false;
}

var redisDS = {
  "name":       "redisDS",
  "host":       process.env.REDIS_HOST || "pub-redis-13713.us-east-1-4.2.ec2.garantiadata.com",
  "port":       process.env.REDIS_PORT || 13713,
  "password":   process.env.REDIS_AUTH || "$%^g*d33p&*",
  "database":   0,
  "connector":  "redis",
  "debug":      process.env.REDIS_DEBUG || true
};

console.log('datasources DB SSL ',      postgresDS.ssl );
console.log('datasources DB HOST ',     postgresDS.host );
console.log('datasources DB PORT ',     postgresDS.port );
console.log('datasources DB DATABASE ', postgresDS.database );
console.log('datasources DB USERNAME ', postgresDS.username );
console.log('datasources DB DEBUG ',    postgresDS.debug );
console.log('datasources DB POOL ',     postgresDS.poolSize );

console.log('datasources REDIS HOST',   redisDS.host);
console.log('datasources REDIS PORT',   redisDS.port);
console.log('datasources REDIS DEBUG',  redisDS.debug);

module.exports = {
  "postgresDS": postgresDS,
  "redisDS": redisDS
};
