var loopback = require('loopback');
var client = require('./myredis');

// getIndex will find the index.html file in redis, embed some session data in it, then send it back in the response
var getIndex = function(req, res, indexKey, accessToken) {
    //console.log('getIndex: accessToken is ', accessToken);

    //we have to have an accessToken for this to work. find it wherever it might be
    if (!accessToken)
        accessToken = req.session.accessToken;
    if (!accessToken) {
        accessToken = req.accessToken;
        if (accessToken) {
            // if we pull the accesstoken out of the request,
            // then it is a model with a related user model.
            // must JSONify it to get at the user model correctly.
            accessToken = accessToken.toJSON();
        }
    }
    if (!accessToken) {
        var ctx = loopback.getCurrentContext();
        if (ctx) {
            //console.log("getIndex: ctx is ", ctx);
            accessToken = ctx.get('accessToken') || ctx.accessToken || null;
            if (accessToken) {
                // if we pull the accesstoken out of the context,
                // then it is a model with a related user model.
                // must JSONify it to get at the user model correctly.
                accessToken = accessToken.toJSON();
            }
        }
    }
    // we didnt' find an access token. give up.
    if (!accessToken) {
      console.log('getIndex: did not find an accessToken, giving up');
      return res.redirect('/');
    }

    console.log("getIndex: looking for indexKey ", indexKey);
    client.get(indexKey, function(err, indexValue) {
        if (err) {
            console.log("getIndex: redis.get error " + err);
            return res.send('<p>Not Found</p>');
        }
        if (indexValue) {
            var escapeName = function(name) {
              if (name) {
                // HTML escape single quotes before embedding in header
                return name.replace(/'/g,"&#39");
              } else {
                return undefined;
              }
            };
            // got the value of index.html, send it with proper content-type
            res.set('Content-Type', 'text/html');
            // <meta name="GD-session" content='{"token":"kDAlEgnbqOKaboNulmliuDWaZ6ny5ARa3BIhBXaDGWlCNNX3l7OTkrfjxdv7PTrc","user":{"username":"jperry","firstName":"James","middleName":null,"lastName":"Perry","jobTitle":null,"officePhone":null,"mobilePhone":null,"email":null,"id":39}}'>
            // embed session data into html whereever it is found
            var user = accessToken.user;
            user.firstName = escapeName(user.firstName);
            user.middleName = escapeName(user.middleName);
            user.lastName = escapeName(user.lastName);
            user.jobTitle = escapeName(user.jobTitle);
            indexValue = indexValue.replace(/##gd-session##/gi, JSON.stringify({
                user    : user,
                token   : accessToken.id,
                playerId: accessToken.playerId
            }));


            // embed indexKey/version # into html whereever it is found
            indexValue = indexValue.replace(/##gd-key##/gi, indexKey);

            // return the modified index.html as the client to launch
            console.log('getIndex: found index.html');
            return res.send(indexValue);
        } else {
            // nothing found for index.html, send 404
            res.set('Content-Type', 'text/html');
            console.log('getIndex: did not find index.html, returning not found');
            return res.send("<p>Not Found</p>");
        }
    });
};

// get the app's index.html file from redis
// two forms are valid: with and without a key (SHA)
// when no key is provided, then load the key @ "<appname>:current", then
// return the value of that key
// when a key is provided, then load the file @ "<appname>:<key>" then return
// the value of that key
function serveIndex(req, res, accessToken) {
    console.log('serveIndex: got query %j', req.query);
    var indexKey = req.query.index_key;
    var indexPrefix;
    if (req.session.client) {
      if (req.session.client === 'coach') {
      indexPrefix = "godeeptest:";
    } else if (req.session.client === 'player')
      indexPrefix = "godeep-player:";
    }
    if (!indexKey) {
        console.log('serveIndex: trying %scurrent',indexPrefix);
        indexKey = indexPrefix + "current";
        client.get(indexKey, function(err, keyValue) {
            if (keyValue) {
                return getIndex(req, res, keyValue, accessToken);
            } else {
                console.log('serveIndex: did not find current index key, returning not found');
                return res.send('<p>Not Found</p>');
            }
        });
    } else {
        console.log('serveIndex: trying key %s', indexKey);
        indexKey = indexPrefix + indexKey;
        return getIndex(req, res, indexKey, accessToken);
    }
}

module.exports = serveIndex;
