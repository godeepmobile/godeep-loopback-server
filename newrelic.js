/**
 * New Relic agent configuration.
 *
 * See lib/config.defaults.js in the agent distribution for a more complete
 * description of configuration variables and their potential values.
 */
exports.config = {
  /**
   * Array of application names.
   */
  app_name : [process.env.APP_NAME || 'GD LoopBack API'],
  /**
   * Your New Relic license key.
   */
  license_key : '9255fa8a36a80c116a0e52b2b6761e9d6072c7e3',
  logging : {
    /**
     * Level at which to log. 'trace' is most useful to New Relic when diagnosing
     * issues with the agent, 'info' and higher will impose the least overhead on
     * production applications.
     */
    level : process.env.NEWRELIC_LOGGING || 'info'
  }
};
