--liquibase formatted sql

--changeset rambert:9
--comment added pass_word column, deleted role and fullName columns
DROP VIEW IF EXISTS godeepmobile.view_active_user_non_players;
CREATE VIEW godeepmobile.view_active_user_non_players AS
 SELECT
    ( SELECT
        guta.id
      FROM
        godeepmobile.go_user_team_assignment guta,
        godeepmobile.go_user_role_type gurt
      WHERE
        gu.id = guta.go_user_id AND
        guta.go_user_role_type_id = gurt.id AND
        gurt.id <> 1 AND
        guta.end_date IS NULL
    ) AS go_user_team_assignment_id,
    ( SELECT
        guta.go_team_id
      FROM
        godeepmobile.go_user_team_assignment guta,
        godeepmobile.go_user_role_type gurt
      WHERE
        gu.id = guta.go_user_id AND
        guta.go_user_role_type_id = gurt.id AND
        gurt.id <> 1 AND
        guta.end_date IS NULL
    ) AS go_team_id,
    ( SELECT
        gurt.id
      FROM
        godeepmobile.go_user_team_assignment guta,
        godeepmobile.go_user_role_type gurt
      WHERE
        gu.id = guta.go_user_id AND
        guta.go_user_role_type_id = gurt.id AND
        gurt.id <> 1 AND
        guta.end_date IS NULL
    ) AS go_user_role_type_id,
    gu.id,
    gu.user_name,
    gu.pass_word,
    gu.first_name,
    gu.middle_name,
    gu.last_name,
    gu.email,
    gu.job_title,
    gu.office_phone,
    gu.mobile_phone
 FROM
    godeepmobile.go_user gu
 WHERE (
      gu.id IN (
        SELECT
          gu.id
        FROM
          godeepmobile.go_user_team_assignment guta,
          godeepmobile.go_user_role_type gurt
        WHERE
          gu.id = guta.go_user_id AND
          guta.go_user_role_type_id = gurt.id AND
          gurt.id <> 1 AND
          guta.end_date IS NULL
      )
  )
  ORDER BY gu.last_name, gu.first_name;
