﻿--liquibase formatted sql

--changeset carlos:11 runOnChange:true
--comment updated view_platoon_event_grades_by_quarter

DROP VIEW view_platoon_event_grades_by_quarter;

CREATE OR REPLACE VIEW view_platoon_event_grades_by_quarter AS 
 SELECT concat(event.id, '-', grade.go_platoon_type_id, '-', play.quarter) AS id,
    event.go_team_id,
    event.season,
    grade.go_platoon_type_id,
    event.date,
    event.id AS team_event_id,
    play.quarter,
    count(grade.id) AS num_plays,
    sum(grade.cat_1_grade)::integer AS sum_cat1_grade,
    sum(grade.cat_2_grade)::integer AS sum_cat2_grade,
    sum(grade.cat_3_grade)::integer AS sum_cat3_grade,
    sum(grade.cat_1_grade + grade.cat_2_grade + grade.cat_3_grade)::integer AS sum_overall_grade,
    round(avg(grade.cat_1_grade)::numeric, 2) AS avg_cat1_grade,
    round(avg(grade.cat_2_grade)::numeric, 2) AS avg_cat2_grade,
    round(avg(grade.cat_3_grade)::numeric, 2) AS avg_cat3_grade,
    round((avg(grade.cat_1_grade + grade.cat_2_grade + grade.cat_3_grade) / 3::numeric)::numeric, 2) AS avg_overall_grade
   FROM team_play_grade grade
     JOIN team_play_data play ON play.id = grade.team_play_data_id
     JOIN team_event event ON play.team_event_id = event.id
  GROUP BY event.id, grade.go_platoon_type_id, play.quarter
  ORDER BY event.id, grade.go_platoon_type_id, play.quarter;

  --rollback DROP VIEW view_platoon_event_grades_by_quarter;