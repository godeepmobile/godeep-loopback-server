--liquibase formatted sql

--changeset rambert:56 runOnChange:true stripComments:false splitStatements:false
--comment add team_player_id column to go_user_team_assignment table

ALTER TABLE godeepmobile.go_user_team_assignment ADD team_player_id uuid;


UPDATE go_user_team_assignment
SET team_player_id = query.player_id
FROM (
    SELECT player.id AS player_id, _user.id AS user_id
    FROM go_user _user INNER JOIN go_user_team_assignment assgiment ON _user.id = assgiment.go_user_id
        INNER JOIN team_player player ON player.go_user_team_assignment_id = assgiment.id
    WHERE player.go_user_team_assignment_id IS NOT NULL 
        AND assgiment.go_user_id = _user.id
        AND assgiment.go_user_role_type_id = 1
    ) as query
WHERE go_user_team_assignment.go_user_id = query.user_id;


--rollback ALTER TABLE godeepmobile.go_user_team_assignment DROP COLUMN team_player_id;