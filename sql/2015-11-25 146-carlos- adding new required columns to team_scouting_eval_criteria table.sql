﻿--liquibase formatted sql

--changeset rambert:146 runOnChange:true splitStatements:false stripComments:false
--comment added new required columns to team_scouting_eval_criteria table

ALTER TABLE godeepmobile.team_scouting_eval_criteria ADD calculated_target_score NUMERIC(10, 2) DEFAULT 5.0;
ALTER TABLE godeepmobile.team_scouting_eval_criteria ADD custom_target_score NUMERIC(10, 2);
ALTER TABLE godeepmobile.team_scouting_eval_criteria ADD use_calculated_target_score BOOLEAN DEFAULT true;

--rollback ALTER TABLE godeepmobile.team_scouting_eval_criteria DROP COLUMN calculated_target_score; ALTER TABLE godeepmobile.team_scouting_eval_criteria DROP COLUMN custom_target_score; ALTER TABLE godeepmobile.team_scouting_eval_criteria DROP COLUMN use_calculated_target_score;