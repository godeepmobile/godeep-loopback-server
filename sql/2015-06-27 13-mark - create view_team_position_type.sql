--liquibase formatted sql

--changeset mark:13 runOnChange:true stripComments:false
--comment create view_team_position_type view
CREATE OR REPLACE VIEW view_team_position_type AS
 SELECT tpt.id,
    tpt.go_team_id,
    tpt.go_position_type_id,
    tpt.team_position_group_id,
    tpt.go_platoon_type_id,
    tpt.start_date,
    tpt.end_date,
    ( SELECT go_position_type.name
           FROM go_position_type
          WHERE go_position_type.id = tpt.go_position_type_id) AS name,
    ( SELECT go_position_type.short_name
           FROM go_position_type
          WHERE go_position_type.id = tpt.go_position_type_id) AS short_name
   FROM team_position_type tpt;
--rollback drop view view_team_position_type;
