--liquibase formatted sql

--changeset mark:4 runOnChange:true stripComments:false
--comment add go_organization_id column to view_go_team
CREATE OR REPLACE VIEW view_go_team AS
 SELECT gt.id,
    ( SELECT go.name
           FROM go_organization go
          WHERE go.id = gt.go_organization_id) AS name,
    gt.short_name,
    gt.abbreviation,
    gt.mascot,
    ( SELECT gc.name
           FROM go_conference gc
          WHERE gc.id = gt.go_conference_id) AS conference_name,
    gt.go_sport_type_id,
    gt.addr_street1,
    gt.addr_street2,
    gt.addr_city,
    gt.addr_state_id,
    gt.addr_postal_code,
    gt.addr_phone_main,
    gt.addr_phone_fax,
    gt.home_stadium,
    gt.go_surface_type_id,
    gt.film_system,
    gt.go_conference_id,
    gt.asset_base_url,
    gt.go_organization_id
   FROM go_team gt
  ORDER BY gt.id;

--rollback drop view view_go_team; CREATE OR REPLACE VIEW view_go_team AS SELECT gt.id, ( SELECT go.name FROM go_organization go WHERE go.id = gt.go_organization_id) AS name, gt.short_name, gt.abbreviation, gt.mascot, ( SELECT gc.name FROM go_conference gc WHERE gc.id = gt.go_conference_id) AS conference_name, gt.go_sport_type_id, gt.addr_street1, gt.addr_street2, gt.addr_city, gt.addr_state_id, gt.addr_postal_code, gt.addr_phone_main, gt.addr_phone_fax, gt.home_stadium, gt.go_surface_type_id, gt.film_system, gt.go_conference_id, gt.asset_base_url FROM go_team gt ORDER BY gt.id;

--changeset mark:5 runOnChange:true stripComments:false
--comment add unique constraint to go_team_id column of team_configuration table
ALTER TABLE team_configuration
  ADD CONSTRAINT team_configuration_unique_team_id UNIQUE(go_team_id);
--rollback ALTER TABLE team_configuration drop CONSTRAINT IF EXISTS team_configuration_unique_team_id;
