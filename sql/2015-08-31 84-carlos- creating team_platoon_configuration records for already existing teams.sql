﻿--liquibase formatted sql

--changeset carlos:84 runOnChange:true splitStatements:false
--comment created team_platoon_configuration records for already existing teams

--using pg_temp schema to create this as a temporal function which will be dropper when the current connection is closed
CREATE OR REPLACE FUNCTION pg_temp.create_team_platoon_configuration_records() RETURNS integer AS
$BODY$
DECLARE 
	go_team_id uuid;
BEGIN
	FOR go_team_id IN
		SELECT id
		FROM godeepmobile.go_team
	LOOP
		INSERT INTO godeepmobile.team_platoon_configuration (go_team_id, go_platoon_type_id, num_plays) VALUES (go_team_id, 1, 150); --off
		INSERT INTO godeepmobile.team_platoon_configuration (go_team_id, go_platoon_type_id, num_plays) VALUES (go_team_id, 2, 150); --def
		INSERT INTO godeepmobile.team_platoon_configuration (go_team_id, go_platoon_type_id, num_plays) VALUES (go_team_id, 3, 50); --st
		INSERT INTO godeepmobile.team_platoon_configuration (go_team_id, go_platoon_type_id, num_plays) VALUES (go_team_id, 4, 300); --all
	END LOOP;

	RETURN 1;
END;
$BODY$
LANGUAGE plpgsql VOLATILE
COST 100;

--executing the function
SELECT pg_temp.create_team_platoon_configuration_records();

--rollback DROP FUNCTION IF EXISTS 