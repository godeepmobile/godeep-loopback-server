--liquibase formatted sql

--changeset carlos:16 runOnChange:true splitStatements:false
--comment changed reference from team_position_type to position_type on team_grade

ALTER TABLE godeepmobile.team_play_grade DROP CONSTRAINT fk_team_play_grade_position_played_team_position_type;

UPDATE godeepmobile.team_play_grade SET position_played = (SELECT go_position_type_id FROM godeepmobile.team_position_type WHERE id = position_played);

ALTER TABLE godeepmobile.team_play_grade ADD CONSTRAINT fk_team_play_grade_position_played_position_type FOREIGN KEY (position_played) REFERENCES godeepmobile.go_position_type(id);

--rollback ALTER TABLE godeepmobile.team_play_grade DROP CONSTRAINT fk_team_play_grade_position_played_position_type;

