﻿--liquibase formatted sql

--changeset carlos:12 runOnChange:true
--comment added team_cutup sample data
INSERT INTO godeepmobile.team_cutup (id, go_team_id, team_event_id, which_platoon, num_plays) VALUES (1, '00000000-0000-0000-0000-000000000006', 1, 1, 50);
INSERT INTO godeepmobile.team_cutup (id, go_team_id, team_event_id, which_platoon, num_plays) VALUES (2, '00000000-0000-0000-0000-000000000006', 1, 2, 50);
INSERT INTO godeepmobile.team_cutup (id, go_team_id, team_event_id, which_platoon, num_plays) VALUES (3, '00000000-0000-0000-0000-000000000006', 1, 3, 50);
ALTER SEQUENCE godeepmobile.team_cutup_id_seq restart with 5000;

UPDATE godeepmobile.team_play_data SET team_cutup_id = 1 WHERE id < 51;
UPDATE godeepmobile.team_play_data SET team_cutup_id = 2 WHERE id > 50 AND id < 101;
UPDATE godeepmobile.team_play_data SET team_cutup_id = 3 WHERE id > 100 AND id < 151;

--rollback UPDATE godeepmobile.team_play_data SET team_cutup_id = null; DELETE FROM godeepmobile.team_cutup; ALTER SEQUENCE godeepmobile.team_cutup_id_seq restart with 1;