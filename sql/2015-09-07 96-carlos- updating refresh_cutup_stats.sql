﻿--liquibase formatted sql

--changeset carlos:96 runOnChange:true splitStatements:false stripComments:false
--comment updated refresh_cutup_stats

CREATE OR REPLACE FUNCTION refresh_cutup_stats(cutup_id_parm integer)
  RETURNS integer AS
$BODY$
DECLARE
    mviews RECORD;
    stat godeepmobile.team_player_game_cutup_stat%ROWTYPE;
BEGIN
    perform godeepmobile.create_game_stats_for_cutup(cutup_id_parm);
    FOR mviews IN SELECT * FROM godeepmobile.view_team_player_event_grades_by_cutup where "teamCutup" = cutup_id_parm LOOP



        with stat_id as (select id from godeepmobile.team_player_game_cutup_stat where team_player_id = mviews."teamPlayer" and team_cutup_id = cutup_id_parm)
        update
            godeepmobile.team_player_game_cutup_stat
        SET
            num_plays = mviews."numPlays",
            cat1_grade = mviews."avgCat1Grade",
            cat2_grade = mviews."avgCat2Grade",
            cat3_grade = mviews."avgCat3Grade",
            overall_grade = mviews."avgOverallGrade",
            last_update = now()
        WHERE
            id = (select id from stat_id);
    END LOOP;

    --deleting stats for players that have no grades anymore
    DELETE FROM godeepmobile.team_player_game_cutup_stat WHERE team_cutup_id = cutup_id_parm AND team_player_id NOT IN (
	SELECT "teamPlayer" FROM godeepmobile.view_team_player_event_grades_by_cutup where "teamCutup" = cutup_id_parm
    );
    
    RETURN 1;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

--rollback DROP FUNCTION refresh_cutup_stats(integer);