--liquibase formatted sql

--changeset mark:28 runOnChange:true stripComments:false splitStatements:false
--comment create view_team_position_group

CREATE OR REPLACE VIEW view_team_position_group AS
 SELECT tpg.id,
    tpg.go_team_id,
    tpg.name,
    tpg.go_platoon_type_id,
    tpg.start_date,
    tpg.end_date,
    ARRAY( SELECT tpt.go_position_type_id
           FROM team_position_type tpt
          WHERE tpt.team_position_group_id = tpg.id AND tpt.end_date IS NULL) AS go_position_type_ids
   FROM team_position_group tpg;

--rollback drop view view_team_position_group;
