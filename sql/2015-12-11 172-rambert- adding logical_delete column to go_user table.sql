--liquibase formatted sql

--changeset rambert:171 runOnChange:true splitStatements:false stripComments:false
--comment add logical_delete column to go_user table

ALTER TABLE godeepmobile.go_user ADD logical_delete boolean DEFAULT false;

-- Season Stats
CREATE OR REPLACE VIEW view_go_user AS
  SELECT user_.*
  FROM godeepmobile.go_user user_
  WHERE user_.logical_delete = false;

--rollback ALTER TABLE godeepmobile.go_user DROP COLUMN logical_delete; DROP VIEW view_go_user;
