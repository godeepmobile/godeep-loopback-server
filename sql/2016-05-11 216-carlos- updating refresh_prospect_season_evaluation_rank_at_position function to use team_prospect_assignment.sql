﻿--liquibase formatted sql

--changeset carlos:216 runOnChange:true splitStatements:false stripComments:false
--comment updated refresh_prospect_season_evaluation_rank_at_position function to use team_prospect_assignment

CREATE OR REPLACE FUNCTION godeepmobile.refresh_prospect_season_evaluation_rank_at_position (
  team_id_parm uuid,
  season_parm integer,
  position_id_parm integer)
  RETURNS void AS
$BODY$
DECLARE
  record_value RECORD;
  record_exists integer;
BEGIN

  FOR record_value IN

    SELECT
      evaluation_season_stat.go_team_id,
      evaluation_season_stat.team_player_id,
      evaluation_season_stat.season,
      prospect.position_1,
      DENSE_RANK() OVER (ORDER BY evaluation_season_stat.overall_eval_avg DESC, evaluation_season_stat.target_match DESC) AS rank_at_post
    FROM godeepmobile.player_evaluation_season_stat evaluation_season_stat,
      godeepmobile.view_team_prospect_assignment prospect
    WHERE evaluation_season_stat.go_team_id = team_id_parm::uuid
      AND prospect.team_player_id = evaluation_season_stat.team_player_id
      AND evaluation_season_stat.season = season_parm
      AND prospect.season = season_parm
      AND prospect.position_1 = position_id_parm
      AND evaluation_season_stat.overall_eval_avg IS NOT NULL

      LOOP
      -- check if record already exists
      SELECT COUNT(*) INTO record_exists
      FROM godeepmobile.player_rank_overall_at_position_season_stat
      WHERE go_team_id = team_id_parm::uuid
        AND team_player_id = record_value.team_player_id
        AND season = season_parm
        AND go_position_type_id = position_id_parm;

      IF record_exists = 0 THEN
        INSERT INTO godeepmobile.player_rank_overall_at_position_season_stat (
          go_team_id,
          team_player_id,
          season,
          go_position_type_id,
          --overall_eval_avg,
          --target_match,
          --num_evaluations_at_post,
          rank_at_post
        )
        VALUES (
          team_id_parm::uuid,
          record_value.team_player_id,
          season_parm,
          position_id_parm,
          -- record_value.overall_eval_avg,
          -- record_value.target_match,
          -- record_value.num_evaluations_at_post,
          record_value.rank_at_post
        );
      ELSE
        UPDATE
            godeepmobile.player_rank_overall_at_position_season_stat
        SET
      --  go_team_id,
        --team_player_id,
        -- overall_eval_avg = record_value.overall_eval_avg,
        -- target_match = record_value.target_match,
        -- num_evaluations_at_post = record_value.num_evaluations_at_post,
        rank_at_post = record_value.rank_at_post
        WHERE go_team_id = team_id_parm::uuid
            AND team_player_id = record_value.team_player_id
            AND season = season_parm
            AND go_position_type_id = position_id_parm;
      END IF;
      END LOOP;

END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

--rollback DROP FUNCTION godeepmobile.refresh_prospect_evaluation_rank_at_position(uuid, integer, integer);
