--liquibase formatted sql

--changeset rambert:109 runOnChange:true stripComments:false splitStatements:false
--comment create team_scouting_eval_score_group table

CREATE TABLE godeepmobile.team_scouting_eval_score_group (
  id                                serial  NOT NULL,
  go_team_id                        uuid NOT NULL,
  team_player_id                    uuid NOT NULL,
  go_user_id                        integer NOT NULL,
  is_completed                      boolean NOT NULL DEFAULT false,
  go_position_type_id               integer NOT NULL,
  date                              date,
  last_update                       date,
  CONSTRAINT pk_team_scouting_eval_score_group PRIMARY KEY ( id ),
  CONSTRAINT fk_team_scouting_eval_score_group_go_team FOREIGN KEY (go_team_id)
      REFERENCES go_team (id),
  CONSTRAINT fk_team_scouting_eval_score_group_team_player FOREIGN KEY (team_player_id)
      REFERENCES team_player (id),
  CONSTRAINT fk_team_scouting_eval_score_group_team_go_user FOREIGN KEY (go_user_id)
      REFERENCES go_user (id),
  CONSTRAINT fk_team_scouting_eval_score_group_go_position_type FOREIGN KEY (go_position_type_id)
      REFERENCES go_position_type (id)
);

DROP TABLE IF EXISTS  godeepmobile.team_scouting_eval_score;

CREATE TABLE godeepmobile.team_scouting_eval_score (
  id                                serial  NOT NULL,
  go_team_id                        uuid NOT NULL,
  team_scouting_eval_criteria_id    integer NOT NULL,
  team_scouting_eval_score_group_id integer NOT NULL,
  go_scouting_eval_criteria_type_id integer NOT NULL,
  note                              character varying(256),
  score                             smallint CHECK(score >= 1 AND score <= 9),
  CONSTRAINT pk_team_scouting_eval_score PRIMARY KEY ( id ),
  CONSTRAINT fk_team_scouting_eval_score_go_team FOREIGN KEY (go_team_id)
      REFERENCES go_team (id),
  CONSTRAINT fk_team_scouting_eval_score_team_scouting_eval_criteria FOREIGN KEY (team_scouting_eval_criteria_id)
      REFERENCES team_scouting_eval_criteria (id),
  CONSTRAINT fk_team_scouting_eval_score_team_scouting_eval_score_group FOREIGN KEY (team_scouting_eval_score_group_id)
      REFERENCES team_scouting_eval_score_group (id),
  CONSTRAINT fk_team_scouting_eval_score_go_scouting_eval_criteria_type FOREIGN KEY (go_scouting_eval_criteria_type_id)
          REFERENCES go_scouting_eval_criteria_type (id)
);

--rollback DROP TABLE IF EXISTS  godeepmobile.team_scouting_eval_score; DROP TABLE IF EXISTS  godeepmobile.team_scouting_eval_score_group;
