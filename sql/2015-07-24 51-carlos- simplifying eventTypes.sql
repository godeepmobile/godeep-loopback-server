--liquibase formatted sql

--changeset carlos:51 runOnChange:true
--comment simplified eventTypes

UPDATE godeepmobile.team_event SET go_event_type_id = et.type FROM (
	SELECT id, 
		CASE is_practice
			WHEN true THEN 2
			WHEN false THEN 1
		END AS type
	FROM godeepmobile.go_event_type
) AS et
WHERE go_event_type_id = et.id;

DELETE FROM godeepmobile.go_event_type WHERE id > 2;

UPDATE godeepmobile.go_event_type SET name = 'Game', description = 'Regular game' WHERE id = 1;
UPDATE godeepmobile.go_event_type SET name = 'Practice', description = 'Practice session' WHERE id = 2;

--rollback UPDATE godeepmobile.go_event_type SET name = 'Reg Season Game', description = 'Scheduled regular season game' WHERE id = 1; UPDATE godeepmobile.go_event_type SET name = 'Drill', description = 'Practice session that is not a scrimmage' WHERE id = 2; INSERT INTO godeepmobile.go_event_type( id, name, description, short_name ) VALUES ( 3, 'Scrimmage', 'Practice session involving full platoons', 'Practice-scrimmage' ); INSERT INTO godeepmobile.go_event_type( id, name, description, short_name ) VALUES ( 4, 'Scrimmage Game', 'Live game conditions, not involving another team', 'Game-Intrasquad' ); INSERT INTO godeepmobile.go_event_type( id, name, description, short_name ) VALUES ( 5, 'Pre-Season Game', 'Game conditions against outside opponent ', 'Game-Preseason' ); INSERT INTO godeepmobile.go_event_type( id, name, description, short_name ) VALUES ( 6, 'Post-Season Game', 'Playoff game', 'Game-Postseason' );