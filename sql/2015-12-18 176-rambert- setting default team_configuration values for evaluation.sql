--liquibase formatted sql

--changeset rambert:176 runOnChange:true splitStatements:false stripComments:false
--comment setting default team_configuration values for evaluation

ALTER TABLE godeepmobile.team_configuration ALTER COLUMN scout_eval_score_min SET DEFAULT 1;
ALTER TABLE godeepmobile.team_configuration ALTER COLUMN scout_eval_score_max SET DEFAULT 6;
ALTER TABLE godeepmobile.team_configuration ALTER COLUMN player_evaluation_greater_multiplier SET DEFAULT 2;
ALTER TABLE godeepmobile.team_configuration ALTER COLUMN player_evaluation_lesser_multiplier SET DEFAULT 0.5;
