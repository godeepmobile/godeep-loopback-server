﻿--liquibase formatted sql

--changeset carlos:230 runOnChange:true splitStatements:false stripComments:false
--comment added data block to grades and block for dynamic grade fields to team configuration

ALTER TABLE team_play_grade ADD custom_fields jsonb;

CREATE OR REPLACE VIEW view_team_play_grade AS 
 SELECT *
   FROM team_play_grade
  WHERE (team_play_grade.team_player_id IN ( SELECT team_player.id
           FROM team_player
          WHERE team_player.end_date IS NULL));

ALTER TABLE team_configuration ADD grade_fields jsonb;

--rollback ALTER TABLE team_play_grade DROP COLUMN custom_fields; ALTER TABLE team_configuration DROP COLUMN grade_fields;