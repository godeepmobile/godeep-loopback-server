--liquibase formatted sql

--changeset rambert:89 runOnChange:true stripComments:false splitStatements:false
--comment change organization id on go_organization table so that only professional teams have 3 and the rest have 2

UPDATE godeepmobile.go_organization
SET go_organization_type_id = 2
WHERE go_conference_id NOT IN (15, 16);

--rollback UPDATE godeepmobile.go_organization SET go_organization_type_id = 3 WHERE go_conference_id NOT IN (15, 16);