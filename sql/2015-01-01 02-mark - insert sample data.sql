--liquibase formatted sql

--changeset mark:2
INSERT INTO godeepmobile.go_body_type( id, name, description ) VALUES ( 1, 'Long, high CG', 'Long, high center of gravity' );
INSERT INTO godeepmobile.go_body_type( id, name, description ) VALUES ( 2, 'Long, low CG', 'Long, low center of gravity' );
INSERT INTO godeepmobile.go_body_type( id, name, description ) VALUES ( 3, 'Short, heavy, low CG', 'Short, heavy, low center of gravity' );

INSERT INTO godeepmobile.go_event_type( id, name, description, short_name ) VALUES ( 1, 'Reg Season Game', 'Scheduled regular season game', 'Game-RegSeason' );
INSERT INTO godeepmobile.go_event_type( id, name, description, short_name ) VALUES ( 2, 'Drill', 'Practice session that is not a scrimmage', 'Practice-drill' );
INSERT INTO godeepmobile.go_event_type( id, name, description, short_name ) VALUES ( 3, 'Scrimmage', 'Practice session involving full platoons', 'Practice-scrimmage' );
INSERT INTO godeepmobile.go_event_type( id, name, description, short_name ) VALUES ( 4, 'Scrimmage Game', 'Live game conditions, not involving another team', 'Game-Intrasquad' );
INSERT INTO godeepmobile.go_event_type( id, name, description, short_name ) VALUES ( 5, 'Pre-Season Game', 'Game conditions against outside opponent ', 'Game-Preseason' );
INSERT INTO godeepmobile.go_event_type( id, name, description, short_name ) VALUES ( 6, 'Post-Season Game', 'Playoff game', 'Game-Postseason' );

INSERT INTO godeepmobile.go_field_condition( id, name ) VALUES ( 1, 'Wet, unsure footing' );
INSERT INTO godeepmobile.go_field_condition( id, name ) VALUES ( 2, 'Dry, fast' );
INSERT INTO godeepmobile.go_field_condition( id, name ) VALUES ( 3, 'Indoor' );
INSERT INTO godeepmobile.go_field_condition( id, name ) VALUES ( 4, 'Snow - heavy' );
INSERT INTO godeepmobile.go_field_condition( id, name ) VALUES ( 5, 'Snow - light' );
INSERT INTO godeepmobile.go_field_condition( id, name ) VALUES ( 6, 'Damp, medium footing' );

INSERT INTO godeepmobile.go_organization_type( id, name ) VALUES ( 1, 'High School Sports' );
INSERT INTO godeepmobile.go_organization_type( id, name ) VALUES ( 2, 'College Sports' );
INSERT INTO godeepmobile.go_organization_type( id, name ) VALUES ( 3, 'Professional Sports' );
INSERT INTO godeepmobile.go_organization_type( id, name ) VALUES ( 4, 'Premier League Sports' );

INSERT INTO godeepmobile.go_permission( id, description ) VALUES ( 'PLAYER', 'Player functions' );
INSERT INTO godeepmobile.go_permission( id, description ) VALUES ( 'GRADER', 'Grade play functions' );
INSERT INTO godeepmobile.go_permission( id, description ) VALUES ( 'REPORT-VIEWER', 'View report functions' );
INSERT INTO godeepmobile.go_permission( id, description ) VALUES ( 'TEAM-ADMIN', 'Team admin functions' );
INSERT INTO godeepmobile.go_permission( id, description ) VALUES ( 'ADMIN', 'SuperUser Admin functions' );

INSERT INTO godeepmobile.go_platoon_type( id, name, description ) VALUES ( 0, 'Unassigned', 'Unknown/Unassigned' );
INSERT INTO godeepmobile.go_platoon_type( id, name, description ) VALUES ( 1, 'Offense', 'Offense' );
INSERT INTO godeepmobile.go_platoon_type( id, name, description ) VALUES ( 2, 'Defense', 'Defense' );
INSERT INTO godeepmobile.go_platoon_type( id, name, description ) VALUES ( 3, 'Special Teams', 'Special Teams' );

INSERT INTO godeepmobile.go_play_result_type( id, name, description ) VALUES (  1,'Penalty - Offense','Penalty resulting in loss of yardage for offense');
INSERT INTO godeepmobile.go_play_result_type( id, name, description ) VALUES (  2,'Punt','Punt in which possession changes sides');
INSERT INTO godeepmobile.go_play_result_type( id, name, description ) VALUES (  3,'Touchdown','Touchdown scored by offense');
INSERT INTO godeepmobile.go_play_result_type( id, name, description ) VALUES (  4,'TO - Interception','Turnover - defense intercepted a pass');
INSERT INTO godeepmobile.go_play_result_type( id, name, description ) VALUES (  5,'TO - Fumble','Turnover - defense recovered a fumble');
INSERT INTO godeepmobile.go_play_result_type( id, name, description ) VALUES (  6,'Penalty - Defense','Penalty resulting in loss of yardage for defense');
INSERT INTO godeepmobile.go_play_result_type( id, name, description ) VALUES (  7,'Penalty - No Play','Penalty resulting in no loss of yardage; replay the down');
INSERT INTO godeepmobile.go_play_result_type( id, name, description ) VALUES (  8,'Punt / TO','Punt in which offense retains possession due to turnover');
INSERT INTO godeepmobile.go_play_result_type( id, name, description ) VALUES (  9,'TO - Defensive TD','Turnover - defense took possession and scored a touchdown');
INSERT INTO godeepmobile.go_play_result_type( id, name, description ) VALUES (  10,'First Down','Offense got a first down');
INSERT INTO godeepmobile.go_play_result_type( id, name, description ) VALUES (  11,'Extra point','Extra point conversion');
INSERT INTO godeepmobile.go_play_result_type( id, name, description ) VALUES (  12,'Two Point Conv','Two point conversion');
INSERT INTO godeepmobile.go_play_result_type( id, name, description ) VALUES (  13,'Kickoff','Kickoff in which receiving team gains possession');
INSERT INTO godeepmobile.go_play_result_type( id, name, description ) VALUES (  14,'Kickoff / TO','Kickoff in which kicking team gains possession via turnover');
INSERT INTO godeepmobile.go_play_result_type( id, name, description ) VALUES (  15,'FG / Made','Successful field goal attempt');
INSERT INTO godeepmobile.go_play_result_type( id, name, description ) VALUES (  16,'FG / Missed','Unsuccessful field goal attempt');
INSERT INTO godeepmobile.go_play_result_type( id, name, description ) VALUES (  17,'Lost on Downs','Possession changed after unsuccessful 4th down by offense');

INSERT INTO godeepmobile.go_play_type( id, name, description ) VALUES ( 1, 'Run - Inside', null );
INSERT INTO godeepmobile.go_play_type( id, name, description ) VALUES ( 2, 'Run - Outside', null );
INSERT INTO godeepmobile.go_play_type( id, name, description ) VALUES ( 3, 'Pass - Short', null );
INSERT INTO godeepmobile.go_play_type( id, name, description ) VALUES ( 4, 'Pass - Mid', null );
INSERT INTO godeepmobile.go_play_type( id, name, description ) VALUES ( 5, 'Pass - Deep', null );
INSERT INTO godeepmobile.go_play_type( id, name, description ) VALUES ( 6, 'Punt', null );
INSERT INTO godeepmobile.go_play_type( id, name, description ) VALUES ( 7, 'Field Goal', null );
INSERT INTO godeepmobile.go_play_type( id, name, description ) VALUES ( 8, 'PAT - 1', null );
INSERT INTO godeepmobile.go_play_type( id, name, description ) VALUES ( 9, 'PAT - 2', null );
INSERT INTO godeepmobile.go_play_type( id, name, description ) VALUES ( 10, 'Kickoff', null );
INSERT INTO godeepmobile.go_play_type( id, name, description ) VALUES ( 11, 'No Play', null );

INSERT INTO godeepmobile.go_sport_type( id, name, description ) VALUES ( 1, 'Football', 'football' );
INSERT INTO godeepmobile.go_sport_type( id, name, description ) VALUES ( 2, 'Basketball', null );
INSERT INTO godeepmobile.go_sport_type( id, name, description ) VALUES ( 3, 'Soccer', null );
INSERT INTO godeepmobile.go_sport_type( id, name, description ) VALUES ( 4, 'hockey', null );
INSERT INTO godeepmobile.go_sport_type( id, name, description ) VALUES ( 5, 'field hockey', null );
INSERT INTO godeepmobile.go_sport_type( id, name, description ) VALUES ( 6, 'touch football', null );

INSERT INTO godeepmobile.go_state( id, name ) VALUES ( 1, 'AL' );
INSERT INTO godeepmobile.go_state( id, name ) VALUES ( 2, 'AK' );
INSERT INTO godeepmobile.go_state( id, name ) VALUES ( 3, 'AZ' );
INSERT INTO godeepmobile.go_state( id, name ) VALUES ( 4, 'AR' );
INSERT INTO godeepmobile.go_state( id, name ) VALUES ( 5, 'CA' );
INSERT INTO godeepmobile.go_state( id, name ) VALUES ( 6, 'CO' );
INSERT INTO godeepmobile.go_state( id, name ) VALUES ( 7, 'CT' );
INSERT INTO godeepmobile.go_state( id, name ) VALUES ( 8, 'DE' );
INSERT INTO godeepmobile.go_state( id, name ) VALUES ( 9, 'FL' );
INSERT INTO godeepmobile.go_state( id, name ) VALUES ( 10, 'GA' );
INSERT INTO godeepmobile.go_state( id, name ) VALUES ( 11, 'HI' );
INSERT INTO godeepmobile.go_state( id, name ) VALUES ( 12, 'ID' );
INSERT INTO godeepmobile.go_state( id, name ) VALUES ( 13, 'IL' );
INSERT INTO godeepmobile.go_state( id, name ) VALUES ( 14, 'IN' );
INSERT INTO godeepmobile.go_state( id, name ) VALUES ( 15, 'IA' );
INSERT INTO godeepmobile.go_state( id, name ) VALUES ( 16, 'KS' );
INSERT INTO godeepmobile.go_state( id, name ) VALUES ( 17, 'KY' );
INSERT INTO godeepmobile.go_state( id, name ) VALUES ( 18, 'LA' );
INSERT INTO godeepmobile.go_state( id, name ) VALUES ( 19, 'ME' );
INSERT INTO godeepmobile.go_state( id, name ) VALUES ( 20, 'MD' );
INSERT INTO godeepmobile.go_state( id, name ) VALUES ( 21, 'MA' );
INSERT INTO godeepmobile.go_state( id, name ) VALUES ( 22, 'MI' );
INSERT INTO godeepmobile.go_state( id, name ) VALUES ( 23, 'MN' );
INSERT INTO godeepmobile.go_state( id, name ) VALUES ( 24, 'MS' );
INSERT INTO godeepmobile.go_state( id, name ) VALUES ( 25, 'MO' );
INSERT INTO godeepmobile.go_state( id, name ) VALUES ( 26, 'MT' );
INSERT INTO godeepmobile.go_state( id, name ) VALUES ( 27, 'NE' );
INSERT INTO godeepmobile.go_state( id, name ) VALUES ( 28, 'NV' );
INSERT INTO godeepmobile.go_state( id, name ) VALUES ( 29, 'NH' );
INSERT INTO godeepmobile.go_state( id, name ) VALUES ( 30, 'NJ' );
INSERT INTO godeepmobile.go_state( id, name ) VALUES ( 31, 'NM' );
INSERT INTO godeepmobile.go_state( id, name ) VALUES ( 32, 'NY' );
INSERT INTO godeepmobile.go_state( id, name ) VALUES ( 33, 'NC' );
INSERT INTO godeepmobile.go_state( id, name ) VALUES ( 34, 'ND' );
INSERT INTO godeepmobile.go_state( id, name ) VALUES ( 35, 'OH' );
INSERT INTO godeepmobile.go_state( id, name ) VALUES ( 36, 'OK' );
INSERT INTO godeepmobile.go_state( id, name ) VALUES ( 37, 'OR' );
INSERT INTO godeepmobile.go_state( id, name ) VALUES ( 38, 'PA' );
INSERT INTO godeepmobile.go_state( id, name ) VALUES ( 39, 'RI' );
INSERT INTO godeepmobile.go_state( id, name ) VALUES ( 40, 'SC' );
INSERT INTO godeepmobile.go_state( id, name ) VALUES ( 41, 'SD' );
INSERT INTO godeepmobile.go_state( id, name ) VALUES ( 42, 'TN' );
INSERT INTO godeepmobile.go_state( id, name ) VALUES ( 43, 'TX' );
INSERT INTO godeepmobile.go_state( id, name ) VALUES ( 44, 'UT' );
INSERT INTO godeepmobile.go_state( id, name ) VALUES ( 45, 'VT' );
INSERT INTO godeepmobile.go_state( id, name ) VALUES ( 46, 'VA' );
INSERT INTO godeepmobile.go_state( id, name ) VALUES ( 47, 'WA' );
INSERT INTO godeepmobile.go_state( id, name ) VALUES ( 48, 'WV' );
INSERT INTO godeepmobile.go_state( id, name ) VALUES ( 49, 'WI' );
INSERT INTO godeepmobile.go_state( id, name ) VALUES ( 50, 'WY' );

INSERT INTO godeepmobile.go_surface_type( id, name ) VALUES ( 1, 'Field turf' );
INSERT INTO godeepmobile.go_surface_type( id, name ) VALUES ( 2, 'Natural grass, well maintained' );
INSERT INTO godeepmobile.go_surface_type( id, name ) VALUES ( 3, 'Natural grass, poorly maintained' );
INSERT INTO godeepmobile.go_surface_type( id, name ) VALUES ( 4, 'Astroturf (old)' );


INSERT INTO godeepmobile.go_user_role_type( id, name, description ) VALUES ( 1, 'Player', 'team player' );
INSERT INTO godeepmobile.go_user_role_type( id, name, description ) VALUES ( 2, 'Head Coach', 'team head coach' );
INSERT INTO godeepmobile.go_user_role_type( id, name, description ) VALUES ( 3, 'Assistant Coach', 'team assistant coach' );
INSERT INTO godeepmobile.go_user_role_type( id, name, description ) VALUES ( 4, 'Admin / Dir of Operations', 'team admin or director of operations' );
INSERT INTO godeepmobile.go_user_role_type( id, name, description ) VALUES ( 5, 'Graduate Assistant', 'team graduate assistant coach' );
INSERT INTO godeepmobile.go_user_role_type( id, name, description ) VALUES ( 6, 'Non-Coaching Resource', 'team non-coaching resource' );

-- player only gets PLAYER permission
INSERT INTO godeepmobile.go_role_permission( id, go_user_role_type_id, go_permission_id ) VALUES ( 1, 1, 'PLAYER' );

-- head coach gets GRADER, TEAM-ADMIN, and REPORT-VIEWER permissions
INSERT INTO godeepmobile.go_role_permission( id, go_user_role_type_id, go_permission_id ) VALUES ( 2, 2, 'GRADER' );
INSERT INTO godeepmobile.go_role_permission( id, go_user_role_type_id, go_permission_id ) VALUES ( 3, 2, 'TEAM-ADMIN' );
INSERT INTO godeepmobile.go_role_permission( id, go_user_role_type_id, go_permission_id ) VALUES ( 6, 2, 'REPORT-VIEWER' );

-- assistant coach gets GRADER, TEAM-ADMIN, and REPORT-VIEWER permissions temporarily
INSERT INTO godeepmobile.go_role_permission( id, go_user_role_type_id, go_permission_id ) VALUES ( 4, 3, 'GRADER' );
INSERT INTO godeepmobile.go_role_permission( id, go_user_role_type_id, go_permission_id ) VALUES ( 7, 3, 'TEAM-ADMIN' );
INSERT INTO godeepmobile.go_role_permission( id, go_user_role_type_id, go_permission_id ) VALUES ( 8, 3, 'REPORT-VIEWER' );


INSERT INTO godeepmobile.go_role_permission( id, go_user_role_type_id, go_permission_id ) VALUES ( 5, 4, 'TEAM-ADMIN' );
INSERT INTO godeepmobile.go_role_permission( id, go_user_role_type_id, go_permission_id ) VALUES ( 9, 4, 'TEAM-ADMIN' );
INSERT INTO godeepmobile.go_role_permission( id, go_user_role_type_id, go_permission_id ) VALUES ( 10, 4, 'REPORT-VIEWER' );


INSERT INTO godeepmobile.go_conference( name, description, id, go_organization_type_id, level ) VALUES ( 'ACC', 'Atlantic Coast Conference', 1, 2, 'FBS' );
INSERT INTO godeepmobile.go_conference( name, description, id, go_organization_type_id, level ) VALUES ( 'SEC', 'Southeastern Conference', 2, 2, 'FBS' );
INSERT INTO godeepmobile.go_conference( name, description, id, go_organization_type_id, level ) VALUES ( 'PAC12', 'Pacific Coast 12', 3, 2, 'FBS' );
INSERT INTO godeepmobile.go_conference( name, description, id, go_organization_type_id, level ) VALUES ( 'BIG 10', 'Big 10', 4, 2, 'FBS' );
INSERT INTO godeepmobile.go_conference( name, description, id, go_organization_type_id, level ) VALUES ( 'IVY', 'Ivy League', 5, 2, 'FCS' );
INSERT INTO godeepmobile.go_conference( name, description, id, go_organization_type_id, level ) VALUES ( 'BIG 12', 'Big Twelve Conference', 6, 2, 'FBS' );
INSERT INTO godeepmobile.go_conference( name, description, id, go_organization_type_id, level ) VALUES ( 'C-USA', 'Conference USA', 7, 2, 'FBS' );
INSERT INTO godeepmobile.go_conference( name, description, id, go_organization_type_id, level ) VALUES ( 'IND', 'Independent - no conference', 8, 2, 'D-3' );
INSERT INTO godeepmobile.go_conference( name, description, id, go_organization_type_id, level ) VALUES ( 'MAC', 'Mid-American Conference', 9, 2, 'D-3' );
INSERT INTO godeepmobile.go_conference( name, description, id, go_organization_type_id, level ) VALUES ( 'MWC', 'Mountain West Conference', 10, 2, 'FBS' );
INSERT INTO godeepmobile.go_conference( name, description, id, go_organization_type_id, level ) VALUES ( 'AAC', 'American Athletic Conference', 11, 2, 'FBS' );
INSERT INTO godeepmobile.go_conference( name, description, id, go_organization_type_id, level ) VALUES ( 'SBC', 'Sun Belt Conference', 12, 2, 'FBS' );
INSERT INTO godeepmobile.go_conference( name, description, id, go_organization_type_id, level ) VALUES ( 'PFL', 'Pioneer Football League', 13, 2, 'FCS' );
INSERT INTO godeepmobile.go_conference( name, description, id, go_organization_type_id, level ) VALUES ( 'PL', 'Patriot League', 14, 2, 'FCS' );
INSERT INTO godeepmobile.go_conference( name, description, id, go_organization_type_id, level ) VALUES ( 'AFC', 'American Football Conference', 15, 3, 'NFL' );
INSERT INTO godeepmobile.go_conference( name, description, id, go_organization_type_id, level ) VALUES ( 'NFC', 'National Football Conference', 16, 3, 'NFL' );
INSERT INTO godeepmobile.go_conference( name, description, id, go_organization_type_id, level ) VALUES ( 'AAC', 'American Athletic Conference', 17, 2, 'FBS' );
INSERT INTO godeepmobile.go_conference( name, description, id, go_organization_type_id, level ) VALUES ( 'BIG SKY', 'Big Sky Conference', 18, 2, 'FCS' );
INSERT INTO godeepmobile.go_conference( name, description, id, go_organization_type_id, level ) VALUES ( 'NEC', 'Northeast Conference', 19, 2, 'FCS' );
INSERT INTO godeepmobile.go_conference( name, description, id, go_organization_type_id, level ) VALUES ( 'MVFC', 'Missouri Valley Football Conference', 20, 2, 'FCS' );

INSERT INTO godeepmobile.go_position_type( id, name, go_sport_type_id, short_name, go_platoon_type_id ) VALUES ( 1, 'Quarterback', 1, 'QB', 1 );
INSERT INTO godeepmobile.go_position_type( id, name, go_sport_type_id, short_name, go_platoon_type_id ) VALUES ( 2, 'Inside Linebacker', 1, 'ILB', 2 );
INSERT INTO godeepmobile.go_position_type( id, name, go_sport_type_id, short_name, go_platoon_type_id ) VALUES ( 3, 'Wide Receiver', 1, 'WR', 1 );
INSERT INTO godeepmobile.go_position_type( id, name, go_sport_type_id, short_name, go_platoon_type_id ) VALUES ( 4, 'Fullback', 1, 'FB', 1 );
INSERT INTO godeepmobile.go_position_type( id, name, go_sport_type_id, short_name, go_platoon_type_id ) VALUES ( 5, 'Halfback', 1, 'HB', 1 );
INSERT INTO godeepmobile.go_position_type( id, name, go_sport_type_id, short_name, go_platoon_type_id ) VALUES ( 6, 'Running Back', 1, 'RB', 1 );
INSERT INTO godeepmobile.go_position_type( id, name, go_sport_type_id, short_name, go_platoon_type_id ) VALUES ( 7, 'Tight End', 1, 'TE', 1 );
INSERT INTO godeepmobile.go_position_type( id, name, go_sport_type_id, short_name, go_platoon_type_id ) VALUES ( 8, 'Center', 1, 'OC', 1 );
INSERT INTO godeepmobile.go_position_type( id, name, go_sport_type_id, short_name, go_platoon_type_id ) VALUES ( 9, 'Offensive Guard', 1, 'OG', 1 );
INSERT INTO godeepmobile.go_position_type( id, name, go_sport_type_id, short_name, go_platoon_type_id ) VALUES ( 10, 'Offensive Tackle', 1, 'OT', 1 );
INSERT INTO godeepmobile.go_position_type( id, name, go_sport_type_id, short_name, go_platoon_type_id ) VALUES ( 11, 'Cornerback', 1, 'DC', 2 );
INSERT INTO godeepmobile.go_position_type( id, name, go_sport_type_id, short_name, go_platoon_type_id ) VALUES ( 12, 'Safety', 1, 'S', 2 );
INSERT INTO godeepmobile.go_position_type( id, name, go_sport_type_id, short_name, go_platoon_type_id ) VALUES ( 13, 'Free Safety', 1, 'FS', 2 );
INSERT INTO godeepmobile.go_position_type( id, name, go_sport_type_id, short_name, go_platoon_type_id ) VALUES ( 14, 'Strong Safety', 1, 'SS', 2 );
INSERT INTO godeepmobile.go_position_type( id, name, go_sport_type_id, short_name, go_platoon_type_id ) VALUES ( 15, 'Sam Linebacker', 1, 'SLB', 2 );
INSERT INTO godeepmobile.go_position_type( id, name, go_sport_type_id, short_name, go_platoon_type_id ) VALUES ( 16, 'Mike Linebacker', 1, 'MLB', 2 );
INSERT INTO godeepmobile.go_position_type( id, name, go_sport_type_id, short_name, go_platoon_type_id ) VALUES ( 17, 'Will Linebacker', 1, 'WLB', 2 );
INSERT INTO godeepmobile.go_position_type( id, name, go_sport_type_id, short_name, go_platoon_type_id ) VALUES ( 18, 'Outside Linebacker', 1, 'OLB', 2 );
INSERT INTO godeepmobile.go_position_type( id, name, go_sport_type_id, short_name, go_platoon_type_id ) VALUES ( 19, 'Rush End', 1, 'RE', 2 );
INSERT INTO godeepmobile.go_position_type( id, name, go_sport_type_id, short_name, go_platoon_type_id ) VALUES ( 20, 'Defensive End', 1, 'DE', 2 );
INSERT INTO godeepmobile.go_position_type( id, name, go_sport_type_id, short_name, go_platoon_type_id ) VALUES ( 21, 'Defensive Tackle', 1, 'DT', 2 );
INSERT INTO godeepmobile.go_position_type( id, name, go_sport_type_id, short_name, go_platoon_type_id ) VALUES ( 22, 'Nose Tackle', 1, 'NT', 2 );
INSERT INTO godeepmobile.go_position_type( id, name, go_sport_type_id, short_name, go_platoon_type_id ) VALUES ( 23, 'Kicker', 1, 'K', 3 );
INSERT INTO godeepmobile.go_position_type( id, name, go_sport_type_id, short_name, go_platoon_type_id ) VALUES ( 24, 'Punter', 1, 'P', 3 );
INSERT INTO godeepmobile.go_position_type( id, name, go_sport_type_id, short_name, go_platoon_type_id ) VALUES ( 25, 'Gunner', 1, 'GUN', 3 );
INSERT INTO godeepmobile.go_position_type( id, name, go_sport_type_id, short_name, go_platoon_type_id ) VALUES ( 26, 'Long Snapper', 1, 'LS', 3 );
INSERT INTO godeepmobile.go_position_type( id, name, go_sport_type_id, short_name, go_platoon_type_id ) VALUES ( 27, 'Holder', 1, 'H-K', 3 );
INSERT INTO godeepmobile.go_position_type( id, name, go_sport_type_id, short_name, go_platoon_type_id ) VALUES ( 28, 'Return Specialist', 1, 'RS', 3 );
INSERT INTO godeepmobile.go_position_type( id, name, go_sport_type_id, short_name, go_platoon_type_id ) VALUES ( 29, 'Athlete', 1, 'ATH', 0 );
INSERT INTO godeepmobile.go_position_type( id, name, go_sport_type_id, short_name, go_platoon_type_id ) VALUES ( 30, 'Right Guard', 1, 'RG', 1 );
INSERT INTO godeepmobile.go_position_type( id, name, go_sport_type_id, short_name, go_platoon_type_id ) VALUES ( 31, 'Left Guard', 1, 'LG', 1 );
INSERT INTO godeepmobile.go_position_type( id, name, go_sport_type_id, short_name, go_platoon_type_id ) VALUES ( 32, 'Right Tackle', 1, 'RT', 1 );
INSERT INTO godeepmobile.go_position_type( id, name, go_sport_type_id, short_name, go_platoon_type_id ) VALUES ( 33, 'Left Tackle', 1, 'LT', 1 );
