--liquibase formatted sql

--changeset rambert:58 runOnChange:true stripComments:false splitStatements:false
--comment Add Footbal Operation/Scouting user type to go_user_role_type


-- insert Footbal Operation/Scouting user role type
INSERT INTO godeepmobile.go_user_role_type( id, name, description ) VALUES ( 7, 'Football Operations/Scouting', 'Football operations/scouting');

-- insert Footbal Operation/Scouting into role permission
INSERT INTO godeepmobile.go_role_permission( id, go_user_role_type_id, go_permission_id ) VALUES ( 11, 7, 'GRADER' );
INSERT INTO godeepmobile.go_role_permission( id, go_user_role_type_id, go_permission_id ) VALUES ( 12, 7, 'TEAM-ADMIN' );
INSERT INTO godeepmobile.go_role_permission( id, go_user_role_type_id, go_permission_id ) VALUES ( 13, 7, 'REPORT-VIEWER' );

--rollback DELETE FROM godeepmobile.go_role_permission where id in (11, 12, 13); DELETE FROM godeepmobile.go_user_role_type where id = 7;