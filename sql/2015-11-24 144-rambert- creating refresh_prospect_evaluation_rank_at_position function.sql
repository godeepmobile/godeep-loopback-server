--liquibase formatted sql

--changeset rambert:144 runOnChange:true splitStatements:false stripComments:false
--comment creating refresh_player_general_season_ranking function

CREATE OR REPLACE FUNCTION godeepmobile.refresh_prospect_evaluation_rank_at_position (
  team_id_parm uuid,
  season_parm integer,
  position_id_parm integer)
  RETURNS void AS
$BODY$
DECLARE
  record_value RECORD;
  record_exists integer;
BEGIN

  FOR record_value IN
    SELECT stat.go_team_id,
        stat.season,
        stat.team_player_id,
        score_group.go_position_type_id,
        stat.team_scouting_eval_score_group_id,
        stat.target_match,
        stat.overall_eval_avg,
        --stat.major_factors_eval_avg,
        --stat.critical_factors_eval_avg,
        --stat.position_skills_eval_avg,
        RANK() OVER (ORDER BY stat.overall_eval_avg DESC, stat.target_match DESC) AS rank_at_post
    FROM godeepmobile.player_evaluation_stat stat,
         godeepmobile.team_scouting_eval_score_group score_group,
         godeepmobile.view_team_prospect prospect
    WHERE stat.go_team_id = team_id_parm::uuid
          AND score_group.season = season_parm
            AND stat.season = score_group.season
            AND score_group.go_position_type_id = position_id_parm
          AND stat.team_scouting_eval_score_group_id = score_group.id
          AND prospect.id = stat.team_player_id

      LOOP
      -- check if record already exists
      SELECT COUNT(*) INTO record_exists
      FROM godeepmobile.player_rank_overall_at_position
      WHERE go_team_id = team_id_parm::uuid
        AND team_player_id = record_value.team_player_id
        AND season = season_parm
        AND go_position_type_id = position_id_parm
        AND team_scouting_eval_score_group_id = record_value.team_scouting_eval_score_group_id;

      IF record_exists = 0 THEN
        INSERT INTO godeepmobile.player_rank_overall_at_position (
          go_team_id,
          team_player_id,
          season,
          go_position_type_id,
          team_scouting_eval_score_group_id,
          overall_eval_avg,
          target_match,
          rank_at_post
        )
        VALUES (
          team_id_parm::uuid,
          record_value.team_player_id,
          season_parm,
          position_id_parm,
          record_value.team_scouting_eval_score_group_id,
          record_value.overall_eval_avg,
          record_value.target_match,
          record_value.rank_at_post
        );
      ELSE
        UPDATE
            godeepmobile.player_rank_overall_at_position
        SET
      --  go_team_id,
        --team_player_id,
        overall_eval_avg = record_value.overall_eval_avg,
        target_match = record_value.target_match,
        rank_at_post = record_value.rank_at_post
        WHERE go_team_id = team_id_parm::uuid
          AND team_player_id = record_value.team_player_id
          AND season = season_parm
          AND go_position_type_id = position_id_parm
          AND team_scouting_eval_score_group_id = record_value.team_scouting_eval_score_group_id;
      END IF;
      END LOOP;

END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

--rollback DROP FUNCTION godeepmobile.refresh_prospect_evaluation_rank_at_position(uuid, integer, integer);
