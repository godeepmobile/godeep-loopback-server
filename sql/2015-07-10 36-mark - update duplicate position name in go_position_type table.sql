--liquibase formatted sql

--changeset mark:36 runOnChange:true stripComments:false splitStatements:false
--comment update duplicate position name in go_position_type table

UPDATE godeepmobile.go_position_type set name = 'Defensive Line' where id=36;
