﻿--liquibase formatted sql

--changeset carlos:215 runOnChange:true splitStatements:false stripComments:false
--comment updated view_prospect_evaluation_season_stat to use view_team_prospect_assignment

CREATE OR REPLACE VIEW view_prospect_evaluation_season_stat AS 
 SELECT prospect.team_player_id as id,
    prospect.go_team_id,
    prospect.team_player_id AS team_prospect_id,
    (SELECT first_name FROM view_team_prospect WHERE id = prospect.team_player_id),--prospect.first_name,
    (SELECT last_name FROM view_team_prospect WHERE id = prospect.team_player_id),--prospect.last_name,
    prospect.season,
    evaluation_season_stat.num_evaluations,
    evaluation_season_stat.target_match,
    evaluation_season_stat.major_factors_eval_avg,
    evaluation_season_stat.critical_factors_eval_avg,
    evaluation_season_stat.position_skills_eval_avg,
    evaluation_season_stat.overall_eval_avg,
    evaluation_season_stat.overall_eval_avg_rank,
    evaluation_season_stat.rank_overall,
    ( SELECT prospect_season_stat.rank_at_post
           FROM view_prospect_rank_overall_at_position_season_stat prospect_season_stat
          WHERE prospect_season_stat.go_team_id = prospect.go_team_id AND 
		prospect_season_stat.team_prospect_id = prospect.team_player_id AND 
		prospect_season_stat.season = evaluation_season_stat.season AND 
		prospect_season_stat.go_position_type_id = (SELECT position_1 FROM view_team_prospect_assignment WHERE team_player_id = prospect.team_player_id AND season = evaluation_season_stat.season)) AS rank_at_post,
    prospect.id AS team_prospect_assignment_id
   FROM player_evaluation_season_stat evaluation_season_stat
     RIGHT JOIN view_team_prospect_assignment prospect ON evaluation_season_stat.team_player_id = prospect.team_player_id AND evaluation_season_stat.season = prospect.season;

-- rollback DROP VIEW view_prospect_evaluation_season_stat;