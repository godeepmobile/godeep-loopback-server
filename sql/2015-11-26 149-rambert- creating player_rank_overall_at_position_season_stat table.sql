--liquibase formatted sql

--changeset rambert:149 runOnChange:true stripComments:false splitStatements:false
--comment create player_rank_overall_at_position_season_stat table

CREATE TABLE godeepmobile.player_rank_overall_at_position_season_stat  (
  go_team_id                 uuid NOT NULL,
  team_player_id             uuid NOT NULL,
  season                     integer NOT NULL,
  go_position_type_id        integer NOT NULL,
  overall_eval_avg           numeric(100,2),
  target_match               smallint,
  num_evaluations_at_post    smallint,
  rank_at_post               smallint,
  CONSTRAINT pk_player_rank_overall_at_position_season_stat PRIMARY KEY ( go_team_id, team_player_id, season, go_position_type_id),
  CONSTRAINT fk_player_rank_overall_at_position_season_stat_go_team FOREIGN KEY ( go_team_id )
      REFERENCES go_team (id),
  CONSTRAINT fk_player_rank_overall_at_position_season_stat_team_player FOREIGN KEY ( team_player_id )
      REFERENCES team_player (id),
  CONSTRAINT fk_player_rank_overall_at_position_season_stat_go_position_type FOREIGN KEY ( go_position_type_id )
      REFERENCES go_position_type (id)
);

--rollback DROP TABLE IF EXISTS  godeepmobile.player_rank_overall_at_position_season_stat;
