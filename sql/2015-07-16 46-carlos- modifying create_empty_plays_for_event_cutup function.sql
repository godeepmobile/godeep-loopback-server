--liquibase formatted sql

--changeset carlos:46 runOnChange:true splitStatements:false
--comment modified create_empty_plays_for_event_cutup function
CREATE OR REPLACE FUNCTION godeepmobile.create_empty_plays_for_event_cutup(
    how_many_parm integer,
    team_event_id_parm integer,
    team_cutup_id_parm integer,
    go_team_id_parm text)
   RETURNS VOID AS
$BODY$
DECLARE
	max_play_number integer;
	exists integer;
BEGIN
	if how_many_parm <= 0 then
		RAISE EXCEPTION 'How many is not positive non-zero number %', how_many_parm USING HINT = 'Please check your how many parameter';
	end if;

	select count(cutup.id) into exists from godeepmobile.team_cutup cutup where cutup.go_team_id = go_team_id_parm::uuid and cutup.team_event_id = team_event_id_parm and cutup.id = team_cutup_id_parm;
	if exists != 1 then
		RAISE EXCEPTION 'Team cutup % does not exist for team %', team_event_id_parm, go_team_id_parm USING HINT = 'Please check your event and team parameters';
	end if;

	FOR i IN (1)..(how_many_parm) LOOP
		INSERT INTO godeepmobile.team_play_data (play_number, team_event_id, go_team_id, team_cutup_id ) VALUES (i, team_event_id_parm, go_team_id_parm::uuid, team_cutup_id_parm);
	END LOOP;
END;
$BODY$
  LANGUAGE plpgsql;

--rollback DROP FUNCTION IF EXISTS godeepmobile.create_empty_plays_for_event_cutup(integer,integer,integer,text);
