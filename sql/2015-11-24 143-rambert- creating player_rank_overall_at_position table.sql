--liquibase formatted sql

--changeset rambert:143 runOnChange:true stripComments:false splitStatements:false
--comment create player_rank_overall_at_position table

CREATE TABLE godeepmobile.player_rank_overall_at_position  (
  go_team_id                 uuid NOT NULL,
  team_player_id             uuid NOT NULL,
  season                     integer NOT NULL,
  go_position_type_id        integer NOT NULL,
  team_scouting_eval_score_group_id integer NOT NULL,
  overall_eval_avg            numeric(100,2),
  target_match               smallint,
  rank_at_post               smallint,
  CONSTRAINT pk_player_rank_overall_at_position PRIMARY KEY ( go_team_id, team_player_id, season, team_scouting_eval_score_group_id),
  CONSTRAINT fk_player_rank_overall_at_position_go_team FOREIGN KEY ( go_team_id )
      REFERENCES go_team (id),
  CONSTRAINT fk_player_rank_overall_at_position_team_player FOREIGN KEY ( team_player_id )
      REFERENCES team_player (id),
  CONSTRAINT fk_player_rank_overall_at_position_go_position_type FOREIGN KEY ( go_position_type_id )
      REFERENCES go_position_type (id),
  CONSTRAINT fk_player_rank_overall_at_pos_team_scouting_eval_score_group FOREIGN KEY ( team_scouting_eval_score_group_id )
      REFERENCES team_scouting_eval_score_group (id)
);

--rollback DROP TABLE IF EXISTS  godeepmobile.player_rank_overall_at_position;
