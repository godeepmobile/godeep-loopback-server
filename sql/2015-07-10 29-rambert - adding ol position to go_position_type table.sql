--liquibase formatted sql

--changeset rambert:29 runOnChange:true stripComments:false splitStatements:false
--comment add Offensive Line missing position to go_position_type table

-- insert Offensive Line position into global position types table in Offensive platoon
INSERT INTO godeepmobile.go_position_type( id, name, go_sport_type_id, short_name, go_platoon_type_id ) VALUES ( 37, 'Offensive Line', 1, 'OL', 1 );

-- insert these 1 new position into appropriate global position group
INSERT INTO go_position_type_to_group_assignment(go_position_type_id,go_position_group_id,go_platoon_type_id) VALUES ( 37,4,1 ); -- add to offensive line group

--rollback DELETE FROM go_position_type_to_group_assignment where go_position_type_id = 37; DELETE FROM go_position_type where id = 37;
