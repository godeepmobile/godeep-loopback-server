--liquibase formatted sql

--changeset rambert:167 runOnChange:true splitStatements:false stripComments:false
--comment creating refresh_player_season_overall_rank_at_position function

CREATE OR REPLACE FUNCTION godeepmobile.refresh_player_season_overall_rank_at_position (
  team_id_parm uuid,
  season_parm integer,
  position_id_parm integer)
  RETURNS void AS
$BODY$
DECLARE
  record_value RECORD;
  team_conf    RECORD;
  record_exists integer;
BEGIN

  -- updating overall rank at position
  SELECT  (conf.grade_base - (conf.grade_increment * 2)) AS grade_eval_min,
    (conf.grade_base + (conf.grade_increment * 2)) AS grade_eval_max,
    conf.scout_eval_score_min,
    conf.scout_eval_score_max  INTO team_conf
  FROM godeepmobile.team_configuration conf
  WHERE conf.go_team_id = team_id_parm::uuid;

  FOR record_value IN

  SELECT
      subquery.go_team_id,
      subquery.team_player_id,
      subquery.season,
      subquery.team_position_type_id_pos_1,
      RANK() OVER (ORDER BY subquery.overall_grade_evaluation_value DESC) AS rank_at_pos
    FROM (
        SELECT grade_stat.go_team_id,
          player.id AS team_player_id, grade_stat.season,
          player.team_position_type_id_pos_1,
          grade_stat.overall_grade_game_rank,
          eval_stat.rank_overall,
          ((CASE WHEN grade_stat.overall_grade_game = 0 THEN NULL ELSE grade_stat.overall_grade_game END * 2) +
          -- converting evaluation value to the same interval values as grade
          -- (((OldValue - EvalMin) * (GradeMax - GradeMin)) / (EvalMax - EvalMin)) + GradeMin
          ((((CASE WHEN eval_stat.overall_eval_avg = 0 THEN NULL ELSE eval_stat.overall_eval_avg END - team_conf.scout_eval_score_min)
           * (team_conf.grade_eval_max - team_conf.grade_eval_min))
           / (team_conf.scout_eval_score_max - team_conf.scout_eval_score_min))
           + team_conf.grade_eval_min))  as overall_grade_evaluation_value

        FROM godeepmobile.team_player_season_stat grade_stat JOIN
             godeepmobile.view_team_player player
             ON grade_stat.team_player_id = player.id
             JOIN godeepmobile.player_evaluation_season_stat eval_stat
             ON player.id = eval_stat.team_player_id
        WHERE player.go_team_id = team_id_parm::uuid
              AND grade_stat.season = season_parm
              AND eval_stat.season = grade_stat.season
              AND grade_stat.overall_grade_game > 0
	      AND eval_stat.overall_eval_avg > 0
              AND player.team_position_type_id_pos_1 = position_id_parm
        ORDER BY overall_grade_evaluation_value ASC, eval_stat.rank_overall, grade_stat.overall_grade_game_rank
      ) as subquery

      LOOP
      -- check if record already exists
      SELECT COUNT(*) INTO record_exists
      FROM godeepmobile.player_overall_season_rank_at_position_stat
      WHERE go_team_id = team_id_parm::uuid
        AND team_player_id = record_value.team_player_id
        AND season = season_parm
        AND go_position_type_id = position_id_parm;

      IF record_exists = 0 THEN
        INSERT INTO godeepmobile.player_overall_season_rank_at_position_stat (
          go_team_id,
          team_player_id,
          season,
          go_position_type_id,
          rank_at_post
        )
        VALUES (
          team_id_parm::uuid,
          record_value.team_player_id,
          season_parm,
          position_id_parm,
          record_value.rank_at_pos
        );
      ELSE
        UPDATE
            godeepmobile.player_overall_season_rank_at_position_stat
        SET
          rank_at_post = record_value.rank_at_pos
        WHERE go_team_id = team_id_parm::uuid
            AND team_player_id = record_value.team_player_id
            AND season = season_parm
            AND go_position_type_id = position_id_parm;
      END IF;
      END LOOP;

END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

--rollback DROP FUNCTION godeepmobile.refresh_player_season_overall_rank_at_position(uuid, integer, integer);
