--liquibase formatted sql

--changeset rambert:71 runOnChange:true stripComments:false splitStatements:false
--comment add go_level table

CREATE TABLE godeepmobile.go_level (
  id          integer  NOT NULL ,
  name        character varying(100)  ,
  short_name  character varying(8)  ,
  description character varying(256) ,
  CONSTRAINT  pk_go_level PRIMARY KEY ( id )
);

-- insert level picklist values
INSERT INTO godeepmobile.go_level( id, name, short_name, description ) VALUES ( 1, 'National Football League', 'NFL', 'National Football League' );
INSERT INTO godeepmobile.go_level( id, name, short_name, description ) VALUES ( 2, 'Football Bowl Subdivision', 'FBS', 'Football Bowl Subdivision' );
INSERT INTO godeepmobile.go_level( id, name, short_name, description ) VALUES ( 3, 'Football Championship Subdivision', 'FCS', 'Football Championship Subdivision' );
INSERT INTO godeepmobile.go_level( id, name, short_name, description ) VALUES ( 4, 'Division II', 'D-2', 'Division II' );
INSERT INTO godeepmobile.go_level( id, name, short_name, description ) VALUES ( 5, 'Division III', 'D-3', 'Division III' );
INSERT INTO godeepmobile.go_level( id, name, short_name, description ) VALUES ( 6, 'Junior College', 'JUCO', 'Junior College' );
INSERT INTO godeepmobile.go_level( id, name, short_name, description ) VALUES ( 7, 'High School', 'HS', 'High School' );

-- add go_level_id constraint to team_player
ALTER TABLE godeepmobile.team_player ADD go_level_id INTEGER;
ALTER TABLE godeepmobile.team_player ADD CONSTRAINT fk_team_player_go_level FOREIGN KEY ( go_level_id ) REFERENCES godeepmobile.go_level( id );

-- add go_level_id constraint to go_conference
ALTER TABLE godeepmobile.go_conference ADD go_level_id INTEGER;
ALTER TABLE godeepmobile.go_conference ADD CONSTRAINT fk_go_conference_go_level FOREIGN KEY ( go_level_id ) REFERENCES godeepmobile.go_level( id );

-- setup go_level_id ids for go_conference
UPDATE godeepmobile.go_conference
SET go_level_id = query.go_level_id
FROM (
    SELECT level.id AS go_level_id, conference.id AS go_conference_id
    FROM go_level level 
        INNER JOIN go_conference conference ON level.short_name = conference.level
    ) as query
WHERE godeepmobile.go_conference.id = query.go_conference_id;

-- dropping temporarily view_season_stat_ids_for_players view due to dependency on view_team_player view
DROP VIEW IF EXISTS view_season_stat_ids_for_players;
-- dropping temporarily view_season_stat_ids_for_players view due to dependency on team_player table
DROP VIEW IF EXISTS view_team_player;

-- changing level column to go_level_id
CREATE OR REPLACE VIEW view_team_player AS
SELECT team_player.id,
    team_player.last_name,
    team_player.middle_name,
    team_player.first_name,
    team_player.go_team_id,
    team_player.data,
    team_player.go_user_team_assignment_id,
    team_player.class_year,
    team_player.birth_date,
    team_player.jersey_number,
    team_player.season,
    team_player.height,
    team_player.weight,
    team_player.graduation_year,
    team_player.wingspan,
    team_player.forty_yard,
    team_player.go_body_type_id,
    team_player.hand_size,
    team_player.vertical_jump,
    team_player.email,
    team_player.sat,
    team_player.act,
    team_player.gpa,
    team_player.go_state_id,
    team_player.team_position_type_id_pos_1,
    team_player.team_position_type_id_pos_2,
    team_player.team_position_type_id_pos_3,
    team_player.team_position_type_id_pos_st,
    team_player.high_school,
    team_player.coach,
    team_player.coach_phone,
    team_player.go_level_id,
    team_player.is_veteran,
    team_player.start_date,
    team_player.end_date,
        CASE
            WHEN (team_player.team_position_type_id_pos_st IS NULL) THEN false
            ELSE true
        END AS is_special_teams,
    ( WITH offense AS (
                 SELECT team_position_type.id
                   FROM team_position_type
                  WHERE (team_position_type.go_platoon_type_id = 1)
                )
         SELECT
                CASE
                    WHEN (((team_player.team_position_type_id_pos_1 IN ( SELECT offense.id
                       FROM offense)) OR (team_player.team_position_type_id_pos_2 IN ( SELECT offense.id
                       FROM offense))) OR (team_player.team_position_type_id_pos_3 IN ( SELECT offense.id
                       FROM offense))) THEN true
                    ELSE false
                END AS "case") AS is_offense,
    ( WITH defense AS (
                 SELECT team_position_type.id
                   FROM team_position_type
                  WHERE (team_position_type.go_platoon_type_id = 2)
                )
         SELECT
                CASE
                    WHEN (((team_player.team_position_type_id_pos_1 IN ( SELECT defense.id
                       FROM defense)) OR (team_player.team_position_type_id_pos_2 IN ( SELECT defense.id
                       FROM defense))) OR (team_player.team_position_type_id_pos_3 IN ( SELECT defense.id
                       FROM defense))) THEN true
                    ELSE false
                END AS "case") AS is_defense,
    ( SELECT tpcs.id
           FROM team_player_career_stat tpcs
          WHERE (team_player.id = tpcs.team_player_id)) AS team_player_career_stat,
    ARRAY( SELECT tpss.id
           FROM team_player_season_stat tpss
          WHERE (team_player.id = tpss.team_player_id)
          ORDER BY tpss.season DESC) AS team_player_season_stats
   FROM team_player
  WHERE team_player.end_date IS NULL
  ORDER BY team_player.jersey_number;

-- re-creating view_season_stat_ids_for_players
 CREATE OR REPLACE VIEW view_season_stat_ids_for_players AS
 SELECT player.go_team_id,
    player.id AS team_player_id,
    stats.id AS team_player_season_stat_id,
    player.season,
    player.is_veteran
   FROM view_team_player player
     LEFT JOIN team_player_season_stat stats ON stats.team_player_id = player.id
  ORDER BY player.id;

--rollback ALTER TABLE godeepmobile.go_conference DROP COLUMN go_level_id; DROP VIEW IF EXISTS view_season_stat_ids_for_players; DROP VIEW IF EXISTS view_team_player; ALTER TABLE godeepmobile.team_player DROP COLUMN go_level_id; DROP TABLE godeepmobile.go_level; CREATE OR REPLACE VIEW view_team_player AS SELECT team_player.id, team_player.last_name, team_player.middle_name, team_player.first_name, team_player.go_team_id, team_player.data, team_player.go_user_team_assignment_id, team_player.class_year, team_player.birth_date, team_player.jersey_number, team_player.season, team_player.height, team_player.weight, team_player.graduation_year, team_player.wingspan, team_player.forty_yard, team_player.go_body_type_id, team_player.hand_size, team_player.vertical_jump, team_player.email, team_player.sat, team_player.act, team_player.gpa, team_player.go_state_id, team_player.team_position_type_id_pos_1, team_player.team_position_type_id_pos_2, team_player.team_position_type_id_pos_3, team_player.team_position_type_id_pos_st, team_player.high_school, team_player.coach, team_player.coach_phone, team_player.level, team_player.is_veteran, team_player.start_date, team_player.end_date, CASE WHEN (team_player.team_position_type_id_pos_st IS NULL) THEN false ELSE true END AS is_special_teams, ( WITH offense AS ( SELECT team_position_type.id FROM team_position_type WHERE (team_position_type.go_platoon_type_id = 1) ) SELECT CASE WHEN (((team_player.team_position_type_id_pos_1 IN ( SELECT offense.id FROM offense)) OR (team_player.team_position_type_id_pos_2 IN ( SELECT offense.id FROM offense))) OR (team_player.team_position_type_id_pos_3 IN ( SELECT offense.id FROM offense))) THEN true ELSE false END AS "case") AS is_offense, ( WITH defense AS (SELECT team_position_type.id FROM team_position_type WHERE (team_position_type.go_platoon_type_id = 2)) SELECT CASE WHEN (((team_player.team_position_type_id_pos_1 IN ( SELECT defense.id FROM defense)) OR (team_player.team_position_type_id_pos_2 IN ( SELECT defense.id FROM defense))) OR (team_player.team_position_type_id_pos_3 IN ( SELECT defense.id FROM defense))) THEN true ELSE false END AS "case") AS is_defense, ( SELECT tpcs.id FROM team_player_career_stat tpcs WHERE (team_player.id = tpcs.team_player_id)) AS team_player_career_stat, ARRAY( SELECT tpss.id FROM team_player_season_stat tpss WHERE (team_player.id = tpss.team_player_id) ORDER BY tpss.season DESC) AS team_player_season_stats FROM team_player WHERE team_player.end_date IS NULL ORDER BY team_player.jersey_number; CREATE OR REPLACE VIEW view_season_stat_ids_for_players AS SELECT player.go_team_id, player.id AS team_player_id, stats.id AS team_player_season_stat_id, player.season, player.is_veteran FROM view_team_player player LEFT JOIN team_player_season_stat stats ON stats.team_player_id = player.id ORDER BY player.id;



