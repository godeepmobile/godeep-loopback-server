--liquibase formatted sql

--changeset rambert:54 runOnChange:true stripComments:false splitStatements:false
--comment update view_team_player_event_grades

-- dropping temporarily view_game_stat_ids_for_players_with_event_plays view due to dependency on view_team_player_event_grades view
DROP VIEW IF EXISTS view_game_stat_ids_for_players_with_event_plays;
-- dropping temporarily view_team_player_event_game_grades view due to dependency on view_team_player_event_grades view
DROP VIEW IF EXISTS view_team_player_event_game_grades;

DROP VIEW IF EXISTS view_team_player_event_grades;

CREATE OR REPLACE VIEW view_team_player_event_grades AS
SELECT player.id AS "teamPlayer",
  player.season,
  count(grade.id)::integer AS "numPlays",
  sum(grade.cat_1_grade)::integer AS "sumCat1Grade",
  sum(grade.cat_2_grade)::integer AS "sumCat2Grade",
  sum(grade.cat_3_grade)::integer AS "sumCat3Grade",
  sum(grade.cat_1_grade + grade.cat_2_grade + grade.cat_3_grade)::integer AS "sumOverallGrade",
  round(avg(grade.cat_1_grade)::numeric, 2) AS "avgCat1Grade",
  round(avg(grade.cat_2_grade)::numeric, 2) AS "avgCat2Grade",
  round(avg(grade.cat_3_grade)::numeric, 2) AS "avgCat3Grade",
  round((avg(grade.cat_1_grade + grade.cat_2_grade + grade.cat_3_grade) / 3::numeric)::numeric, 2) AS "avgOverallGrade",
  play.team_event_id AS "teamEvent",
  event.date,
  player.go_team_id AS team
 FROM team_player player
   JOIN team_play_grade grade ON grade.team_player_id = player.id
   JOIN team_play_data play ON play.id = grade.team_play_data_id
   JOIN team_event event ON play.team_event_id = event.id
GROUP BY player.id, event.date, play.team_event_id
ORDER BY player.id;


CREATE OR REPLACE VIEW view_game_stat_ids_for_players_with_event_plays AS
SELECT grades.team AS go_team_id,
  grades."teamPlayer" AS team_player_id,
  grades."teamEvent" AS team_event_id,
  grades."numPlays" AS num_plays,
  stats.id AS team_player_game_stat_id,
  grades.season,
  grades.date
 FROM view_team_player_event_grades grades
   LEFT JOIN team_player_game_stat stats ON stats.team_player_id = grades."teamPlayer"
ORDER BY grades."teamPlayer";

CREATE OR REPLACE VIEW view_team_player_event_game_grades AS 
SELECT grades.team AS go_team_id,
  grades."teamPlayer" AS team_player_id,
  grades."teamEvent" AS team_event_id,
  grades.season,
  event.name AS event_name,
  event.is_practice,
  grades."avgCat1Grade" AS avg_cat1_grade,
  grades."avgCat2Grade" AS avg_cat2_grade,
  grades."avgCat3Grade" AS avg_cat3_grade,
  grades."avgOverallGrade" AS avg_overall_grade
 FROM view_team_player_event_grades grades
   JOIN view_team_event event ON event.id = grades."teamEvent"
ORDER BY event.date;

--rollback DROP VIEW IF EXISTS view_game_stat_ids_for_players_with_event_plays; DROP VIEW IF EXISTS view_team_player_event_game_grades; DROP VIEW IF EXISTS view_team_player_event_grades; CREATE VIEW view_team_player_event_grades AS SELECT player.id AS "teamPlayer", player.season, count(grade.id)::integer AS "numPlays", sum(grade.cat_1_grade)::integer AS "sumCat1Grade", sum(grade.cat_2_grade)::integer AS "sumCat2Grade", sum(grade.cat_3_grade)::integer AS "sumCat3Grade", sum(grade.cat_1_grade + grade.cat_2_grade + grade.cat_3_grade)::integer AS "sumOverallGrade", avg(grade.cat_1_grade)::integer AS "avgCat1Grade", avg(grade.cat_2_grade)::integer AS "avgCat2Grade", avg(grade.cat_3_grade)::integer AS "avgCat3Grade", (avg(grade.cat_1_grade + grade.cat_2_grade + grade.cat_3_grade)/3)::integer AS "avgOverallGrade", play.team_event_id AS "teamEvent", event.date, player.go_team_id AS team FROM godeepmobile.team_player player JOIN godeepmobile.team_play_grade grade ON grade.team_player_id = player.id JOIN godeepmobile.team_play_data play ON play.id = grade.team_play_data_id JOIN godeepmobile.team_event event ON play.team_event_id = event.id GROUP BY player.id, event.date, play.team_event_id ORDER BY player.id; CREATE OR REPLACE VIEW view_game_stat_ids_for_players_with_event_plays AS SELECT grades.team AS go_team_id, grades."teamPlayer" AS team_player_id, grades."teamEvent" AS team_event_id, grades."numPlays" AS num_plays, stats.id AS team_player_game_stat_id, grades.season, grades.date FROM view_team_player_event_grades grades LEFT JOIN team_player_game_stat stats ON stats.team_player_id = grades."teamPlayer" ORDER BY grades."teamPlayer"; CREATE OR REPLACE VIEW view_team_player_event_game_grades AS SELECT grades.team AS go_team_id, grades."teamPlayer" AS team_player_id, grades."teamEvent" AS team_event_id, grades.season, event.name AS event_name, event.is_practice, grades."avgCat1Grade" AS avg_cat1_grade, grades."avgCat2Grade" AS avg_cat2_grade, grades."avgCat3Grade" AS avg_cat3_grade,  grades."avgOverallGrade" AS avg_overall_grade FROM view_team_player_event_grades grades JOIN view_team_event event ON event.id = grades."teamEvent" ORDER BY event.date;