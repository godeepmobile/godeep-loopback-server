﻿--liquibase formatted sql

--changeset carlos:86 runOnChange:true splitStatements:false
--comment added gsis_participation_field to team_platoon_configuration table

ALTER TABLE team_platoon_configuration ADD gsis_participation_field VARCHAR(15);

UPDATE team_platoon_configuration SET gsis_participation_field = 'OFF PLAYERS' WHERE go_platoon_type_id = 1;
UPDATE team_platoon_configuration SET gsis_participation_field = 'DEF PLAYERS' WHERE go_platoon_type_id = 2;
UPDATE team_platoon_configuration SET gsis_participation_field = 'PLAYERS' WHERE go_platoon_type_id = 3;
UPDATE team_platoon_configuration SET gsis_participation_field = 'PLAYERS' WHERE go_platoon_type_id = 4;

--rollback ALTER TABLE team_platoon_configuration DROP COLUMN gsis_participation_field;


