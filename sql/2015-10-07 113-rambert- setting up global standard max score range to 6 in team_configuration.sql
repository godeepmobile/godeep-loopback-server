--liquibase formatted sql

--changeset rambert:113 runOnChange:true stripComments:false splitStatements:false
--comment setup global standard max score range to 6 in team_configuration

UPDATE godeepmobile.team_configuration SET scout_eval_score_max = 6;

--rollback UPDATE godeepmobile.team_configuration SET scout_eval_score_max = 9;
