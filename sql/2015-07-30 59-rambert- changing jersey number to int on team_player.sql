--liquibase formatted sql

--changeset rambert:59 runOnChange:true stripComments:false splitStatements:false
--comment change team_player.jersey_number datatype from smallint to integer

-- dropping temporarily view_season_stat_ids_for_players view due to dependency on view_team_player view
DROP VIEW IF EXISTS view_season_stat_ids_for_players;
-- dropping temporarily view_season_stat_ids_for_players view due to dependency on team_player table
DROP VIEW IF EXISTS view_team_player;

-- changing jersey number data type
ALTER TABLE team_player ALTER COLUMN jersey_number TYPE integer using jersey_number::integer;

-- re-creating view_team_player
CREATE OR REPLACE VIEW view_team_player AS
SELECT team_player.id,
    team_player.last_name,
    team_player.middle_name,
    team_player.first_name,
    team_player.go_team_id,
    team_player.data,
    team_player.go_user_team_assignment_id,
    team_player.class_year,
    team_player.birth_date,
    team_player.jersey_number,
    team_player.season,
    team_player.height,
    team_player.weight,
    team_player.graduation_year,
    team_player.wingspan,
    team_player.forty_yard,
    team_player.go_body_type_id,
    team_player.hand_size,
    team_player.vertical_jump,
    team_player.email,
    team_player.sat,
    team_player.act,
    team_player.gpa,
    team_player.go_state_id,
    team_player.team_position_type_id_pos_1,
    team_player.team_position_type_id_pos_2,
    team_player.team_position_type_id_pos_3,
    team_player.team_position_type_id_pos_st,
    team_player.high_school,
    team_player.coach,
    team_player.coach_phone,
    team_player.level,
    team_player.is_veteran,
    team_player.start_date,
    team_player.end_date,
        CASE
            WHEN (team_player.team_position_type_id_pos_st IS NULL) THEN false
            ELSE true
        END AS is_special_teams,
    ( WITH offense AS (
                 SELECT team_position_type.id
                   FROM team_position_type
                  WHERE (team_position_type.go_platoon_type_id = 1)
                )
         SELECT
                CASE
                    WHEN (((team_player.team_position_type_id_pos_1 IN ( SELECT offense.id
                       FROM offense)) OR (team_player.team_position_type_id_pos_2 IN ( SELECT offense.id
                       FROM offense))) OR (team_player.team_position_type_id_pos_3 IN ( SELECT offense.id
                       FROM offense))) THEN true
                    ELSE false
                END AS "case") AS is_offense,
    ( WITH defense AS (
                 SELECT team_position_type.id
                   FROM team_position_type
                  WHERE (team_position_type.go_platoon_type_id = 2)
                )
         SELECT
                CASE
                    WHEN (((team_player.team_position_type_id_pos_1 IN ( SELECT defense.id
                       FROM defense)) OR (team_player.team_position_type_id_pos_2 IN ( SELECT defense.id
                       FROM defense))) OR (team_player.team_position_type_id_pos_3 IN ( SELECT defense.id
                       FROM defense))) THEN true
                    ELSE false
                END AS "case") AS is_defense,
    ( SELECT tpcs.id
           FROM team_player_career_stat tpcs
          WHERE (team_player.id = tpcs.team_player_id)) AS team_player_career_stat,
    ARRAY( SELECT tpss.id
           FROM team_player_season_stat tpss
          WHERE (team_player.id = tpss.team_player_id)
          ORDER BY tpss.season DESC) AS team_player_season_stats
   FROM team_player
  WHERE team_player.end_date IS NULL
  ORDER BY team_player.jersey_number;

-- re-creating view_season_stat_ids_for_players
 CREATE OR REPLACE VIEW view_season_stat_ids_for_players AS
 SELECT player.go_team_id,
    player.id AS team_player_id,
    stats.id AS team_player_season_stat_id,
    player.season,
    player.is_veteran
   FROM view_team_player player
     LEFT JOIN team_player_season_stat stats ON stats.team_player_id = player.id
  ORDER BY player.id;

--rollback DROP VIEW IF EXISTS view_season_stat_ids_for_players; DROP VIEW IF EXISTS view_team_player; ALTER TABLE team_player ALTER COLUMN jersey_number TYPE smallint; CREATE OR REPLACE VIEW view_team_player AS SELECT team_player.id, team_player.last_name, team_player.middle_name, team_player.first_name, team_player.go_team_id, team_player.data, team_player.go_user_team_assignment_id, team_player.class_year, team_player.birth_date, team_player.jersey_number, team_player.season, team_player.height, team_player.weight, team_player.graduation_year, team_player.wingspan, team_player.forty_yard, team_player.go_body_type_id, team_player.hand_size, team_player.vertical_jump, team_player.email, team_player.sat, team_player.act, team_player.gpa, team_player.go_state_id, team_player.team_position_type_id_pos_1, team_player.team_position_type_id_pos_2, team_player.team_position_type_id_pos_3, team_player.team_position_type_id_pos_st, team_player.high_school, team_player.coach, team_player.coach_phone, team_player.level, team_player.is_veteran, team_player.start_date, team_player.end_date, CASE WHEN (team_player.team_position_type_id_pos_st IS NULL) THEN false ELSE true END AS is_special_teams, ( WITH offense AS ( SELECT team_position_type.id FROM team_position_type WHERE (team_position_type.go_platoon_type_id = 1) ) SELECT CASE WHEN (((team_player.team_position_type_id_pos_1 IN ( SELECT offense.id FROM offense)) OR (team_player.team_position_type_id_pos_2 IN ( SELECT offense.id FROM offense))) OR (team_player.team_position_type_id_pos_3 IN ( SELECT offense.id FROM offense))) THEN true ELSE false END AS "case") AS is_offense, ( WITH defense AS (SELECT team_position_type.id FROM team_position_type WHERE (team_position_type.go_platoon_type_id = 2)) SELECT CASE WHEN (((team_player.team_position_type_id_pos_1 IN ( SELECT defense.id FROM defense)) OR (team_player.team_position_type_id_pos_2 IN ( SELECT defense.id FROM defense))) OR (team_player.team_position_type_id_pos_3 IN ( SELECT defense.id FROM defense))) THEN true ELSE false END AS "case") AS is_defense, ( SELECT tpcs.id FROM team_player_career_stat tpcs WHERE (team_player.id = tpcs.team_player_id)) AS team_player_career_stat, ARRAY( SELECT tpss.id FROM team_player_season_stat tpss WHERE (team_player.id = tpss.team_player_id) ORDER BY tpss.season DESC) AS team_player_season_stats FROM team_player WHERE team_player.end_date IS NULL ORDER BY team_player.jersey_number; CREATE OR REPLACE VIEW view_season_stat_ids_for_players AS SELECT player.go_team_id, player.id AS team_player_id, stats.id AS team_player_season_stat_id, player.season, player.is_veteran FROM view_team_player player LEFT JOIN team_player_season_stat stats ON stats.team_player_id = player.id ORDER BY player.id;