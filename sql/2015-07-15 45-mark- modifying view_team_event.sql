--liquibase formatted sql

--changeset mark:44 runOnChange:true stripComments:false splitStatements:false
--comment modify view_grade_report_data view and model
DROP VIEW IF EXISTS view_team_event;
CREATE OR REPLACE VIEW view_team_event AS
 SELECT event.id,
    event.go_team_id,
    event.date,
    event."time",
    event.go_event_type_id,
    event.go_field_condition_id,
    event.go_surface_type_id,
    event.description,
    event.grade_base,
    event.grade_increment,
    event.data,
    event.season,
    event.name,
    event.location,
    event.city,
    event.go_state_id,
    event.is_home_game,
    event.score,
    event.opponent_score,
    event.opponent_organization_id,
    ( SELECT org.name
           FROM go_organization org
          WHERE org.id = event.opponent_organization_id) AS opponent_name,
    ( SELECT org.short_name
           FROM go_organization org
          WHERE org.id = event.opponent_organization_id) AS opponent_short_name,
    ( SELECT org.espn_name
           FROM go_organization org
          WHERE org.id = event.opponent_organization_id) AS opponent_abbreviation,
    ( SELECT org.mascot
           FROM go_organization org
          WHERE org.id = event.opponent_organization_id) AS opponent_mascot,
   ( SELECT team.name
          FROM view_go_team team
         WHERE team.id = event.go_team_id) AS team_name,
   ( SELECT team.short_name
          FROM view_go_team team
         WHERE team.id = event.go_team_id) AS team_short_name,
   ( SELECT team.abbreviation
          FROM view_go_team team
         WHERE team.id = event.go_team_id) AS team_abbreviation,
   ( SELECT team.mascot
          FROM view_go_team team
         WHERE team.id = event.go_team_id) AS team_mascot
   FROM team_event event;

--rollback DROP VIEW IF EXISTS godeepmobile.view_team_event;CREATE OR REPLACE VIEW godeepmobile.view_team_event AS SELECT event.id, event.go_team_id, event.date, event.time, event.go_event_type_id, event.go_field_condition_id, event.go_surface_type_id, event.description, event.go_game_event_id, event.grade_base, event.grade_increment, event.data, event.season, event.name, event.is_home_game, event.score, event.opponent_score, event.location, event.city, event.go_state_id, event.opponent_organization_id, organization.name AS opponent_name, organization.short_name AS opponent_short_name, organization.espn_name AS opponent_abbreviation, organization.mascot AS opponent_mascot FROM godeepmobile.team_event event LEFT OUTER JOIN godeepmobile.go_organization organization ON event.opponent_organization_id = organization.id;
