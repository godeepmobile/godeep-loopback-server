--liquibase formatted sql

--changeset rambert:123 runOnChange:true stripComments:false splitStatements:false
--comment add overall_eval_avg_ranking player_evaluation_stat table

-- add go_position_type_id constraint to go_scouting_eval_criteria
ALTER TABLE godeepmobile.player_evaluation_stat ADD overall_eval_avg_ranking numeric(100, 2);

--rollback ALTER TABLE godeepmobile.player_evaluation_stat DROP COLUMN overall_eval_avg_ranking;
