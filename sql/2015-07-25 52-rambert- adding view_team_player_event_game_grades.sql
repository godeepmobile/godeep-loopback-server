--liquibase formatted sql

--changeset rambert:52 runOnChange:true stripComments:false splitStatements:false
--comment create view_team_player_event_game_grades

DROP VIEW IF EXISTS view_team_player_event_game_grades;
CREATE OR REPLACE VIEW view_team_player_event_game_grades AS 
 SELECT grades.team AS go_team_id,
    grades."teamPlayer" AS team_player_id,
    grades."teamEvent" AS team_event_id,
    grades.season,
    event.name AS event_name,
    event.is_practice,
    grades."avgCat1Grade" AS avg_cat1_grade,
    grades."avgCat2Grade" AS avg_cat2_grade,
    grades."avgCat3Grade" AS avg_cat3_grade,
    grades."avgOverallGrade" AS avg_overall_grade
   FROM view_team_player_event_grades grades
     JOIN view_team_event event ON event.id = grades."teamEvent"
  ORDER BY grades."teamPlayer";

--rollback DROP VIEW view_team_player_event_game_grades;
