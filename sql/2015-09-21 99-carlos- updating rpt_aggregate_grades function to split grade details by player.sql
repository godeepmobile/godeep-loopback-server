﻿--liquibase formatted sql

--changeset carlos:99 runOnChange:true stripComments:false splitStatements:false
--comment updated rpt_aggregate_grades function to split grade details by player

-- Function: rpt_aggregate_grades(text, text, text, text, text, text)

CREATE OR REPLACE FUNCTION rpt_aggregate_grades(
    team_id text,
    report_type text,
    dimension_type text,
    dimension_id text,
    period_type text,
    period_id text)
  RETURNS json AS
$BODY$

-- Season Aggregation
-- aggregated grades per play based on selection of subject group.
-- so if it is "offensive linemen" we would show one line per play, the average of all linemen (same as if it were a single game).
-- each game would be in its own collapsible section within the detail table.
--

-- logic table
----------------------------------------------------------------------------------
-- player   position    platoon   season  event         show plays?   show events?
-- one      .           .         one     (multiple)    yes           yes
-- one      .           .         .       one           yes           yes
-- .        one         .         one     (multiple)    yes           yes
-- .        one         .         .       one           yes           yes
-- .        .           one       one     (multiple)    yes           yes
-- .        .           one       .       one           yes           yes
----------------------------------------------------------------------------------
-- one      .           .         all     (all)         no            yes
-- all      .           .         one     (multiple)    no            no
-- all      .           .         all     (all)         no            no
-- all      .           .         .       one           no            yes
-- .        one         .         all     (all)         no            yes
-- .        all         .         one     (multiple)    no            no
-- .        all         .         all     (all)         no            no
-- .        all         .         .       one           no            yes
-- .        .           one       all     (all)         no            yes
-- .        .           all       one     (multiple)    no            no
-- .        .           all       all     (all)         no            no
-- .        .           all       .       one           no            yes

-- tests
--select * from rpt_aggregate_grades(NULL, 'game-performance', 'player',   '00000000-0000-0000-0000-000000000834', 'season', '2015');
--select * from rpt_aggregate_grades('00000000-0000-0000-0000-000000000006', NULL, 'player',   '00000000-0000-0000-0000-000000000834', 'season', '2015');
--select * from rpt_aggregate_grades('00000000-0000-0000-0000-000000000006', 'game-performance', NULL,   '00000000-0000-0000-0000-000000000834', 'season', '2015');
--select * from rpt_aggregate_grades('00000000-0000-0000-0000-000000000006', 'game-performance', 'player',   NULL, 'season', '2015');
--select * from rpt_aggregate_grades('00000000-0000-0000-0000-000000000006', 'game-performance', 'player',   '00000000-0000-0000-0000-000000000834', NULL, '2015');
--select * from rpt_aggregate_grades('00000000-0000-0000-0000-000000000006', 'game-performance', 'player',   '00000000-0000-0000-0000-000000000834', 'season', NULL);
--select * from rpt_aggregate_grades('00000000-0000-0000-0000-000000000006', 'game-performance', 'player',   '00000000-0000-0000-0000-000000000834', 'event', NULL);

--select * from rpt_aggregate_grades('3b64ab51-f121-4197-ad3c-1dc05bdb4286', 'game-performance', 'player',   NULL, 'season', '2015');
--select * from rpt_aggregate_grades('3b64ab51-f121-4197-ad3c-1dc05bdb4286', 'game-performance', 'platoon',  NULL, 'season', '2015');
--select * from rpt_aggregate_grades('3b64ab51-f121-4197-ad3c-1dc05bdb4286', 'game-performance', 'position', NULL, 'season', '2015');
--select * from rpt_aggregate_grades('3b64ab51-f121-4197-ad3c-1dc05bdb4286', 'game-performance', 'player',   NULL, 'event',  '5020');
--select * from rpt_aggregate_grades('3b64ab51-f121-4197-ad3c-1dc05bdb4286', 'game-performance', 'platoon',  NULL, 'event',  '5020');
--select * from rpt_aggregate_grades('3b64ab51-f121-4197-ad3c-1dc05bdb4286', 'game-performance', 'position', NULL, 'event',  '5020');

--select * from rpt_aggregate_grades('3b64ab51-f121-4197-ad3c-1dc05bdb4286', 'game-performance', 'player',   '7a55bae0-f9d4-4add-af92-e7396c2d032c', 'season', '2015');
--select * from rpt_aggregate_grades('3b64ab51-f121-4197-ad3c-1dc05bdb4286', 'game-performance', 'platoon',  '1',    'season', '2015');
--select * from rpt_aggregate_grades('3b64ab51-f121-4197-ad3c-1dc05bdb4286', 'game-performance', 'position', '1',    'season', '2015');
--select * from rpt_aggregate_grades('3b64ab51-f121-4197-ad3c-1dc05bdb4286', 'game-performance', 'player',   '7a55bae0-f9d4-4add-af92-e7396c2d032c', 'event',  '5020');
--select * from rpt_aggregate_grades('3b64ab51-f121-4197-ad3c-1dc05bdb4286', 'game-performance', 'platoon',  '1',    'event',  '5020');
--select * from rpt_aggregate_grades('3b64ab51-f121-4197-ad3c-1dc05bdb4286', 'game-performance', 'position-group', '1',    'event',  '5020');



DECLARE
        _detail_grades_aggregated_player text         := $$null$$;
        _detail_grades_aggregated_position text       := $$null$$;
        _detail_grades_aggregated_platoon text        := $$null$$;
        _detail_grades_aggregated_play_id text    := $$$$;
        _detail_grades_aggregated_event text          := $$null$$;

        _detail_grades_aggregated_group_by TEXT := $$GROUP BY event.go_team_id $$;
        _detail_grades_aggregated_order_by TEXT;

        _detail_play_grades_select_play_number text := $$$$;
        _detail_play_grades_select_more_fields TEXT := $$$$;

        _position_group_query TEXT;

        -- clauses shared by multiple queries, NOTE(carlos) the first clause is filtering games only
        _common_where text := $$ event.go_team_id = '$$ || team_id || $$'$$;
        _common_group_by_play_number_flag bool := true;
        _common_group_by_event_flag bool := true;

        -- we will calculate detail aggregated grades for selected parameters
        _detail_grades_query TEXT;
        _detail_grades_query_test TEXT;
        _detail_grades_aggregated_play_grades_query TEXT;

        _detail_play_grades_query TEXT;

        _detail_events_query TEXT;

        -- we will calculate summary grades for selected parameters
        _summary_grades_query TEXT;
        _summary_grades_query_json TEXT;
        _summary_grades_query_select TEXT;
        _summary_grades_query_with TEXT := $$ WHERE grade.go_team_id = '$$ || team_id || $$'$$;
        _summary_grades_query_group TEXT := $$$$;
        _summary_group_by_quarter_flag bool := FALSE;
        _overall_grades_query TEXT;

        -- we will create a temp table to hold intermediate results
        _create_tmp_table_query TEXT;
        _tmp_table_count integer;

        _result_query TEXT;
        _result json;
        _result_dimension_value TEXT;

        _individual_player_flag bool := false;
        _grade_aggregations TEXT;
        _aggregated_grade_fields TEXT;

        _report_by_players_where TEXT := $$$$;

BEGIN
    -- report_type will eventually determine what grades/events/plays to report on. for now, we will access all team grades that match dimension/period parameters
    -- for example, red-zone reporting will limit grades to those plays where the abs(play.yard_line) is < 20 and third-down reporting will limit grades to those plays where the play.down = 3
    if team_id IS NULL then
        RAISE EXCEPTION 'Team Id  parameter cannot be NULL' USING HINT = 'Please check your Team parameter';
    end if;

    CASE report_type
        WHEN 'game-performance' THEN
            -- do nothing for now
        ELSE
            RAISE EXCEPTION 'Report type parameter cannot be NULL' USING HINT = 'Please check your Report Type parameter';
    END CASE;

    -- switch on dimension. this will determine which fields to include in output, how to group output for aggregation
    CASE dimension_type
        WHEN 'player' THEN
            -- aggregating by player
            if dimension_id IS NOT NULL then
                -- filter on one player
                _common_where = _common_where || $$ AND grade.team_player_id = '$$ || dimension_id || $$'$$;
                _summary_grades_query_with = _summary_grades_query_with || $$ AND team_player_id = '$$ || dimension_id || $$'$$;
                _individual_player_flag = true;
                --_result_dimension_value = $$'$$ || dimension_id || $$'$$;
                _result_dimension_value = $$ (select concat(last_name, ', ', first_name) from team_player where id = '$$ || dimension_id || $$') $$;
            else
                -- all players - don't filter
                _common_group_by_play_number_flag = false;
                _common_group_by_event_flag = false;
            end if;
            _detail_grades_aggregated_player = $$ grade.team_player_id$$;
            _detail_grades_aggregated_group_by = _detail_grades_aggregated_group_by || $$, grade.team_player_id$$;
            _detail_grades_aggregated_order_by = $$ grade.team_player_id$$;
            _summary_grades_query_select = $$ grade.team_player_id AS player $$;
            _summary_grades_query_group = $$ , grade.team_player_id $$;

        WHEN 'platoon' THEN
            -- aggregating by platoon
            if dimension_id IS NOT NULL then
                -- filter on one platoon
                _common_where = _common_where || $$ AND grade.go_platoon_type_id = $$ || dimension_id;
                _summary_grades_query_with = _summary_grades_query_with || $$ AND go_platoon_type_id = $$ || dimension_id;
                _result_dimension_value = $$ (select name from go_platoon_type where id = $$ || dimension_id || $$) $$;
            else
                -- all platoons - don't filter
                _common_group_by_play_number_flag = false;
                _common_group_by_event_flag = false;
            end if;
            _detail_grades_aggregated_platoon = $$ grade.go_platoon_type_id$$;
            _detail_grades_aggregated_group_by = _detail_grades_aggregated_group_by || $$, grade.go_platoon_type_id$$;
            _detail_grades_aggregated_order_by = $$ grade.go_platoon_type_id$$;
            _summary_grades_query_select = $$ grade.go_platoon_type_id AS platoon $$;
            _summary_grades_query_group = $$ , grade.go_platoon_type_id $$;
        WHEN 'position-group' THEN
            -- aggregating by position group
             _position_group_query = $$
                   SELECT
                        tpt.go_position_type_id
                    FROM
                        team_position_type tpt JOIN team_position_group tpg on tpt.team_position_group_id = tpg.id
                    WHERE
                        tpt.end_date IS NULL AND tpg.id = $$ || dimension_id || $$ AND tpt.go_team_id = '$$ || team_id || $$'$$;

            if dimension_id IS NOT NULL then
                -- filter on one position type
                _common_where = _common_where || $$ AND grade.position_played in ($$ || _position_group_query || $$) $$;
                _summary_grades_query_with = _summary_grades_query_with || $$ AND position_played in ($$ || _position_group_query || $$) $$;
               _result_dimension_value = $$ (select name from team_position_group where id = $$ || dimension_id || $$) $$;
             else
                -- all position types - don't filter
                _common_group_by_play_number_flag = false;
                _common_group_by_event_flag = false;
            end if;
            _detail_grades_aggregated_position = $$ grade.position_played$$;
            --_detail_grades_aggregated_group_by = _detail_grades_aggregated_group_by || $$, grade.position_played$$;
            _detail_grades_aggregated_order_by = $$ play.id$$;
            --_summary_grades_query_select = $$
            --(select short_name from go_position_type where id = grade.position_played) AS "positionType"
            --    $$;
            --_summary_grades_query_group = $$ , grade.position_played $$;
            _summary_grades_query_select = $$ event.date $$;
            _summary_grades_query_group = $$ $$;

        ELSE
            -- unknown aggregation, probably an error
            RAISE EXCEPTION 'Dimension parameter % is not valid', dimension_type USING HINT = 'Please check your Dimension parameter';
    END CASE;
    --RAISE NOTICE 'finished case dimension, dimension_type =%, dimension_id = %, _common_where = %', dimension_type, dimension_id, _common_where ;

    -- switch on period. this will determine which fields to include in output, how to group output for aggregation
    CASE period_type
        WHEN 'season' THEN
            -- aggregating across seasons
            if period_id IS NOT NULL then
                -- filter on one season
                _common_where = _common_where || $$ AND (SELECT is_practice FROM view_team_event WHERE id = event.id) = false AND season = $$ || period_id;
                _summary_grades_query_with = _summary_grades_query_with  || $$ AND season = $$ || period_id;
            else
                RAISE EXCEPTION 'Season Id parameter % is not valid', period_id USING HINT = 'Please check your Season Id parameter';
            end if;
            _detail_grades_aggregated_group_by = _detail_grades_aggregated_group_by || $$, event.season $$;
        WHEN 'event' THEN
            -- aggregating across events
            if period_id IS NOT NULL then
                -- filter on one event
                _common_where = _common_where || $$ AND team_event_id = $$ || period_id;
                _summary_grades_query_with = _summary_grades_query_with  || $$ AND event.id = $$ || period_id;
                _summary_group_by_quarter_flag = true;
                _common_group_by_event_flag = true;
            else
                RAISE EXCEPTION 'Event Id parameter % is not valid', period_id USING HINT = 'Please check your Event Id parameter';
            end if;
        ELSE
            RAISE EXCEPTION 'Period parameter % is not valid', period_type USING HINT = 'Please check your Period parameter';
    END CASE;
    --RAISE NOTICE 'finished case period, where_clauses = %, period_where = %, period_id = %, group_by = %', where_clauses, period_where, period_id, group_by;

    if _common_group_by_event_flag then
        -- grouping by events means we need to update group by, order by, and event select substitution
        _detail_grades_aggregated_group_by = _detail_grades_aggregated_group_by || $$, event.id$$;
        _detail_grades_aggregated_event = $$event.id$$;
        _detail_grades_aggregated_order_by = _detail_grades_aggregated_order_by || $$, event.id$$;
    end if;

    if _common_group_by_play_number_flag then
        _detail_grades_aggregated_play_id = $$ play.id::INTEGER AS "playId", $$;
        _detail_grades_aggregated_group_by = _detail_grades_aggregated_group_by || $$, play.id$$;
        _detail_grades_aggregated_order_by = _detail_grades_aggregated_order_by || $$, play.play_number$$;

        _detail_play_grades_select_play_number  = $$aggregated_play_data."playId",$$;
        _detail_play_grades_select_more_fields  = $$individual_play_data.quarter,
                                 individual_play_data.play_number as "playNumber",
                                 individual_play_data.game_down AS "gameDown",
                                 individual_play_data.game_distance as "gameDistance",
                                 individual_play_data.game_possession as "gamePossession",
                                 --(select short_name from go_position_type where id = "positionType") as "positionType",
                                 (select "name" from go_play_type where id = individual_play_data.go_play_type_id) as "playType",
                                 (SELECT "name" from go_play_result_type where id = individual_play_data.go_play_result_type_id) as "playResult",
                                 individual_play_data.yard_line as "yardLine",
                                 $$;
    ELSE
        _detail_grades_aggregated_play_id = $$ NULL::integer AS "playId", $$;
        _detail_play_grades_select_play_number  = $$aggregated_play_data."playId",$$;
    end if;

    IF _summary_group_by_quarter_flag then
        _summary_grades_query_select = _summary_grades_query_select || $$, play.quarter,
        case play.quarter when 1 then '1st Qtr' when 2 then '2nd Qtr' when 3 then '3rd Qtr' when 4 then '4th Qtr' else 'None' end AS series$$;
        _summary_grades_query_group = _summary_grades_query_group || $$, play.quarter$$;
    else
        _summary_grades_query_select = _summary_grades_query_select || $$, concat('Game ', row_number() over ()) AS series$$;
    END if;

        if _individual_player_flag then
                _detail_grades_aggregated_group_by = $$$$;
                _grade_aggregations = $$
                       (select name from go_play_factor where id = grade.go_play_factor_id) AS "factor",
                        grade.go_play_factor_id as "factorId",
                        grade.poa as "poa",
                        grade.hard as "hard",
                        grade.notes as "notes",
                        grade.players_can_see_notes as "playersCanSeeNotes",
                        (select short_name from go_position_type where id = grade.position_played) AS "positionPlayed",
                        1::integer AS "numPlays",
                        grade.cat_1_grade::numeric AS "cat1Grade",
                        grade.cat_2_grade::numeric AS "cat2Grade",
                        grade.cat_3_grade::numeric AS "cat3Grade",
                        (grade.cat_1_grade + grade.cat_2_grade + grade.cat_3_grade)::integer AS "sumOverallGrade",
                        round((grade.cat_1_grade + grade.cat_2_grade + grade.cat_3_grade) / 3::numeric, 2) AS "overallGrade"
                $$;
                _aggregated_grade_fields = $$
                        aggregated_play_data."factor",
                        aggregated_play_data."factorId",
                        aggregated_play_data."poa",
                        aggregated_play_data."hard",
                        aggregated_play_data."notes",
                        aggregated_play_data."playersCanSeeNotes",
                        aggregated_play_data."positionPlayed",
                        aggregated_play_data."numPlays",
                        aggregated_play_data."cat1Grade",
                        aggregated_play_data."cat2Grade",
                        aggregated_play_data."cat3Grade",
                        aggregated_play_data."sumOverallGrade",
                        aggregated_play_data."overallGrade"
                $$;

        else
                _grade_aggregations = $$
                        count(grade.id)::integer AS "numPlays",
                        sum(grade.cat_1_grade + grade.cat_2_grade + grade.cat_3_grade)::integer AS "sumOverallGrade",
                        round(avg(grade.cat_1_grade)::numeric, 2) AS "cat1Grade",
                        round(avg(grade.cat_2_grade)::numeric, 2) AS "cat2Grade",
                        round(avg(grade.cat_3_grade)::numeric, 2) AS "cat3Grade",
                        round((avg(grade.cat_1_grade + grade.cat_2_grade + grade.cat_3_grade) / 3::numeric)::numeric, 2) AS "overallGrade"
                $$;
                _aggregated_grade_fields = $$
                        aggregated_play_data."numPlays",
                        aggregated_play_data."cat1Grade",
                        aggregated_play_data."cat2Grade",
                        aggregated_play_data."cat3Grade",
                        aggregated_play_data."sumOverallGrade",
                        aggregated_play_data."overallGrade"
                $$;
        end if;




    --RAISE NOTICE '_common_where = %', _common_where;

    --RAISE NOTICE 'group_by = %', _detail_grades_aggregated_group_by;
    --RAISE NOTICE '_common_group_by_event_flag = %', _common_group_by_event_flag;
    --RAISE NOTICE '_common_group_by_play_number_flag = %', _common_group_by_play_number_flag;

    --RAISE NOTICE 'play_number = %', _detail_grades_aggregated_play_id;

    _create_tmp_table_query = $$
                DROP TABLE IF EXISTS tmp_grade_ids;
        CREATE TEMP TABLE tmp_grade_ids ON COMMIT DROP AS
            SELECT grade.id
            FROM
                team_play_grade grade
                JOIN team_play_data play ON play.id = grade.team_play_data_id
                JOIN team_event event ON play.team_event_id = event.id
            WHERE
                $$ || _common_where || $$;
        CREATE INDEX tmp_grade_index_id ON tmp_grade_ids(id);
        ANALYZE tmp_grade_ids;
        $$;

    EXECUTE _create_tmp_table_query;
    --EXECUTE $$ select count(*) from tmp_grade_ids $$ into _tmp_table_count ;
    --raise NOTICE 'tmp_table_count is %', _tmp_table_count;
	
    _summary_grades_query = $$
        SELECT
            event.id,
            event.name,
            $$ || _summary_grades_query_select || $$,
            count(grade.id)::integer AS "numPlays",
            round(avg(grade.cat_1_grade)::NUMERIC, 2) AS "cat1Grade",
            round(avg(grade.cat_2_grade)::NUMERIC, 2) AS "cat2Grade",
            round(avg(grade.cat_3_grade)::NUMERIC, 2) AS "cat3Grade",
            round((avg(grade.cat_1_grade + grade.cat_2_grade + grade.cat_3_grade) / 3::numeric)::numeric, 2) AS "overallGrade"
        FROM
            team_play_grade grade
        JOIN team_play_data play ON play.id = grade.team_play_data_id
            JOIN team_event event ON play.team_event_id = event.id,
            tmp_grade_ids
        $$ || _summary_grades_query_with || $$ AND tmp_grade_ids.id = grade.id
        GROUP BY event.id
        $$ || _summary_grades_query_group || $$
        ORDER BY event.id
        $$ || _summary_grades_query_group || $$

        $$;

    _summary_grades_query_json = $$ (select array_to_json(array_agg(row_to_json(t))) from (
        $$ || _summary_grades_query || $$
    ) t )
        $$;

    --RAISE NOTICE '_summary_grades_query = %', _summary_grades_query;

    _overall_grades_query = $$ (select array_to_json(array_agg(row_to_json(z))) from (
        SELECT
	    round(avg("cat1Grade")::NUMERIC, 2) as "cat1Grade",
	    round(avg("cat2Grade")::NUMERIC, 2) as "cat2Grade",
	    round(avg("cat3Grade")::NUMERIC, 2) as "cat3Grade",
	    round(avg("overallGrade")::NUMERIC, 2) as "overallGrade"
        FROM
		( $$ || _summary_grades_query || $$ ) X 
    ) z )
	$$;

    -- this is the actual query to aggregate the grades based on teh parameters
    _detail_grades_aggregated_play_grades_query =  $$ (
        SELECT
            event.season AS "season",
            $$ || _detail_grades_aggregated_event       || $$::integer  AS "teamEvent",
            $$ || _detail_grades_aggregated_play_id || $$
            $$ || _grade_aggregations || $$
        FROM
            team_play_grade grade
            JOIN team_play_data play ON play.id = grade.team_play_data_id
            JOIN team_event event ON play.team_event_id = event.id,
            tmp_grade_ids
        WHERE
            $$ || _common_where || $$ AND grade.id = tmp_grade_ids.id
            $$ || _detail_grades_aggregated_group_by || $$
        ORDER BY
            $$ || _detail_grades_aggregated_order_by || $$
    ) $$;
    --RAISE NOTICE '_detail_grades_aggregated_play_grades_query = %', _detail_grades_aggregated_play_grades_query;

    -- this turns the grades into a JSON object, and only returns the fields specified below
    -- requires WITH play_data
    _detail_play_grades_query = $$
        json_agg(row_to_json(( SELECT y FROM (
            SELECT
                $$ || _detail_play_grades_select_play_number || $$
                $$ || _detail_play_grades_select_more_fields || $$
                $$ || _aggregated_grade_fields || $$
        ) y )))
    $$;

    -- return Events as JSON
    -- requires WITH event_data
    IF not _individual_player_flag AND _summary_group_by_quarter_flag THEN

	CASE dimension_type
		WHEN 'platoon' THEN _report_by_players_where = $$ AND grade.go_platoon_type_id = $$ || dimension_id;
		WHEN 'position-group' THEN _report_by_players_where = $$ AND grade.position_played in ($$ || _position_group_query || $$) $$;
	END CASE;
    
	_detail_events_query = $$
		json_agg(row_to_json(( SELECT x FROM (
		    SELECT
			event_data.id,
			event_data.date,
			event_data.name,
			(select array_to_json(array_agg(row_to_json(p))) from (
			    SELECT
				team_player.first_name AS "firstName",
				team_player.last_name AS "lastName",
				team_player.jersey_number AS "jerseyNumber",
				(select short_name from go_position_type where id = team_player.team_position_type_id_pos_1) AS "position",
				(select row_to_json(o) from (
					SELECT
					    count(grade.id)::integer AS "numPlays",
					    round(avg(grade.cat_1_grade)::NUMERIC, 2) AS "cat1Grade",
					    round(avg(grade.cat_2_grade)::NUMERIC, 2) AS "cat2Grade",
					    round(avg(grade.cat_3_grade)::NUMERIC, 2) AS "cat3Grade",
					    round((avg(grade.cat_1_grade + grade.cat_2_grade + grade.cat_3_grade) / 3::numeric)::numeric, 2) AS "overallGrade"
					FROM team_play_grade grade, team_play_data pl 
						WHERE grade.team_player_id = team_player.id AND grade.team_play_data_id = pl.id AND pl.team_event_id = $$ || period_id || $$
						$$ || _report_by_players_where || $$
				) o) AS "gradesSummary",
				(select array_to_json(array_agg(row_to_json(g))) from (
					SELECT 
						(select name from go_play_factor where id = grade.go_play_factor_id) AS "factor",
						grade.go_play_factor_id as "factorId",
						grade.poa as "poa",
						grade.hard as "hard",
						grade.notes as "notes",
						grade.players_can_see_notes as "playersCanSeeNotes",
						(select short_name from go_position_type where id = grade.position_played) AS "positionPlayed",
						1::integer AS "numPlays",
						grade.cat_1_grade::numeric AS "cat1Grade",
						grade.cat_2_grade::numeric AS "cat2Grade",
						grade.cat_3_grade::numeric AS "cat3Grade",
						(grade.cat_1_grade + grade.cat_2_grade + grade.cat_3_grade)::integer AS "sumOverallGrade",
						round((grade.cat_1_grade + grade.cat_2_grade + grade.cat_3_grade) / 3::numeric, 2) AS "overallGrade",
						pl.quarter,
						pl.play_number as "playNumber",
						pl.game_down AS "gameDown",
						pl.game_distance as "gameDistance",
						pl.game_possession as "gamePossession",
						(select "name" from go_play_type where id = pl.go_play_type_id) as "playType",
						(SELECT "name" from go_play_result_type where id = pl.go_play_result_type_id) as "playResult",
						pl.yard_line as "yardLine"
					FROM team_play_grade grade, team_play_data pl 
					WHERE grade.team_player_id = team_player.id AND
						grade.team_play_data_id = pl.id AND pl.team_event_id = $$ || period_id || $$
						$$ || _report_by_players_where || $$
					ORDER BY "playNumber") g) AS "grades"
			    FROM 
				team_player 
			    WHERE 
				go_team_id = '$$ || team_id || $$' AND
				id IN (
					SELECT team_player_id 
					FROM team_play_grade grade, team_play_data pl 
					WHERE grade.team_play_data_id = pl.id AND pl.team_event_id = $$ || period_id || $$
					$$ || _report_by_players_where || $$)
			) p) AS "teamPlayers",
			$$ || _detail_play_grades_query || $$ as grades
		    FROM
			aggregated_play_data, individual_play_data
		    where
			aggregated_play_data."teamEvent" = event_data.id AND
			aggregated_play_data."playId" = individual_play_data.id
		) x )))
	$$;
    ELSE
	_detail_events_query = $$
		json_agg(row_to_json(( SELECT x FROM (
		    SELECT
			event_data.id,
			event_data.date,
			event_data.name,
			$$ || _detail_play_grades_query || $$ as grades
		    FROM
			aggregated_play_data, individual_play_data
		    where
			aggregated_play_data."teamEvent" = event_data.id AND
			aggregated_play_data."playId" = individual_play_data.id
		) x )))
	$$;
    END IF;

    -- this turns the defined season query into a JSON object
    _detail_grades_query = $$ json_agg(row_to_json(( SELECT x FROM (
        WITH
            aggregated_play_data as $$ || _detail_grades_aggregated_play_grades_query || $$,
            individual_play_data AS (SELECT * from team_play_data WHERE go_team_id = '$$ || team_id || $$' order by play_number),
            event_data as (SELECT * FROM team_event where go_team_id = '$$ || team_id || $$' )
        SELECT
            event_data.season,
            $$ || _detail_events_query || $$ as events
        FROM
            event_data
        where
            event_data.season in (select distinct season from aggregated_play_data) AND -- only include seasons with actual play/grade data
            event_data.id in (select distinct "teamEvent" from aggregated_play_data)
        GROUP BY
            event_data.season
    ) x )))
    $$;
    --RAISE NOTICE '_detail_grades_query = %', _detail_grades_query;

    --_detail_grades_query_test = $$ select $$||_detail_grades_query;
    --EXECUTE _detail_grades_query_test into _result;
    --RAISE NOTICE '_detail_grades_query_test =%', _result;

    _result_query = $$ select
        row_to_json(( SELECT x FROM ( SELECT
           '$$ || report_type                        || $$' AS "reportType",
           '$$ || dimension_id                       || $$' AS "$$ || dimension_type || $$",
            $$ || _result_dimension_value            || $$ AS "groupByValue",
           '$$ || dimension_type                     || $$' AS "groupByName",
            $$ || period_id                          || $$ AS "$$ || period_type || $$",
            $$ || _common_group_by_play_number_flag  || $$ as "groupByPlayNumber",
            $$ || _common_group_by_event_flag        || $$ as "groupByEvent",
            $$ || _individual_player_flag            || $$ as "hasPlayDetails",
            $$ || _detail_grades_query               || $$ as "detailGrades",
            $$ || _summary_grades_query_json         || $$ as "summaryGrades",
            $$ || _overall_grades_query              || $$ as "overallGrades"
        ) x ))
    $$;
    --RAISE NOTICE '_result_query = %', _result_query;
    EXECUTE _result_query INTO _result;
    --RAISE NOTICE '_result = %', _result;

    return _result;
    -- execute the query and return the result
--  RETURN QUERY EXECUTE _result_query USING _detail_grades_aggregated_player, _detail_grades_aggregated_platoon, _detail_grades_aggregated_position, _common_where, _detail_grades_aggregated_group_by, _detail_grades_aggregated_order_by, _detail_grades_aggregated_event, _detail_grades_aggregated_play_id;
END
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

--rollback  DROP FUNCTION IF EXISTS rpt_aggregate_grades(text, text, text, text, text, integer);