--liquibase formatted sql

--changeset rambert:103 runOnChange:true stripComments:false splitStatements:false
--comment create team_scouting_eval_criteria table

CREATE TABLE godeepmobile.team_scouting_eval_criteria (
  id                                    serial NOT NULL,
  name                                  varchar(100)  NOT NULL,
  weight                                smallint NOT NULL,
  go_team_id                            uuid NOT NULL,
  go_position_type_id                   integer NOT NULL,
  go_scouting_eval_criteria_type_id     integer  NOT NULL,
  CONSTRAINT pk_team_scouting_eval_criteria PRIMARY KEY ( id ),
  CONSTRAINT fk_team_scouting_eval_criteria_go_team FOREIGN KEY (go_team_id)
      REFERENCES go_team (id),
  CONSTRAINT fk_team_scouting_eval_criteria_go_position_type FOREIGN KEY (go_position_type_id)
      REFERENCES go_position_type (id),
  CONSTRAINT fk_team_scouting_eval_criteria_go_scouting_eval_criteria_type FOREIGN KEY (go_scouting_eval_criteria_type_id)
      REFERENCES go_scouting_eval_criteria_type (id)
);

INSERT INTO godeepmobile.team_scouting_eval_criteria
             (name, weight, go_team_id, go_position_type_id, go_scouting_eval_criteria_type_id)
  SELECT  go_eval_criteria.name, go_eval_criteria.weight, team.id, pos.id, go_eval_criteria.go_scouting_eval_criteria_type_id
  FROM go_position_type pos, go_team team, go_scouting_eval_criteria go_eval_criteria
  ORDER BY team.id, pos.id, go_eval_criteria.go_scouting_eval_criteria_type_id, go_eval_criteria.id;

--rollback DROP TABLE IF EXISTS  godeepmobile.team_scouting_eval_criteria; DELETE FROM godeepmobile.team_scouting_eval_criteria;
