--liquibase formatted sql

--changeset rambert:138 runOnChange:true splitStatements:false stripComments:false
--comment create player_evaluation_season_ranking function

CREATE OR REPLACE FUNCTION godeepmobile.refresh_player_evaluation_career_ranking(
  team_id_parm uuid)
  RETURNS void AS
$BODY$
DECLARE
  career_record RECORD;
  career_record_exists integer;
BEGIN

  -- gets player score overall using using values that have been calculated with multipliers and position importance
  FOR career_record IN SELECT subquery.go_team_id,
      subquery.team_player_id,
      ROUND(subquery.target_match, 2) AS target_match,
      ROUND(subquery.major_factors_eval_avg, 2) AS major_factors_eval_avg,
      ROUND(subquery.critical_factors_eval_avg, 2) AS critical_factors_eval_avg,
      ROUND(subquery.position_skills_eval_avg, 2) AS position_skills_eval_avg,
      ROUND(subquery.overall_eval_avg, 2) AS overall_eval_avg,
      ROUND(subquery.overall_eval_avg_rank, 2) AS overall_eval_avg_rank,
      RANK() OVER (ORDER BY subquery.overall_eval_avg_rank DESC, subquery.target_match DESC) AS rank_overall
    FROM (
      SELECT
      stat.go_team_id,
      stat.team_player_id,
      AVG(stat.target_match) AS target_match,
      AVG(stat.major_factors_eval_avg) AS major_factors_eval_avg,
      AVG(stat.critical_factors_eval_avg) AS critical_factors_eval_avg,
      AVG(stat.position_skills_eval_avg) AS position_skills_eval_avg,
      AVG(stat.overall_eval_avg) AS overall_eval_avg,
      AVG(stat.overall_eval_avg_rank) AS overall_eval_avg_rank
      FROM godeepmobile.player_evaluation_season_stat stat,
           godeepmobile.team_scouting_eval_score_group score_group,
           godeepmobile.view_team_player player
      WHERE stat.go_team_id = team_id_parm::uuid
            AND player.id = stat.team_player_id
       GROUP BY stat.go_team_id, stat.season, stat.team_player_id, player.height, player.last_name
    ) AS subquery
  LOOP
    -- check if record already exists
    SELECT COUNT(id) INTO career_record_exists
    FROM godeepmobile.player_evaluation_career_stat
    WHERE go_team_id = team_id_parm::uuid
      AND team_player_id = career_record.team_player_id;

    IF career_record_exists = 0 THEN
      INSERT INTO godeepmobile.player_evaluation_career_stat (
        go_team_id,
        team_player_id,
        target_match,
        major_factors_eval_avg,
        critical_factors_eval_avg,
        position_skills_eval_avg,
        overall_eval_avg,
        overall_eval_avg_rank,
        rank_overall
      )
      VALUES (
        team_id_parm::uuid,
        career_record.team_player_id,
        career_record.target_match,
        career_record.major_factors_eval_avg,
        career_record.critical_factors_eval_avg,
        career_record.position_skills_eval_avg,
        career_record.overall_eval_avg,
        career_record.overall_eval_avg_rank,
        career_record.rank_overall
      );
    ELSE
      UPDATE
          godeepmobile.player_evaluation_season_stat
      SET
        target_match = career_record.target_match,
        major_factors_eval_avg = career_record.major_factors_eval_avg,
        critical_factors_eval_avg = career_record.critical_factors_eval_avg,
        position_skills_eval_avg = career_record.position_skills_eval_avg,
        overall_eval_avg = career_record.overall_eval_avg,
        overall_eval_avg_rank = career_record.overall_eval_avg_rank,
        rank_overall = career_record.rank_overall
      WHERE go_team_id = team_id_parm::uuid
        AND team_player_id = career_record.team_player_id;
    END IF;
  END LOOP;

END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

--rollback DROP FUNCTION godeepmobile.refresh_player_evaluation_career_ranking(uuid);
