--liquibase formatted sql

--changeset mark:26 runOnChange:true stripComments:false splitStatements:false
--comment add view_platoon_event_grades and view_position_group_event_grades
drop view if exists view_platoon_event_grades;
CREATE OR REPLACE VIEW view_platoon_event_grades AS
  SELECT event.go_team_id AS team,
    event.season,
    grade.go_platoon_type_id AS "platoonType",
    event.date,
    event.id AS "teamEvent",
    count(grade.id)::integer AS "numPlays",
    sum(grade.cat_1_grade)::integer AS "sumCat1Grade",
    sum(grade.cat_2_grade)::integer AS "sumCat2Grade",
    sum(grade.cat_3_grade)::integer AS "sumCat3Grade",
    sum(grade.cat_1_grade + grade.cat_2_grade + grade.cat_3_grade)::integer AS "sumOverallGrade",
    avg(grade.cat_1_grade)::integer AS "avgCat1Grade",
    avg(grade.cat_2_grade)::integer AS "avgCat2Grade",
    avg(grade.cat_3_grade)::integer AS "avgCat3Grade",
    (avg(grade.cat_1_grade + grade.cat_2_grade + grade.cat_3_grade) / 3::numeric)::integer AS "avgOverallGrade"
   FROM team_play_grade grade
     JOIN team_play_data play ON play.id = grade.team_play_data_id
     JOIN team_event event ON play.team_event_id = event.id
  GROUP BY event.id, grade.go_platoon_type_id
  ORDER BY event.id, grade.go_platoon_type_id;

drop view if exists view_platoon_event_grades_by_quarter;
CREATE OR REPLACE VIEW view_platoon_event_grades_by_quarter AS
  SELECT
      concat(event.id, '-', grade.go_platoon_type_id, '-', play.quarter) AS id,
      event.go_team_id,
      event.season,
      grade.go_platoon_type_id,
      event.date,
      event.id AS team_event_id,
      play.quarter,
      count(grade.id) AS num_plays,
      sum(grade.cat_1_grade)::integer AS sum_cat1_grade,
      sum(grade.cat_2_grade)::integer AS sum_cat2_grade,
      sum(grade.cat_3_grade)::integer AS sum_cat3_grade,
      sum(grade.cat_1_grade + grade.cat_2_grade + grade.cat_3_grade)::integer AS sum_overall_grade,
      avg(grade.cat_1_grade)::integer AS avg_cat1_grade,
      avg(grade.cat_2_grade)::integer AS avg_cat2_grade,
      avg(grade.cat_3_grade)::integer AS avg_cat3_grade,
      (avg(grade.cat_1_grade + grade.cat_2_grade + grade.cat_3_grade) / 3::numeric)::integer AS avg_overall_grade
     FROM team_play_grade grade
       JOIN team_play_data play ON play.id = grade.team_play_data_id
       JOIN team_event event ON play.team_event_id = event.id
    GROUP BY event.id, grade.go_platoon_type_id, play.quarter
    ORDER BY event.id, grade.go_platoon_type_id, play.quarter;

drop view if exists view_position_group_event_grades;
CREATE OR REPLACE VIEW view_position_group_event_grades AS
   SELECT event.go_team_id AS team,
      event.season,
      grade.team_position_group_id AS "teamPositionGroup",
      event.date,
      event.id AS "teamEvent",
      count(grade.id)::integer AS "numPlays",
      sum(grade.cat_1_grade)::integer AS "sumCat1Grade",
      sum(grade.cat_2_grade)::integer AS "sumCat2Grade",
      sum(grade.cat_3_grade)::integer AS "sumCat3Grade",
      sum(grade.cat_1_grade + grade.cat_2_grade + grade.cat_3_grade)::integer AS "sumOverallGrade",
      avg(grade.cat_1_grade)::integer AS "avgCat1Grade",
      avg(grade.cat_2_grade)::integer AS "avgCat2Grade",
      avg(grade.cat_3_grade)::integer AS "avgCat3Grade",
      (avg(grade.cat_1_grade + grade.cat_2_grade + grade.cat_3_grade) / 3::numeric)::integer AS "avgOverallGrade"
     FROM team_play_grade grade
       JOIN team_play_data play ON play.id = grade.team_play_data_id
       JOIN team_event event ON play.team_event_id = event.id
    GROUP BY event.id, grade.team_position_group_id
    ORDER BY event.id, grade.team_position_group_id;

drop view if exists view_position_group_event_grades_by_quarter;
CREATE OR REPLACE VIEW view_position_group_event_grades_by_quarter AS
  SELECT
        concat(grade.team_position_group_id, '-', event.id, '-', play.quarter) AS id,
        event.go_team_id,
        event.season,
        grade.team_position_group_id,
        event.date,
        event.id AS team_event_id,

        play.quarter,
        count(grade.id) AS num_plays,
        sum(grade.cat_1_grade)::integer AS sum_cat1_grade,
        sum(grade.cat_2_grade)::integer AS sum_cat2_grade,
        sum(grade.cat_3_grade)::integer AS sum_cat3_grade,
        sum(grade.cat_1_grade + grade.cat_2_grade + grade.cat_3_grade)::integer AS sum_overall_grade,
        avg(grade.cat_1_grade)::integer AS avg_cat1_grade,
        avg(grade.cat_2_grade)::integer AS avg_cat2_grade,
        avg(grade.cat_3_grade)::integer AS avg_cat3_grade,
        (avg(grade.cat_1_grade + grade.cat_2_grade + grade.cat_3_grade) / 3::numeric)::integer AS avg_overall_grade

    FROM team_play_grade grade
         JOIN team_play_data play ON play.id = grade.team_play_data_id
         JOIN team_event event ON play.team_event_id = event.id
      GROUP BY event.id, grade.team_position_group_id, play.quarter
      ORDER BY event.id, grade.team_position_group_id, play.quarter;

--rollback DROP VIEW view_platoon_event_grades;
--rollback DROP VIEW view_platoon_event_grades_by_quarter;
--rollback DROP VIEW view_position_group_event_grades_by_quarter;
--rollback DROP VIEW view_position_group_event_grades;
