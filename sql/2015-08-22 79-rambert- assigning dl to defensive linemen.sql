--liquibase formatted sql

--changeset rambert:79 runOnChange:true stripComments:false splitStatements:false
--comment change global configuration table that assigned DL to Linebackers - assign to Defensive Linemen

UPDATE godeepmobile.go_position_type_to_group_assignment
SET go_position_group_id = 8
WHERE go_position_type_id = 36 AND go_position_group_id = 7;

--rollback UPDATE godeepmobile.go_position_type_to_group_assignment SET go_position_group_id = 7 WHERE go_position_type_id = 36 AND go_position_group_id = 8;



