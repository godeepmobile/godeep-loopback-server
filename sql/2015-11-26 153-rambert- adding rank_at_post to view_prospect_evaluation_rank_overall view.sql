--liquibase formatted sql

--changeset rambert:153 runOnChange:true stripComments:false splitStatements:false
--comment create view_prospect_evaluation_rank_overall view

DROP VIEW IF EXISTS view_prospect_evaluation_season_stat;

CREATE OR REPLACE VIEW view_prospect_evaluation_season_stat AS
  SELECT prospect.id,
    prospect.go_team_id,
    prospect.id AS team_prospect_id,
    prospect.first_name,
    prospect.last_name,
    evaluation_season_stat.season,
    evaluation_season_stat.num_evaluations,
    evaluation_season_stat.target_match,
    evaluation_season_stat.major_factors_eval_avg,
    evaluation_season_stat.critical_factors_eval_avg,
    evaluation_season_stat.position_skills_eval_avg,
    evaluation_season_stat.overall_eval_avg,
    evaluation_season_stat.overall_eval_avg_rank,
    evaluation_season_stat.rank_overall,
    (SELECT prospect_season_stat.rank_at_post
     FROM  view_prospect_rank_overall_at_position_season_stat prospect_season_stat
     WHERE prospect_season_stat.go_team_id = prospect.go_team_id
       AND prospect_season_stat.team_prospect_id = prospect.id
       AND prospect_season_stat.season = evaluation_season_stat.season
       AND prospect_season_stat.go_position_type_id = prospect.team_position_type_id_pos_1
     ) AS rank_at_post
   FROM player_evaluation_season_stat evaluation_season_stat
     RIGHT JOIN view_team_prospect prospect ON evaluation_season_stat.team_player_id = prospect.id;

--rollback DROP VIEW view_prospect_evaluation_season_stat;
