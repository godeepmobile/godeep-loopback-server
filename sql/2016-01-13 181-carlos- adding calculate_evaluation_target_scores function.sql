﻿--liquibase formatted sql

--changeset carlos:181 runOnChange:true splitStatements:false stripComments:false
--comment added calculate_evaluation_target_scores function 

CREATE OR REPLACE FUNCTION calculate_evaluation_target_scores(go_team_id_parm text)
  RETURNS integer AS
$BODY$
DECLARE
	exists integer;
	team_config record;
	head_coach_start_date date;
	num_graded_games integer;
	position record;
	eval_criteria record;
	minimun_required_graded_games integer := 5; --n1
	minimun_required_games_with_grades_for_position integer := 5; --n2
	num_top_players integer := 2; --n3
BEGIN
	--check validity of parameters
	select count(team.id) into exists from godeepmobile.go_team team where team.id = go_team_id_parm::uuid;
	if exists != 1 then
		RAISE EXCEPTION 'Team % does not exist', go_team_id_parm USING HINT = 'Please check your team id parameter';
		return 0;
	end if;

	--get the current head coach start date
	select start_date into head_coach_start_date from go_user_team_assignment where go_team_id = go_team_id_parm::uuid and end_date is null and go_user_role_type_id = 2; --n1

	--get the team grades
	select count(distinct event.id) into num_graded_games from team_play_grade grade, team_play_data play, view_team_event event where grade.team_play_data_id = play.id and play.team_event_id = event.id and grade.go_team_id = go_team_id_parm::uuid and event.is_practice = false and event.date > head_coach_start_date;

	--check if the team has the minimun number of graded games, if true proceed to calculate the target scores
	if num_graded_games > minimun_required_graded_games then
		--get the team configuraion
		select * into team_config from team_configuration where go_team_id = go_team_id_parm::uuid;
		
		--get the positions with at least three graded players for the minimun number of graded games
		for position in (select position_played as id from
			(select grade.position_played, event.id as event_id, 
				count(distinct grade.team_player_id) as num_graded_players 
			from team_play_grade grade, team_play_data play, view_team_event event 
			where grade.team_play_data_id = play.id and 
			play.team_event_id = event.id and 
			grade.go_team_id = go_team_id_parm::uuid and 
			event.is_practice = false and 
			event.date > head_coach_start_date
			group by grade.position_played, event.id) x 
		where num_graded_players > 3 
		group by position_played 
		having count(event_id) > minimun_required_games_with_grades_for_position)

		loop
			--get the top two players for the position
			for eval_criteria in (select team_scouting_eval_criteria_id as criteria_id, round(avg(score), 2) as target_score from team_scouting_eval_score where team_scouting_eval_score_group_id in (
				select evaluation_id from (select distinct on (eval.team_player_id) eval.id as evaluation_id, round(avg(eval_score.score), 2) as eval_overall 
				from team_scouting_eval_score_group eval, team_scouting_eval_score eval_score 
				where eval_score.team_scouting_eval_score_group_id = eval.id and
				eval.date > head_coach_start_date and
				eval.go_team_id = go_team_id_parm::uuid and
				team_player_id in (select team_player_id from (select grade.team_player_id, 
						avg(round((grade.cat_1_grade + grade.cat_2_grade + grade.cat_3_grade)/3::numeric, 2)) as overall 
					from team_play_grade grade, team_play_data play, view_team_event event 
					where grade.team_play_data_id = play.id and 
						play.team_event_id = event.id and 
						grade.position_played = 1 and
						grade.go_team_id = go_team_id_parm::uuid and 
						event.is_practice = false and 
						event.date > head_coach_start_date
					group by grade.team_player_id
					having avg(round((grade.cat_1_grade + grade.cat_2_grade + grade.cat_3_grade)/3::numeric, 2)) > team_config.grade_base
					order by overall desc
					limit num_top_players) y)
				group by eval.id order by eval.team_player_id, eval_overall desc) z
			) group by team_scouting_eval_criteria_id)
			
			loop
				
				--RAISE NOTICE 'criteria_tpe % = %', eval_criteria.criteria_id, eval_criteria.target_score;
				update team_scouting_eval_criteria set calculated_target_score = eval_criteria.target_score where id = eval_criteria.criteria_id;
				
			end loop;
		end loop;
	end if;
	
	return 1;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

--rollback DROP FUNCTION IF EXISTS calculate_evaluation_target_scores;
