--liquibase formatted sql

--changeset rambert:68 runOnChange:true stripComments:false splitStatements:false
--comment add decimal for Player Information on team_player_season_stat fields

ALTER TABLE team_player_season_stat ALTER COLUMN cat1_grade_allevent TYPE numeric(1000, 2) USING cat1_grade_allevent::numeric;
ALTER TABLE team_player_season_stat ALTER COLUMN cat2_grade_allevent TYPE numeric(1000, 2) USING cat2_grade_allevent::numeric;
ALTER TABLE team_player_season_stat ALTER COLUMN cat3_grade_allevent TYPE numeric(1000, 2) USING cat3_grade_allevent::numeric;
ALTER TABLE team_player_season_stat ALTER COLUMN impact_platoon_grade_allevent TYPE numeric(1000, 2) USING impact_platoon_grade_allevent::numeric;
ALTER TABLE team_player_season_stat ALTER COLUMN overall_grade_allevent TYPE numeric(1000, 2) USING overall_grade_allevent::numeric;
ALTER TABLE team_player_season_stat ALTER COLUMN impact_platoon_grade_practice TYPE numeric(1000, 2) USING impact_platoon_grade_practice::numeric;
ALTER TABLE team_player_season_stat ALTER COLUMN cat1_grade_practice TYPE numeric(1000, 2) USING cat1_grade_practice::numeric;
ALTER TABLE team_player_season_stat ALTER COLUMN cat2_grade_practice TYPE numeric(1000, 2) USING cat2_grade_practice::numeric;
ALTER TABLE team_player_season_stat ALTER COLUMN cat3_grade_practice TYPE numeric(1000, 2) USING cat3_grade_practice::numeric;
ALTER TABLE team_player_season_stat ALTER COLUMN cat1_grade_game TYPE numeric(1000, 2) USING cat1_grade_game::numeric;
ALTER TABLE team_player_season_stat ALTER COLUMN cat2_grade_game TYPE numeric(1000, 2) USING cat2_grade_game::numeric;
ALTER TABLE team_player_season_stat ALTER COLUMN cat3_grade_game TYPE numeric(1000, 2) USING cat3_grade_game::numeric;

--rollback ALTER TABLE team_player_season_stat ALTER COLUMN cat1_grade_allevent TYPE integer; ALTER TABLE team_player_season_stat ALTER COLUMN cat2_grade_allevent TYPE integer; ALTER TABLE team_player_season_stat ALTER COLUMN cat3_grade_allevent TYPE integer; ALTER TABLE team_player_season_stat ALTER COLUMN impact_platoon_grade_allevent TYPE numeric; ALTER TABLE team_player_season_stat ALTER COLUMN overall_grade_allevent TYPE integer; ALTER TABLE team_player_season_stat ALTER COLUMN impact_platoon_grade_practice TYPE  integer; ALTER TABLE team_player_season_stat ALTER COLUMN cat1_grade_practice TYPE  integer; ALTER TABLE team_player_season_stat ALTER COLUMN cat2_grade_practice TYPE  integer; ALTER TABLE team_player_season_stat ALTER COLUMN cat3_grade_practice TYPE  integer; ALTER TABLE team_player_season_stat ALTER COLUMN cat1_grade_game TYPE  integer; ALTER TABLE team_player_season_stat ALTER COLUMN cat2_grade_game TYPE  integer; ALTER TABLE team_player_season_stat ALTER COLUMN cat3_grade_game TYPE  integer;