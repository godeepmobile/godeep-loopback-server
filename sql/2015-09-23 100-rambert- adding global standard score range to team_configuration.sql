--liquibase formatted sql

--changeset rambert:100 runOnChange:true stripComments:false splitStatements:false
--comment add global standard score range to team_configuration

ALTER TABLE godeepmobile.team_configuration ADD COLUMN scout_eval_score_min smallint NOT NULL DEFAULT 1;
ALTER TABLE godeepmobile.team_configuration ADD COLUMN scout_eval_score_max smallint NOT NULL DEFAULT 9;

--rollback ALTER TABLE godeepmobile.team_configuration DROP COLUMN scout_eval_score_min; ALTER TABLE godeepmobile.team_configuration DROP COLUMN scout_eval_score_max;
