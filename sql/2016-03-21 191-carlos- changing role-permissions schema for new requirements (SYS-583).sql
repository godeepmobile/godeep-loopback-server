﻿--liquibase formatted sql

--changeset carlos:191 runOnChange:true splitStatements:false stripComments:false
--comment changed role-permissions schema for new requirements (SYS-583)

-- adding accocunt type (if the team can grade or evaluate) to team_configuration
ALTER TABLE team_configuration ADD can_grade boolean NOT NULL DEFAULT false;
ALTER TABLE team_configuration ADD can_evaluate boolean NOT NULL DEFAULT false;

UPDATE team_configuration SET can_grade = true;
UPDATE team_configuration SET can_evaluate = true;

-- updating role types and users
UPDATE go_user_role_type SET name = 'Team Admin' WHERE id = 4;
UPDATE go_user_role_type SET name = 'Coach' WHERE id = 3;
UPDATE go_user_team_assignment SET go_user_role_type_id = 3 WHERE go_user_role_type_id = 2;

-- updating permissions
INSERT INTO go_permission VALUES ('SCOUT', 'Scouting/Evaluation functions');

-- updating roles-permissions mapping
DELETE FROM go_role_permission;
INSERT INTO go_role_permission VALUES (1,1,'PLAYER');
INSERT INTO go_role_permission VALUES (2,3,'GRADER');
INSERT INTO go_role_permission VALUES (3,3,'SCOUT');
INSERT INTO go_role_permission VALUES (4,3,'REPORT-VIEWER');
INSERT INTO go_role_permission VALUES (5,4,'TEAM-ADMIN');

--rollback ALTER TABLE team_configuration DROP COLUMN can_grade; ALTER TABLE team_configuration DROP COLUMN can_evaluate;
