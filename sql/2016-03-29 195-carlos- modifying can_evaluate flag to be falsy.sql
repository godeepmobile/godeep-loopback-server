﻿--liquibase formatted sql

--changeset carlos:195 runOnChange:true splitStatements:false stripComments:false
--comment modified can_evaluate flag to be falsy

UPDATE team_configuration SET can_evaluate = false;

--rollback UPDATE team_configuration SET can_evaluate = true;