--liquibase formatted sql

--changeset carlos:19 runOnChange:true
--comment added factor_in_play column to team_play_grade

ALTER TABLE godeepmobile.team_play_grade ADD factor_in_play smallint DEFAULT 0;

 --rollback ALTER TABLE godeepmobile.team_play_grade DROP COLUMN factor_in_play;