--liquibase formatted sql

--changeset carlos:24 runOnChange:true
--comment added reference to go_play_factor on team_play_grade

ALTER TABLE godeepmobile.team_play_grade ADD go_play_factor_id INTEGER DEFAULT 1;
ALTER TABLE godeepmobile.team_play_grade ADD CONSTRAINT fk_team_play_grade_go_play_factor FOREIGN KEY (go_play_factor_id) REFERENCES godeepmobile.go_play_factor (id);

--rollback ALTER TABLE godeepmobile.team_play_grade DROP CONSTRAINT fk_team_play_grade_go_play_factor; ALTER TABLE godeepmobile.team_play_grade DROP COLUMN go_play_factor_id;

