--liquibase formatted sql

--changeset rambert:101 runOnChange:true stripComments:false splitStatements:false
--comment create go_scouting_eval_criteria_type table

CREATE TABLE godeepmobile.go_scouting_eval_criteria_type (
  id                   integer  NOT NULL,
  name                 varchar(100)  NOT NULL,
  CONSTRAINT pk_go_scouting_eval_criteria_type PRIMARY KEY ( id )
);

-- inserting new conferences
INSERT INTO godeepmobile.go_scouting_eval_criteria_type( id, name )
                               VALUES ( 1, 'Major Factors' );
INSERT INTO godeepmobile.go_scouting_eval_criteria_type( id, name )
                               VALUES ( 2, 'Critical Factors' );
INSERT INTO godeepmobile.go_scouting_eval_criteria_type( id, name )
                               VALUES ( 3, 'Position Skills' );

--rollback DROP TABLE IF EXISTS  godeepmobile.go_scouting_eval_criteria_type;
