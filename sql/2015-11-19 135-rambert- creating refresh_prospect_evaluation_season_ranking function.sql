--liquibase formatted sql

--changeset rambert:135 runOnChange:true splitStatements:false stripComments:false
--comment create refresh_prospect_evaluation_season_ranking function


CREATE OR REPLACE FUNCTION godeepmobile.refresh_prospect_evaluation_season_ranking(
  team_id_parm uuid,
  season_parm integer)
  RETURNS void AS
$BODY$
DECLARE
  season_record RECORD;
  season_record_exists integer;
BEGIN

  -- gets player score overall using using values that have been calculated with multipliers and position importance
  FOR season_record IN
    SELECT subquery.go_team_id,
      subquery.season,
      subquery.team_player_id,
      subquery.num_evaluations,
      ROUND(subquery.target_match, 2) AS target_match,
      ROUND(subquery.major_factors_eval_avg, 2) AS major_factors_eval_avg,
      ROUND(subquery.critical_factors_eval_avg, 2) AS critical_factors_eval_avg,
      ROUND(subquery.position_skills_eval_avg, 2) AS position_skills_eval_avg,
      ROUND(subquery.overall_eval_avg, 2) AS overall_eval_avg,
      ROUND(subquery.overall_eval_avg_rank, 2) AS overall_eval_avg_rank,
      RANK() OVER (ORDER BY subquery.overall_eval_avg_rank DESC, subquery.target_match DESC) AS rank_overall
    FROM (
      SELECT
      stat.go_team_id,
      stat.season,
      stat.team_player_id,
      COUNT(stat.team_scouting_eval_score_group_id) AS num_evaluations,
      AVG(stat.target_match) AS target_match,
      AVG(stat.major_factors_eval_avg) AS major_factors_eval_avg,
      AVG(stat.critical_factors_eval_avg) AS critical_factors_eval_avg,
      AVG(stat.position_skills_eval_avg) AS position_skills_eval_avg,
      AVG(stat.overall_eval_avg) AS overall_eval_avg,
      AVG(stat.overall_eval_avg_rank) AS overall_eval_avg_rank
      FROM godeepmobile.player_evaluation_stat stat,
           godeepmobile.team_scouting_eval_score_group score_group,
           godeepmobile.view_team_prospect prospect
      WHERE stat.go_team_id = team_id_parm::uuid
            AND stat.season  = season_parm
            AND stat.team_scouting_eval_score_group_id = score_group.id
            AND prospect.id = stat.team_player_id
       GROUP BY stat.go_team_id, stat.season, stat.team_player_id
     ) AS subquery
  LOOP
    -- check if record already exists
    SELECT COUNT(id) INTO season_record_exists
    FROM godeepmobile.player_evaluation_season_stat
    WHERE go_team_id = team_id_parm::uuid
      AND season = season_parm
      AND team_player_id = season_record.team_player_id;

    IF season_record_exists = 0 THEN
      INSERT INTO godeepmobile.player_evaluation_season_stat (
        go_team_id,
        team_player_id,
        season,
        num_evaluations,
        target_match,
        major_factors_eval_avg,
        critical_factors_eval_avg,
        position_skills_eval_avg,
        overall_eval_avg,
        overall_eval_avg_rank,
        rank_overall
      )
      VALUES (
        team_id_parm::uuid,
        season_record.team_player_id,
        season_record.season,
        season_record.num_evaluations,
        season_record.target_match,
        season_record.major_factors_eval_avg,
        season_record.critical_factors_eval_avg,
        season_record.position_skills_eval_avg,
        season_record.overall_eval_avg,
        season_record.overall_eval_avg_rank,
        season_record.rank_overall
      );
    ELSE
      UPDATE
          godeepmobile.player_evaluation_season_stat
      SET
        num_evaluations = season_record.num_evaluations,
        target_match = season_record.target_match,
        major_factors_eval_avg = season_record.major_factors_eval_avg,
        critical_factors_eval_avg = season_record.critical_factors_eval_avg,
        position_skills_eval_avg = season_record.position_skills_eval_avg,
        overall_eval_avg = season_record.overall_eval_avg,
        overall_eval_avg_rank = season_record.overall_eval_avg_rank,
        rank_overall = season_record.rank_overall
      WHERE go_team_id = team_id_parm::uuid
        AND team_player_id = season_record.team_player_id
        AND season = season_record.season;
    END IF;
  END LOOP;

  perform godeepmobile.refresh_prospect_evaluation_career_ranking(team_id_parm);

END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

--rollback DROP FUNCTION godeepmobile.refresh_prospect_evaluation_season_ranking(uuid, integer);
