﻿--liquibase formatted sql

--changeset carlos:222 runOnChange:true splitStatements:false stripComments:false
--comment changed default value for calculated_target_score on team_scouting_eval_criteria

ALTER TABLE godeepmobile.team_scouting_eval_criteria ALTER COLUMN calculated_target_score SET DEFAULT 4.5;
UPDATE godeepmobile.team_scouting_eval_criteria SET calculated_target_score = 4.5 WHERE calculated_target_score = 5.0;

--rollback ALTER TABLE godeepmobile.team_scouting_eval_criteria ALTER COLUMN calculated_target_score SET DEFAULT 5.0; UPDATE godeepmobile.team_scouting_eval_criteria SET calculated_target_score = 5.0 WHERE calculated_target_score = 4.5;
