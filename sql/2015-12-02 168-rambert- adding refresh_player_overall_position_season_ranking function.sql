--liquibase formatted sql

--changeset rambert:168 runOnChange:true splitStatements:false stripComments:false
--comment updating refresh_player_overall_position_season_ranking function

CREATE OR REPLACE FUNCTION godeepmobile.refresh_player_overall_position_season_ranking(
  team_id_parm uuid,
  season_parm integer)
  RETURNS void AS
$BODY$
DECLARE
  record_value RECORD;
BEGIN
  UPDATE godeepmobile.player_overall_season_rank_at_position_stat
  SET rank_at_post = NULL
  WHERE go_team_id = team_id_parm::uuid
      AND season = season_parm;

  -- generate evaluation stat values
  FOR record_value IN
    SELECT player.team_position_type_id_pos_1
    FROM godeepmobile.view_team_player_season_stat season_stat
      JOIN view_team_player player ON season_stat.team_player_id = player.id
      WHERE season_stat.go_team_id = team_id_parm::uuid
        AND season_stat.season = season_parm
      GROUP BY player.team_position_type_id_pos_1
  LOOP
      perform godeepmobile.refresh_player_season_overall_rank_at_position(team_id_parm, season_parm, record_value.team_position_type_id_pos_1);
  END LOOP;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

--rollback DROP FUNCTION godeepmobile.refresh_player_overall_position_season_ranking(uuid, integer);
