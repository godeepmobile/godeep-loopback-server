﻿--liquibase formatted sql

--changeset carlos:88 runOnChange:true splitStatements:false stripComments:false
--comment created create_empty_plays_for_event_cutup(integer, integer, text)

CREATE OR REPLACE FUNCTION create_empty_plays_for_event_cutup(
    team_event_id_parm integer,
    team_cutup_id_parm integer,
    go_team_id_parm text)
  RETURNS void AS
$BODY$
DECLARE
	max_play_number integer;
	exists integer;
	how_many integer;
BEGIN
	select count(cutup.id) into exists from godeepmobile.team_cutup cutup where cutup.go_team_id = go_team_id_parm::uuid and cutup.team_event_id = team_event_id_parm and cutup.id = team_cutup_id_parm;
	if exists != 1 then
		RAISE EXCEPTION 'Team cutup % does not exist for team %', team_event_id_parm, go_team_id_parm USING HINT = 'Please check your event and team parameters';
	end if;

	select num_plays into how_many from godeepmobile.team_platoon_configuration where go_team_id = go_team_id_parm::uuid and go_platoon_type_id = (select which_platoon from godeepmobile.team_cutup where id = team_cutup_id_parm);
	if how_many IS NULL then
		RAISE EXCEPTION 'Team cutup % does not have a default number of plays its platoon', team_cutup_id_parm;
	end if;
	
	FOR i IN (1)..(how_many) LOOP
		INSERT INTO godeepmobile.team_play_data (play_number, team_event_id, go_team_id, team_cutup_id ) VALUES (i, team_event_id_parm, go_team_id_parm::uuid, team_cutup_id_parm);
	END LOOP;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

--rollback DROP FUNCTION create_empty_plays_for_event_cutup(integer, integer, text);