--liquibase formatted sql

--changeset rambert:132 runOnChange:true stripComments:false splitStatements:false
--comment create player_rank_overall table

CREATE TABLE godeepmobile.player_rank_overall (
  id                         serial  NOT NULL,
  go_team_id                 uuid NOT NULL,
  team_player_id             uuid NOT NULL,
  season                     integer NOT NULL,
  rank_overall               smallint,
  overall_evaluation_rank    smallint,
  overall_grade_game_rank    smallint,
  rank_at_post               smallint,
  game_grade_avg             numeric(100,2),
  target_match               smallint,
  major_factors_eval_avg     numeric(100,2),
  critical_factors_eval_avg  numeric(100,2),
  position_skills_eval_avg   numeric(100,2),
  CONSTRAINT pk_player_rank_overall PRIMARY KEY ( id ),
  CONSTRAINT fk_player_rank_overall_go_team FOREIGN KEY (go_team_id)
      REFERENCES go_team (id),
  CONSTRAINT fk_player_rank_overall_team_player FOREIGN KEY (team_player_id)
      REFERENCES team_player (id)
);

--rollback DROP TABLE IF EXISTS  godeepmobile.player_rank_overall;
