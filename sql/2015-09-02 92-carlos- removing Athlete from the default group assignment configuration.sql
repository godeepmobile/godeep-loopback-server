﻿--liquibase formatted sql

--changeset carlos:92 runOnChange:true
--comment removed "Athlete" from the default group assignment configuration

DELETE FROM go_position_type_to_group_assignment WHERE go_position_type_id = 29;

--rollback INSERT INTO go_position_type_to_group_assignment (go_position_type_id, go_position_group_id, go_platoon_type_id) VALUES (29,1,1);