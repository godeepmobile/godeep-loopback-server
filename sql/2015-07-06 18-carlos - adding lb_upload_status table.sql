--liquibase formatted sql

--changeset carlos:18 runOnChange:true
--comment added lb_upload_status table

CREATE TABLE godeepmobile.lb_upload_status (
	id serial NOT NULL,
	modelType varchar(100) NOT NULL,
	status varchar(100) NOT NULL,
	error_description varchar(256),
	num_records integer,
	file_uri varchar(100),
	upload_date date NOT NULL,
	go_user_id integer NOT NULL,
	CONSTRAINT fk_lb_upload_status_go_user FOREIGN KEY (go_user_id) REFERENCES godeepmobile.go_user(id)
 );

 --rollback drop TABLE IF EXISTS godeepmobile.lb_upload_status;