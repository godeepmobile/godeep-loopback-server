--liquibase formatted sql

--changeset mark:21 runOnChange:true stripComments:false splitStatements:false
--comment add short_name column and values to go_platon_type table

--modify table by adding a new column for short name
ALTER TABLE godeepmobile.go_platoon_type ADD short_name varchar(4);

--now fill in this new column with values for existing platoon types
UPDATE godeepmobile.go_platoon_type SET short_name = 'N/A' WHERE id = 0; -- unassigned
UPDATE godeepmobile.go_platoon_type SET short_name = 'OFF' WHERE id = 1; -- offense
UPDATE godeepmobile.go_platoon_type SET short_name = 'DEF' WHERE id = 2; -- defense
UPDATE godeepmobile.go_platoon_type SET short_name = 'ST'  WHERE id = 3; -- special teams
UPDATE godeepmobile.go_platoon_type SET short_name = 'ALL' WHERE id = 4; -- all platoons

--rollback ALTER TABLE godeepmobile.go_platoon_type DROP COLUMN short_name;
