--liquibase formatted sql

--changeset rambert:19 runOnChange:true stripComments:false
--comment changing start_date and end_date data type from team_user_position_group_assignment
ALTER TABLE godeepmobile.team_user_position_group_assignment ALTER COLUMN start_date SET DATA TYPE timestamp;
ALTER TABLE godeepmobile.team_user_position_group_assignment ALTER COLUMN start_date SET DEFAULT ('now'::text)::timestamp;
ALTER TABLE godeepmobile.team_user_position_group_assignment ALTER COLUMN end_date SET DATA TYPE timestamp;

--rollback ALTER TABLE godeepmobile.team_user_position_group_assignment ALTER COLUMN start_date SET DATA TYPE date; ALTER TABLE godeepmobile.team_user_position_group_assignment ALTER COLUMN start_date DROP DEFAULT; ALTER TABLE godeepmobile.team_user_position_group_assignment ALTER COLUMN end_date SET DATA TYPE date;