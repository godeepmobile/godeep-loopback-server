--liquibase formatted sql

--changeset carlos:31 runOnChange:true
--comment added team_player_game_cutup_stat table

CREATE TABLE godeepmobile.team_player_game_cutup_stat(
	id serial NOT NULL,
	go_team_id uuid NOT NULL,
	team_player_id uuid NOT NULL,
	season integer NOT NULL DEFAULT 2015,
	num_plays integer DEFAULT 0,
	cat1_grade integer DEFAULT 0, -- total raw points, not an average.
	cat2_grade integer DEFAULT 0, -- total raw points, not an average.
	cat3_grade integer DEFAULT 0, -- total raw points, not an average.
	overall_grade integer DEFAULT 0, -- total raw points of category grades, not an average.
	team_cutup_id integer,
	date date,
	last_update timestamp without time zone, -- last time this record was updated
	CONSTRAINT pk_team_player_game_cutup_stat PRIMARY KEY (id),
	CONSTRAINT fk_team_player_game_cutup_stat_go_team FOREIGN KEY (go_team_id) REFERENCES go_team (id),
	CONSTRAINT fk_team_player_game_cutup_stat_team_cutup FOREIGN KEY (team_cutup_id) REFERENCES team_cutup (id) MATCH SIMPLE,
	CONSTRAINT fk_team_player_game_cutup_stat_team_player FOREIGN KEY (team_player_id) REFERENCES team_player (id) MATCH SIMPLE,
	CONSTRAINT unique_team_player_id_team_cutup_id UNIQUE (team_cutup_id, team_player_id)
);

--rollback DROP TABLE godeepmobile.team_player_game_cutup_stat;