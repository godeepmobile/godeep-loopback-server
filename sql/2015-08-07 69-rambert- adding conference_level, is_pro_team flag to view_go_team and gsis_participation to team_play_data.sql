--liquibase formatted sql

--changeset carlos:69 runOnChange:true
--comment added conference_level, is_pro_team flag to view_go_team and gsis_participation to team_play_data

CREATE OR REPLACE VIEW view_go_team AS 
 SELECT gt.id,
    ( SELECT go.name
           FROM go_organization go
          WHERE go.id = gt.go_organization_id) AS name,
    gt.short_name,
    gt.abbreviation,
    gt.mascot,
    ( SELECT gc.name
           FROM go_conference gc
          WHERE gc.id = gt.go_conference_id) AS conference_name,
    gt.go_sport_type_id,
    gt.addr_street1,
    gt.addr_street2,
    gt.addr_city,
    gt.addr_state_id,
    gt.addr_postal_code,
    gt.addr_phone_main,
    gt.addr_phone_fax,
    gt.home_stadium,
    gt.go_surface_type_id,
    gt.film_system,
    gt.go_conference_id,
    gt.asset_base_url,
    gt.go_organization_id,
    ( SELECT count(*) AS count
           FROM team_play_grade grade
          WHERE grade.go_team_id = gt.id) AS num_play_grades,
    ( SELECT gc.level
           FROM go_conference gc
          WHERE gc.id = gt.go_conference_id) AS conference_level,
    (gt.go_conference_id = 15 OR gt.go_conference_id = 16) AS is_pro_team
   FROM go_team gt
  ORDER BY gt.id;

  ALTER TABLE godeepmobile.team_play_data ADD gsis_participation INTEGER[11];

--rollback ALTER TABLE godeepmobile.team_play_data DROP COLUMN gsis_participation;