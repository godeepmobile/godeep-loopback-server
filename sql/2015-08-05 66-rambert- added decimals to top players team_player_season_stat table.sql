--liquibase formatted sql

--changeset rambert:66 runOnChange:true stripComments:false splitStatements:false
--comment add decimal to team_player_season_stat fields

ALTER TABLE team_player_season_stat ALTER COLUMN impact_posgroup_grade_game TYPE numeric(1000, 2) USING impact_posgroup_grade_game::numeric;
ALTER TABLE team_player_season_stat ALTER COLUMN impact_platoon_grade_game TYPE numeric(1000, 2) USING impact_platoon_grade_game::numeric;
ALTER TABLE team_player_season_stat ALTER COLUMN overall_grade_practice TYPE numeric(1000, 2) USING overall_grade_practice::numeric;

--rollback ALTER TABLE team_player_season_stat ALTER COLUMN impact_posgroup_grade_game TYPE numeric; ALTER TABLE team_player_season_stat ALTER COLUMN impact_platoon_grade_game TYPE numeric; ALTER TABLE team_player_season_stat ALTER COLUMN overall_grade_practice TYPE integer;
