﻿--liquibase formatted sql

--changeset carlos:129 runOnChange:true
--comment added adding last_graded_play to grade status

ALTER TABLE team_event_grade_status ADD last_graded_play INTEGER;
ALTER TABLE team_event_grade_status RENAME COLUMN completed_date TO last_submit_date;

DROP VIEW view_team_event_grade_status;

CREATE VIEW view_team_event_grade_status AS 
 SELECT team_event_grade_status.id,
    team_event_grade_status.go_team_id,
    team_event_grade_status.go_user_id,
    team_event_grade_status.team_event_id,
    team_event_grade_status.team_cutup_id,
    team_event_grade_status.last_submit_date,
    team_event_grade_status.started_date,
    team_event_grade_status.last_graded_play,
        CASE
            WHEN team_event_grade_status.started_date IS NULL THEN 'not started'::text
            ELSE 'in process'::text
        END AS status
   FROM team_event_grade_status;

--rollback ALTER TABLE team_event_grade_status DROP COLUMN last_graded_play; ALTER TABLE team_event_grade_status RENAME COLUMN  last_submit_date TO completed_date;