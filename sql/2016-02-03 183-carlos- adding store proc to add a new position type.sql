﻿--liquibase formatted sql

--changeset carlos:183 runOnChange:true splitStatements:false stripComments:false
--comment added store proc to add new position type 

CREATE OR REPLACE FUNCTION add_new_position_type(_name text, _short_name text, sport_type integer, platoon integer, _sort_index integer, position_group integer, position_to_clone_eval_criteria integer)
  RETURNS integer AS
$BODY$
DECLARE
	new_position_type_id integer;
	team_position_group_id integer;
	team record;
BEGIN
	-- inserting the new position
	INSERT INTO go_position_type (
		id,
		name, 
		short_name, 
		go_sport_type_id, 
		go_platoon_type_id, 
		sort_index
	) VALUES (
		(SELECT MAX(id) + 1 FROM go_position_type),
		_name,
		_short_name,
		sport_type,
		platoon,
		_sort_index
	) RETURNING id INTO new_position_type_id;

	-- linking to an existing position group
	INSERT INTO go_position_type_to_group_assignment (go_position_type_id, go_position_group_id, go_platoon_type_id) VALUES (new_position_type_id, position_group, platoon);

	-- adding the global scouting/evaluation criteria
	INSERT INTO go_scouting_eval_criteria (id, name, go_scouting_eval_criteria_type_id, go_position_type_id, go_importance_id, sort_index)
		SELECT ROW_NUMBER() OVER (ORDER BY id) + (SELECT MAX(id) FROM go_scouting_eval_criteria), name, go_scouting_eval_criteria_type_id, new_position_type_id, go_importance_id, sort_index FROM go_scouting_eval_criteria WHERE go_position_type_id = position_to_clone_eval_criteria;

	FOR team IN SELECT id FROM go_team
	LOOP
		-- recovering the team's position group id
		SELECT tg.id INTO team_position_group_id FROM team_position_group tg, go_position_group gg WHERE tg.name = gg.name AND gg.id = position_group AND tg.go_team_id = team.id;

		IF team_position_group_id IS NOT NULL THEN
			RAISE NOTICE 'team % has the position group (%) assigned to it', team.id, team_position_group_id;
			
			-- inserting the position type at a team level if the team has the position group assigned to it
			INSERT INTO team_position_type (
				go_team_id,
				go_position_type_id,
				team_position_group_id,
				go_platoon_type_id
			) VALUES (
				team.id,
				new_position_type_id,
				team_position_group_id,
				platoon
			);

			-- adding the team scouting/evaluation criteria
			 INSERT INTO godeepmobile.team_scouting_eval_criteria (name, go_team_id, go_position_type_id, go_scouting_eval_criteria_type_id, go_importance_id, sort_index)
				SELECT name, team.id, go_position_type_id, go_scouting_eval_criteria_type_id, go_importance_id, sort_index FROM go_scouting_eval_criteria WHERE go_position_type_id = new_position_type_id;

			-- adding importance for the new position
			INSERT INTO team_position_importance (go_team_id, go_position_type_id, go_importance_id) VALUES (team.id, new_position_type_id, DEFAULT);
		END IF;
	END LOOP;
	
	return 1;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

--rollback DROP FUNCTION add_new_position_type(text, text, integer, integer, integer, integer, integer);