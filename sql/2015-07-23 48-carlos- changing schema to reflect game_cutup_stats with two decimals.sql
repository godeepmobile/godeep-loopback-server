--liquibase formatted sql

--changeset carlos:48 runOnChange:true splitStatements:false
--comment changed schema to reflect game_cutup_stats with two decimals

DROP VIEW IF EXISTS godeepmobile.view_game_stat_ids_for_players_with_cutup_plays;
DROP VIEW IF EXISTS godeepmobile.view_team_player_event_grades_by_cutup;

CREATE OR REPLACE VIEW godeepmobile.view_team_player_event_grades_by_cutup AS 
 SELECT player.id AS "teamPlayer",
    player.season,
    count(grade.id)::integer AS "numPlays",
    sum(grade.cat_1_grade)::integer AS "sumCat1Grade",
    sum(grade.cat_2_grade)::integer AS "sumCat2Grade",
    sum(grade.cat_3_grade)::integer AS "sumCat3Grade",
    sum(grade.cat_1_grade + grade.cat_2_grade + grade.cat_3_grade)::integer AS "sumOverallGrade",
    round(avg(grade.cat_1_grade)::numeric, 2) AS "avgCat1Grade",
    round(avg(grade.cat_2_grade)::numeric, 2) AS "avgCat2Grade",
    round(avg(grade.cat_3_grade)::numeric, 2) AS "avgCat3Grade",
    round((avg(grade.cat_1_grade + grade.cat_2_grade + grade.cat_3_grade) / 3::numeric)::numeric, 2) AS "avgOverallGrade",
    play.team_cutup_id AS "teamCutup",
    event.date,
    player.go_team_id AS team
   FROM godeepmobile.team_player player
     JOIN team_play_grade grade ON grade.team_player_id = player.id
     JOIN team_play_data play ON play.id = grade.team_play_data_id
     JOIN team_event event ON play.team_event_id = event.id
  GROUP BY player.id, event.date, play.team_cutup_id
  ORDER BY player.id;

CREATE OR REPLACE VIEW godeepmobile.view_game_stat_ids_for_players_with_cutup_plays AS 
 SELECT grades.team AS go_team_id,
    grades."teamPlayer" AS team_player_id,
    grades."teamCutup" AS team_cutup_id,
    grades."numPlays" AS num_plays,
    stats.id AS team_player_game_cutup_stat_id,
    grades.season,
    grades.date
   FROM godeepmobile.view_team_player_event_grades_by_cutup grades
     LEFT JOIN godeepmobile.team_player_game_cutup_stat stats ON stats.team_player_id = grades."teamPlayer" AND stats.team_cutup_id = grades."teamCutup"
  ORDER BY grades."teamPlayer";

ALTER TABLE godeepmobile.team_player_game_cutup_stat ALTER COLUMN cat1_grade TYPE numeric;
ALTER TABLE godeepmobile.team_player_game_cutup_stat ALTER COLUMN cat2_grade TYPE numeric;
ALTER TABLE godeepmobile.team_player_game_cutup_stat ALTER COLUMN cat3_grade TYPE numeric;
ALTER TABLE godeepmobile.team_player_game_cutup_stat ALTER COLUMN overall_grade TYPE numeric;

UPDATE godeepmobile.team_player_game_cutup_stat 
SET cat1_grade = grade_stat."avgCat1Grade", cat2_grade = grade_stat."avgCat2Grade", cat3_grade = grade_stat."avgCat3Grade", overall_grade = grade_stat."avgOverallGrade"
FROM (SELECT "avgCat1Grade", "avgCat2Grade", "avgCat3Grade", "avgOverallGrade", "teamPlayer", "teamCutup" FROM godeepmobile.view_team_player_event_grades_by_cutup) AS grade_stat
WHERE team_player_id = grade_stat."teamPlayer" AND team_cutup_id = grade_stat."teamCutup";

--rollback DROP VIEW IF EXISTS godeepmobile.view_game_stat_ids_for_players_with_cutup_plays; DROP VIEW IF EXISTS godeepmobile.view_team_player_event_grades_by_cutup;