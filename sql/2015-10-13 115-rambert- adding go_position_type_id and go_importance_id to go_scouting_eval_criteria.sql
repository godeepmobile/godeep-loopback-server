--liquibase formatted sql

--changeset rambert:115 runOnChange:true stripComments:false splitStatements:false
--comment adding go_position_type_id and go_importance_id to go_scouting_eval_criteria.sql

-- delete go_scouting_eval_criteria contents
DELETE FROM godeepmobile.go_scouting_eval_criteria;

-- add go_position_type_id constraint to go_scouting_eval_criteria
ALTER TABLE godeepmobile.go_scouting_eval_criteria ADD go_position_type_id INTEGER NOT NULL;
ALTER TABLE godeepmobile.go_scouting_eval_criteria ADD CONSTRAINT fk_go_scouting_eval_criteria_go_position_type FOREIGN KEY ( go_position_type_id ) REFERENCES godeepmobile.go_position_type( id );

-- add go_importance_id constraint to go_scouting_eval_criteria
ALTER TABLE godeepmobile.go_scouting_eval_criteria ADD go_importance_id INTEGER DEFAULT 3;
ALTER TABLE godeepmobile.go_scouting_eval_criteria ADD CONSTRAINT fk_go_scouting_eval_criteria_go_importance FOREIGN KEY ( go_importance_id ) REFERENCES godeepmobile.go_importance( id );

-- weight is being replaced by go_importance_id
ALTER TABLE godeepmobile.go_scouting_eval_criteria DROP COLUMN weight RESTRICT;

-- populating go_scouting_eval_criteria table
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (1, 1, 1, 'Personal/Behavior');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (2, 1, 1, 'Athletic Ability');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (3, 1, 1, 'Strength & Explosion');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (4, 1, 1, 'Competitiveness');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (5, 1, 1, 'Toughness');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (6, 1, 1, 'Mental/Learning');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (7, 1, 1, 'Injury/Durability');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (8, 1, 2, 'Leadership');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (9, 1, 2, 'Decision Making');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (10, 1, 2, 'Overall Accuracy');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (11, 1, 2, 'Clutch Production');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (12, 1, 3, 'Accuracy Short');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (13, 1, 3, 'Accuracy Long');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (14, 1, 3, 'Arm Strength');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (15, 1, 3, 'Mechanics');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (16, 1, 3, 'Mobility');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (17, 1, 3, 'Type of Production - System / Skills');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (18, 2, 1, 'Personal/Behavior');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (19, 2, 1, 'Athletic Ability');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (20, 2, 1, 'Strength & Explosion');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (21, 2, 1, 'Competitiveness');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (22, 2, 1, 'Toughness');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (23, 2, 1, 'Mental/Learning');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (24, 2, 1, 'Injury/Durability');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (25, 2, 2, 'Run Production');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (26, 2, 2, 'Pass Production');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (27, 2, 2, 'Instincts');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (28, 2, 2, 'QB the Defense');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (29, 2, 2, 'Tackling');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (30, 2, 3, 'Read / React');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (31, 2, 3, 'Stack Ability');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (32, 2, 3, 'Shed');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (33, 2, 3, 'Move Through Trash');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (34, 2, 3, 'Range (SL to SL)');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (35, 2, 3, 'Pass Coverage');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (36, 2, 3, 'Blitz');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (37, 2, 3, 'Third Down Value');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (38, 2, 3, 'Special Teams Value');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (39, 3, 1, 'Personal/Behavior');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (40, 3, 1, 'Athletic Ability');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (41, 3, 1, 'Strength & Explosion');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (42, 3, 1, 'Competitiveness');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (43, 3, 1, 'Toughness');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (44, 3, 1, 'Mental/Learning');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (45, 3, 1, 'Injury/Durability');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (46, 3, 2, 'Play Maker');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (47, 3, 2, 'Catching Skills');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (48, 3, 2, 'Get Open');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (49, 3, 2, 'Playing Speed');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (50, 3, 2, 'Dependability');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (51, 3, 3, 'Release');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (52, 3, 3, 'Quickness');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (53, 3, 3, 'Deep Threat');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (54, 3, 3, 'Route Running');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (55, 3, 3, 'Run After Catch');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (56, 3, 3, 'Blocker');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (57, 3, 3, 'Third Down Value');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (58, 3, 3, 'Special Teams Value');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (59, 4, 1, 'Personal/Behavior');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (60, 4, 1, 'Athletic Ability');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (61, 4, 1, 'Strength & Explosion');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (62, 4, 1, 'Competitiveness');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (63, 4, 1, 'Toughness');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (64, 4, 1, 'Mental/Learning');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (65, 4, 1, 'Injury/Durability');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (66, 4, 2, 'Production');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (67, 4, 2, 'Durability');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (68, 4, 2, 'FBI / Football Intelligence');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (69, 4, 2, 'Catching Skills');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (70, 4, 3, 'Block Run');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (71, 4, 3, 'Blitz Pickup');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (72, 4, 3, 'Route Running');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (73, 4, 3, 'Inside Run');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (74, 4, 3, 'Power Run');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (75, 4, 3, 'Quickness');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (76, 4, 3, 'Special Teams Value');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (77, 5, 1, 'Personal/Behavior');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (78, 5, 1, 'Athletic Ability');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (79, 5, 1, 'Strength & Explosion');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (80, 5, 1, 'Competitiveness');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (81, 5, 1, 'Toughness');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (82, 5, 1, 'Mental/Learning');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (83, 5, 1, 'Injury/Durability');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (84, 5, 2, 'Playmaker');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (85, 5, 2, 'Ball Security');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (86, 5, 2, 'Stamina');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (87, 5, 2, 'Catching Skills');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (88, 5, 2, 'Playing Speed');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (89, 5, 3, 'Avoid Ability');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (90, 5, 3, 'Inside Run');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (91, 5, 3, 'Outside Run');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (92, 5, 3, 'Power Run');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (93, 5, 3, 'Vision');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (94, 5, 3, 'Durability');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (95, 5, 3, 'Route Running');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (96, 5, 3, 'Blitz Pickup');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (97, 5, 3, 'Third Down Role');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (98, 5, 3, 'Special Teams Value');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (99, 6, 1, 'Personal/Behavior');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (100, 6, 1, 'Athletic Ability');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (101, 6, 1, 'Strength & Explosion');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (102, 6, 1, 'Competitiveness');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (103, 6, 1, 'Toughness');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (104, 6, 1, 'Mental/Learning');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (105, 6, 1, 'Injury/Durability');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (106, 6, 2, 'Playmaker');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (107, 6, 2, 'Ball Security');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (108, 6, 2, 'Stamina');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (109, 6, 2, 'Catching Skills');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (110, 6, 2, 'Playing Speed');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (111, 6, 2, 'Instincts');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (112, 6, 3, 'Avoid Ability');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (113, 6, 3, 'Inside Run');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (114, 6, 3, 'Outside Run');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (115, 6, 3, 'Power Run');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (116, 6, 3, 'Vision');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (117, 6, 3, 'Durability');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (118, 6, 3, 'Route Running');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (119, 6, 3, 'Blitz Pickup');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (120, 6, 3, 'Third Down Role');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (121, 6, 3, 'Special Teams Value');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (122, 7, 1, 'Personal/Behavior');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (123, 7, 1, 'Athletic Ability');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (124, 7, 1, 'Strength & Explosion');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (125, 7, 1, 'Competitiveness');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (126, 7, 1, 'Toughness');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (127, 7, 1, 'Mental/Learning');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (128, 7, 1, 'Injury/Durability');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (129, 7, 2, 'Production');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (130, 7, 2, 'Get Separation / Open');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (131, 7, 2, 'Catching Skills');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (132, 7, 2, 'Blocking Ability');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (133, 7, 2, 'FBI / Football Intelligence');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (134, 7, 3, 'Initial Quickness');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (135, 7, 3, 'Release');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (136, 7, 3, 'Quickness');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (137, 7, 3, 'Deep Threat');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (138, 7, 3, 'Route Running');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (139, 7, 3, 'Run After Catch');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (140, 7, 3, 'Third Down Value');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (141, 7, 3, 'Special Teams Value');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (142, 8, 1, 'Personal/Behavior');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (143, 8, 1, 'Athletic Ability');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (144, 8, 1, 'Strength & Explosion');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (145, 8, 1, 'Competitiveness');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (146, 8, 1, 'Toughness');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (147, 8, 1, 'Mental/Learning');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (148, 8, 1, 'Injury/Durability');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (149, 8, 2, 'Size');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (150, 8, 2, 'Playing Strength');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (151, 8, 2, 'Quickness');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (152, 8, 2, 'Dependability');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (153, 8, 2, 'Instincts');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (154, 8, 3, 'Initial Quickness');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (155, 8, 3, 'Run Block (Point of Attack / Back Side)');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (156, 8, 3, 'Second Level');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (157, 8, 3, 'Pull / Trap');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (158, 8, 3, 'Finish the Block');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (159, 8, 3, 'Pass Set');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (160, 8, 3, 'Hand Use');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (161, 8, 3, 'vs Power Rush');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (162, 8, 3, 'vs Speed Rush');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (163, 8, 3, 'Second Position Value');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (164, 9, 1, 'Personal/Behavior');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (165, 9, 1, 'Athletic Ability');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (166, 9, 1, 'Strength & Explosion');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (167, 9, 1, 'Competitiveness');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (168, 9, 1, 'Toughness');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (169, 9, 1, 'Mental/Learning');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (170, 9, 1, 'Injury/Durability');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (171, 9, 2, 'Size');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (172, 9, 2, 'Playing Strength');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (173, 9, 2, 'Quickness');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (174, 9, 2, 'Dependability');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (175, 9, 2, 'Instincts');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (176, 9, 3, 'Initial Quickness');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (177, 9, 3, 'Run Block (Point of Attack / Back Side)');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (178, 9, 3, 'Second Level');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (179, 9, 3, 'Pull / Trap');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (180, 9, 3, 'Finish the Block');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (181, 9, 3, 'Pass Set');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (182, 9, 3, 'Hand Use');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (183, 9, 3, 'vs Power Rush');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (184, 9, 3, 'vs Speed Rush');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (185, 9, 3, 'Second Position Value');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (186, 10, 1, 'Personal/Behavior');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (187, 10, 1, 'Athletic Ability');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (188, 10, 1, 'Strength & Explosion');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (189, 10, 1, 'Competitiveness');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (190, 10, 1, 'Toughness');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (191, 10, 1, 'Mental/Learning');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (192, 10, 1, 'Injury/Durability');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (193, 10, 2, 'Size');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (194, 10, 2, 'Playing Strength');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (195, 10, 2, 'Quickness');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (196, 10, 2, 'Dependability');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (197, 10, 2, 'Instincts');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (198, 10, 3, 'Initial Quickness');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (199, 10, 3, 'Run Block (Point of Attack / Back Side)');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (200, 10, 3, 'Second Level');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (201, 10, 3, 'Pull / Trap');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (202, 10, 3, 'Finish the Block');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (203, 10, 3, 'Pass Set');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (204, 10, 3, 'Hand Use');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (205, 10, 3, 'vs Power Rush');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (206, 10, 3, 'vs Speed Rush');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (207, 10, 3, 'Second Position Value');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (208, 11, 1, 'Personal/Behavior');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (209, 11, 1, 'Athletic Ability');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (210, 11, 1, 'Strength & Explosion');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (211, 11, 1, 'Competitiveness');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (212, 11, 1, 'Toughness');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (213, 11, 1, 'Mental/Learning');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (214, 11, 1, 'Injury/Durability');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (215, 11, 2, 'Production vs Pass');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (216, 11, 2, 'Instincts');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (217, 11, 2, 'Reactive Athleticism');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (218, 11, 2, 'Playing Speed');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (219, 11, 3, 'Concentration');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (220, 11, 3, 'Pedal/Hips');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (221, 11, 3, 'Close');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (222, 11, 3, 'Zone Coverage');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (223, 11, 3, 'M/M Tight');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (224, 11, 3, 'M/M Off');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (225, 11, 3, 'Ball Skills');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (226, 11, 3, 'Catching Skills');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (227, 11, 3, 'Run Support');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (228, 11, 3, 'Tackling');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (229, 11, 3, 'Third Down Value');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (230, 11, 3, 'Special Teams Value');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (231, 12, 1, 'Personal/Behavior');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (232, 12, 1, 'Athletic Ability');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (233, 12, 1, 'Strength & Explosion');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (234, 12, 1, 'Competitiveness');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (235, 12, 1, 'Toughness');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (236, 12, 1, 'Mental/Learning');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (237, 12, 1, 'Injury/Durability');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (238, 12, 2, 'Pass Production');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (239, 12, 2, 'Run Production');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (240, 12, 2, 'Tackling');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (241, 12, 2, 'Playing Speed');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (242, 12, 2, 'QB the Secondary');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (243, 12, 2, 'Instincts');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (244, 12, 3, 'Deep Zone');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (245, 12, 3, 'Range');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (246, 12, 3, 'Short Zone');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (247, 12, 3, 'Man-to-Man Cover');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (248, 12, 3, 'Ball Skills');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (249, 12, 3, 'Run Support');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (250, 12, 3, 'Third Down Value');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (251, 12, 3, 'Special Teams Value');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (252, 13, 1, 'Personal/Behavior');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (253, 13, 1, 'Athletic Ability');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (254, 13, 1, 'Strength & Explosion');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (255, 13, 1, 'Competitiveness');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (256, 13, 1, 'Toughness');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (257, 13, 1, 'Mental/Learning');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (258, 13, 1, 'Injury/Durability');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (259, 13, 2, 'Pass Production');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (260, 13, 2, 'Run Production');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (261, 13, 2, 'Tackling');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (262, 13, 2, 'Playing Speed');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (263, 13, 2, 'QB the Secondary');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (264, 13, 2, 'Instincts');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (265, 13, 3, 'Deep Zone');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (266, 13, 3, 'Range');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (267, 13, 3, 'Short Zone');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (268, 13, 3, 'Man-to-Man Cover');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (269, 13, 3, 'Ball Skills');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (270, 13, 3, 'Run Support');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (271, 13, 3, 'Third Down Value');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (272, 13, 3, 'Special Teams Value');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (273, 14, 1, 'Personal/Behavior');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (274, 14, 1, 'Athletic Ability');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (275, 14, 1, 'Strength & Explosion');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (276, 14, 1, 'Competitiveness');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (277, 14, 1, 'Toughness');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (278, 14, 1, 'Mental/Learning');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (279, 14, 1, 'Injury/Durability');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (280, 14, 2, 'Pass Production');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (281, 14, 2, 'Run Production');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (282, 14, 2, 'Tackling');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (283, 14, 2, 'Playing Speed');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (284, 14, 2, 'QB the Secondary');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (285, 14, 2, 'Instincts');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (286, 14, 3, 'Deep Zone');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (287, 14, 3, 'Range');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (288, 14, 3, 'Short Zone');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (289, 14, 3, 'Man-to-Man Cover');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (290, 14, 3, 'Ball Skills');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (291, 14, 3, 'Run Support');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (292, 14, 3, 'Third Down Value');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (293, 14, 3, 'Special Teams Value');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (294, 15, 1, 'Personal/Behavior');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (295, 15, 1, 'Athletic Ability');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (296, 15, 1, 'Strength & Explosion');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (297, 15, 1, 'Competitiveness');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (298, 15, 1, 'Toughness');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (299, 15, 1, 'Mental/Learning');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (300, 15, 1, 'Injury/Durability');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (301, 15, 2, 'Run Production');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (302, 15, 2, 'Pass Production');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (303, 15, 2, 'Playing Strength');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (304, 15, 2, 'Set Edge');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (305, 15, 2, 'Instincts');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (306, 15, 2, 'Tackling');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (307, 15, 3, 'Read / React');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (308, 15, 3, 'Lateral Agility');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (309, 15, 3, 'Re-route Ability');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (310, 15, 3, 'Pass Coverage');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (311, 15, 3, 'Pass Rush Ability');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (312, 15, 3, 'Third Down Value');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (313, 16, 1, 'Personal/Behavior');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (314, 16, 1, 'Athletic Ability');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (315, 16, 1, 'Strength & Explosion');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (316, 16, 1, 'Competitiveness');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (317, 16, 1, 'Toughness');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (318, 16, 1, 'Mental/Learning');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (319, 16, 1, 'Injury/Durability');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (320, 16, 2, 'Run Production');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (321, 16, 2, 'Pass Production');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (322, 16, 2, 'Instincts');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (323, 16, 2, 'QB the Defense');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (324, 16, 2, 'Tackling');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (325, 16, 3, 'Read / React');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (326, 16, 3, 'Stack Ability');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (327, 16, 3, 'Shed');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (328, 16, 3, 'Move Through Trash');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (329, 16, 3, 'Range (SL to SL)');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (330, 16, 3, 'Pass Coverage');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (331, 16, 3, 'Blitz');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (332, 16, 3, 'Third Down Value');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (333, 16, 3, 'Special Teams Value');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (334, 17, 1, 'Personal/Behavior');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (335, 17, 1, 'Athletic Ability');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (336, 17, 1, 'Strength & Explosion');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (337, 17, 1, 'Competitiveness');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (338, 17, 1, 'Toughness');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (339, 17, 1, 'Mental/Learning');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (340, 17, 1, 'Injury/Durability');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (341, 17, 2, 'Run Production');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (342, 17, 2, 'Pass Production');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (343, 17, 2, 'Instincts');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (344, 17, 2, 'Tackling');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (345, 17, 2, 'Special Teams Value');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (346, 17, 3, 'Read / React');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (347, 17, 3, 'Stack Ability');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (348, 17, 3, 'Shed');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (349, 17, 3, 'Move Through Trash');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (350, 17, 3, 'Range');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (351, 17, 3, 'Zone Coverage');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (352, 17, 3, 'Man-to-Man Coverage');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (353, 17, 3, 'Blitz Ability');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (354, 17, 3, 'Third Down Value');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (355, 18, 1, 'Personal/Behavior');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (356, 18, 1, 'Athletic Ability');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (357, 18, 1, 'Strength & Explosion');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (358, 18, 1, 'Competitiveness');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (359, 18, 1, 'Toughness');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (360, 18, 1, 'Mental/Learning');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (361, 18, 1, 'Injury/Durability');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (362, 18, 2, 'Run Production');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (363, 18, 2, 'Pass Production');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (364, 18, 2, 'Playing Strength');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (365, 18, 2, 'Set Edge');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (366, 18, 2, 'Instincts');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (367, 18, 2, 'Tackling');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (368, 18, 3, 'Read / React');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (369, 18, 3, 'Lateral Agility');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (370, 18, 3, 'Re-route Ability');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (371, 18, 3, 'Pass Coverage');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (372, 18, 3, 'Pass Rush Ability');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (373, 18, 3, 'Third Down Value');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (374, 19, 1, 'Personal/Behavior');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (375, 19, 1, 'Athletic Ability');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (376, 19, 1, 'Strength & Explosion');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (377, 19, 1, 'Competitiveness');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (378, 19, 1, 'Toughness');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (379, 19, 1, 'Mental/Learning');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (380, 19, 1, 'Injury/Durability');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (381, 19, 2, 'Size');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (382, 19, 2, 'First Step Explosion');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (383, 19, 2, 'Speed Rush');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (384, 19, 2, 'Intensity');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (385, 19, 2, 'Production');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (386, 19, 2, 'Instincts');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (387, 19, 3, 'Hand Use');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (388, 19, 3, 'Power Rush');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (389, 19, 3, 'vs the Run');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (390, 19, 3, 'Tackler');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (391, 19, 3, 'Special Teams Value');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (392, 20, 1, 'Personal/Behavior');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (393, 20, 1, 'Athletic Ability');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (394, 20, 1, 'Strength & Explosion');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (395, 20, 1, 'Competitiveness');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (396, 20, 1, 'Toughness');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (397, 20, 1, 'Mental/Learning');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (398, 20, 1, 'Injury/Durability');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (399, 20, 2, 'Size');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (400, 20, 2, 'Playing Strength');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (401, 20, 2, 'First Step Explosion');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (402, 20, 2, 'Discipline');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (403, 20, 2, 'Instincts');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (404, 20, 3, 'Two-Gap Ability');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (405, 20, 3, 'Play Laterally');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (406, 20, 3, 'Power Rush');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (407, 20, 3, 'Edge Rush');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (408, 20, 3, 'Pursuit Effort');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (409, 20, 3, 'Third Down Value');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (410, 20, 3, 'Burst to QB');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (411, 20, 3, 'Tackling');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (412, 21, 1, 'Personal/Behavior');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (413, 21, 1, 'Athletic Ability');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (414, 21, 1, 'Strength & Explosion');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (415, 21, 1, 'Competitiveness');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (416, 21, 1, 'Toughness');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (417, 21, 1, 'Mental/Learning');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (418, 21, 1, 'Injury/Durability');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (419, 21, 2, 'Size');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (420, 21, 2, 'Playing Strength');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (421, 21, 2, 'Quickness');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (422, 21, 2, 'Discipline');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (423, 21, 2, 'Instincts');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (424, 21, 3, 'Two-Gap Ability');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (425, 21, 3, 'Play Laterally');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (426, 21, 3, 'Power Rush');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (427, 21, 3, 'Pursuit Effort');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (428, 21, 3, 'Third Down Value');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (429, 21, 3, 'Tackling');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (430, 22, 1, 'Personal/Behavior');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (431, 22, 1, 'Athletic Ability');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (432, 22, 1, 'Strength & Explosion');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (433, 22, 1, 'Competitiveness');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (434, 22, 1, 'Toughness');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (435, 22, 1, 'Mental/Learning');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (436, 22, 1, 'Injury/Durability');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (437, 22, 2, 'Size');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (438, 22, 2, 'Playing Strength');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (439, 22, 2, 'Quickness');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (440, 22, 2, 'Discipline');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (441, 22, 2, 'Instincts');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (442, 22, 3, 'Initial Quickness');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (443, 22, 3, 'Two-Gap Ability');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (444, 22, 3, 'Play Laterally');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (445, 22, 3, 'Power Rush');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (446, 22, 3, 'Pursuit Effort');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (447, 22, 3, 'Third Down Value');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (448, 22, 3, 'Tackling');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (449, 23, 1, 'Personal/Behavior');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (450, 23, 1, 'Athletic Ability');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (451, 23, 1, 'Strength & Explosion');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (452, 23, 1, 'Competitiveness');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (453, 23, 1, 'Toughness');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (454, 23, 1, 'Mental/Learning');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (455, 23, 1, 'Injury/Durability');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (456, 23, 2, 'Leg Speed');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (457, 23, 2, 'Leg Strength');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (458, 23, 2, 'Accuracy');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (459, 23, 3, 'Mechanics');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (460, 23, 3, 'Operation Time');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (461, 23, 3, 'vs Elements');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (462, 23, 3, 'Primary Surface');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (463, 23, 3, 'Rise');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (464, 23, 3, 'Tackling');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (465, 23, 3, 'Kickoff Ability');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (466, 24, 1, 'Personal/Behavior');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (467, 24, 1, 'Athletic Ability');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (468, 24, 1, 'Strength & Explosion');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (469, 24, 1, 'Competitiveness');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (470, 24, 1, 'Toughness');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (471, 24, 1, 'Mental/Learning');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (472, 24, 1, 'Injury/Durability');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (473, 24, 2, 'Leg Strength');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (474, 24, 2, 'Punt from End Zone');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (475, 24, 2, 'Hands');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (476, 24, 2, 'Handling Time');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (477, 24, 2, 'Hang Time');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (478, 24, 3, 'Directional Ability');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (479, 24, 3, 'Number of Steps');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (480, 24, 3, 'Left-footed');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (481, 24, 3, 'Holder');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (482, 24, 3, 'Position Versatility');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (483, 26, 1, 'Personal/Behavior');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (484, 26, 1, 'Athletic Ability');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (485, 26, 1, 'Strength & Explosion');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (486, 26, 1, 'Competitiveness');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (487, 26, 1, 'Toughness');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (488, 26, 1, 'Mental/Learning');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (489, 26, 1, 'Injury/Durability');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (490, 26, 2, 'Production');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (491, 26, 2, 'Durability');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (492, 26, 3, 'Accuracy');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (493, 26, 3, 'Velocity');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (494, 26, 3, 'Blocking');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (495, 26, 3, 'Cover Ability');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (496, 26, 3, 'Tackling');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (497, 26, 3, 'Off / Def Position Value');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (498, 26, 3, 'Lateral Agility');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (499, 28, 1, 'Personal/Behavior');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (500, 28, 1, 'Athletic Ability');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (501, 28, 1, 'Strength & Explosion');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (502, 28, 1, 'Competitiveness');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (503, 28, 1, 'Toughness');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (504, 28, 1, 'Mental/Learning');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (505, 28, 1, 'Injury/Durability');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (506, 28, 2, 'Judgement / Poise');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (507, 28, 2, 'Courage');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (508, 28, 3, 'Catching Skills');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (509, 28, 3, 'Run Skills');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (510, 28, 3, 'Playing Speed');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (511, 28, 3, 'Play Maker');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (512, 28, 3, 'Ball Security');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (513, 28, 3, 'vs Elements');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (514, 30, 1, 'Personal/Behavior');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (515, 30, 1, 'Athletic Ability');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (516, 30, 1, 'Strength & Explosion');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (517, 30, 1, 'Competitiveness');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (518, 30, 1, 'Toughness');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (519, 30, 1, 'Mental/Learning');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (520, 30, 1, 'Injury/Durability');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (521, 30, 2, 'Size');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (522, 30, 2, 'Playing Strength');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (523, 30, 2, 'Quickness');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (524, 30, 2, 'Dependability');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (525, 30, 2, 'Instincts');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (526, 30, 3, 'Initial Quickness');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (527, 30, 3, 'Run Block (Point of Attack / Back Side)');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (528, 30, 3, 'Second Level');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (529, 30, 3, 'Pull / Trap');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (530, 30, 3, 'Finish the Block');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (531, 30, 3, 'Pass Set');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (532, 30, 3, 'Hand Use');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (533, 30, 3, 'vs Power Rush');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (534, 30, 3, 'vs Speed Rush');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (535, 30, 3, 'Second Position Value');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (536, 31, 1, 'Personal/Behavior');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (537, 31, 1, 'Athletic Ability');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (538, 31, 1, 'Strength & Explosion');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (539, 31, 1, 'Competitiveness');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (540, 31, 1, 'Toughness');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (541, 31, 1, 'Mental/Learning');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (542, 31, 1, 'Injury/Durability');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (543, 31, 2, 'Size');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (544, 31, 2, 'Playing Strength');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (545, 31, 2, 'Quickness');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (546, 31, 2, 'Dependability');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (547, 31, 2, 'Instincts');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (548, 31, 3, 'Initial Quickness');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (549, 31, 3, 'Run Block (Point of Attack / Back Side)');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (550, 31, 3, 'Second Level');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (551, 31, 3, 'Pull / Trap');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (552, 31, 3, 'Finish the Block');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (553, 31, 3, 'Pass Set');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (554, 31, 3, 'Hand Use');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (555, 31, 3, 'vs Power Rush');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (556, 31, 3, 'vs Speed Rush');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (557, 31, 3, 'Second Position Value');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (558, 32, 1, 'Personal/Behavior');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (559, 32, 1, 'Athletic Ability');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (560, 32, 1, 'Strength & Explosion');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (561, 32, 1, 'Competitiveness');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (562, 32, 1, 'Toughness');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (563, 32, 1, 'Mental/Learning');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (564, 32, 1, 'Injury/Durability');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (565, 32, 2, 'Size');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (566, 32, 2, 'Playing Strength');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (567, 32, 2, 'Quickness');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (568, 32, 2, 'Dependability');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (569, 32, 2, 'Instincts');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (570, 32, 3, 'Initial Quickness');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (571, 32, 3, 'Run Block (Point of Attack / Back Side)');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (572, 32, 3, 'Second Level');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (573, 32, 3, 'Pull / Trap');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (574, 32, 3, 'Finish the Block');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (575, 32, 3, 'Pass Set');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (576, 32, 3, 'Hand Use');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (577, 32, 3, 'vs Power Rush');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (578, 32, 3, 'vs Speed Rush');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (579, 32, 3, 'Second Position Value');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (580, 33, 1, 'Personal/Behavior');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (581, 33, 1, 'Athletic Ability');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (582, 33, 1, 'Strength & Explosion');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (583, 33, 1, 'Competitiveness');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (584, 33, 1, 'Toughness');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (585, 33, 1, 'Mental/Learning');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (586, 33, 1, 'Injury/Durability');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (587, 33, 2, 'Size');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (588, 33, 2, 'Playing Strength');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (589, 33, 2, 'Quickness');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (590, 33, 2, 'Dependability');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (591, 33, 2, 'Instincts');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (592, 33, 3, 'Initial Quickness');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (593, 33, 3, 'Run Block (Point of Attack / Back Side)');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (594, 33, 3, 'Second Level');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (595, 33, 3, 'Pull / Trap');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (596, 33, 3, 'Finish the Block');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (597, 33, 3, 'Pass Set');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (598, 33, 3, 'Hand Use');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (599, 33, 3, 'vs Power Rush');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (600, 33, 3, 'vs Speed Rush');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (601, 33, 3, 'Second Position Value');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (602, 34, 1, 'Personal/Behavior');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (603, 34, 1, 'Athletic Ability');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (604, 34, 1, 'Strength & Explosion');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (605, 34, 1, 'Competitiveness');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (606, 34, 1, 'Toughness');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (607, 34, 1, 'Mental/Learning');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (608, 34, 1, 'Injury/Durability');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (609, 34, 2, 'Production vs Pass');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (610, 34, 2, 'Instincts');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (611, 34, 2, 'Reactive Athleticism');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (612, 34, 2, 'Playing Speed');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (613, 34, 3, 'Concentration');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (614, 34, 3, 'Pedal/Hips');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (615, 34, 3, 'Close');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (616, 34, 3, 'Zone Coverage');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (617, 34, 3, 'M/M Tight');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (618, 34, 3, 'M/M Off');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (619, 34, 3, 'Ball Skills');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (620, 34, 3, 'Catching Skills');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (621, 34, 3, 'Run Support');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (622, 34, 3, 'Tackling');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (623, 34, 3, 'Third Down Value');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (624, 34, 3, 'Special Teams Value');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (625, 35, 1, 'Personal/Behavior');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (626, 35, 1, 'Athletic Ability');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (627, 35, 1, 'Strength & Explosion');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (628, 35, 1, 'Competitiveness');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (629, 35, 1, 'Toughness');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (630, 35, 1, 'Mental/Learning');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (631, 35, 1, 'Injury/Durability');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (632, 35, 2, 'Run Production');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (633, 35, 2, 'Pass Production');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (634, 35, 2, 'Instincts');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (635, 35, 2, 'QB the Defense');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (636, 35, 2, 'Tackling');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (637, 35, 3, 'Read / React');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (638, 35, 3, 'Stack Ability');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (639, 35, 3, 'Shed');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (640, 35, 3, 'Move Through Trash');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (641, 35, 3, 'Range (SL to SL)');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (642, 35, 3, 'Pass Coverage');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (643, 35, 3, 'Blitz');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (644, 35, 3, 'Third Down Value');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (645, 35, 3, 'Special Teams Value');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (646, 36, 1, 'Personal/Behavior');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (647, 36, 1, 'Athletic Ability');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (648, 36, 1, 'Strength & Explosion');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (649, 36, 1, 'Competitiveness');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (650, 36, 1, 'Toughness');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (651, 36, 1, 'Mental/Learning');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (652, 36, 1, 'Injury/Durability');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (653, 36, 2, 'Size');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (654, 36, 2, 'Playing Strength');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (655, 36, 2, 'Quickness');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (656, 36, 2, 'Discipline');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (657, 36, 2, 'Instincts');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (658, 36, 3, 'Initial Quickness');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (659, 36, 3, 'Two-Gap Ability');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (660, 36, 3, 'Play Laterally');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (661, 36, 3, 'Power Rush');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (662, 36, 3, 'Pursuit Effort');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (663, 36, 3, 'Third Down Value');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (664, 36, 3, 'Tackling');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (665, 37, 1, 'Personal/Behavior');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (666, 37, 1, 'Athletic Ability');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (667, 37, 1, 'Strength & Explosion');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (668, 37, 1, 'Competitiveness');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (669, 37, 1, 'Toughness');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (670, 37, 1, 'Mental/Learning');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (671, 37, 1, 'Injury/Durability');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (672, 37, 2, 'Size');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (673, 37, 2, 'Playing Strength');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (674, 37, 2, 'Quickness');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (675, 37, 2, 'Dependability');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (676, 37, 2, 'Instincts');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (677, 37, 3, 'Initial Quickness');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (678, 37, 3, 'Run Block (Point of Attack / Back Side)');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (679, 37, 3, 'Second Level');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (680, 37, 3, 'Pull / Trap');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (681, 37, 3, 'Finish the Block');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (682, 37, 3, 'Pass Set');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (683, 37, 3, 'Hand Use');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (684, 37, 3, 'vs Power Rush');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (685, 37, 3, 'vs Speed Rush');
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_position_type_id, go_scouting_eval_criteria_type_id, name)
  VALUES  (686, 37, 3, 'Second Position Value');

-- deleting team_scouting_eval_score, team_target_profile, team_scouting_eval_criteria to re-populate
DELETE FROM godeepmobile.team_scouting_eval_score;
DELETE FROM godeepmobile.team_target_profile;
DELETE FROM godeepmobile.team_scouting_eval_criteria;

-- populating team_scouting_eval_criteria table
INSERT INTO godeepmobile.team_scouting_eval_criteria
             (name, go_importance_id, go_team_id, go_position_type_id, go_scouting_eval_criteria_type_id)
  SELECT  go_eval_criteria.name, go_eval_criteria.go_importance_id, team.id, go_eval_criteria.go_position_type_id, go_eval_criteria.go_scouting_eval_criteria_type_id
  FROM go_team team, go_scouting_eval_criteria go_eval_criteria
  ORDER BY team.id, go_eval_criteria.go_position_type_id, go_eval_criteria.go_scouting_eval_criteria_type_id, go_eval_criteria.id;

-- populating team_target_profile table
INSERT INTO godeepmobile.team_target_profile
               (go_team_id, go_position_type_id, team_scouting_eval_criteria_id, go_scouting_eval_criteria_type_id, calculated_target_score)
    SELECT  team_eval_criteria.go_team_id, team_eval_criteria.go_position_type_id, team_eval_criteria.id, team_eval_criteria.go_scouting_eval_criteria_type_id, 4 as calculated_target_score
    FROM team_scouting_eval_criteria team_eval_criteria
    ORDER BY team_eval_criteria.go_team_id, team_eval_criteria.go_position_type_id, team_eval_criteria.go_scouting_eval_criteria_type_id, team_eval_criteria.id;

--rollback ALTER TABLE godeepmobile.go_scouting_eval_criteria DROP COLUMN go_position_type_id RESTRICT; ALTER TABLE godeepmobile.go_scouting_eval_criteria DROP COLUMN go_importance_id RESTRICT;
