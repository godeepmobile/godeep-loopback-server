--liquibase formatted sql

--changeset rambert:171 runOnChange:true splitStatements:false stripComments:false
--comment updating refresh_player_general_season_ranking function

CREATE OR REPLACE FUNCTION godeepmobile.refresh_player_general_season_ranking (
  team_id_parm uuid,
  season_parm integer)
  RETURNS void AS
$BODY$
DECLARE
  record_value RECORD;
  team_conf RECORD;
  record_exists integer;
BEGIN

  FOR record_value IN
    SELECT
      grade_stat.go_team_id,
      player.id AS team_player_id,
      grade_stat.season,
      eval_stat.rank_overall AS overall_evaluation_rank,
      grade_stat.overall_grade_game_rank,
      grade_stat.overall_grade_game,
      eval_stat.target_match,
      eval_stat.overall_eval_avg,
      eval_stat.major_factors_eval_avg,
      eval_stat.critical_factors_eval_avg,
      eval_stat.position_skills_eval_avg
    FROM godeepmobile.team_player_season_stat grade_stat FULL OUTER JOIN
         godeepmobile.view_team_player player
         ON grade_stat.team_player_id = player.id
         FULL OUTER JOIN godeepmobile.player_evaluation_season_stat eval_stat
         ON player.id = eval_stat.team_player_id
    WHERE player.go_team_id = team_id_parm::uuid
          AND (grade_stat.season = season_parm
               OR eval_stat.season = grade_stat.season)
    ORDER BY eval_stat.rank_overall, grade_stat.overall_grade_game_rank

      LOOP
      -- check if record already exists
      SELECT COUNT(id) INTO record_exists
      FROM godeepmobile.player_rank_overall
      WHERE go_team_id = team_id_parm::uuid
        AND team_player_id = record_value.team_player_id
        AND season = season_parm;

      IF record_exists = 0 THEN
        INSERT INTO godeepmobile.player_rank_overall (
          go_team_id,
          team_player_id,
          season,
          overall_evaluation_rank,
          overall_grade_game_rank,
          --rank_at_post,
          game_grade_avg,
          target_match,
          overall_eval_avg,
          major_factors_eval_avg,
          critical_factors_eval_avg,
          position_skills_eval_avg
        )
        VALUES (
          team_id_parm::uuid,
          record_value.team_player_id,
          record_value.season,
          record_value.overall_evaluation_rank,
          record_value.overall_grade_game_rank,
          --0, -- rank at post tbd
          record_value.overall_grade_game,
          record_value.target_match,
          record_value.overall_eval_avg,
          record_value.major_factors_eval_avg,
          record_value.critical_factors_eval_avg,
          record_value.position_skills_eval_avg
        );
      ELSE
        UPDATE
            godeepmobile.player_rank_overall
        SET
        overall_evaluation_rank = record_value.overall_evaluation_rank,
        overall_grade_game_rank = record_value.overall_grade_game_rank,
        game_grade_avg = record_value.overall_grade_game,
        target_match = record_value.target_match,
        overall_eval_avg = record_value.overall_eval_avg,
        major_factors_eval_avg = record_value.major_factors_eval_avg,
        critical_factors_eval_avg = record_value.critical_factors_eval_avg,
        position_skills_eval_avg = record_value.position_skills_eval_avg
        WHERE go_team_id = team_id_parm::uuid
          AND team_player_id = record_value.team_player_id
          AND season = record_value.season;
      END IF;
      END LOOP;

      -- updating overall ranking

      SELECT  (conf.grade_base - (conf.grade_increment * 2)) AS grade_eval_min,
        (conf.grade_base + (conf.grade_increment * 2)) AS grade_eval_max,
        conf.scout_eval_score_min,
        conf.scout_eval_score_max  INTO team_conf
      FROM godeepmobile.team_configuration conf
      WHERE conf.go_team_id = team_id_parm::uuid;

      UPDATE
          godeepmobile.player_rank_overall
      SET
          rank_overall = null
      WHERE go_team_id = team_id_parm::uuid
        AND season = season_parm;

        -- update ranking
        UPDATE godeepmobile.player_rank_overall AS rank_overall
        SET rank_overall=subquery.rank
        FROM (
  	SELECT
        rankquery.go_team_id,
        rankquery.team_player_id,
        rankquery.season,
        RANK() OVER (ORDER BY rankquery.overall_grade_evaluation_rank_value DESC) AS rank
      FROM (
        SELECT grade_stat.go_team_id,
           player.id AS team_player_id,
           grade_stat.season,
           ((CASE WHEN grade_stat.overall_grade_game = 0 THEN NULL ELSE grade_stat.overall_grade_game END * 2) +
           -- converting evaluation value to the same interval values as grade
           -- (((OldValue - EvalMin) * (GradeMax - GradeMin)) / (EvalMax - EvalMin)) + GradeMin
           ((((CASE WHEN eval_stat.overall_eval_avg = 0 THEN NULL ELSE eval_stat.overall_eval_avg END - team_conf.scout_eval_score_min)
            * (team_conf.grade_eval_max - team_conf.grade_eval_min))
            / (team_conf.scout_eval_score_max - team_conf.scout_eval_score_min))
            + team_conf.grade_eval_min)) as overall_grade_evaluation_rank_value
          FROM godeepmobile.team_player_season_stat grade_stat
               JOIN godeepmobile.view_team_player player
               ON grade_stat.team_player_id = player.id
               JOIN godeepmobile.player_evaluation_season_stat eval_stat
               ON player.id = eval_stat.team_player_id
          WHERE player.go_team_id = team_id_parm::uuid
                AND grade_stat.season = season_parm
                AND eval_stat.season = grade_stat.season
                AND grade_stat.overall_grade_game > 0
	              AND eval_stat.overall_eval_avg > 0
        ) as rankquery
        WHERE rankquery.overall_grade_evaluation_rank_value IS NOT NULL
        ) AS subquery
        WHERE rank_overall.go_team_id = subquery.go_team_id
            AND rank_overall.team_player_id = subquery.team_player_id
            AND rank_overall.season = subquery.season;

END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

--rollback DROP FUNCTION godeepmobile.refresh_player_general_season_ranking(uuid, integer);
