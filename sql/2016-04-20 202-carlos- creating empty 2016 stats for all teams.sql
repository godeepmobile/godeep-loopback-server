﻿--liquibase formatted sql

--changeset carlos:202 runOnChange:true splitStatements:false stripComments:false
--comment created empty 2016 stats for all teams

SELECT refresh_season_stats(id, 2016) FROM go_team;

--rollback DELETE FROM team_player_season_stats WHERE season = 2016;