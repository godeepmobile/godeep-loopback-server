﻿--liquibase formatted sql

--changeset carlos:186 runOnChange:true splitStatements:false stripComments:false
--comment added team_player_assignment

CREATE TABLE team_player_assignment (
	id serial NOT NULL,
	team_player_id uuid NOT NULL,
	go_team_id uuid NOT NULL,
	go_user_team_assignment_id integer,
	jersey_number integer,
	position_1 integer,
	position_2 integer,
	position_3 integer,
	position_st integer,
	start_date timestamp DEFAULT ('now'::text)::timestamp NOT NULL,
	end_date timestamp,
	is_veteran boolean DEFAULT false,
	CONSTRAINT pk_team_player_assignment PRIMARY KEY (id),
	CONSTRAINT fk_team_player_assignment_team_player FOREIGN KEY (team_player_id) REFERENCES team_player (id),
	CONSTRAINT fk_team_player_assignment_go_team FOREIGN KEY (go_team_id) REFERENCES go_team (id)	
);

CREATE OR REPLACE VIEW view_team_player_assignment AS
		SELECT tpa.*,  
			CASE WHEN EXTRACT(MONTH FROM tpa.start_date)::integer <= 2 
				THEN EXTRACT(YEAR FROM tpa.start_date)::integer - 1 
				ELSE EXTRACT(YEAR FROM tpa.start_date)::integer 
			END AS season,
			(WITH offense AS (
				 SELECT go_position_type.id
				   FROM go_position_type
				  WHERE go_position_type.go_platoon_type_id = 1
				)
			 SELECT
				CASE
				    WHEN (tpa.position_1 IN ( SELECT offense.id
				       FROM offense)) OR (tpa.position_2 IN ( SELECT offense.id
				       FROM offense)) OR (tpa.position_3 IN ( SELECT offense.id
				       FROM offense)) THEN true
				    ELSE false
				END AS "case") AS is_offense,
			(WITH defense AS (
				 SELECT go_position_type.id
				   FROM go_position_type
				  WHERE go_position_type.go_platoon_type_id = 2
				)
			 SELECT
				CASE
				    WHEN (tpa.position_1 IN ( SELECT defense.id
				       FROM defense)) OR (tpa.position_2 IN ( SELECT defense.id
				       FROM defense)) OR (tpa.position_3 IN ( SELECT defense.id
				       FROM defense)) THEN true
				    ELSE false
				END AS "case") AS is_defense,
			CASE
			    WHEN tpa.position_st IS NULL THEN false
			    ELSE true
			END AS is_special_teams,
			(SELECT tpcs.id
			FROM team_player_career_stat tpcs
			WHERE tpa.team_player_id = tpcs.team_player_id AND tpcs.go_team_id = tpa.go_team_id) AS team_player_career_stat,
			ARRAY( SELECT tpss.id
			FROM team_player_season_stat tpss
			WHERE tpa.team_player_id = tpss.team_player_id AND tpss.go_team_id = tpa.go_team_id
			ORDER BY tpss.season DESC) AS team_player_season_stats
		FROM team_player_assignment tpa 
		WHERE tpa.end_date IS NULL;

-- moving the data from team_player to team_player_assignment
INSERT INTO team_player_assignment (
	team_player_id,
	go_team_id,
	go_user_team_assignment_id,
	jersey_number,
	position_1,
	position_2,
	position_3,
	position_st,
	start_date,
	end_date,
	is_veteran
) SELECT 
	id, 
	go_team_id, 
	go_user_team_assignment_id,
	jersey_number, 
	team_position_type_id_pos_1, 
	team_position_type_id_pos_2, 
	team_position_type_id_pos_3, 
	team_position_type_id_pos_st,
	start_date,
	end_date,
	is_veteran
FROM team_player
WHERE is_prospect = false;

--rollback DROP VIEW view_team_player_assignment; DROP TABLE team_player_assignment;