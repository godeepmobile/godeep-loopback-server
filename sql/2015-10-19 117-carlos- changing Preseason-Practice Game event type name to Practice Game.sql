﻿--liquibase formatted sql

--changeset carlos:117 runOnChange:true
--comment changed 'Preseason/Practice Game' event type name to 'Practice Game'

UPDATE go_event_type SET name = 'Practice Game', short_name = 'Practice-Game' WHERE id = 3;

--rollback ALTER TABLE go_event_type SET name = 'Preseason/Practice Game', short_name = 'Preseason-Practice' WHERE id = 3;