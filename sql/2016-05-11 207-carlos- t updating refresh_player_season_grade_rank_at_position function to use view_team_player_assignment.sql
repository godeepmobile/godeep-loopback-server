﻿--liquibase formatted sql

--changeset carlos:207 runOnChange:true splitStatements:false stripComments:false
--comment updated refresh_player_season_grade_rank_at_position function to use view_team_player_assignment

CREATE OR REPLACE FUNCTION godeepmobile.refresh_player_season_grade_rank_at_position (
  team_id_parm uuid,
  season_parm integer,
  position_id_parm integer)
  RETURNS void AS
$BODY$
DECLARE
  record_value RECORD;
  record_exists integer;
BEGIN

  FOR record_value IN
    SELECT
      season_stat.go_team_id,
      season_stat.team_player_id,
      season_stat.season,
      player.position_1,
      DENSE_RANK() OVER (ORDER BY season_stat.overall_grade_game DESC) AS rank_game_at_post
    FROM godeepmobile.view_team_player_season_stat season_stat,
      godeepmobile.view_team_player_assignment player
    WHERE season_stat.go_team_id = team_id_parm::uuid
      AND player.team_player_id = season_stat.team_player_id
      AND season_stat.season = season_parm
      AND season_stat.overall_grade_game IS NOT NULL
      AND season_stat.overall_grade_game > 0
      AND player.position_1 = position_id_parm
      AND player.season = season_parm

      LOOP
      -- check if record already exists
      SELECT COUNT(*) INTO record_exists
      FROM godeepmobile.player_grade_season_rank_at_position_stat
      WHERE go_team_id = team_id_parm::uuid
        AND team_player_id = record_value.team_player_id
        AND season = season_parm
        AND go_position_type_id = position_id_parm;
      IF record_exists = 0 THEN
        INSERT INTO godeepmobile.player_grade_season_rank_at_position_stat (
          go_team_id,
          team_player_id,
          season,
          go_position_type_id,
          rank_game_at_post
        )
        VALUES (
          team_id_parm::uuid,
          record_value.team_player_id,
          season_parm,
          position_id_parm,
          record_value.rank_game_at_post
        );
      ELSE
        UPDATE
            godeepmobile.player_grade_season_rank_at_position_stat
        SET
          rank_game_at_post = record_value.rank_game_at_post
        WHERE go_team_id = team_id_parm::uuid
            AND team_player_id = record_value.team_player_id
            AND season = season_parm
            AND go_position_type_id = position_id_parm;
      END IF;
      END LOOP;

      FOR record_value IN
        SELECT
          season_stat.go_team_id,
          season_stat.team_player_id,
          season_stat.season,
          player.team_position_type_id_pos_1,
          RANK() OVER (ORDER BY season_stat.overall_grade_practice DESC) AS rank_practice_at_post
        FROM godeepmobile.view_team_player_season_stat season_stat,
          godeepmobile.view_team_player player
        WHERE season_stat.go_team_id = team_id_parm::uuid
          AND player.id = season_stat.team_player_id
          AND season_stat.season = season_parm
          AND season_stat.overall_grade_practice IS NOT NULL
          AND season_stat.overall_grade_practice > 0
          AND player.team_position_type_id_pos_1 = position_id_parm

          LOOP
          -- check if record already exists
          SELECT COUNT(*) INTO record_exists
          FROM godeepmobile.player_grade_season_rank_at_position_stat
          WHERE go_team_id = team_id_parm::uuid
            AND team_player_id = record_value.team_player_id
            AND season = season_parm
            AND go_position_type_id = position_id_parm;

          IF record_exists = 0 THEN
            INSERT INTO godeepmobile.player_grade_season_rank_at_position_stat (
              go_team_id,
              team_player_id,
              season,
              go_position_type_id,
              rank_practice_at_post
            )
            VALUES (
              team_id_parm::uuid,
              record_value.team_player_id,
              season_parm,
              position_id_parm,
              record_value.rank_practice_at_post
            );
          ELSE
            UPDATE
                godeepmobile.player_grade_season_rank_at_position_stat
            SET
              rank_practice_at_post = record_value.rank_practice_at_post
            WHERE go_team_id = team_id_parm::uuid
                AND team_player_id = record_value.team_player_id
                AND season = season_parm
                AND go_position_type_id = position_id_parm;
          END IF;
          END LOOP;

END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

--rollback DROP FUNCTION godeepmobile.refresh_player_season_grade_rank_at_position(uuid, integer, integer);
