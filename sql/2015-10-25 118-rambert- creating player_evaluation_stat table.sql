--liquibase formatted sql

--changeset rambert:118 runOnChange:true stripComments:false splitStatements:false
--comment create player_evaluation_stat table

CREATE TABLE godeepmobile.player_evaluation_stat (
  id                                serial  NOT NULL,
  go_team_id                        uuid NOT NULL,
  team_player_id                    uuid NOT NULL,
  go_user_id                        integer NOT NULL,
  team_scouting_eval_score_group_id integer NOT NULL,
  season                            integer NOT NULL,
  target_match                      smallint,
  overall_eval_avg                  numeric(100,2),
  major_factors_eval_avg            numeric(100,2),
  critical_factors_eval_avg         numeric(100,2),
  position_skills_eval_avg          numeric(100,2),
  rank_overall                      smallint,
  rank_at_post                      smallint,
  date                              date,
  CONSTRAINT pk_player_evaluation_stat PRIMARY KEY ( id ),
  CONSTRAINT fk_player_evaluation_stat_go_team FOREIGN KEY (go_team_id)
      REFERENCES go_team (id),
  CONSTRAINT fk_player_evaluation_stat_team_player FOREIGN KEY (team_player_id)
      REFERENCES team_player (id),
  CONSTRAINT fk_player_evaluation_stat_team_go_user FOREIGN KEY (go_user_id)
      REFERENCES go_user (id),
  CONSTRAINT fk_player_evaluation_stat_team_scouting_eval_score_group FOREIGN KEY (team_scouting_eval_score_group_id)
      REFERENCES team_scouting_eval_score_group (id)
);

--rollback DROP TABLE IF EXISTS  godeepmobile.player_evaluation_stat;
