﻿--liquibase formatted sql

--changeset carlos:91 runOnChange:true
--comment added sort_index to go_position_type

ALTER TABLE go_position_type ADD sort_index integer;

UPDATE go_position_type SET sort_index = 1 WHERE id = 33;
UPDATE go_position_type SET sort_index = 2 WHERE id = 31;
UPDATE go_position_type SET sort_index = 3 WHERE id = 8;
UPDATE go_position_type SET sort_index = 4 WHERE id = 30;
UPDATE go_position_type SET sort_index = 5 WHERE id = 32;
UPDATE go_position_type SET sort_index = 6 WHERE id = 10;
UPDATE go_position_type SET sort_index = 7 WHERE id = 9;
UPDATE go_position_type SET sort_index = 8 WHERE id = 37;
UPDATE go_position_type SET sort_index = 9 WHERE id = 7;
UPDATE go_position_type SET sort_index = 10 WHERE id = 3;
UPDATE go_position_type SET sort_index = 11 WHERE id = 1;
UPDATE go_position_type SET sort_index = 12 WHERE id = 6;
UPDATE go_position_type SET sort_index = 13 WHERE id = 5;
UPDATE go_position_type SET sort_index = 14 WHERE id = 4;
UPDATE go_position_type SET sort_index = 15 WHERE id = 19;
UPDATE go_position_type SET sort_index = 16 WHERE id = 21;
UPDATE go_position_type SET sort_index = 17 WHERE id = 22;
UPDATE go_position_type SET sort_index = 18 WHERE id = 36;
UPDATE go_position_type SET sort_index = 19 WHERE id = 20;
UPDATE go_position_type SET sort_index = 20 WHERE id = 18;
UPDATE go_position_type SET sort_index = 21 WHERE id = 15;
UPDATE go_position_type SET sort_index = 22 WHERE id = 16;
UPDATE go_position_type SET sort_index = 23 WHERE id = 2;
UPDATE go_position_type SET sort_index = 24 WHERE id = 17;
UPDATE go_position_type SET sort_index = 25 WHERE id = 35;
UPDATE go_position_type SET sort_index = 26 WHERE id = 11;
UPDATE go_position_type SET sort_index = 27 WHERE id = 34;
UPDATE go_position_type SET sort_index = 28 WHERE id = 14;
UPDATE go_position_type SET sort_index = 29 WHERE id = 13;
UPDATE go_position_type SET sort_index = 30 WHERE id = 12;
UPDATE go_position_type SET sort_index = 31 WHERE id = 26;
UPDATE go_position_type SET sort_index = 32 WHERE id = 27;
UPDATE go_position_type SET sort_index = 33 WHERE id = 23;
UPDATE go_position_type SET sort_index = 34 WHERE id = 24;
UPDATE go_position_type SET sort_index = 35 WHERE id = 28;
UPDATE go_position_type SET sort_index = 36 WHERE id = 25;

--rollback ALTER TABLE go_position_type DROP COLUMN sort_index;