--liquibase formatted sql

--changeset rambert:126 runOnChange:true splitStatements:false stripComments:false
--comment create player_grade_evaluation_ranking function

-- Renaming function to refresh_player_evaluation_ranking
DROP FUNCTION IF EXISTS godeepmobile.player_grade_evaluation_ranking(uuid, integer);
DROP FUNCTION IF EXISTS godeepmobile.refresh_player_evaluation_ranking(uuid, integer);

CREATE OR REPLACE FUNCTION godeepmobile.refresh_player_evaluation_ranking(
  team_id_parm uuid,
  season_parm integer)
  RETURNS void AS
$BODY$
DECLARE
	player_score_overall_ranking numeric;
  evaluation_exists integer;
  _records jsonb;
BEGIN

    -- gets player score overall using multipliers and position importance
    -- SELECT
    --     ROUND(AVG(CASE
    --                 WHEN team_criteria.is_critical
    --                   THEN
    --                     CASE
    --                       WHEN eval_score.score > ((team_config.scout_eval_score_min + team_config.scout_eval_score_max) / 2 ::numeric) --median_score
    --                         THEN eval_score.score * team_config.player_evaluation_greater_multiplier
    --                       ELSE eval_score.score * team_config.player_evaluation_lesser_multiplier
    --                     END
    --                   ELSE eval_score.score
    --                 END) * position_importance.go_importance_id, 2) INTO player_score_overall_ranking
    --     FROM godeepmobile.team_scouting_eval_score eval_score,
    --       godeepmobile.team_scouting_eval_criteria team_criteria,
    --       godeepmobile.team_scouting_eval_score_group score_group,
    --       godeepmobile.team_position_importance position_importance,
    --       godeepmobile.team_configuration team_config
    --     WHERE eval_score.go_team_id = team_id_parm::uuid
    --       and eval_score.go_team_id = team_criteria.go_team_id
    --       and eval_score.go_team_id = team_config.go_team_id
    --       and eval_score.go_team_id = position_importance.go_team_id
    --       and score_group.go_position_type_id = position_importance.go_position_type_id
    --       and eval_score.team_scouting_eval_score_group_id = score_group.id
    --       and eval_score.team_scouting_eval_criteria_id = team_criteria.id
    --       and score_group.id = evaluation_id_parm
    --    GROUP BY position_importance.go_importance_id;

  --
  -- SELECT COUNT(id) INTO evaluation_exists
  -- FROM godeepmobile.player_evaluation_stat
  -- WHERE go_team_id = team_id_parm::uuid
  --   AND team_scouting_eval_score_group_id = evaluation_id_parm;
  --
  -- RAISE NOTICE 'evaluation_exists %', evaluation_exists;
	-- IF evaluation_exists > 0 THEN
  --
  --   UPDATE
  --       godeepmobile.player_evaluation_stat
  --   SET
  --       overall_eval_avg_rank = player_score_overall_ranking
  --   WHERE go_team_id = team_id_parm::uuid
  --     AND team_scouting_eval_score_group_id = evaluation_id_parm;

    -- update ranking
    UPDATE player_evaluation_stat
    SET rank_overall=subquery.rank
    FROM (SELECT stat.id, --stat.overall_eval_avg_rank, stat.target_match, importance.go_importance_id, player.height, player.last_name,
           RANK() OVER (ORDER BY stat.overall_eval_avg_rank DESC, stat.target_match DESC, importance.go_importance_id DESC) AS rank
    	FROM godeepmobile.player_evaluation_stat stat,
    	     godeepmobile.team_scouting_eval_score_group score_group,
    	     godeepmobile.team_position_importance importance,
    	     godeepmobile.view_team_player player
    	WHERE stat.go_team_id = team_id_parm::uuid
            AND stat.season = season_parm
            AND stat.season = score_group.season
    	      AND stat.go_team_id = importance.go_team_id
    	      AND stat.team_scouting_eval_score_group_id = score_group.id
    	      AND score_group.go_position_type_id = importance.go_position_type_id
    	      AND player.id = stat.team_player_id) AS subquery
    WHERE player_evaluation_stat.id = subquery.id;


END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

--rollback DROP FUNCTION godeepmobile.refresh_player_evaluation_ranking(uuid, integer);
