﻿--liquibase formatted sql

--changeset carlos:83 runOnChange:true
--comment added team_platoon_configuration table

CREATE TABLE team_platoon_configuration (
	id serial NOT NULL,
	go_team_id uuid NOT NULL,
	go_platoon_type_id integer NOT NULL,
	play_data_fields jsonb,
	num_plays integer,
	CONSTRAINT pk_team_platoon_configuration PRIMARY KEY (id),
	CONSTRAINT fk_team_platoon_configuration_go_team FOREIGN KEY (go_team_id) REFERENCES go_team (id),
	CONSTRAINT fk_team_platoon_configuration_go_platoon_type FOREIGN KEY (go_platoon_type_id) REFERENCES go_platoon_type (id)
);

--rollback DROP TABLE IF EXISTS team_platoon_configuration;

