--liquibase formatted sql

--changeset carlos:75 runOnChange:true stripComments:false splitStatements:false
--comment updated stats views

-- Season Stats
CREATE OR REPLACE VIEW view_team_player_season_stat AS
	SELECT * FROM team_player_season_stat
	WHERE team_player_id IN (SELECT id FROM team_player WHERE end_date IS NULL);

-- Game Stats
CREATE OR REPLACE VIEW view_team_player_game_stat AS
	SELECT * FROM team_player_game_stat
	WHERE team_player_id IN (SELECT id FROM team_player WHERE end_date IS NULL);
-- 
-- Game Cutup Stats
CREATE OR REPLACE VIEW view_team_player_game_cutup_stat AS
	SELECT * FROM team_player_game_cutup_stat
	WHERE team_player_id IN (SELECT id FROM team_player WHERE end_date IS NULL);

-- Career Stats
CREATE OR REPLACE VIEW view_team_player_career_stat AS
	SELECT * FROM team_player_career_stat
	WHERE team_player_id IN (SELECT id FROM team_player WHERE end_date IS NULL);

--rollback DROP VIEW IF EXISTS view_team_player_season_stat; DROP VIEW IF EXISTS view_team_player_game_stat; DROP VIEW IF EXISTS view_team_player_game_cutup_stat; DROP VIEW IF EXISTS view_team_player_career_stat; 