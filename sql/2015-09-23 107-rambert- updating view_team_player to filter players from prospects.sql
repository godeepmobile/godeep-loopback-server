--liquibase formatted sql

--changeset rambert:107 runOnChange:true stripComments:false splitStatements:false
--comment update view_team_player to filter players from prospects

CREATE OR REPLACE VIEW view_team_player AS
 SELECT team_player.id,
    team_player.last_name,
    team_player.middle_name,
    team_player.first_name,
    team_player.go_team_id,
    team_player.data,
    team_player.go_user_team_assignment_id,
    team_player.class_year,
    team_player.birth_date,
    team_player.jersey_number,
    team_player.season,
    team_player.height,
    team_player.weight,
    team_player.graduation_year,
    team_player.wingspan,
    team_player.forty_yard,
    team_player.go_body_type_id,
    team_player.hand_size,
    team_player.vertical_jump,
    team_player.email,
    team_player.sat,
    team_player.act,
    team_player.gpa,
    team_player.go_state_id,
    team_player.team_position_type_id_pos_1,
    team_player.team_position_type_id_pos_2,
    team_player.team_position_type_id_pos_3,
    team_player.team_position_type_id_pos_st,
    team_player.high_school,
    team_player.coach,
    team_player.coach_phone,
    team_player.go_level_id,
    team_player.is_veteran,
    team_player.start_date,
    team_player.end_date,
        CASE
            WHEN team_player.team_position_type_id_pos_st IS NULL THEN false
            ELSE true
        END AS is_special_teams,
    ( WITH offense AS (
                 SELECT go_position_type.id
                   FROM go_position_type
                  WHERE go_position_type.go_platoon_type_id = 1
                )
         SELECT
                CASE
                    WHEN (team_player.team_position_type_id_pos_1 IN ( SELECT offense.id
                       FROM offense)) OR (team_player.team_position_type_id_pos_2 IN ( SELECT offense.id
                       FROM offense)) OR (team_player.team_position_type_id_pos_3 IN ( SELECT offense.id
                       FROM offense)) THEN true
                    ELSE false
                END AS "case") AS is_offense,
    ( WITH defense AS (
                 SELECT go_position_type.id
                   FROM go_position_type
                  WHERE go_position_type.go_platoon_type_id = 2
                )
         SELECT
                CASE
                    WHEN (team_player.team_position_type_id_pos_1 IN ( SELECT defense.id
                       FROM defense)) OR (team_player.team_position_type_id_pos_2 IN ( SELECT defense.id
                       FROM defense)) OR (team_player.team_position_type_id_pos_3 IN ( SELECT defense.id
                       FROM defense)) THEN true
                    ELSE false
                END AS "case") AS is_defense,
    ( SELECT tpcs.id
           FROM team_player_career_stat tpcs
          WHERE team_player.id = tpcs.team_player_id) AS team_player_career_stat,
    ARRAY( SELECT tpss.id
           FROM team_player_season_stat tpss
          WHERE team_player.id = tpss.team_player_id
          ORDER BY tpss.season DESC) AS team_player_season_stats
   FROM team_player
  WHERE team_player.end_date IS NULL AND team_player.is_prospect = false
  ORDER BY team_player.jersey_number;

--rollback CREATE OR REPLACE VIEW view_team_player AS SELECT team_player.id, team_player.last_name, team_player.middle_name, team_player.first_name, team_player.go_team_id, team_player.data, team_player.go_user_team_assignment_id, team_player.class_year, team_player.birth_date, team_player.jersey_number, team_player.season, team_player.height, team_player.weight, team_player.graduation_year, team_player.wingspan, team_player.forty_yard, team_player.go_body_type_id, team_player.hand_size, team_player.vertical_jump, team_player.email, team_player.sat, team_player.act, team_player.gpa, team_player.go_state_id, team_player.team_position_type_id_pos_1, team_player.team_position_type_id_pos_2, team_player.team_position_type_id_pos_3, team_player.team_position_type_id_pos_st, team_player.high_school, team_player.coach, team_player.coach_phone, team_player.go_level_id, team_player.is_veteran, team_player.start_date, team_player.end_date, CASE WHEN team_player.team_position_type_id_pos_st IS NULL THEN false ELSE true END AS is_special_teams, ( WITH offense AS ( SELECT go_position_type.id FROM go_position_type WHERE go_position_type.go_platoon_type_id = 1 ) SELECT CASE WHEN (team_player.team_position_type_id_pos_1 IN ( SELECT offense.id FROM offense)) OR (team_player.team_position_type_id_pos_2 IN ( SELECT offense.id FROM offense)) OR (team_player.team_position_type_id_pos_3 IN ( SELECT offense.id FROM offense)) THEN true ELSE false END AS "case") AS is_offense, ( WITH defense AS ( SELECT go_position_type.id FROM go_position_type WHERE go_position_type.go_platoon_type_id = 2 ) SELECT CASE WHEN (team_player.team_position_type_id_pos_1 IN ( SELECT defense.id FROM defense)) OR (team_player.team_position_type_id_pos_2 IN ( SELECT defense.id FROM defense)) OR (team_player.team_position_type_id_pos_3 IN ( SELECT defense.id FROM defense)) THEN true ELSE false END AS "case") AS is_defense, ( SELECT tpcs.id FROM team_player_career_stat tpcs WHERE team_player.id = tpcs.team_player_id) AS team_player_career_stat, ARRAY( SELECT tpss.id FROM team_player_season_stat tpss WHERE team_player.id = tpss.team_player_id ORDER BY tpss.season DESC) AS team_player_season_stats FROM team_player WHERE team_player.end_date IS NULL ORDER BY team_player.jersey_number;
