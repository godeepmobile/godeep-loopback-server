﻿--liquibase formatted sql

--changeset carlos:57 runOnChange:true splitStatements:false
--comment added platoon impact calculation

-- Changing impact_platoon_grade_allevent from integer to numeric on team_player_season_stat
ALTER TABLE godeepmobile.team_player_season_stat ALTER COLUMN impact_platoon_grade_allevent TYPE numeric;

-- Updating refresh_season_stats with player's impact on platoon
CREATE OR REPLACE FUNCTION godeepmobile.refresh_season_stats(
    team_id_parm uuid,
    season_parm integer)
  RETURNS void AS
$BODY$
DECLARE
    game_scores RECORD;
BEGIN
	perform godeepmobile.create_season_stats_for_team(team_id_parm, season_parm);
	FOR game_scores IN (
		SELECT
			team
			,"teamPlayer"
			,season
			,count("teamEvent")     as "numGames"
			,sum("numPlays")        as "numPlays"
			,sum("sumCat1Grade")    as "sumCat1Grade"
			,sum("sumCat2Grade")    as "sumCat2Grade"
			,sum("sumCat3Grade")    as "sumCat3Grade"
			,sum("sumOverallGrade") as "sumOverallGrade"
			,(sum("sumCat1Grade")::numeric / sum("numPlays"))::integer        as "avgCat1Grade"
			,(sum("sumCat2Grade")::numeric / sum("numPlays"))::integer        as "avgCat2Grade"
			,(sum("sumCat3Grade")::numeric / sum("numPlays"))::integer        as "avgCat3Grade"
			,(sum("sumOverallGrade")::numeric / (3*sum("numPlays")))::integer as "avgOverallGrade"
		FROM
			godeepmobile.view_team_player_event_grades
		WHERE
			team = team_id_parm and season = season_parm
		GROUP BY
			team, "teamPlayer", season
		ORDER BY
			"teamPlayer"
	)
	LOOP



        with
		season_stat_id as (select id from godeepmobile.team_player_season_stat where team_player_id = game_scores."teamPlayer" and game_scores.season = season_parm),
		last_game as (select * from godeepmobile.view_team_player_event_grades where "teamPlayer" = game_scores."teamPlayer" and game_scores.season = season_parm ORDER BY date DESC LIMIT 1),
		team_conf as (select grade_base, player_multiplier_1, player_multiplier_2, player_multiplier_3, platoon_multiplier_1, platoon_multiplier_2, platoon_multiplier_3 from godeepmobile.team_configuration where go_team_id = team_id_parm),
		impact_posgroup as (SELECT ROUND(AVG(CASE WHEN team_player_id = game_scores."teamPlayer"
					THEN
						CASE
							WHEN overall > (select grade_base from team_conf) AND go_play_factor_id = 3 THEN
								CASE
									WHEN hard THEN overall * (select player_multiplier_2 from team_conf)
								ELSE
									overall * (select player_multiplier_1 from team_conf)
								END
							WHEN overall < (select grade_base from team_conf) AND go_play_factor_id = 2 THEN
								overall * (select player_multiplier_3 from team_conf)
						ELSE
							overall
						END
					ELSE
						overall
				END), 2) AS "avgGradeWithPlayer",
				ROUND(AVG(CASE WHEN team_player_id != game_scores."teamPlayer" THEN overall END), 2) AS "avgGradeWithoutPlayer" 
			FROM (
				SELECT g.team_player_id, 
					ROUND((g.cat_1_grade + g.cat_2_grade + g.cat_3_grade)/3::numeric, 2) AS overall,
					g.go_play_factor_id,
					g.hard
				FROM godeepmobile.team_play_grade g, 
					godeepmobile.view_team_position_type pt 
				WHERE g.go_team_id = team_id_parm AND
					g.position_played = pt.go_position_type_id AND 
					pt.team_position_group_id IN (SELECT position_type.team_position_group_id
					FROM (SELECT grade.position_played, COUNT(*) AS num_grades
						FROM godeepmobile.team_play_grade grade, 
							godeepmobile.view_team_play_with_event play
						WHERE grade.team_play_data_id = play.id AND 
							play.is_practice = false AND
							grade.team_player_id = game_scores."teamPlayer" AND
							play.season = season_parm
						GROUP BY grade.position_played
						ORDER BY num_grades DESC LIMIT 1) AS team_player_primary_position,
						godeepmobile.view_team_position_type position_type
					WHERE position_type.go_team_id = team_id_parm AND
						team_player_primary_position.position_played = position_type.go_position_type_id)
			) AS grade_overalls),
		impact_platoon as (SELECT ROUND(AVG(CASE WHEN team_player_id = game_scores."teamPlayer"
					THEN
						CASE
							WHEN overall > (select grade_base from team_conf) AND go_play_factor_id = 3 THEN
								CASE
									WHEN hard THEN overall * (select platoon_multiplier_2 from team_conf)
								ELSE
									overall * (select platoon_multiplier_1 from team_conf)
								END
							WHEN overall < (select grade_base from team_conf) AND go_play_factor_id = 2 THEN
								overall * (select platoon_multiplier_3 from team_conf)
						ELSE
							overall
						END
					ELSE
						overall
				END), 2) AS "avgGradeWithPlayer",
				ROUND(AVG(CASE WHEN team_player_id != game_scores."teamPlayer" THEN overall END), 2) AS "avgGradeWithoutPlayer" 
			FROM (
				SELECT g.team_player_id, 
					ROUND((g.cat_1_grade + g.cat_2_grade + g.cat_3_grade)/3::numeric, 2) AS overall,
					g.go_play_factor_id,
					g.hard
				FROM godeepmobile.team_play_grade g, 
					godeepmobile.view_team_position_type pt 
				WHERE g.go_team_id = team_id_parm AND
					g.position_played = pt.go_position_type_id AND
					pt.go_platoon_type_id IN (SELECT go_platoon_type_id FROM (SELECT grade.position_played, COUNT(*) AS num_grades
						FROM godeepmobile.team_play_grade grade, 
							godeepmobile.view_team_play_with_event play
						WHERE grade.team_play_data_id = play.id AND 
							play.is_practice = false AND
							grade.team_player_id = game_scores."teamPlayer" AND
							play.season = season_parm
						GROUP BY grade.position_played
						ORDER BY num_grades DESC LIMIT 1) AS player_grades, go_position_type position_type 
						WHERE player_grades.position_played = position_type.id)
			) AS grade_overalls)
        update
		godeepmobile.team_player_season_stat
        SET
		num_games                       = game_scores."numGames"
		,num_plays_game                 = game_scores."numPlays"
		,cat1_grade_game                = game_scores."avgCat1Grade"
		,cat2_grade_game                = game_scores."avgCat2Grade"
		,cat3_grade_game                = game_scores."avgCat3Grade"
		,overall_grade_game             = game_scores."avgOverallGrade"
		,num_plays_allevent             = game_scores."numPlays"
		,cat1_grade_allevent            = game_scores."avgCat1Grade"
		,cat2_grade_allevent            = game_scores."avgCat2Grade"
		,cat3_grade_allevent            = game_scores."avgCat3Grade"
		,overall_grade_allevent         = game_scores."avgOverallGrade"
		,num_plays_last_game            = last_game."numPlays"
		,cat1_grade_last_game           = last_game."avgCat1Grade"
		,cat2_grade_last_game           = last_game."avgCat2Grade"
		,cat3_grade_last_game           = last_game."avgCat3Grade"
		,overall_grade_last_game        = last_game."avgOverallGrade"
		,impact_posgroup_grade_allevent = (select "avgGradeWithPlayer" from impact_posgroup) - (select "avgGradeWithoutPlayer" from impact_posgroup)
		,impact_platoon_grade_allevent  = (select "avgGradeWithPlayer" from impact_platoon) - (select "avgGradeWithoutPlayer" from impact_platoon)
	FROM
		last_game
        WHERE
		godeepmobile.team_player_season_stat.id = (select id from season_stat_id);
    END LOOP;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

--rollback DROP FUNCTION godeepmobile.refresh_season_stats(uuid, integer);