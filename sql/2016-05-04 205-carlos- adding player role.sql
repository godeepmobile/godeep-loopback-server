﻿--liquibase formatted sql

--changeset carlos:205 runOnChange:true splitStatements:false stripComments:false
--comment added player role

UPDATE go_user_role_type SET available = true WHERE id = 1;

--rollback UPDATE go_user_role_type SET available = false WHERE id = 1;