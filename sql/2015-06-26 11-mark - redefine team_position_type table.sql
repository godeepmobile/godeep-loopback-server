--liquibase formatted sql

--changeset mark:11 runOnChange:true stripComments:false
--comment redefine team_position_type table
ALTER TABLE team_position_type DROP COLUMN name;
ALTER TABLE team_position_type DROP COLUMN short_name;


update team_player p
  set team_position_type_id_pos_1 = gp.id
from
  team_player ps
  join team_position_type tp on tp.id = ps.team_position_type_id_pos_1
  join go_position_type gp on gp.id = tp.go_position_type_id
where
	ps.team_position_type_id_pos_1 is not null and
  p.id = ps.id
;
update team_player p
  set team_position_type_id_pos_2 = gp.id
from
  team_player ps
  join team_position_type tp on tp.id = ps.team_position_type_id_pos_2
  join go_position_type gp on gp.id = tp.go_position_type_id
where
	ps.team_position_type_id_pos_2 is not null and
  p.id = ps.id
;
update team_player p
  set team_position_type_id_pos_3 = gp.id
from
  team_player ps
  join team_position_type tp on tp.id = ps.team_position_type_id_pos_3
  join go_position_type gp on gp.id = tp.go_position_type_id
where
	ps.team_position_type_id_pos_3 is not null and
  p.id = ps.id
;
update team_player p
  set team_position_type_id_pos_st = gp.id
from
  team_player ps
  join team_position_type tp on tp.id = ps.team_position_type_id_pos_st
  join go_position_type gp on gp.id = tp.go_position_type_id
where
	ps.team_position_type_id_pos_st is not null and
  p.id = ps.id
;

--rollback alter table team_position_type ADD COLUMN name varchar(100)  NOT NULL; alter table team_position_type ADD COLUMN short_name varchar(5);
--unfortunately no rollback for changing team_position_type_id fields :-(
