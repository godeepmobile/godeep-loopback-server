﻿--liquibase formatted sql

--changeset carlos:121 runOnChange:true
--comment added reference to event type on team report

ALTER TABLE team_report ADD go_event_type_id integer NOT NULL;
ALTER TABLE team_report ADD CONSTRAINT fk_team_report_go_event_type FOREIGN KEY (go_event_type_id) REFERENCES go_event_type;

--rollback ALTER COLUMN team_report DROP CONSTRAINT fk_team_report_go_event_type; ALTER TABLE DROP team_report COLUMN go_event_type_id;

