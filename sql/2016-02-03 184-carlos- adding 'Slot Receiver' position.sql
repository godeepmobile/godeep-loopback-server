﻿--liquibase formatted sql

--changeset carlos:184 runOnChange:true splitStatements:false stripComments:false
--comment added 'Slot Receiver' position

SELECT add_new_position_type('Slot Receiver', 'SLT', 1, 1, 10, 3, 3);

--rollback DELETE FROM team_scouting_eval_criteria WHERE go_position_type_id = (SELECT MAX(id) FROM go_position_type); DELETE FROM team_position_importance WHERE go_position_type_id = (SELECT MAX(id) FROM go_position_type); DELETE FROM team_position_type WHERE go_position_type_id = (SELECT MAX(id) FROM go_position_type); DELETE FROM go_scouting_eval_criteria WHERE go_position_type_id = (SELECT MAX(id) FROM go_position_type); DELETE FROM go_position_type_to_group_assignment WHERE go_position_type_id = (SELECT MAX(id) FROM go_position_type);  DELETE FROM go_position_type WHERE id = (SELECT MAX(id) FROM go_position_type);