--liquibase formatted sql

--changeset rambert:90 runOnChange:true stripComments:false splitStatements:false
--comment add Unspecified records to go_conference table

-- dropping temporary views to update go_conference table
DROP VIEW view_team_play_with_event;
DROP VIEW view_team_player_event_game_grades;
DROP VIEW view_team_event;
DROP VIEW IF EXISTS view_go_team;

-- modifying the length of column name
ALTER TABLE godeepmobile.go_conference ALTER COLUMN name TYPE varchar (50);

-- inserting new conferences
INSERT INTO godeepmobile.go_conference( name, description, id, go_organization_type_id, level, go_level_id )
                               VALUES ( 'Unspecified - FBS', 'Unspecified - FBS', 29, 2, 'FBS', 2);
INSERT INTO godeepmobile.go_conference( name, description, id, go_organization_type_id, level, go_level_id )
                               VALUES ( 'Unspecified - FCS', 'Unspecified - FCS', 30, 2, 'FCS', 3);
INSERT INTO godeepmobile.go_conference( name, description, id, go_organization_type_id, level, go_level_id )
                               VALUES ( 'Unspecified - D-2', 'Unspecified - D-2', 31, 2, 'D-2', 4);
INSERT INTO godeepmobile.go_conference( name, description, id, go_organization_type_id, level, go_level_id )
                               VALUES ( 'Unspecified - D-3', 'Unspecified - D-3', 32, 2, 'D-3', 5);
INSERT INTO godeepmobile.go_conference( name, description, id, go_organization_type_id, level, go_level_id )
                               VALUES ( 'Unspecified - JUCO', 'Unspecified - JUCO', 33, 2, 'JUCO', 6);
INSERT INTO godeepmobile.go_conference( name, description, id, go_organization_type_id, level, go_level_id )
                               VALUES ( 'Unspecified - HS', 'Unspecified - HS', 34, 1, 'HS', 7);

CREATE OR REPLACE VIEW view_go_team AS 
 SELECT gt.id,
    ( SELECT go.name
           FROM go_organization go
          WHERE go.id = gt.go_organization_id) AS name,
    gt.short_name,
    gt.abbreviation,
    gt.mascot,
    ( SELECT gc.name
           FROM go_conference gc
          WHERE gc.id = gt.go_conference_id) AS conference_name,
    gt.go_sport_type_id,
    gt.addr_street1,
    gt.addr_street2,
    gt.addr_city,
    gt.addr_state_id,
    gt.addr_postal_code,
    gt.addr_phone_main,
    gt.addr_phone_fax,
    gt.home_stadium,
    gt.go_surface_type_id,
    gt.film_system,
    gt.go_conference_id,
    gt.asset_base_url,
    gt.go_organization_id,
    ( SELECT count(*) AS count
           FROM team_play_grade grade
          WHERE grade.go_team_id = gt.id) AS num_play_grades,
    ( SELECT gc.level
           FROM go_conference gc
          WHERE gc.id = gt.go_conference_id) AS conference_level,
    gt.go_conference_id = 15 OR gt.go_conference_id = 16 AS is_pro_team
   FROM go_team gt
  ORDER BY gt.id;

-- View: view_team_event

CREATE OR REPLACE VIEW view_team_event AS 
 SELECT event.id,
    event.go_team_id,
    event.date,
    event."time",
    event.go_event_type_id,
    event.go_field_condition_id,
    event.go_surface_type_id,
    event.description,
    event.grade_base,
    event.grade_increment,
    event.data,
    event.season,
    event.name,
    event.location,
    event.city,
    event.go_state_id,
    event.is_home_game,
    event.score,
    event.opponent_score,
    event.opponent_organization_id,
    ( SELECT org.name
           FROM go_organization org
          WHERE org.id = event.opponent_organization_id) AS opponent_name,
    ( SELECT org.short_name
           FROM go_organization org
          WHERE org.id = event.opponent_organization_id) AS opponent_short_name,
    ( SELECT org.espn_name
           FROM go_organization org
          WHERE org.id = event.opponent_organization_id) AS opponent_abbreviation,
    ( SELECT org.mascot
           FROM go_organization org
          WHERE org.id = event.opponent_organization_id) AS opponent_mascot,
    ( SELECT team.name
           FROM view_go_team team
          WHERE team.id = event.go_team_id) AS team_name,
    ( SELECT team.short_name
           FROM view_go_team team
          WHERE team.id = event.go_team_id) AS team_short_name,
    ( SELECT team.abbreviation
           FROM view_go_team team
          WHERE team.id = event.go_team_id) AS team_abbreviation,
    ( SELECT team.mascot
           FROM view_go_team team
          WHERE team.id = event.go_team_id) AS team_mascot,
    ( SELECT event_type.is_practice
           FROM go_event_type event_type
          WHERE event_type.id = event.go_event_type_id) AS is_practice,
    ( SELECT event_type.is_match
           FROM go_event_type event_type
          WHERE event_type.id = event.go_event_type_id) AS is_match
   FROM team_event event;

CREATE OR REPLACE VIEW view_team_player_event_game_grades AS 
 SELECT grades.team AS go_team_id,
    grades."teamPlayer" AS team_player_id,
    grades."teamEvent" AS team_event_id,
    grades.season,
    event.name AS event_name,
    event.is_practice,
    grades."avgCat1Grade" AS avg_cat1_grade,
    grades."avgCat2Grade" AS avg_cat2_grade,
    grades."avgCat3Grade" AS avg_cat3_grade,
    grades."avgOverallGrade" AS avg_overall_grade
   FROM view_team_player_event_grades grades
     JOIN view_team_event event ON event.id = grades."teamEvent"
  ORDER BY event.date;

CREATE OR REPLACE VIEW view_team_play_with_event AS 
 SELECT tpd.id,
    tpd.play_number,
    tpd.game_down,
    tpd.game_distance,
    tpd.game_possession,
    tpd.go_play_result_type_id,
    tpd.team_event_id,
    tpd.team_practice_drill_type_id,
    tpd.data,
    tpd.go_team_id,
    te.date,
    te.season,
    te.is_practice
   FROM team_play_data tpd
     JOIN view_team_event te ON tpd.team_event_id = te.id;

--rollback DROP VIEW view_team_play_with_event; DROP VIEW view_team_player_event_game_grades; DROP VIEW view_team_event; DROP VIEW IF EXISTS view_go_team; DELETE FROM godeepmobile.go_conference WHERE id IN (29, 30, 31, 32, 33, 34); ALTER TABLE godeepmobile.go_conference ALTER COLUMN name TYPE varchar (10); CREATE OR REPLACE VIEW view_go_team AS SELECT gt.id, ( SELECT go.name FROM go_organization go WHERE go.id = gt.go_organization_id) AS name, gt.short_name, gt.abbreviation, gt.mascot, ( SELECT gc.name FROM go_conference gc WHERE gc.id = gt.go_conference_id) AS conference_name, gt.go_sport_type_id, gt.addr_street1, gt.addr_street2, gt.addr_city, gt.addr_state_id, gt.addr_postal_code, gt.addr_phone_main, gt.addr_phone_fax, gt.home_stadium, gt.go_surface_type_id, gt.film_system, gt.go_conference_id, gt.asset_base_url, gt.go_organization_id, ( SELECT count(*) AS count FROM team_play_grade grade WHERE grade.go_team_id = gt.id) AS num_play_grades, ( SELECT gc.level FROM go_conference gc WHERE gc.id = gt.go_conference_id) AS conference_level, gt.go_conference_id = 15 OR gt.go_conference_id = 16 AS is_pro_team FROM go_team gt ORDER BY gt.id; CREATE OR REPLACE VIEW view_team_event AS SELECT event.id, event.go_team_id, event.date, event."time", event.go_event_type_id, event.go_field_condition_id, event.go_surface_type_id, event.description, event.grade_base, event.grade_increment, event.data, event.season, event.name, event.location, event.city, event.go_state_id, event.is_home_game, event.score, event.opponent_score, event.opponent_organization_id, ( SELECT org.name FROM go_organization org WHERE org.id = event.opponent_organization_id) AS opponent_name, ( SELECT org.short_name FROM go_organization org WHERE org.id = event.opponent_organization_id) AS opponent_short_name, ( SELECT org.espn_name FROM go_organization org WHERE org.id = event.opponent_organization_id) AS opponent_abbreviation, ( SELECT org.mascot FROM go_organization org WHERE org.id = event.opponent_organization_id) AS opponent_mascot, ( SELECT team.name FROM view_go_team team WHERE team.id = event.go_team_id) AS team_name, ( SELECT team.short_name FROM view_go_team team WHERE team.id = event.go_team_id) AS team_short_name, ( SELECT team.abbreviation FROM view_go_team team WHERE team.id = event.go_team_id) AS team_abbreviation, ( SELECT team.mascot FROM view_go_team team WHERE team.id = event.go_team_id) AS team_mascot, ( SELECT event_type.is_practice FROM go_event_type event_type WHERE event_type.id = event.go_event_type_id) AS is_practice, ( SELECT event_type.is_match FROM go_event_type event_type WHERE event_type.id = event.go_event_type_id) AS is_match FROM team_event event; CREATE OR REPLACE VIEW view_team_player_event_game_grades AS SELECT grades.team AS go_team_id, grades."teamPlayer" AS team_player_id, grades."teamEvent" AS team_event_id, grades.season, event.name AS event_name, event.is_practice, grades."avgCat1Grade" AS avg_cat1_grade, grades."avgCat2Grade" AS avg_cat2_grade, grades."avgCat3Grade" AS avg_cat3_grade, grades."avgOverallGrade" AS avg_overall_grade FROM view_team_player_event_grades grades JOIN view_team_event event ON event.id = grades."teamEvent" ORDER BY event.date; CREATE OR REPLACE VIEW view_team_play_with_event AS  SELECT tpd.id, tpd.play_number, tpd.game_down, tpd.game_distance, tpd.game_possession, tpd.go_play_result_type_id, tpd.team_event_id, tpd.team_practice_drill_type_id, tpd.data, tpd.go_team_id, te.date, te.season, te.is_practice FROM team_play_data tpd JOIN view_team_event te ON tpd.team_event_id = te.id;
