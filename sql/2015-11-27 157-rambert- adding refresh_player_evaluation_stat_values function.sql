--liquibase formatted sql

--changeset rambert:156 runOnChange:true splitStatements:false stripComments:false
--comment updating refresh_evaluation_stat_values function

CREATE OR REPLACE FUNCTION godeepmobile.refresh_evaluation_stat_values(
  team_id_parm uuid,
  evaluation_id_parm integer)
  RETURNS void AS
$BODY$
DECLARE
  record_value RECORD;
  record_exists integer;
BEGIN

  -- generate evaluation stat values
  FOR record_value IN
    SELECT
     score_group.go_team_id,
     score_group.team_player_id,
     score_group.go_user_id,
     score_group.id AS team_scouting_eval_score_group_id,
     score_group.season,
     score_group.date,
     ROUND(avg( (eval_score.score * 100) /
       ( CASE WHEN eval_criteria.use_calculated_target_score
              THEN eval_criteria.calculated_target_score
              ELSE eval_criteria.custom_target_score
         END)), 2) AS target_match,

     ROUND(avg(eval_score.score), 2) AS overall_eval_avg,

     (SELECT ROUND(avg(eval_score_.score), 2)
         FROM godeepmobile.team_scouting_eval_score eval_score_,
           godeepmobile.team_scouting_eval_score_group score_group_
         WHERE eval_score_.go_team_id = eval_score.go_team_id
           AND eval_score_.team_scouting_eval_score_group_id = score_group_.id
           AND score_group_.id = score_group.id
           AND eval_score_.go_scouting_eval_criteria_type_id = 1
     ) AS  major_factors_eval_avg,

     (SELECT ROUND(avg(eval_score_.score), 2)
         FROM godeepmobile.team_scouting_eval_score eval_score_,
           godeepmobile.team_scouting_eval_score_group score_group_
         WHERE eval_score_.go_team_id = eval_score.go_team_id
           AND eval_score_.team_scouting_eval_score_group_id = score_group_.id
           AND score_group_.id = score_group.id
           AND eval_score_.go_scouting_eval_criteria_type_id = 2
     ) AS critical_factors_eval_avg,

     (SELECT ROUND(avg(eval_score_.score), 2)
         FROM godeepmobile.team_scouting_eval_score eval_score_,
           godeepmobile.team_scouting_eval_score_group score_group_
         WHERE eval_score_.go_team_id = eval_score.go_team_id
           AND eval_score_.team_scouting_eval_score_group_id = score_group_.id
           AND score_group_.id = score_group.id
           AND eval_score_.go_scouting_eval_criteria_type_id = 3
     ) AS position_skills_eval_avg,

     ROUND(AVG(CASE
                 WHEN eval_criteria.is_critical
                   THEN
                     CASE
                       WHEN eval_score.score > ((team_config.scout_eval_score_min + team_config.scout_eval_score_max) / 2 ::numeric) --median_score
                         THEN eval_score.score * team_config.player_evaluation_greater_multiplier
                       ELSE eval_score.score * team_config.player_evaluation_lesser_multiplier
                     END
                   ELSE eval_score.score
                 END) * position_importance.go_importance_id, 2) as overall_eval_avg_rank

     FROM godeepmobile.team_scouting_eval_score eval_score,
         godeepmobile.team_scouting_eval_score_group score_group,
         godeepmobile.team_scouting_eval_criteria eval_criteria,
         godeepmobile.team_position_importance position_importance,
         godeepmobile.team_configuration team_config
     WHERE score_group.id = evaluation_id_parm
         AND score_group.go_team_id = team_id_parm::uuid
         AND eval_score.go_team_id = score_group.go_team_id
         AND eval_score.go_team_id = eval_criteria.go_team_id
         AND eval_score.team_scouting_eval_score_group_id = score_group.id
         AND eval_score.team_scouting_eval_criteria_id = eval_criteria.id
         AND eval_score.go_team_id = position_importance.go_team_id
         AND eval_score.go_team_id = team_config.go_team_id
         AND score_group.go_position_type_id = position_importance.go_position_type_id
         AND eval_criteria.go_position_type_id = score_group.go_position_type_id
     GROUP BY eval_score.go_team_id, score_group.id, position_importance.go_importance_id

  LOOP
      SELECT COUNT(id) INTO record_exists
      FROM godeepmobile.player_evaluation_stat
      WHERE go_team_id = team_id_parm::uuid
        AND team_scouting_eval_score_group_id = evaluation_id_parm;

        IF record_exists = 0 THEN
          INSERT INTO godeepmobile.player_evaluation_stat (
            go_team_id,
            team_player_id,
            go_user_id,
            team_scouting_eval_score_group_id,
            season,
            date,
            target_match,
            overall_eval_avg,
            major_factors_eval_avg,
            critical_factors_eval_avg,
            position_skills_eval_avg,
            overall_eval_avg_rank
          )
          VALUES (
            record_value.go_team_id,
            record_value.team_player_id,
            record_value.go_user_id,
            record_value.team_scouting_eval_score_group_id,
            record_value.season,
            record_value.date,
            record_value.target_match,
            record_value.overall_eval_avg,
            record_value.major_factors_eval_avg,
            record_value.critical_factors_eval_avg,
            record_value.position_skills_eval_avg,
            record_value.overall_eval_avg_rank
          );
        ELSE
          UPDATE
              godeepmobile.player_evaluation_stat
          SET
              target_match = record_value.target_match,
              overall_eval_avg = record_value.overall_eval_avg,
              major_factors_eval_avg = record_value.major_factors_eval_avg,
              critical_factors_eval_avg = record_value.critical_factors_eval_avg,
              position_skills_eval_avg = record_value.position_skills_eval_avg,
              overall_eval_avg_rank = record_value.overall_eval_avg_rank
          WHERE go_team_id = team_id_parm::uuid
            AND team_scouting_eval_score_group_id = evaluation_id_parm;
        END IF;
  END LOOP;

END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

--rollback DROP FUNCTION godeepmobile.refresh_evaluation_stat_values(uuid, integer);
