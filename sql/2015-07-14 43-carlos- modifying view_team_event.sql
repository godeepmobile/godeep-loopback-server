--liquibase formatted sql

--changeset carlos:43 runOnChange:true stripComments:false splitStatements:false
--comment modified view_team_event

DROP VIEW IF EXISTS godeepmobile.view_team_event;

CREATE OR REPLACE VIEW godeepmobile.view_team_event AS 
	SELECT event.id,
	    event.go_team_id,
	    event.date,
	    event.time,
	    event.go_event_type_id,
	    event.go_field_condition_id,
	    event.go_surface_type_id,
	    event.description,
	    event.go_game_event_id,
	    event.grade_base,
	    event.grade_increment,
	    event.data,
	    event.season,
	    event.name,
	    event.is_home_game,
	    event.score,
	    event.opponent_score,
	    event.location,
	    event.city,
	    event.go_state_id,
	    event.opponent_organization_id,
	    organization.name AS opponent_name,
	    organization.short_name AS opponent_short_name,
	    organization.espn_name AS opponent_abbreviation,
	    organization.mascot AS opponent_mascot
	FROM godeepmobile.team_event event
	LEFT OUTER JOIN godeepmobile.go_organization organization ON event.opponent_organization_id = organization.id;

--rollback DROP VIEW IF EXISTS godeepmobile.view_team_event;