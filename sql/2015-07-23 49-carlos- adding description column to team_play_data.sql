--liquibase formatted sql

--changeset carlos:49 runOnChange:true
--comment added description column to team_play_data

ALTER TABLE godeepmobile.team_play_data ADD description VARCHAR(1024);

--rollback ALTER TABLE godeepmobile.team_play_data DROP COLUMN description;