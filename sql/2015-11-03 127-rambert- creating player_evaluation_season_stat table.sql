--liquibase formatted sql

--changeset rambert:127 runOnChange:true stripComments:false splitStatements:false
--comment create player_evaluation_season_stat table

CREATE TABLE godeepmobile.player_evaluation_season_stat (
  id                         serial  NOT NULL,
  go_team_id                 uuid NOT NULL,
  team_player_id             uuid NOT NULL,
  season                     integer NOT NULL,
  num_evaluations            integer,
  target_match               smallint,
  major_factors_eval_avg     numeric(100,2),
  critical_factors_eval_avg  numeric(100,2),
  position_skills_eval_avg   numeric(100,2),
  overall_eval_avg           numeric(100,2),
  overall_eval_avg_rank      numeric(100,2),
  rank_overall               smallint,
  rank_at_post               smallint,
  CONSTRAINT uq_player_evaluation_season_stat_season_team_player_id
      UNIQUE (go_team_id, season, team_player_id),
  CONSTRAINT pk_player_evaluation_season_stat PRIMARY KEY ( id ),
  CONSTRAINT fk_player_evaluation_season_stat_go_team FOREIGN KEY (go_team_id)
      REFERENCES go_team (id),
  CONSTRAINT fk_player_evaluation_season_stat_team_player FOREIGN KEY (team_player_id)
      REFERENCES team_player (id)
);

CREATE TABLE godeepmobile.player_evaluation_career_stat (
  id                         serial  NOT NULL,
  go_team_id                 uuid NOT NULL,
  team_player_id             uuid NOT NULL,
  num_seasons                integer,
  target_match               smallint,
  major_factors_eval_avg     numeric(100,2),
  critical_factors_eval_avg  numeric(100,2),
  position_skills_eval_avg   numeric(100,2),
  overall_eval_avg           numeric(100,2),
  overall_eval_avg_rank      numeric(100,2),
  rank_overall               smallint,
  rank_at_post               smallint,
  CONSTRAINT uq_player_evaluation_career_stat_season_team_player_id
      UNIQUE (go_team_id, team_player_id),
  CONSTRAINT pk_player_evaluation_career_stat PRIMARY KEY ( id ),
  CONSTRAINT fk_player_evaluation_career_stat_go_team FOREIGN KEY (go_team_id)
      REFERENCES go_team (id),
  CONSTRAINT fk_player_evaluation_career_stat_team_player FOREIGN KEY (team_player_id)
      REFERENCES team_player (id)
);

--rollback DROP TABLE IF EXISTS  godeepmobile.player_evaluation_season_stat; DROP TABLE IF EXISTS  godeepmobile.player_evaluation_career_stat;
