--liquibase formatted sql

--changeset rambert:137 runOnChange:true stripComments:false splitStatements:false
--comment create view_prospect_evaluation_stat

CREATE OR REPLACE VIEW view_prospect_evaluation_stat AS
  SELECT
    evaluation_stat.id,
    evaluation_stat.go_team_id,
    evaluation_stat.team_player_id as team_prospect_id,
    evaluation_stat.go_user_id,
    evaluation_stat.team_scouting_eval_score_group_id,
    evaluation_stat.season,
    evaluation_stat.target_match,
    evaluation_stat.overall_eval_avg,
    evaluation_stat.major_factors_eval_avg,
    evaluation_stat.critical_factors_eval_avg,
    evaluation_stat.position_skills_eval_avg,
    evaluation_stat.rank_overall,
    evaluation_stat.rank_at_post,
    evaluation_stat.date,
    evaluation_stat.overall_eval_avg_rank
  FROM godeepmobile.player_evaluation_stat evaluation_stat
      JOIN godeepmobile.view_team_prospect prospect
      ON evaluation_stat.team_player_id = prospect.id;


CREATE OR REPLACE VIEW view_player_evaluation_stat AS
  SELECT
    evaluation_stat.id,
    evaluation_stat.go_team_id,
    evaluation_stat.team_player_id,
    evaluation_stat.go_user_id,
    evaluation_stat.team_scouting_eval_score_group_id,
    evaluation_stat.season,
    evaluation_stat.target_match,
    evaluation_stat.overall_eval_avg,
    evaluation_stat.major_factors_eval_avg,
    evaluation_stat.critical_factors_eval_avg,
    evaluation_stat.position_skills_eval_avg,
    evaluation_stat.rank_overall,
    evaluation_stat.rank_at_post,
    evaluation_stat.date,
    evaluation_stat.overall_eval_avg_rank
  FROM godeepmobile.player_evaluation_stat evaluation_stat
      JOIN godeepmobile.view_team_player player
      ON evaluation_stat.team_player_id = player.id;
--rollback DROP VIEW view_prospect_evaluation_stat; DROP VIEW view_player_evaluation_stat;
