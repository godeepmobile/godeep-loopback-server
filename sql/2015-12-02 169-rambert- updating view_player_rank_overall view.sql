--changeset rambert:169 runOnChange:true stripComments:false splitStatements:false
--comment updating view_player_rank_overall view

CREATE OR REPLACE VIEW view_player_rank_overall AS
  SELECT
    rank_overall.id,
    rank_overall.go_team_id,
    rank_overall.team_player_id,
    rank_overall.season,
    rank_overall.rank_overall,
    rank_overall.overall_evaluation_rank,
    rank_overall.overall_grade_game_rank,
    rank_overall.rank_at_post,
    rank_overall.game_grade_avg,
    rank_overall.target_match,
    rank_overall.major_factors_eval_avg,
    rank_overall.critical_factors_eval_avg,
    rank_overall.position_skills_eval_avg,
    rank_overall.overall_eval_avg,
    (SELECT player_season_stat.rank_at_post
     FROM  view_player_rank_overall_at_position_season_stat player_season_stat,
           view_team_player player
     WHERE player_season_stat.go_team_id = player.go_team_id
       AND player_season_stat.team_player_id = player.id
       AND rank_overall.team_player_id = player.id
       AND player_season_stat.season = rank_overall.season
       AND player_season_stat.go_position_type_id = player.team_position_type_id_pos_1
     ) AS rank_at_evaluation_post,

     ( SELECT rank_at_position_stat.rank_game_at_post
      FROM player_grade_season_rank_at_position_stat rank_at_position_stat,
           view_team_player player
      WHERE rank_at_position_stat.go_team_id = rank_overall.go_team_id
              AND rank_overall.go_team_id = player.go_team_id
              AND rank_overall.team_player_id = player.id
              AND rank_at_position_stat.team_player_id = player.id
        AND rank_at_position_stat.team_player_id = rank_overall.team_player_id
        AND rank_at_position_stat.season = rank_overall.season
        AND rank_at_position_stat.go_position_type_id = player.team_position_type_id_pos_1) AS rank_at_grade_post,
    ( SELECT rank_at_position_stat.rank_at_post
          FROM player_overall_season_rank_at_position_stat rank_at_position_stat,
               view_team_player player
          WHERE rank_at_position_stat.go_team_id = rank_overall.go_team_id
                  AND rank_overall.go_team_id = player.go_team_id
                  AND rank_overall.team_player_id = player.id
                  AND rank_at_position_stat.team_player_id = player.id
            AND rank_at_position_stat.team_player_id = rank_overall.team_player_id
            AND rank_at_position_stat.season = rank_overall.season
            AND rank_at_position_stat.go_position_type_id = player.team_position_type_id_pos_1) AS rank_at_overall_post

  FROM player_rank_overall rank_overall;

--rollback DROP VIEW view_player_rank_overall;
