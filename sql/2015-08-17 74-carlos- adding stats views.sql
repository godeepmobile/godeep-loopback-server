﻿--liquibase formatted sql

--changeset carlos:74 runOnChange:true stripComments:false splitStatements:false
--comment added stats views

-- Season Stats
CREATE OR REPLACE VIEW view_team_player_season_stat AS
	SELECT stat.* FROM team_player_season_stat stat, team_player player
	WHERE stat.team_player_id = player.id AND player.end_date IS NULL;

-- Game Stats
CREATE OR REPLACE VIEW view_team_player_game_stat AS
	SELECT stat.* FROM team_player_game_stat stat, team_player player
	WHERE stat.team_player_id = player.id AND player.end_date IS NULL;
-- 
-- Game Cutup Stats
CREATE OR REPLACE VIEW view_team_player_game_cutup_stat AS
	SELECT stat.* FROM team_player_game_cutup_stat stat, team_player player
	WHERE stat.team_player_id = player.id AND player.end_date IS NULL;

-- Career Stats
CREATE OR REPLACE VIEW view_team_player_career_stat AS
	SELECT stat.* FROM team_player_career_stat stat, team_player player
	WHERE stat.team_player_id = player.id AND player.end_date IS NULL;

--rollback DROP VIEW IF EXISTS view_team_player_season_stat; DROP VIEW IF EXISTS view_team_player_game_stat; DROP VIEW IF EXISTS view_team_player_game_cutup_stat; DROP VIEW IF EXISTS view_team_player_career_stat; 