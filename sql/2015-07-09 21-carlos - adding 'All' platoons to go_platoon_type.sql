--liquibase formatted sql

--changeset carlos:21 runOnChange:true
--comment added 'All' platoons to go_platoon_type

INSERT INTO godeepmobile.go_platoon_type( id, name, description ) VALUES ( 4, 'All', 'All Platoons' );

--rollback DELETE FROM godeepmobile.go_platoon_type WHERE id = 4;

