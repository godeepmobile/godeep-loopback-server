--liquibase formatted sql

--changeset rambert:64 runOnChange:true stripComments:false splitStatements:false
--comment add num_play_grades column to view_go_team view

-- dropping temporarily view_team_player_event_game_grades view due to dependency on view_team_event view
DROP VIEW IF EXISTS view_team_player_event_game_grades;
-- dropping temporarily view_team_play_with_event view due to dependency on view_team_event view
DROP VIEW IF EXISTS view_team_play_with_event;
-- dropping temporarily view_team_event view due to dependency on view_go_team view
DROP VIEW IF EXISTS view_team_event;


DROP VIEW IF EXISTS view_go_team;
-- add num_play_grades column to view_go_team view
CREATE OR REPLACE VIEW view_go_team AS 
 SELECT gt.id,
    ( SELECT go.name
           FROM go_organization go
          WHERE go.id = gt.go_organization_id) AS name,
    gt.short_name,
    gt.abbreviation,
    gt.mascot,
    ( SELECT gc.name
           FROM go_conference gc
          WHERE gc.id = gt.go_conference_id) AS conference_name,
    gt.go_sport_type_id,
    gt.addr_street1,
    gt.addr_street2,
    gt.addr_city,
    gt.addr_state_id,
    gt.addr_postal_code,
    gt.addr_phone_main,
    gt.addr_phone_fax,
    gt.home_stadium,
    gt.go_surface_type_id,
    gt.film_system,
    gt.go_conference_id,
    gt.asset_base_url,
    gt.go_organization_id,
    (SELECT COUNT(*) FROM team_play_grade grade WHERE grade.go_team_id = gt.id) AS num_play_grades
   FROM go_team gt
  ORDER BY gt.id;

-- re-creating view_team_event view
CREATE OR REPLACE VIEW view_team_event AS 
 SELECT event.id,
    event.go_team_id,
    event.date,
    event."time",
    event.go_event_type_id,
    event.go_field_condition_id,
    event.go_surface_type_id,
    event.description,
    event.grade_base,
    event.grade_increment,
    event.data,
    event.season,
    event.name,
    event.location,
    event.city,
    event.go_state_id,
    event.is_home_game,
    event.score,
    event.opponent_score,
    event.opponent_organization_id,
    ( SELECT org.name
           FROM go_organization org
          WHERE org.id = event.opponent_organization_id) AS opponent_name,
    ( SELECT org.short_name
           FROM go_organization org
          WHERE org.id = event.opponent_organization_id) AS opponent_short_name,
    ( SELECT org.espn_name
           FROM go_organization org
          WHERE org.id = event.opponent_organization_id) AS opponent_abbreviation,
    ( SELECT org.mascot
           FROM go_organization org
          WHERE org.id = event.opponent_organization_id) AS opponent_mascot,
    ( SELECT team.name
           FROM view_go_team team
          WHERE team.id = event.go_team_id) AS team_name,
    ( SELECT team.short_name
           FROM view_go_team team
          WHERE team.id = event.go_team_id) AS team_short_name,
    ( SELECT team.abbreviation
           FROM view_go_team team
          WHERE team.id = event.go_team_id) AS team_abbreviation,
    ( SELECT team.mascot
           FROM view_go_team team
          WHERE team.id = event.go_team_id) AS team_mascot,
    ( SELECT event_type.is_practice
           FROM go_event_type event_type
          WHERE event_type.id = event.go_event_type_id) AS is_practice
   FROM team_event event;

-- re-creating view_team_player_event_game_grades view
CREATE OR REPLACE VIEW view_team_player_event_game_grades AS 
SELECT grades.team AS go_team_id,
  grades."teamPlayer" AS team_player_id,
  grades."teamEvent" AS team_event_id,
  grades.season,
  event.name AS event_name,
  event.is_practice,
  grades."avgCat1Grade" AS avg_cat1_grade,
  grades."avgCat2Grade" AS avg_cat2_grade,
  grades."avgCat3Grade" AS avg_cat3_grade,
  grades."avgOverallGrade" AS avg_overall_grade
 FROM view_team_player_event_grades grades
   JOIN view_team_event event ON event.id = grades."teamEvent"
ORDER BY event.date;

CREATE OR REPLACE VIEW view_team_play_with_event AS 
 SELECT tpd.id,
    tpd.play_number,
    tpd.game_down,
    tpd.game_distance,
    tpd.game_possession,
    tpd.go_play_result_type_id,
    tpd.team_event_id,
    tpd.team_practice_drill_type_id,
    tpd.data,
    tpd.go_team_id,
    te.date,
    te.season,
    te.is_practice
   FROM team_play_data tpd
     JOIN view_team_event te ON tpd.team_event_id = te.id;
--rollback DROP VIEW IF EXISTS view_team_player_event_game_grades; DROP VIEW IF EXISTS view_team_play_with_event; DROP VIEW IF EXISTS view_team_event; DROP VIEW IF EXISTS view_go_team; CREATE OR REPLACE VIEW view_go_team AS SELECT gt.id, ( SELECT go.name FROM go_organization go WHERE go.id = gt.go_organization_id) AS name, gt.short_name, gt.abbreviation, gt.mascot, ( SELECT gc.name FROM go_conference gc WHERE gc.id = gt.go_conference_id) AS conference_name, gt.go_sport_type_id, gt.addr_street1, gt.addr_street2, gt.addr_city, gt.addr_state_id, gt.addr_postal_code, gt.addr_phone_main, gt.addr_phone_fax, gt.home_stadium, gt.go_surface_type_id, gt.film_system, gt.go_conference_id, gt.asset_base_url, gt.go_organization_id FROM go_team gt ORDER BY gt.id; CREATE OR REPLACE VIEW view_team_event AS SELECT event.id, event.go_team_id, event.date, event."time", event.go_event_type_id, event.go_field_condition_id, event.go_surface_type_id, event.description, event.grade_base, event.grade_increment, event.data, event.season, event.name, event.location, event.city, event.go_state_id, event.is_home_game, event.score, event.opponent_score, event.opponent_organization_id, ( SELECT org.name FROM go_organization org WHERE org.id = event.opponent_organization_id) AS opponent_name, ( SELECT org.short_name FROM go_organization org WHERE org.id = event.opponent_organization_id) AS opponent_short_name, ( SELECT org.espn_name FROM go_organization org WHERE org.id = event.opponent_organization_id) AS opponent_abbreviation, ( SELECT org.mascot FROM go_organization org WHERE org.id = event.opponent_organization_id) AS opponent_mascot, ( SELECT team.name FROM view_go_team team WHERE team.id = event.go_team_id) AS team_name, ( SELECT team.short_name FROM view_go_team team WHERE team.id = event.go_team_id) AS team_short_name, ( SELECT team.abbreviation FROM view_go_team team WHERE team.id = event.go_team_id) AS team_abbreviation, ( SELECT team.mascot FROM view_go_team team WHERE team.id = event.go_team_id) AS team_mascot, ( SELECT event_type.is_practice FROM go_event_type event_type WHERE event_type.id = event.go_event_type_id) AS is_practice FROM team_event event; CREATE OR REPLACE VIEW view_team_player_event_game_grades AS SELECT grades.team AS go_team_id, grades."teamPlayer" AS team_player_id, grades."teamEvent" AS team_event_id, grades.season, event.name AS event_name, event.is_practice, grades."avgCat1Grade" AS avg_cat1_grade, grades."avgCat2Grade" AS avg_cat2_grade, grades."avgCat3Grade" AS avg_cat3_grade, grades."avgOverallGrade" AS avg_overall_grade FROM view_team_player_event_grades grades JOIN view_team_event event ON event.id = grades."teamEvent" ORDER BY event.date; CREATE OR REPLACE VIEW view_team_play_with_event AS  SELECT tpd.id, tpd.play_number, tpd.game_down, tpd.game_distance, tpd.game_possession, tpd.go_play_result_type_id, tpd.team_event_id, tpd.team_practice_drill_type_id, tpd.data, tpd.go_team_id, te.date, te.season, te.is_practice FROM team_play_data tpd JOIN view_team_event te ON tpd.team_event_id = te.id;