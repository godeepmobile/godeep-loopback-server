--liquibase formatted sql

--changeset rambert:141 runOnChange:true stripComments:false splitStatements:false
--comment create view_player_evaluation_season_stat view

CREATE OR REPLACE VIEW view_player_evaluation_season_stat AS
  SELECT
    player.id,
    player.go_team_id,
    player.id as team_player_id,
    evaluation_season_stat.season,
    evaluation_season_stat.num_evaluations,
    evaluation_season_stat.target_match,
    evaluation_season_stat.major_factors_eval_avg,
    evaluation_season_stat.critical_factors_eval_avg,
    evaluation_season_stat.position_skills_eval_avg,
    evaluation_season_stat.overall_eval_avg,
    evaluation_season_stat.overall_eval_avg_rank,
    evaluation_season_stat.rank_overall,
    evaluation_season_stat.rank_at_post
  FROM player_evaluation_season_stat evaluation_season_stat
      RIGHT JOIN godeepmobile.view_team_player player
      ON evaluation_season_stat.team_player_id = player.id;

--rollback DROP VIEW view_player_evaluation_season_stat;
