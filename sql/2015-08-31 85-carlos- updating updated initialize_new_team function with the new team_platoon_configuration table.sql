﻿--liquibase formatted sql

--changeset carlos:85 runOnChange:true splitStatements:false stripComments:false
--comment updated initialize_new_team function with the new team_platoon_configuration table

CREATE OR REPLACE FUNCTION initialize_new_team(go_team_id_parm text)
  RETURNS integer AS
$BODY$
DECLARE
	pos_group RECORD;
	pos_type RECORD;
	new_group_id integer;
	exists integer;
BEGIN
  --check validity of parameters
	select count(team.id) into exists from godeepmobile.go_team team where team.id = go_team_id_parm::uuid;
	if exists != 1 then
		RAISE EXCEPTION 'Team % does not exist', go_team_id_parm USING HINT = 'Please check your team id parameter';
		return 0;
	end if;

	-- create team configuration using team_configuration table defaults
	INSERT INTO godeepmobile.team_configuration (go_team_id) VALUES (go_team_id_parm::uuid);

	-- create team platoon configuration records
	INSERT INTO godeepmobile.team_platoon_configuration (go_team_id, go_platoon_type_id, num_plays) VALUES (go_team_id_parm::uuid, 1, 150); --off
	INSERT INTO godeepmobile.team_platoon_configuration (go_team_id, go_platoon_type_id, num_plays) VALUES (go_team_id_parm::uuid, 2, 150); --def
	INSERT INTO godeepmobile.team_platoon_configuration (go_team_id, go_platoon_type_id, num_plays) VALUES (go_team_id_parm::uuid, 3, 50); --st
	INSERT INTO godeepmobile.team_platoon_configuration (go_team_id, go_platoon_type_id, num_plays) VALUES (go_team_id_parm::uuid, 4, 300); --all

	-- copy global position groups into team positions groups
	FOR pos_group IN
		SELECT *
		FROM godeepmobile.go_position_group
	LOOP
		-- copy this position group
		RAISE NOTICE 'creating team position group %', quote_ident(pos_group.name);
		insert into godeepmobile.team_position_group (
			id,
			go_team_id,
			name,
			go_platoon_type_id
		)
		values (
			DEFAULT,
			go_team_id_parm::uuid,
			pos_group.name,
			pos_group.go_platoon_type_id
		)
		returning id into new_group_id;	-- new_group_id how has the id of the new team position group RECORD

		-- go_position_type_to_group_assignment table maps specific GO position types into a GO position group
		-- team_position_type table maps specific TEAM position types into a TEAM position group
		-- copy position type-to-group assignments for this position group
		FOR pos_type IN
			SELECT *
			FROM godeepmobile.go_position_type_to_group_assignment
			where go_position_type_to_group_assignment.go_position_group_id = pos_group.id
		LOOP
			RAISE NOTICE 'creating team assignment for new_group_id %', new_group_id;
			insert into godeepmobile.team_position_type (
				id,
				go_team_id,
				go_position_type_id,
				team_position_group_id,
				go_platoon_type_id,
				start_date,
				end_date
			)
			values (
				DEFAULT,
				go_team_id_parm::uuid,
				pos_type.go_position_type_id,
				new_group_id,
				pos_type.go_platoon_type_id,
				now(),
				null
			);
		END LOOP;	-- position type assignments per group
	END LOOP;	-- position groups

	return 1;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

--rollback DROP FUNCTION IF EXISTS initialize_new_team;