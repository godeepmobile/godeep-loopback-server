﻿--liquibase formatted sql

--changeset carlos:218 runOnChange:true splitStatements:false stripComments:false
--comment fixing view_team_player_season_stat

CREATE OR REPLACE VIEW view_team_player_season_stat AS
SELECT season_stat.id,
    season_stat.go_team_id,
    season_stat.team_player_id,
    season_stat.season,
    season_stat.num_games,
    season_stat.num_plays_game,
    season_stat.cat1_grade_game,
    season_stat.cat2_grade_game,
    season_stat.cat3_grade_game,
    season_stat.overall_grade_game,
    season_stat.unit_impact_grade_game,
    season_stat.impact_platoon_grade_game,
    season_stat.impact_posgroup_grade_game,
    season_stat.num_practices,
    season_stat.num_plays_practice,
    season_stat.cat1_grade_practice,
    season_stat.cat2_grade_practice,
    season_stat.cat3_grade_practice,
    season_stat.overall_grade_practice,
    season_stat.impact_platoon_grade_practice,
    season_stat.impact_posgroup_grade_practice,
    season_stat.num_allevents,
    season_stat.num_plays_allevent,
    season_stat.cat1_grade_allevent,
    season_stat.cat2_grade_allevent,
    season_stat.cat3_grade_allevent,
    season_stat.overall_grade_allevent,
    season_stat.impact_platoon_grade_allevent,
    season_stat.impact_posgroup_grade_allevent,
    season_stat.num_plays_last_game,
    season_stat.cat1_grade_last_game,
    season_stat.cat2_grade_last_game,
    season_stat.cat3_grade_last_game,
    season_stat.overall_grade_last_game,
    season_stat.impact_platoon_grade_last_game,
    season_stat.impact_posgroup_grade_last_game,
    season_stat.is_veteran,
    season_stat.overall_grade_game_rank,
    season_stat.rank_overall_practice,
    ( SELECT rank_at_position_stat.rank_game_at_post
           FROM player_grade_season_rank_at_position_stat rank_at_position_stat
          WHERE rank_at_position_stat.go_team_id = player.go_team_id AND 
		rank_at_position_stat.team_player_id = player.id AND 
		rank_at_position_stat.season = season_stat.season AND 
		rank_at_position_stat.go_position_type_id = (SELECT position_1 FROM view_team_player_assignment WHERE team_player_id = season_stat.team_player_id AND season = season_stat.season)) AS rank_game_at_post,
    ( SELECT rank_at_position_stat.rank_practice_at_post
           FROM player_grade_season_rank_at_position_stat rank_at_position_stat
          WHERE rank_at_position_stat.go_team_id = player.go_team_id AND 
		rank_at_position_stat.team_player_id = player.id AND 
		rank_at_position_stat.season = season_stat.season AND 
		rank_at_position_stat.go_position_type_id = (SELECT position_1 FROM view_team_player_assignment WHERE team_player_id = season_stat.team_player_id AND season = season_stat.season)) AS rank_practice_at_post,
    ( SELECT view_team_player_assignment.id
           FROM view_team_player_assignment
          WHERE view_team_player_assignment.team_player_id = season_stat.team_player_id AND view_team_player_assignment.season = season_stat.season) AS team_player_assignment_id,
    ( SELECT view_player_rank_overall.rank_at_grade_post
           FROM view_player_rank_overall
          WHERE view_player_rank_overall.team_player_id = season_stat.team_player_id AND view_player_rank_overall.season = season_stat.season LIMIT 1) AS rank_at_grade_post
   FROM team_player_season_stat season_stat
     JOIN view_team_player player ON season_stat.team_player_id = player.id;

--rollback DROP VIEW view_team_player_season_stat;