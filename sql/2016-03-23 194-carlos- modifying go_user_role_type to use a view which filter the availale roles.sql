﻿--liquibase formatted sql

--changeset carlos:194 runOnChange:true splitStatements:false stripComments:false
--comment modified go_user_role_type to use a view which filter the availale roles

ALTER TABLE go_user_role_type ADD COLUMN available boolean NOT NULL DEFAULT false;

UPDATE go_user_role_type SET name = 'Coach/Team Admin' WHERE id = 4;

UPDATE go_user_role_type SET available = true WHERE id = 3;
UPDATE go_user_role_type SET available = true WHERE id = 4;

CREATE OR REPLACE VIEW view_go_user_role_type AS
	SELECT role.id,
		role.name,
		role.description,
		role.created,
		role.modified
	FROM go_user_role_type role
	WHERE role.available = true;

--rollback DROP VIEW IF EXISTS view_go_user_role_type; ALTER TABLE go_user_role_type DROP COLUMN available; UPDATE go_user_role_type SET name = 'Team Admin' WHERE id = 4;