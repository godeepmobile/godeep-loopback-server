--liquibase formatted sql

--changeset rambert:166 runOnChange:true stripComments:false splitStatements:false
--comment create player_overall_season_rank_at_position_stat table

CREATE TABLE godeepmobile.player_overall_season_rank_at_position_stat  (
  go_team_id                 uuid NOT NULL,
  team_player_id             uuid NOT NULL,
  season                     integer NOT NULL,
  go_position_type_id        integer NOT NULL,
  rank_at_post               smallint,
  CONSTRAINT pk_player_overall_season_rank_at_position_stat PRIMARY KEY ( go_team_id, team_player_id, season, go_position_type_id),
  CONSTRAINT fk_player_overall_season_rank_at_position_stat_go_team FOREIGN KEY ( go_team_id )
      REFERENCES go_team (id),
  CONSTRAINT fk_player_overall_season_rank_at_position_stat_team_player FOREIGN KEY ( team_player_id )
      REFERENCES team_player (id),
  CONSTRAINT fk_player_overall_season_rank_at_position_stat_go_position_type FOREIGN KEY ( go_position_type_id )
      REFERENCES go_position_type (id)
);

--rollback DROP TABLE IF EXISTS  godeepmobile.player_overall_season_rank_at_position_stat;
