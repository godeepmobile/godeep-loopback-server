--liquibase formatted sql

--changeset carlos:4
ALTER TABLE godeepmobile.go_game_event ADD time TIME;
ALTER TABLE godeepmobile.go_game_event ADD location VARCHAR(256);
ALTER TABLE godeepmobile.go_game_event ADD city VARCHAR(256);
ALTER TABLE godeepmobile.go_game_event ADD go_state_id INTEGER;
ALTER TABLE godeepmobile.go_game_event ADD CONSTRAINT fk_team_player_go_state FOREIGN KEY (go_state_id) REFERENCES godeepmobile.go_state(id);
--rollback ALTER TABLE godeepmobile.go_game_event DROP COLUMN time; ALTER TABLE godeepmobile.go_game_event DROP COLUMN location; ALTER TABLE godeepmobile.go_game_event DROP COLUMN city; ALTER TABLE godeepmobile.go_game_event DROP COLUMN go_state_id;

--changeset carlos:5
ALTER TABLE godeepmobile.team_event ADD name VARCHAR(256);
--rollback ALTER TABLE godeepmobile.team_event DROP COLUMN name;
