--liquibase formatted sql

--changeset carlos:30 runOnChange:true
--comment added view_team_player_event_grades_by_cutup

DROP VIEW IF EXISTS view_team_player_event_grades_by_cutup;

CREATE OR REPLACE VIEW view_team_player_event_grades_by_cutup AS 
SELECT player.id AS "teamPlayer",
	player.season,
	count(grade.id)::integer AS "numPlays",
	sum(grade.cat_1_grade)::integer AS "sumCat1Grade",
	sum(grade.cat_2_grade)::integer AS "sumCat2Grade",
	sum(grade.cat_3_grade)::integer AS "sumCat3Grade",
	sum(grade.cat_1_grade + grade.cat_2_grade + grade.cat_3_grade)::integer AS "sumOverallGrade",
	avg(grade.cat_1_grade)::integer AS "avgCat1Grade",
	avg(grade.cat_2_grade)::integer AS "avgCat2Grade",
	avg(grade.cat_3_grade)::integer AS "avgCat3Grade",
	(avg(grade.cat_1_grade + grade.cat_2_grade + grade.cat_3_grade) / 3::numeric)::integer AS "avgOverallGrade",
	play.team_cutup_id AS "teamCutup",
	event.date,
	player.go_team_id AS team
FROM team_player player
	JOIN team_play_grade grade ON grade.team_player_id = player.id
	JOIN team_play_data play ON play.id = grade.team_play_data_id
	JOIN team_event event ON play.team_event_id = event.id
GROUP BY player.id, event.date, play.team_cutup_id
ORDER BY player.id;

--rollback DROP VIEW IF EXISTS view_team_player_event_grades_by_cutup;