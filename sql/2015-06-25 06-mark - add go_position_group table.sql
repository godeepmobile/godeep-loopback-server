--liquibase formatted sql

--changeset mark:6 runOnChange:true stripComments:false
--comment add go_position_group table
CREATE TABLE go_position_group (
  id                   serial  NOT NULL,
  name                 varchar(100)  NOT NULL,
  go_platoon_type_id   integer  ,
  CONSTRAINT pk_go_position_group PRIMARY KEY ( id )
 );
 --rollback drop TABLE IF EXISTS go_position_group;

CREATE INDEX idx_go_position_group ON go_position_group ( go_platoon_type_id );

INSERT INTO go_position_group( id, name, go_platoon_type_id ) VALUES ( 1, 'Quarterbacks', 1 );
INSERT INTO go_position_group( id, name, go_platoon_type_id ) VALUES ( 2, 'Running Backs', 1 );
INSERT INTO go_position_group( id, name, go_platoon_type_id ) VALUES ( 3, 'Wide Receivers', 1 );
INSERT INTO go_position_group( id, name, go_platoon_type_id ) VALUES ( 4, 'Offensive Linemen', 1 );
INSERT INTO go_position_group( id, name, go_platoon_type_id ) VALUES ( 5, 'Tight Ends', 1 );
INSERT INTO go_position_group( id, name, go_platoon_type_id ) VALUES ( 6, 'Defensive Backs', 2 );
INSERT INTO go_position_group( id, name, go_platoon_type_id ) VALUES ( 7, 'Linebackers', 2 );
INSERT INTO go_position_group( id, name, go_platoon_type_id ) VALUES ( 8, 'Defensive Linemen', 2 );
INSERT INTO go_position_group( id, name, go_platoon_type_id ) VALUES ( 9, 'Inside Linebackers', 2 );
INSERT INTO go_position_group( id, name, go_platoon_type_id ) VALUES ( 10, 'Outside Linebackers', 2 );
INSERT INTO go_position_group( id, name, go_platoon_type_id ) VALUES ( 11, 'Safeties', 2 );
INSERT INTO go_position_group( id, name, go_platoon_type_id ) VALUES ( 12, 'Cornerbacks', 2 );
INSERT INTO go_position_group( id, name, go_platoon_type_id ) VALUES ( 13, 'Kicking Specialists', 3 );
