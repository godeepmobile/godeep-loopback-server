--liquibase formatted sql

--changeset rambert:106 runOnChange:true stripComments:false splitStatements:false
--comment add is_prospect flag to team_player table

ALTER TABLE godeepmobile.team_player ADD COLUMN is_prospect boolean NOT NULL DEFAULT false;

--rollback ALTER TABLE godeepmobile.team_player DROP COLUMN is_prospect;
