--liquibase formatted sql

--changeset rambert:150 runOnChange:true splitStatements:false stripComments:false
--comment creating refresh_prospect_season_evaluation_rank_at_position function

CREATE OR REPLACE FUNCTION godeepmobile.refresh_prospect_season_evaluation_rank_at_position (
  team_id_parm uuid,
  season_parm integer,
  position_id_parm integer)
  RETURNS void AS
$BODY$
DECLARE
  record_value RECORD;
  record_exists integer;
BEGIN

  FOR record_value IN
    -- SELECT subquery.*,
    -- RANK() OVER (ORDER BY subquery.overall_eval_avg DESC, subquery.target_match DESC) AS rank_at_post
    -- FROM (
    -- SELECT
    --   go_team_id,
    --   team_prospect_id,
    --   season,
    --   go_position_type_id,
    --   COUNT(*) AS num_evaluations_at_post,
    --   AVG(overall_eval_avg) AS overall_eval_avg,
    --   AVG(target_match) AS target_match
    -- FROM godeepmobile.view_prospect_rank_overall_at_position
    -- WHERE go_position_type_id = position_id_parm
    --   AND go_team_id = team_id_parm::uuid
    --   AND season = season_parm
    -- GROUP BY go_team_id, season, team_prospect_id, go_position_type_id) AS subquery

    SELECT
      evaluation_season_stat.go_team_id,
      evaluation_season_stat.team_player_id,
      evaluation_season_stat.season,
      prospect.team_position_type_id_pos_1,
      RANK() OVER (ORDER BY evaluation_season_stat.overall_eval_avg DESC, evaluation_season_stat.target_match DESC) AS rank_at_post
    FROM godeepmobile.player_evaluation_season_stat evaluation_season_stat,
      godeepmobile.view_team_prospect prospect
    WHERE evaluation_season_stat.go_team_id = team_id_parm::uuid
      AND prospect.id = evaluation_season_stat.team_player_id
      AND evaluation_season_stat.season = season_parm
      AND prospect.team_position_type_id_pos_1 = position_id_parm

      LOOP
      -- check if record already exists
      SELECT COUNT(*) INTO record_exists
      FROM godeepmobile.player_rank_overall_at_position_season_stat
      WHERE go_team_id = team_id_parm::uuid
        AND team_player_id = record_value.team_player_id
        AND season = season_parm
        AND go_position_type_id = position_id_parm;

      IF record_exists = 0 THEN
        INSERT INTO godeepmobile.player_rank_overall_at_position_season_stat (
          go_team_id,
          team_player_id,
          season,
          go_position_type_id,
          --overall_eval_avg,
          --target_match,
          --num_evaluations_at_post,
          rank_at_post
        )
        VALUES (
          team_id_parm::uuid,
          record_value.team_player_id,
          season_parm,
          position_id_parm,
          -- record_value.overall_eval_avg,
          -- record_value.target_match,
          -- record_value.num_evaluations_at_post,
          record_value.rank_at_post
        );
      ELSE
        UPDATE
            godeepmobile.player_rank_overall_at_position_season_stat
        SET
      --  go_team_id,
        --team_player_id,
        -- overall_eval_avg = record_value.overall_eval_avg,
        -- target_match = record_value.target_match,
        -- num_evaluations_at_post = record_value.num_evaluations_at_post,
        rank_at_post = record_value.rank_at_post
        WHERE go_team_id = team_id_parm::uuid
            AND team_player_id = record_value.team_player_id
            AND season = season_parm
            AND go_position_type_id = position_id_parm;
      END IF;
      END LOOP;

END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

--rollback DROP FUNCTION godeepmobile.refresh_prospect_evaluation_rank_at_position(uuid, integer, integer);
