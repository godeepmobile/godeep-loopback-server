﻿--liquibase formatted sql

--changeset carlos:227 runOnChange:true splitStatements:false stripComments:false
--comment added create_cutup_for_each_platoon column to team_cofiguration table

ALTER TABLE team_configuration ADD create_cutup_for_each_platoon boolean NOT NULL DEFAULT true;

--rollback ALTER TABLE team_configuration DROP COLUMN create_cutup_for_each_platoon;