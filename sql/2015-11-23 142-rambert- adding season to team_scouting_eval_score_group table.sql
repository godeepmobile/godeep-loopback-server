--changeset rambert:142 runOnChange:true stripComments:false splitStatements:false
--comment add season to team_scouting_eval_score_group table

ALTER TABLE godeepmobile.team_scouting_eval_score_group ADD season integer;

UPDATE
    godeepmobile.team_scouting_eval_score_group
SET
    season = 2015;
--rollback ALTER TABLE godeepmobile.team_scouting_eval_score_group DROP COLUMN season;
