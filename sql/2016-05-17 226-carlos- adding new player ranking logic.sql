﻿--liquibase formatted sql

--changeset carlos:226 runOnChange:true splitStatements:false stripComments:false
--comment added new player ranking logic

CREATE OR REPLACE FUNCTION refresh_player_general_season_ranking(
    team_id_parm uuid,
    season_parm integer)
  RETURNS void AS
$BODY$
DECLARE
  record_value RECORD;
  record_exists integer;
BEGIN

  FOR record_value IN
    SELECT
      subquery.go_team_id,
      subquery.team_player_id,
      subquery.season,
      subquery.overall_evaluation_rank,
      subquery.overall_grade_game_rank,
      subquery.overall_grade_game,
      subquery.overall_grade_evaluation_value,
      subquery.target_match,
      subquery.overall_eval_avg,
      subquery.major_factors_eval_avg,
      subquery.critical_factors_eval_avg,
      subquery.position_skills_eval_avg,
      DENSE_RANK() OVER (ORDER BY subquery.overall_grade_game DESC, subquery.target_match DESC, subquery.overall_eval_avg DESC) AS rank,
      NULLIF((SELECT count(grade.id) FROM team_play_grade grade, team_play_data play, team_event evt 
	WHERE grade.team_play_data_id = play.id AND 
	play.team_event_id = evt.id AND evt.season = season_parm AND 
	grade.team_player_id = subquery.team_player_id AND 
	grade.go_team_id = team_id_parm::uuid), 0) AS num_plays
    FROM (
      SELECT grade_stat.go_team_id, player.id AS team_player_id, grade_stat.season, eval_stat.rank_overall AS overall_evaluation_rank, grade_stat.overall_grade_game_rank,
         NULLIF(ROUND((
         ((CASE WHEN (grade_stat.overall_grade_game_rank IS NOT NULL) THEN grade_stat.overall_grade_game_rank
              ELSE 0
           END) * 2::numeric)
         +
         ((CASE WHEN (eval_stat.rank_overall IS NOT NULL) THEN eval_stat.rank_overall
              ELSE 0
           END) )) / 3::numeric, 2), 0.00) AS overall_grade_evaluation_value,
         grade_stat.overall_grade_game,
         eval_stat.target_match,
         eval_stat.overall_eval_avg,
         eval_stat.major_factors_eval_avg,
         eval_stat.critical_factors_eval_avg,
         eval_stat.position_skills_eval_avg
        FROM godeepmobile.team_player_season_stat grade_stat FULL OUTER JOIN
             godeepmobile.view_team_player player
             ON grade_stat.team_player_id = player.id
             FULL OUTER JOIN godeepmobile.player_evaluation_season_stat eval_stat
             ON player.id = eval_stat.team_player_id
        WHERE player.go_team_id = team_id_parm::uuid
              AND (grade_stat.season = season_parm
                  OR eval_stat.season = season_parm)
        ORDER BY grade_stat.overall_grade_game, eval_stat.target_match, eval_stat.overall_eval_avg
      ) as subquery WHERE subquery.season = season_parm

      LOOP
      -- check if record already exists
      SELECT COUNT(id) INTO record_exists
      FROM godeepmobile.player_rank_overall
      WHERE go_team_id = team_id_parm::uuid
        AND team_player_id = record_value.team_player_id
        AND season = record_value.season;

      IF record_exists = 0 THEN
        INSERT INTO godeepmobile.player_rank_overall (
          go_team_id,
          team_player_id,
          season,
          rank_overall,
          overall_evaluation_rank,
          overall_grade_game_rank,
          rank_at_post,
          game_grade_avg,
          target_match,
          overall_eval_avg,
          major_factors_eval_avg,
          critical_factors_eval_avg,
          position_skills_eval_avg,
          num_plays
        )
        VALUES (
          team_id_parm::uuid,
          record_value.team_player_id,
          record_value.season,
          record_value.rank,
          record_value.overall_evaluation_rank,
          record_value.overall_grade_game_rank,
          0, -- rank at post tbd
          record_value.overall_grade_game,
          record_value.target_match,
          record_value.overall_eval_avg,
          record_value.major_factors_eval_avg,
          record_value.critical_factors_eval_avg,
          record_value.position_skills_eval_avg,
          record_value.num_plays
        );
      ELSE
        UPDATE
            godeepmobile.player_rank_overall
        SET
      --  go_team_id,
        --team_player_id,
        rank_overall = record_value.rank,
        overall_evaluation_rank = record_value.overall_evaluation_rank,
        overall_grade_game_rank = record_value.overall_grade_game_rank,
        rank_at_post = 0, -- rank at post tbd
        game_grade_avg = record_value.overall_grade_game,
        target_match = record_value.target_match,
        overall_eval_avg = record_value.overall_eval_avg,
        major_factors_eval_avg = record_value.major_factors_eval_avg,
        critical_factors_eval_avg = record_value.critical_factors_eval_avg,
        position_skills_eval_avg = record_value.position_skills_eval_avg,
        num_plays = record_value.num_plays
        WHERE go_team_id = team_id_parm::uuid
          AND team_player_id = record_value.team_player_id
          AND season = record_value.season;
      END IF;
      END LOOP;

END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

--rollback DROP FUNCTION godeepmobile.refresh_player_general_season_ranking;