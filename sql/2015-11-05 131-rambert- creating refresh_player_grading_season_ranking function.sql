--liquibase formatted sql

--changeset rambert:131 runOnChange:true splitStatements:false stripComments:false
--comment creating refresh_player_grading_season_ranking function

CREATE OR REPLACE FUNCTION godeepmobile.refresh_player_grading_season_ranking (
  team_id_parm uuid,
  season_parm integer)
  RETURNS void AS
$BODY$
DECLARE

BEGIN

  -- sets player overall grade game ranking
  UPDATE godeepmobile.team_player_season_stat
  SET overall_grade_game_rank = subquery.rank
  FROM (
    SELECT player.id AS player_id, stat.go_team_id,
           RANK() OVER (ORDER BY stat.overall_grade_game DESC, importance.go_importance_id DESC) AS rank
    	FROM godeepmobile.team_player_season_stat stat,
    	     godeepmobile.view_team_player player,
    	     godeepmobile.team_position_importance importance
    	WHERE stat.go_team_id = team_id_parm::uuid
    	     AND stat.go_team_id = player.go_team_id
    	     AND stat.team_player_id = player.id
    	     AND stat.go_team_id = importance.go_team_id
    	     AND player.team_position_type_id_pos_1 = importance.go_position_type_id
    	     AND stat.season = season_parm
  ) AS subquery
  WHERE godeepmobile.team_player_season_stat.team_player_id = subquery.player_id
    AND godeepmobile.team_player_season_stat.go_team_id = subquery.go_team_id;

  --refreshing player overall grade game ranking
  perform godeepmobile.refresh_player_general_season_ranking(team_id_parm, season_parm);

END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

--rollback DROP FUNCTION godeepmobile.refresh_player_grading_season_ranking(uuid, integer);
