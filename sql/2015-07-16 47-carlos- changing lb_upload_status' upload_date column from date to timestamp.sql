--liquibase formatted sql

--changeset carlos:47 runOnChange:true
--comment changed lb_upload_status' upload_date column from date to timestamp

ALTER TABLE godeepmobile.lb_upload_status ALTER COLUMN upload_date TYPE timestamp without time zone;

--rollback ALTER TABLE godeepmobile.lb_upload_status ALTER COLUMN upload_date TYPE date;