--liquibase formatted sql

--changeset rambert:37 runOnChange:true stripComments:false splitStatements:false
--comment add has_changed_password column to go_user table

ALTER TABLE godeepmobile.go_user ADD has_changed_password bool DEFAULT false;

--rollback ALTER TABLE godeepmobile.go_user DROP COLUMN has_changed_password;