--liquibase formatted sql

--changeset carlos:33 runOnChange:true splitStatements:false
--comment added create_game_stats_for_cutup function

CREATE OR REPLACE FUNCTION godeepmobile.create_game_stats_for_cutup(cutup_id_parm integer)
  RETURNS integer AS
$BODY$
DECLARE
	mviews RECORD;
	stat_id integer;
BEGIN
	FOR mviews IN SELECT * FROM godeepmobile.view_game_stat_ids_for_players_with_cutup_plays where team_cutup_id = cutup_id_parm AND team_player_game_stat_id IS NULL LOOP
		--RAISE NOTICE 'creating game stats for team player %', quote_ident(mviews.team_player_id::text);
		insert into
			godeepmobile.team_player_game_cutup_stat (
			  go_team_id,
			  team_player_id,
			  season,
			  team_cutup_id,
			  date,
			  last_update
		  )
		values (
			mviews.go_team_id,
			mviews.team_player_id,
			mviews.season,
			cutup_id_parm,
			mviews.date,
			now()
		)
		;
	END LOOP;
	RETURN 1;
END;
$BODY$
  LANGUAGE plpgsql;

--rollback DROP FUNCTION godeepmobile.create_game_stats_for_cutup(integer);

