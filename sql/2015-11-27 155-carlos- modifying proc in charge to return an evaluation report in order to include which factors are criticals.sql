﻿--liquibase formatted sql

--changeset rambert:155 runOnChange:true splitStatements:false stripComments:false
--comment mofified proc in charge to return an evaluation report in order to include which factors are criticals

CREATE OR REPLACE FUNCTION refresh_player_evaluation(
    team_id_parm uuid,
    evaluation_id_parm integer)
  RETURNS json AS
$BODY$
DECLARE
	target_match_value numeric;
	player_score_overall numeric;
  score_major_factors_eval_avg numeric;
  score_critical_factors_eval_avg numeric;
  score_position_skills_eval_avg numeric;
  evaluation_exists integer;
  is_prospect_value boolean;
  _records jsonb;
  evaluation RECORD;
BEGIN

  -- gets player target match
  SELECT ROUND(avg( (eval_score.score * 100) / target.calculated_target_score), 0) INTO target_match_value
    FROM godeepmobile.team_scouting_eval_score eval_score,
      godeepmobile.team_scouting_eval_criteria team_criteria,
      godeepmobile.team_scouting_eval_score_group score_group,
      godeepmobile.team_target_profile target
    WHERE eval_score.go_team_id = team_id_parm::uuid
      and eval_score.go_team_id = target.go_team_id
      and target.go_position_type_id = score_group.go_position_type_id
      and eval_score.team_scouting_eval_score_group_id = score_group.id
      and target.team_scouting_eval_criteria_id = eval_score.team_scouting_eval_criteria_id
      and eval_score.team_scouting_eval_criteria_id = team_criteria.id
      and score_group.id = evaluation_id_parm;

    -- gets player score overall
    SELECT  ROUND(avg(eval_score.score), 2) INTO player_score_overall
    FROM godeepmobile.team_scouting_eval_score eval_score,
      godeepmobile.team_scouting_eval_score_group score_group
    WHERE eval_score.go_team_id = team_id_parm::uuid
      and eval_score.team_scouting_eval_score_group_id = score_group.id
      and score_group.id = evaluation_id_parm;

    -- major_factors_eval_avg criteria type overall
    SELECT ROUND(avg(eval_score.score), 2) INTO score_major_factors_eval_avg
    FROM godeepmobile.team_scouting_eval_score eval_score,
      godeepmobile.team_scouting_eval_score_group score_group
    WHERE eval_score.go_team_id = team_id_parm::uuid
      AND eval_score.team_scouting_eval_score_group_id = score_group.id
      AND score_group.id = evaluation_id_parm
      AND eval_score.go_scouting_eval_criteria_type_id = 1;

    -- critical_factors_eval_avg criteria type overall
    SELECT ROUND(avg(eval_score.score), 2) INTO score_critical_factors_eval_avg
    FROM godeepmobile.team_scouting_eval_score eval_score,
      godeepmobile.team_scouting_eval_score_group score_group
    WHERE eval_score.go_team_id = team_id_parm::uuid
      AND eval_score.team_scouting_eval_score_group_id = score_group.id
      AND score_group.id = evaluation_id_parm
      AND eval_score.go_scouting_eval_criteria_type_id = 2;

    -- position_skills_eval_avg criteria type overall
    SELECT ROUND(avg(eval_score.score), 2) INTO score_position_skills_eval_avg
    FROM godeepmobile.team_scouting_eval_score eval_score,
      godeepmobile.team_scouting_eval_score_group score_group
    WHERE eval_score.go_team_id = team_id_parm::uuid
      AND eval_score.team_scouting_eval_score_group_id = score_group.id
      AND score_group.id = evaluation_id_parm
      AND eval_score.go_scouting_eval_criteria_type_id = 3;

  SELECT array_to_json(array_agg(row_to_json(records))) INTO _records
    FROM (
      SELECT criteria_type.id,
        criteria_type.name,
        ROUND(avg(eval_score.score), 2) AS "scoreAvg",
        ROUND(avg(target.calculated_target_score), 2) AS "targetScoreAvg",
        array_agg(team_criteria.name) AS "categories",
        array_agg(CASE WHEN team_criteria.is_critical THEN team_criteria.name ELSE null END) AS "criticals",
        array_agg(eval_score.score) AS "playerScores",
        array_agg(eval_score.note) AS "notes",
        array_agg(target.calculated_target_score) AS "targetScores"
      FROM godeepmobile.team_scouting_eval_score eval_score,
        godeepmobile.team_scouting_eval_criteria team_criteria,
        godeepmobile.team_scouting_eval_score_group score_group,
        godeepmobile.team_target_profile target,
        godeepmobile.go_scouting_eval_criteria_type criteria_type
      WHERE eval_score.go_team_id = team_id_parm::uuid
        AND eval_score.go_team_id = target.go_team_id
        AND target.go_position_type_id = score_group.go_position_type_id
        AND eval_score.team_scouting_eval_score_group_id = score_group.id
        AND target.team_scouting_eval_criteria_id = eval_score.team_scouting_eval_criteria_id
        AND eval_score.team_scouting_eval_criteria_id = team_criteria.id
        AND score_group.id = evaluation_id_parm
        AND eval_score.go_scouting_eval_criteria_type_id = criteria_type.id
        AND target.go_scouting_eval_criteria_type_id = eval_score.go_scouting_eval_criteria_type_id
      GROUP BY criteria_type.id ) records;

  SELECT COUNT(id) INTO evaluation_exists
  FROM godeepmobile.player_evaluation_stat
  WHERE go_team_id = team_id_parm::uuid
    AND team_scouting_eval_score_group_id = evaluation_id_parm;

  RAISE NOTICE 'evaluation_exists %', evaluation_exists;
	IF evaluation_exists = 0 THEN
    FOR evaluation IN
      SELECT * FROM godeepmobile.team_scouting_eval_score_group WHERE ID = evaluation_id_parm
    LOOP
      INSERT INTO godeepmobile.player_evaluation_stat (
        go_team_id,
        team_player_id,
        go_user_id,
        team_scouting_eval_score_group_id,
        season,
        target_match,
        overall_eval_avg,
        major_factors_eval_avg,
        critical_factors_eval_avg,
        position_skills_eval_avg
      )
      VALUES (
        team_id_parm::uuid,
        evaluation.team_player_id,
        evaluation.go_user_id,
        evaluation.id,
        2015,
        target_match_value,
        player_score_overall,
        score_major_factors_eval_avg,
        score_critical_factors_eval_avg,
        score_position_skills_eval_avg

      );
    END LOOP;
  ELSE
      UPDATE
          godeepmobile.player_evaluation_stat
      SET
          target_match = target_match_value,
          overall_eval_avg = player_score_overall,
          major_factors_eval_avg = score_major_factors_eval_avg,
          critical_factors_eval_avg = score_critical_factors_eval_avg,
          position_skills_eval_avg = score_position_skills_eval_avg
      WHERE go_team_id = team_id_parm::uuid
        AND team_scouting_eval_score_group_id = evaluation_id_parm;
	END IF;

  -- is prospect
  SELECT player.is_prospect INTO is_prospect_value
  FROM godeepmobile.team_scouting_eval_score_group score_group,
         godeepmobile.team_player player
  WHERE score_group.id = evaluation_id_parm
    AND score_group.go_team_id = team_id_parm::uuid
    AND score_group.go_team_id = player.go_team_id
    AND player.id = score_group.team_player_id;

  IF is_prospect_value THEN
    -- ranking for prospects
    perform godeepmobile.refresh_prospect_evaluation_ranking(team_id_parm, evaluation_id_parm);
    perform refresh_prospect_evaluation_season_ranking(team_id_parm, 2015);
  ELSE
    -- ranking for players
    perform godeepmobile.refresh_player_evaluation_ranking(team_id_parm, evaluation_id_parm);
    perform refresh_player_evaluation_season_ranking(team_id_parm, 2015);
  END IF;

  RETURN json_build_object('targetMatch', target_match_value,
                           'playerScoreOverall', player_score_overall,
                           'records', _records);
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
  
--rollback DROP FUNCTION refresh_player_evaluation(uuid, integer);