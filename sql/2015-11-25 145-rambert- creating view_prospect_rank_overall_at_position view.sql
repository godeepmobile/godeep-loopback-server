--liquibase formatted sql

--changeset rambert:145 runOnChange:true stripComments:false splitStatements:false
--comment create view_prospect_rank_overall_at_position

CREATE OR REPLACE VIEW view_prospect_rank_overall_at_position AS
  SELECT
  overall_at_position.go_team_id,
  overall_at_position.team_player_id as team_prospect_id,
  overall_at_position.season,
  overall_at_position.go_position_type_id,
  position_type.short_name,
  overall_at_position.team_scouting_eval_score_group_id,
  overall_at_position.overall_eval_avg,
  overall_at_position.target_match,
  overall_at_position.rank_at_post
  FROM godeepmobile.player_rank_overall_at_position overall_at_position
      JOIN godeepmobile.view_team_prospect prospect
      ON overall_at_position.team_player_id = prospect.id,
      go_position_type position_type
  WHERE position_type.id = go_position_type_id;

--rollback DROP VIEW view_prospect_rank_overall_at_position;
