--liquibase formatted sql

--changeset carlos:11 runOnChange:true
--comment added reference to team_cutup on team_play_data
ALTER TABLE godeepmobile.team_play_data ADD team_cutup_id INTEGER;
ALTER TABLE godeepmobile.team_play_data ADD CONSTRAINT fk_team_play_data_team_cutup FOREIGN KEY (team_cutup_id) REFERENCES godeepmobile.team_cutup(id);

--rollback DROP CONSTRAINT fk_team_play_data_team_cutup; ALTER TABLE godeepmobile.team_play_data DROP COLUMN team_cutup_id;