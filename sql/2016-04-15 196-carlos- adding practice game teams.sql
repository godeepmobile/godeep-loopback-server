﻿--liquibase formatted sql

--changeset carlos:196 runOnChange:true splitStatements:false stripComments:false
--comment added practice game teams

ALTER TABLE team_event ADD practice_game_team_1 varchar(512);
ALTER TABLE team_event ADD practice_game_team_2 varchar(512);

CREATE OR REPLACE VIEW view_team_event AS 
 SELECT event.id,
    event.go_team_id,
    event.date,
    event."time",
    event.go_event_type_id,
    event.go_field_condition_id,
    event.go_surface_type_id,
    event.description,
    event.grade_base,
    event.grade_increment,
    event.data,
    event.season,
    event.name,
    event.location,
    event.city,
    event.go_state_id,
    event.is_home_game,
    event.score,
    event.opponent_score,
    event.opponent_organization_id,
    ( SELECT org.name
           FROM go_organization org
          WHERE org.id = event.opponent_organization_id) AS opponent_name,
    ( SELECT org.short_name
           FROM go_organization org
          WHERE org.id = event.opponent_organization_id) AS opponent_short_name,
    ( SELECT org.espn_name
           FROM go_organization org
          WHERE org.id = event.opponent_organization_id) AS opponent_abbreviation,
    ( SELECT org.mascot
           FROM go_organization org
          WHERE org.id = event.opponent_organization_id) AS opponent_mascot,
    ( SELECT team.name
           FROM view_go_team team
          WHERE team.id = event.go_team_id) AS team_name,
    ( SELECT team.short_name
           FROM view_go_team team
          WHERE team.id = event.go_team_id) AS team_short_name,
    ( SELECT team.abbreviation
           FROM view_go_team team
          WHERE team.id = event.go_team_id) AS team_abbreviation,
    ( SELECT team.mascot
           FROM view_go_team team
          WHERE team.id = event.go_team_id) AS team_mascot,
    ( SELECT event_type.is_practice
           FROM go_event_type event_type
          WHERE event_type.id = event.go_event_type_id) AS is_practice,
    ( SELECT event_type.is_match
           FROM go_event_type event_type
          WHERE event_type.id = event.go_event_type_id) AS is_match,
    event.practice_game_team_1,
    event.practice_game_team_2
   FROM team_event event;

--rollback DROP VIEW view_team_event; ALTER TABLE team_event DROP COLUMN practice_game_team_1; ALTER TABLE team_event DROP COLUMN practice_game_team_2;