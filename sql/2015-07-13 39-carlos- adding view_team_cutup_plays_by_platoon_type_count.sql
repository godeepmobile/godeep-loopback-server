--liquibase formatted sql

--changeset carlos:39 runOnChange:true splitStatements:false
--comment added view_team_cutup_plays_by_platoon_type_count

DROP VIEW IF EXISTS godeepmobile.view_team_cutup_plays_by_platoon_type_count;

CREATE OR REPLACE VIEW godeepmobile.view_team_cutup_plays_by_platoon_type_count AS
 SELECT plays.team_cutup_id,
    count(*) AS num_plays,
    sum(
        CASE
            WHEN plays.platoon = 1 THEN 1
            ELSE 0
        END) AS offense_plays,
    sum(
        CASE
            WHEN plays.platoon = 2 THEN 1
            ELSE 0
        END) AS defense_plays,
    sum(
        CASE
            WHEN plays.platoon = 3 THEN 1
            ELSE 0
        END) AS special_teams_plays,
    sum(
        CASE
            WHEN plays.platoon = 0 THEN 1
            ELSE 0
        END) AS unknown_unassigned_plays
   FROM ( SELECT play.team_cutup_id,
            COALESCE(
                CASE
                    WHEN play.game_down >= 1 AND play.game_down <= 3 THEN
                    CASE
                        WHEN play.game_possession::text = team.abbreviation::text THEN 1
                        ELSE 2
                    END
                    WHEN play.game_down = 4 THEN 3
                    ELSE NULL::integer
                END,
                CASE
                    WHEN play_type.id = ANY (ARRAY[6, 7, 8, 9, 10]) THEN 3
                    ELSE NULL::integer
                END, ( SELECT
                        CASE
                            WHEN sum(
                            CASE
                                WHEN platoon_type.id = 3 THEN 1
                                ELSE 0
                            END) >= 2 THEN 3
                            WHEN sum(
                            CASE
                                WHEN platoon_type.id = 1 THEN 1
                                ELSE 0
                            END) >= 2 THEN 1
                            WHEN sum(
                            CASE
                                WHEN platoon_type.id = 2 THEN 1
                                ELSE 0
                            END) >= 2 THEN 2
                            ELSE NULL::integer
                        END AS "case"
                   FROM team_play_grade grade,
                    team_player player,
                    team_position_type position_played,
                    go_platoon_type platoon_type
                  WHERE grade.team_player_id = player.id AND grade.position_played = position_played.id AND position_played.go_platoon_type_id = platoon_type.id AND grade.team_play_data_id = play.id), 0) AS platoon
           FROM team_play_data play
             JOIN go_team team ON play.go_team_id = team.id
             LEFT JOIN go_play_type play_type ON play.go_play_type_id = play_type.id) plays
  GROUP BY plays.team_cutup_id;

  --rollback DROP VIEW godeepmobile.view_team_cutup_plays_by_platoon_type_count;
