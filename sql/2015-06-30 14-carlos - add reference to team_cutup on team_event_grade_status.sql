--liquibase formatted sql

--changeset carlos:14 runOnChange:true
--comment added reference to team_cutup on team_event_grade_status
ALTER TABLE godeepmobile.team_event_grade_status ADD team_cutup_id INTEGER;
ALTER TABLE godeepmobile.team_event_grade_status ADD CONSTRAINT fk_team_event_grade_status_team_cutup FOREIGN KEY (team_cutup_id) REFERENCES godeepmobile.team_cutup(id);

-- updating sample data
UPDATE godeepmobile.team_event_grade_status SET team_cutup_id = 1 WHERE id < 6;

--rollback DROP CONSTRAINT fk_team_event_grade_status_team_cutup; ALTER TABLE godeepmobile.team_event_grade_status DROP COLUMN team_cutup_id;