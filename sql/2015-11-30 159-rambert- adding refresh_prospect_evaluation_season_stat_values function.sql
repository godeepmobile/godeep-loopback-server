--liquibase formatted sql

--changeset rambert:156 runOnChange:true splitStatements:false stripComments:false
--comment updating refresh_prospect_evaluation_season_stat_values function

CREATE OR REPLACE FUNCTION godeepmobile.refresh_prospect_evaluation_season_stat_values(
  team_id_parm uuid,
  season_parm integer)
  RETURNS void AS
$BODY$
DECLARE
  record_value RECORD;
  record_exists integer;
BEGIN

  -- generate evaluation stat values
  FOR record_value IN
    SELECT score_group.*
    FROM godeepmobile.team_scouting_eval_score_group score_group
      JOIN view_team_prospect prospect ON score_group.team_player_id = prospect.id
      WHERE score_group.go_team_id = team_id_parm::uuid
        AND score_group.season = season_parm
  LOOP
      perform godeepmobile.refresh_evaluation_stat_values(team_id_parm, record_value.id);
      perform godeepmobile.refresh_prospect_evaluation_rank_at_position(team_id_parm, season_parm, record_value.go_position_type_id);
      perform godeepmobile.refresh_prospect_season_evaluation_rank_at_position(team_id_parm, season_parm, record_value.go_position_type_id);
  END LOOP;
  perform godeepmobile.refresh_prospect_evaluation_ranking(team_id_parm, season_parm);
  perform godeepmobile.refresh_prospect_evaluation_season_ranking(team_id_parm, season_parm);

  /*
  --perform godeepmobile.refresh_prospect_evaluation_ranking(team_id_parm, evaluation_id_parm);
  perform godeepmobile.refresh_prospect_evaluation_season_ranking(team_id_parm, current_season);
  --perform godeepmobile.refresh_prospect_evaluation_rank_at_position(team_id_parm, current_season, current_position_type_id);
  --perform godeepmobile.refresh_prospect_season_evaluation_rank_at_position(team_id_parm, current_season, current_position_type_id);
  */

END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

--rollback DROP FUNCTION godeepmobile.refresh_prospect_evaluation_season_stat_values(uuid, integer);
