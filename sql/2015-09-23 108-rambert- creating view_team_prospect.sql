--liquibase formatted sql

--changeset rambert:108 runOnChange:true stripComments:false splitStatements:false
--comment creating view_team_prospect

CREATE OR REPLACE VIEW view_team_prospect AS
 SELECT team_player.id,
    team_player.last_name,
    team_player.middle_name,
    team_player.first_name,
    team_player.go_team_id,
    team_player.data,
    team_player.go_user_team_assignment_id,
    team_player.class_year,
    team_player.birth_date,
    team_player.jersey_number,
    team_player.season,
    team_player.height,
    team_player.weight,
    team_player.graduation_year,
    team_player.wingspan,
    team_player.forty_yard,
    team_player.go_body_type_id,
    team_player.hand_size,
    team_player.vertical_jump,
    team_player.sat,
    team_player.act,
    team_player.gpa,
    team_player.go_state_id,
    team_player.team_position_type_id_pos_1,
    team_player.team_position_type_id_pos_2,
    team_player.team_position_type_id_pos_3,
    team_player.team_position_type_id_pos_st,
    team_player.high_school,
    team_player.coach,
    team_player.coach_phone,
    team_player.go_level_id,
    team_player.is_prospect,
    team_player.start_date,
    team_player.end_date
   FROM team_player
  WHERE team_player.end_date IS NULL AND team_player.is_prospect = true
  ORDER BY team_player.jersey_number;

--rollback DROP VIEW view_team_prospect;
