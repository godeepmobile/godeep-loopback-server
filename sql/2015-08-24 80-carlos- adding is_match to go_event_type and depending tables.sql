﻿--liquibase formatted sql

--changeset carlos:80 runOnChange:true
--comment added is_match to go_event_type and depending tables

ALTER TABLE go_event_type ADD is_match boolean DEFAULT false;

UPDATE go_event_type SET is_match = true WHERE id = 1;
INSERT INTO go_event_type VALUES (3, 'Preseason/Practice Game', 'Practice Game', 'Preseason-Practice', true, true);

CREATE OR REPLACE VIEW view_team_event AS 
 SELECT event.id,
    event.go_team_id,
    event.date,
    event."time",
    event.go_event_type_id,
    event.go_field_condition_id,
    event.go_surface_type_id,
    event.description,
    event.grade_base,
    event.grade_increment,
    event.data,
    event.season,
    event.name,
    event.location,
    event.city,
    event.go_state_id,
    event.is_home_game,
    event.score,
    event.opponent_score,
    event.opponent_organization_id,
    ( SELECT org.name
           FROM go_organization org
          WHERE org.id = event.opponent_organization_id) AS opponent_name,
    ( SELECT org.short_name
           FROM go_organization org
          WHERE org.id = event.opponent_organization_id) AS opponent_short_name,
    ( SELECT org.espn_name
           FROM go_organization org
          WHERE org.id = event.opponent_organization_id) AS opponent_abbreviation,
    ( SELECT org.mascot
           FROM go_organization org
          WHERE org.id = event.opponent_organization_id) AS opponent_mascot,
    ( SELECT team.name
           FROM view_go_team team
          WHERE team.id = event.go_team_id) AS team_name,
    ( SELECT team.short_name
           FROM view_go_team team
          WHERE team.id = event.go_team_id) AS team_short_name,
    ( SELECT team.abbreviation
           FROM view_go_team team
          WHERE team.id = event.go_team_id) AS team_abbreviation,
    ( SELECT team.mascot
           FROM view_go_team team
          WHERE team.id = event.go_team_id) AS team_mascot,
    ( SELECT event_type.is_practice
           FROM go_event_type event_type
          WHERE event_type.id = event.go_event_type_id) AS is_practice,
    ( SELECT event_type.is_match
           FROM go_event_type event_type
          WHERE event_type.id = event.go_event_type_id) AS is_match
   FROM team_event event;

--rollback ALTER TABLE go_event_type DROP COLUMN is_match; DELETE FROM go_event_type WHERE id = 3;