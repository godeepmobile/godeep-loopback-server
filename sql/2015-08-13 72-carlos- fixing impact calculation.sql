﻿--liquibase formatted sql

--changeset carlos:72 runOnChange:true splitStatements:false
--comment fixed impact calculation

CREATE OR REPLACE FUNCTION refresh_season_stats(
    team_id_parm uuid,
    season_parm integer)
  RETURNS void AS
$BODY$
DECLARE
    game_scores RECORD;
BEGIN
	perform godeepmobile.create_season_stats_for_team(team_id_parm, season_parm);
	FOR game_scores IN (
		SELECT
			team
			,"teamPlayer"
			,season
			,count("teamEvent")     as "numGames"
			,sum("numPlays")        as "numPlays"
			,sum("sumCat1Grade")    as "sumCat1Grade"
			,sum("sumCat2Grade")    as "sumCat2Grade"
			,sum("sumCat3Grade")    as "sumCat3Grade"
			,sum("sumOverallGrade") as "sumOverallGrade"
			,ROUND((sum("sumCat1Grade")::numeric / sum("numPlays"))::numeric, 2)        as "avgCat1Grade"
			,ROUND((sum("sumCat2Grade")::numeric / sum("numPlays"))::numeric, 2)        as "avgCat2Grade"
			,ROUND((sum("sumCat3Grade")::numeric / sum("numPlays"))::numeric, 2)        as "avgCat3Grade"
			,ROUND((sum("sumOverallGrade")::numeric / (3*sum("numPlays")))::numeric, 2) as "avgOverallGrade"
		FROM
			godeepmobile.view_team_player_event_grades
		WHERE
			team = team_id_parm and season = season_parm
		GROUP BY
			team, "teamPlayer", season
		ORDER BY
			"teamPlayer"
	)
	LOOP



        with
		season_stat_id as (select id from godeepmobile.team_player_season_stat where team_player_id = game_scores."teamPlayer" and game_scores.season = season_parm),
		last_game as (select * from godeepmobile.view_team_player_event_grades where "teamPlayer" = game_scores."teamPlayer" and game_scores.season = season_parm ORDER BY date DESC LIMIT 1),
		team_conf as (select grade_base, player_multiplier_1, player_multiplier_2, player_multiplier_3, platoon_multiplier_1, platoon_multiplier_2, platoon_multiplier_3 from godeepmobile.team_configuration where go_team_id = team_id_parm),

		plays_with_player as (SELECT grade.team_play_data_id
			FROM team_play_grade grade, 
				team_play_data play, 
				view_team_event evt 
			WHERE grade.team_player_id = game_scores."teamPlayer" AND 
				grade.go_team_id = team_id_parm AND
				grade.team_play_data_id = play.id AND 
				play.team_event_id = evt.id AND 
				evt.is_practice = false AND 
				evt.season = season_parm),
				
		player_primary_posgroup_platoon as (SELECT position_type.team_position_group_id AS "positionGroupId", position_type.go_platoon_type_id AS "platoonTypeId"
			FROM (SELECT grade.position_played, COUNT(*) AS num_grades
				FROM godeepmobile.team_play_grade grade, 
					godeepmobile.view_team_play_with_event play
				WHERE grade.team_play_data_id = play.id AND 
					play.is_practice = false AND
					grade.team_player_id = game_scores."teamPlayer" AND
					play.season = season_parm
				GROUP BY grade.position_played
				ORDER BY num_grades DESC LIMIT 1) AS team_player_primary_position,
				godeepmobile.view_team_position_type position_type
			WHERE position_type.go_team_id = team_id_parm AND
				team_player_primary_position.position_played = position_type.go_position_type_id AND
				position_type.end_date IS NULL),

		overall_grades_without_player as (SELECT ROUND((grade.cat_1_grade + grade.cat_2_grade + grade.cat_3_grade)/3::numeric, 2) AS overall_grade,
				pt.team_position_group_id,
				pt.go_platoon_type_id
			FROM team_play_grade grade, 
				team_play_data play, 
				view_team_event evt,
				view_team_position_type pt
			WHERE grade.go_team_id = team_id_parm AND
				grade.team_play_data_id = play.id AND 
				play.team_event_id = evt.id AND 
				evt.is_practice = false AND 
				evt.season = season_parm AND
				grade.position_played = pt.go_position_type_id AND
				pt.go_team_id = team_id_parm AND
				grade.team_play_data_id NOT IN (SELECT team_play_data_id FROM plays_with_player)),
		
		impact_posgroup as (SELECT ROUND(AVG(CASE WHEN team_player_id = game_scores."teamPlayer"
					THEN
						CASE
							WHEN overall > (select grade_base from team_conf) AND go_play_factor_id = 3 THEN
								CASE
									WHEN hard THEN overall * (select player_multiplier_2 from team_conf)
								ELSE
									overall * (select player_multiplier_1 from team_conf)
								END
							WHEN overall < (select grade_base from team_conf) AND go_play_factor_id = 2 THEN
								overall * (select player_multiplier_3 from team_conf)
						ELSE
							overall
						END
					ELSE
						overall
				END), 2) - (SELECT ROUND(AVG(overall_grade), 2) FROM overall_grades_without_player WHERE team_position_group_id IN (SELECT "positionGroupId" FROM player_primary_posgroup_platoon)) AS "impactPosgroupGradeGame" 
			FROM (
				SELECT g.team_player_id, 
					ROUND((g.cat_1_grade + g.cat_2_grade + g.cat_3_grade)/3::numeric, 2) AS overall,
					g.go_play_factor_id,
					g.hard
				FROM godeepmobile.team_play_grade g, 
					godeepmobile.view_team_position_type pt 
				WHERE g.go_team_id = team_id_parm AND
					g.position_played = pt.go_position_type_id AND 
					pt.team_position_group_id IN (SELECT "positionGroupId" FROM player_primary_posgroup_platoon) AND
					g.team_play_data_id IN (SELECT team_play_data_id FROM plays_with_player)
			) AS grade_overalls),
		impact_platoon as (SELECT ROUND(AVG(CASE WHEN team_player_id = game_scores."teamPlayer"
					THEN
						CASE
							WHEN overall > (select grade_base from team_conf) AND go_play_factor_id = 3 THEN
								CASE
									WHEN hard THEN overall * (select platoon_multiplier_2 from team_conf)
								ELSE
									overall * (select platoon_multiplier_1 from team_conf)
								END
							WHEN overall < (select grade_base from team_conf) AND go_play_factor_id = 2 THEN
								overall * (select platoon_multiplier_3 from team_conf)
						ELSE
							overall
						END
					ELSE
						overall
				END), 2) - (SELECT ROUND(AVG(overall_grade), 2) FROM overall_grades_without_player WHERE go_platoon_type_id IN (SELECT "platoonTypeId" FROM player_primary_posgroup_platoon)) AS "impactPlatoonGradeGame" 
			FROM (
				SELECT g.team_player_id, 
					ROUND((g.cat_1_grade + g.cat_2_grade + g.cat_3_grade)/3::numeric, 2) AS overall,
					g.go_play_factor_id,
					g.hard
				FROM godeepmobile.team_play_grade g, 
					godeepmobile.view_team_position_type pt 
				WHERE g.go_team_id = team_id_parm AND
					g.position_played = pt.go_position_type_id AND
					pt.go_platoon_type_id IN (SELECT "platoonTypeId" FROM player_primary_posgroup_platoon) AND
					g.team_play_data_id IN (SELECT team_play_data_id FROM plays_with_player)
			) AS grade_overalls)
        update
		godeepmobile.team_player_season_stat
        SET
		num_games                   = game_scores."numGames"
		,num_plays_game             = game_scores."numPlays"
		,cat1_grade_game            = game_scores."avgCat1Grade"
		,cat2_grade_game            = game_scores."avgCat2Grade"
		,cat3_grade_game            = game_scores."avgCat3Grade"
		,overall_grade_game         = game_scores."avgOverallGrade"
		,num_plays_allevent         = game_scores."numPlays"
		,cat1_grade_allevent        = game_scores."avgCat1Grade"
		,cat2_grade_allevent        = game_scores."avgCat2Grade"
		,cat3_grade_allevent        = game_scores."avgCat3Grade"
		,overall_grade_allevent     = game_scores."avgOverallGrade"
		,num_plays_last_game        = last_game."numPlays"
		,cat1_grade_last_game       = last_game."avgCat1Grade"
		,cat2_grade_last_game       = last_game."avgCat2Grade"
		,cat3_grade_last_game       = last_game."avgCat3Grade"
		,overall_grade_last_game    = last_game."avgOverallGrade"
		,impact_posgroup_grade_game = (select "impactPosgroupGradeGame" from impact_posgroup)
		,impact_platoon_grade_game  = (select "impactPlatoonGradeGame" from impact_platoon)
		,unit_impact_grade_game     = ROUND(((select "impactPosgroupGradeGame" from impact_posgroup) + (select "impactPlatoonGradeGame" from impact_platoon)) / 2::numeric, 2)
	FROM
		last_game
        WHERE
		godeepmobile.team_player_season_stat.id = (select id from season_stat_id);
    END LOOP;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

--rollback DROP FUNCTION godeepmobile.refresh_season_stats(uuid, integer);