--liquibase formatted sql

--changeset rambert:120 runOnChange:true stripComments:false splitStatements:false
--comment adding multiplier and critical flag for player evaluation

-- add player_evaluation_greater_multiplier and player_evaluation_lesser_multiplier to team_configuration
ALTER TABLE godeepmobile.team_configuration ADD player_evaluation_greater_multiplier numeric DEFAULT 2;
ALTER TABLE godeepmobile.team_configuration ADD player_evaluation_lesser_multiplier numeric DEFAULT 0.5;

-- add is_critical flag to team_scouting_eval_criteria
ALTER TABLE godeepmobile.team_scouting_eval_criteria ADD COLUMN is_critical boolean NOT NULL DEFAULT false;

-- adding is_critical flag to view_team_target_scouting_eval_criteria
DROP VIEW IF EXISTS view_team_target_scouting_eval_criteria;
CREATE OR REPLACE VIEW view_team_target_scouting_eval_criteria AS
SELECT criteria.id,
	criteria.name,
	criteria.is_critical,
	criteria.go_team_id,
	criteria.go_importance_id,
	criteria.go_position_type_id,
	criteria.go_scouting_eval_criteria_type_id,
	(SELECT target.id
	 FROM team_target_profile target
	 WHERE criteria.id = target.team_scouting_eval_criteria_id
	   AND target.go_team_id = criteria.go_team_id
	   AND target.go_position_type_id = criteria.go_position_type_id
	   AND target.go_scouting_eval_criteria_type_id = criteria.go_scouting_eval_criteria_type_id
	   ) as team_target_profile_id,
	(SELECT target.calculated_target_score
	 FROM team_target_profile target
	 WHERE criteria.id = target.team_scouting_eval_criteria_id
	   AND target.go_team_id = criteria.go_team_id
	   AND target.go_position_type_id = criteria.go_position_type_id
	   AND target.go_scouting_eval_criteria_type_id = criteria.go_scouting_eval_criteria_type_id
	   ) as calculated_target_score
FROM team_scouting_eval_criteria criteria;

--rollback ALTER TABLE godeepmobile.team_configuration DROP COLUMN player_evaluation_greater_multiplier; ALTER TABLE godeepmobile.team_configuration DROP COLUMN player_evaluation_lesser_multiplier; ALTER TABLE godeepmobile.team_scouting_eval_criteria DROP COLUMN is_critical; DROP VIEW IF EXISTS view_team_target_scouting_eval_criteria;
