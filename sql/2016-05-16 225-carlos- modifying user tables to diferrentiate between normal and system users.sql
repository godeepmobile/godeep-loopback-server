﻿--liquibase formatted sql

--changeset carlos:225 runOnChange:true splitStatements:false stripComments:false
--comment modified user tables to diferrentiate between normal and system users

ALTER TABLE go_user ADD is_system_user boolean DEFAULT false;

UPDATE go_user SET is_system_user = true WHERE user_name LIKE 'admin_%';

DROP VIEW IF EXISTS view_go_user;
CREATE OR REPLACE VIEW view_go_user AS
	SELECT *,concat("left"(first_name::text, 1), "left"(last_name::text, 1)) AS initials
	FROM go_user WHERE logical_delete = false;

CREATE OR REPLACE VIEW view_active_user_non_players AS 
 SELECT ( SELECT guta.id
           FROM go_user_team_assignment guta,
            go_user_role_type gurt
          WHERE gu.id = guta.go_user_id AND guta.go_user_role_type_id = gurt.id AND gurt.id <> 1 AND guta.end_date IS NULL) AS go_user_team_assignment_id,
    ( SELECT guta.go_team_id
           FROM go_user_team_assignment guta,
            go_user_role_type gurt
          WHERE gu.id = guta.go_user_id AND guta.go_user_role_type_id = gurt.id AND gurt.id <> 1 AND guta.end_date IS NULL) AS go_team_id,
    ( SELECT gurt.name
           FROM go_user_team_assignment guta,
            go_user_role_type gurt
          WHERE gu.id = guta.go_user_id AND guta.go_user_role_type_id = gurt.id AND gurt.id <> 1 AND guta.end_date IS NULL) AS role,
    ( SELECT gurt.id
           FROM go_user_team_assignment guta,
            go_user_role_type gurt
          WHERE gu.id = guta.go_user_id AND guta.go_user_role_type_id = gurt.id AND gurt.id <> 1 AND guta.end_date IS NULL) AS go_user_role_type_id,
    gu.id,
    gu.user_name,
    gu.pass_word,
    gu.first_name,
    gu.middle_name,
    gu.last_name,
    gu.email,
    gu.job_title,
    gu.office_phone,
    gu.mobile_phone,
    (gu.last_name::text || ', '::text) || gu.first_name::text AS full_name
   FROM go_user gu
  WHERE gu.id IN ( SELECT gu.id
           FROM go_user_team_assignment guta,
            go_user_role_type gurt
          WHERE gu.id = guta.go_user_id AND guta.go_user_role_type_id = gurt.id AND gurt.id <> 1 AND guta.end_date IS NULL) AND gu.is_system_user = false
  ORDER BY gu.last_name, gu.first_name;

--rollback ALTER TABLE go_user DROP COLUMN is_system_user;