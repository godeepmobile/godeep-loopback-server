--liquibase formatted sql

--changeset rambert:165 runOnChange:true splitStatements:false stripComments:false
--comment updating view_team_player_season_stat view view

CREATE OR REPLACE VIEW view_team_player_season_stat AS
	 SELECT season_stat.*,
	( SELECT rank_at_position_stat.rank_game_at_post
						 FROM player_grade_season_rank_at_position_stat rank_at_position_stat
						WHERE rank_at_position_stat.go_team_id = player.go_team_id
							AND rank_at_position_stat.team_player_id = player.id
							AND rank_at_position_stat.season = season_stat.season
							AND rank_at_position_stat.go_position_type_id = player.team_position_type_id_pos_1) AS rank_game_at_post,
				( SELECT rank_at_position_stat.rank_practice_at_post
						 FROM player_grade_season_rank_at_position_stat rank_at_position_stat
						WHERE rank_at_position_stat.go_team_id = player.go_team_id
							AND rank_at_position_stat.team_player_id = player.id
							AND rank_at_position_stat.season = season_stat.season
							AND rank_at_position_stat.go_position_type_id = player.team_position_type_id_pos_1) AS rank_practice_at_post
		 FROM team_player_season_stat season_stat
			 JOIN view_team_player player ON season_stat.team_player_id = player.id;

--rollback DROP VIEW view_team_player_season_stat;
