--liquibase formatted sql

--changeset rambert:114 runOnChange:true stripComments:false splitStatements:false
--comment create view_team_target_scouting_eval_criteria view and importance pick list

CREATE TABLE godeepmobile.go_importance (
  id          integer  NOT NULL ,
  name        character varying(100) ,
  description character varying(256) ,
  CONSTRAINT  pk_go_importance PRIMARY KEY ( id )
);

-- insert important picklist values
INSERT INTO godeepmobile.go_importance( id, name, description )
                               VALUES ( 1, 'Least', 'Least important' );
INSERT INTO godeepmobile.go_importance( id, name, description )
                               VALUES ( 2, 'Less', 'Less important' );
INSERT INTO godeepmobile.go_importance( id, name, description )
                               VALUES ( 3, 'Normal', 'Normal important' );
INSERT INTO godeepmobile.go_importance( id, name, description )
                               VALUES ( 4, 'More', 'More important' );
INSERT INTO godeepmobile.go_importance( id, name, description )
                               VALUES ( 5, 'Most', 'Most important' );

-- add go_importance_id constraint to team_scouting_eval_criteria
ALTER TABLE godeepmobile.team_scouting_eval_criteria ADD go_importance_id INTEGER DEFAULT 3;
ALTER TABLE godeepmobile.team_scouting_eval_criteria ADD CONSTRAINT fk_team_scouting_eval_criteria_go_importance FOREIGN KEY ( go_importance_id ) REFERENCES godeepmobile.go_importance( id );

ALTER TABLE godeepmobile.team_scouting_eval_criteria DROP CONSTRAINT weight;
ALTER TABLE godeepmobile.team_scouting_eval_criteria DROP COLUMN weight RESTRICT;

CREATE OR REPLACE VIEW view_team_target_scouting_eval_criteria AS
SELECT criteria.id,
	criteria.name,
	criteria.go_team_id,
	criteria.go_importance_id,
	criteria.go_position_type_id,
	criteria.go_scouting_eval_criteria_type_id,
	(SELECT target.id
	 FROM team_target_profile target
	 WHERE criteria.id = target.team_scouting_eval_criteria_id
	   AND target.go_team_id = criteria.go_team_id
	   AND target.go_position_type_id = criteria.go_position_type_id
	   AND target.go_scouting_eval_criteria_type_id = criteria.go_scouting_eval_criteria_type_id
	   ) as team_target_profile_id,
	(SELECT target.calculated_target_score
	 FROM team_target_profile target
	 WHERE criteria.id = target.team_scouting_eval_criteria_id
	   AND target.go_team_id = criteria.go_team_id
	   AND target.go_position_type_id = criteria.go_position_type_id
	   AND target.go_scouting_eval_criteria_type_id = criteria.go_scouting_eval_criteria_type_id
	   ) as calculated_target_score
FROM team_scouting_eval_criteria criteria;

--rollback DROP VIEW view_team_scouting_eval_criteria; ALTER TABLE godeepmobile.team_scouting_eval_criteria DROP COLUMN go_importance_id RESTRICT; DROP TABLE IF EXISTS godeepmobile.go_importance;
