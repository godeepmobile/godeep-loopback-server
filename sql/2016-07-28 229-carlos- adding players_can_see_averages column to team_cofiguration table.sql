﻿--liquibase formatted sql

--changeset carlos:229 runOnChange:true splitStatements:false stripComments:false
--comment added players_can_see_averages column to team_cofiguration table

ALTER TABLE team_configuration ADD players_can_see_averages boolean NOT NULL DEFAULT true;

--rollback ALTER TABLE team_configuration DROP COLUMN players_can_see_averages;