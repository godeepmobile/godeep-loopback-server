﻿--liquibase formatted sql

--changeset carlos:78 runOnChange:true
--comment changed team_event_grade_status to store dates with timezones

DROP VIEW IF EXISTS view_team_event_grade_status;
DROP VIEW IF EXISTS view_active_team_graders;

ALTER TABLE team_event_grade_status ALTER COLUMN completed_date TYPE timestamp with time zone;
ALTER TABLE team_event_grade_status ALTER COLUMN started_date TYPE timestamp with time zone;

CREATE OR REPLACE VIEW view_team_event_grade_status AS 
 SELECT team_event_grade_status.id,
    team_event_grade_status.go_team_id,
    team_event_grade_status.go_user_id,
    team_event_grade_status.team_event_id,
    team_event_grade_status.team_cutup_id,
    team_event_grade_status.completed_date,
    team_event_grade_status.started_date,
        CASE
            WHEN team_event_grade_status.started_date IS NULL THEN 'not started'::text
            WHEN team_event_grade_status.completed_date IS NOT NULL THEN 'completed'::text
            ELSE 'in process'::text
        END AS status
   FROM team_event_grade_status;

CREATE OR REPLACE VIEW view_active_team_graders AS 
 SELECT guta.id,
    guta.go_team_id,
    guta.go_user_id,
    gurt.name AS role,
    ( SELECT "row".completed_date
           FROM ( SELECT team_event_grade_status.go_user_id,
                    team_event_grade_status.go_team_id,
                    team_event_grade_status.team_event_id,
                    team_event_grade_status.completed_date
                   FROM team_event_grade_status
                  WHERE team_event_grade_status.completed_date IS NOT NULL
                  ORDER BY team_event_grade_status.completed_date DESC
                 LIMIT 1) "row"
          WHERE "row".go_user_id = guta.go_user_id) AS last_submit_date,
    concat(gu.first_name, ' ', gu.last_name) AS user_name
   FROM go_user_team_assignment guta,
    go_user gu,
    go_user_role_type gurt,
    go_role_permission grp
  WHERE gu.id = guta.go_user_id AND gurt.id = guta.go_user_role_type_id AND gurt.id = grp.go_user_role_type_id AND grp.go_permission_id::text = 'GRADER'::text AND guta.end_date IS NULL;

--rollback ALTER TABLE team_event_grade_status ALTER COLUMN completed_date TYPE timestamp without time zone; ALTER TABLE team_event_grade_status ALTER COLUMN started_date TYPE timestamp without time zone;