--changeset rambert:134 runOnChange:true stripComments:false splitStatements:false
--comment add overall_eval_avg player_rank_overall table

-- add overall_eval_avg constraint to player_rank_overall
ALTER TABLE godeepmobile.player_rank_overall ADD overall_eval_avg numeric(100,2);

--rollback ALTER TABLE godeepmobile.player_rank_overall DROP COLUMN overall_eval_avg;
