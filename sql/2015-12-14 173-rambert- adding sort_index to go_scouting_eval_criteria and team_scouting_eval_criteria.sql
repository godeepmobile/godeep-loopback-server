--liquibase formatted sql

--changeset rambert:173 runOnChange:true stripComments:false splitStatements:false
--comment adding sort_index to go_scouting_eval_criteria and team_scouting_eval_criteria

-- add sort_index to go_scouting_eval_criteria
ALTER TABLE godeepmobile.go_scouting_eval_criteria ADD sort_index smallint;

-- inserting sort_index values
UPDATE go_scouting_eval_criteria SET
sort_index = subquery.sort_index
FROM (
  SELECT eval_criteria.id, eval_criteria.go_position_type_id, eval_criteria.go_scouting_eval_criteria_type_id,
    RANK() OVER (PARTITION BY eval_criteria.go_position_type_id, eval_criteria.go_scouting_eval_criteria_type_id  ORDER BY eval_criteria.id ASC) AS sort_index
  FROM go_scouting_eval_criteria eval_criteria
) AS subquery
WHERE go_scouting_eval_criteria.id = subquery.id
  AND go_scouting_eval_criteria.go_position_type_id = subquery.go_position_type_id
  AND go_scouting_eval_criteria.go_scouting_eval_criteria_type_id = subquery.go_scouting_eval_criteria_type_id;

-- add sort_index to go_scouting_eval_criteria
ALTER TABLE godeepmobile.team_scouting_eval_criteria ADD sort_index smallint;

-- inserting sort_index values
UPDATE team_scouting_eval_criteria SET
  sort_index = subquery.sort_index
FROM (
  SELECT team_criteria.go_team_id,
    go_criteria.name,
    go_criteria.go_position_type_id,
    go_criteria.go_scouting_eval_criteria_type_id,
    go_criteria.sort_index
  FROM go_scouting_eval_criteria go_criteria,
    team_scouting_eval_criteria team_criteria
  WHERE go_criteria.name = team_criteria.name
  AND go_criteria.go_position_type_id = team_criteria.go_position_type_id
  AND go_criteria.go_scouting_eval_criteria_type_id = team_criteria.go_scouting_eval_criteria_type_id
  ORDER BY team_criteria.go_team_id, team_criteria.go_position_type_id,
  team_criteria.go_scouting_eval_criteria_type_id, go_criteria.sort_index) AS subquery
WHERE team_scouting_eval_criteria.go_team_id = subquery.go_team_id
  AND team_scouting_eval_criteria.name = subquery.name
  AND team_scouting_eval_criteria.go_position_type_id = subquery.go_position_type_id
  AND team_scouting_eval_criteria.go_scouting_eval_criteria_type_id = subquery.go_scouting_eval_criteria_type_id;
--rollback ALTER TABLE godeepmobile.go_scouting_eval_criteria DROP COLUMN sort_index; ALTER TABLE godeepmobile.team_scouting_eval_criteria DROP COLUMN sort_index;
