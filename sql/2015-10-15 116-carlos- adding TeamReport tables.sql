﻿--liquibase formatted sql

--changeset carlos:116 runOnChange:true
--comment added TeamReport tables

CREATE TABLE godeepmobile.go_report_type (
  id                   integer  NOT NULL,
  name                 varchar(100)  NOT NULL,
  description          varchar(256)  ,
  path                 varchar(100)  ,
  CONSTRAINT pk_go_report_type PRIMARY KEY ( id )
 );

INSERT INTO godeepmobile.go_report_type(id, name, description, path) VALUES (1, 'Game Performance', 'Game Performance report', 'gamePerformance');
INSERT INTO godeepmobile.go_report_type(id, name, description, path) VALUES (2, 'Prepare for Group Game Film Review', 'Prepare for Group Game Film Review', 'gameGroupReview');
INSERT INTO godeepmobile.go_report_type(id, name, description, path) VALUES (3, 'Player Comparison', 'Player Comparison', 'playerComparison');
INSERT INTO godeepmobile.go_report_type(id, name, description, path) VALUES (4, 'Down and/or Distance Analysis', 'Down and/or Distance Analysis', 'gameDownDistanceAnalysis');
INSERT INTO godeepmobile.go_report_type(id, name, description, path) VALUES (5, 'Field Position Analysis (Red Zone, etc)', 'Field Position Analysis (Red Zone, etc)', 'gameFieldPositionAnalysis');
INSERT INTO godeepmobile.go_report_type(id, name, description, path) VALUES (6, 'Pick Plays by Play Call, Personnel Group, or other Play Data', 'Pick Plays by Play Call, Personnel Group, or other Play Data', 'gamePlayCallPersonnelAnalysis');

CREATE TABLE godeepmobile.team_report (
  id                   serial  NOT NULL,
  go_team_id           uuid  NOT NULL,
  go_report_type_id    int NOT NULL,
  name                 varchar(256) NOT NULL,
  criteria             varchar(256) NOT NULL,
  criteria_range       varchar(512) NOT NULL,
  scope                varchar(256) NOT NULL,
  scope_range          varchar(512) NOT NULL,
  condition            varchar(512),
  condition_range      varchar(512),
  go_user_id           int NOT NULL,
  is_public            boolean DEFAULT false,
  CONSTRAINT pk_team_report PRIMARY KEY ( id ),
  CONSTRAINT fk_team_report_go_team FOREIGN KEY ( go_team_id ) REFERENCES godeepmobile.go_team( id ),
  CONSTRAINT fk_team_report_go_report_type FOREIGN KEY ( go_report_type_id ) REFERENCES godeepmobile.go_report_type( id ),
  CONSTRAINT fk_team_report_go_user FOREIGN KEY ( go_user_id ) REFERENCES godeepmobile.go_user( id )
);

--rollback DROP TABLE godeepmobile.team_report; DROP TABLE godeepmobile.go_report_type;