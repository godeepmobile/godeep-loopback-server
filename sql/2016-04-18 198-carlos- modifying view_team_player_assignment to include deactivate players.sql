﻿--liquibase formatted sql

--changeset carlos:198 runOnChange:true splitStatements:false stripComments:false
--comment modified view_team_player_assignment to include deactivate players

CREATE OR REPLACE VIEW view_team_player_assignment AS 
 SELECT tpa.id,
    tpa.team_player_id,
    tpa.go_team_id,
    tpa.go_user_team_assignment_id,
    tpa.jersey_number,
    tpa.position_1,
    tpa.position_2,
    tpa.position_3,
    tpa.position_st,
    tpa.start_date,
    tpa.end_date,
    tpa.is_veteran,
        CASE
            WHEN date_part('month'::text, tpa.start_date)::integer <= 2 THEN date_part('year'::text, tpa.start_date)::integer - 1
            ELSE date_part('year'::text, tpa.start_date)::integer
        END AS season,
    ( WITH offense AS (
                 SELECT go_position_type.id
                   FROM go_position_type
                  WHERE go_position_type.go_platoon_type_id = 1
                )
         SELECT
                CASE
                    WHEN (tpa.position_1 IN ( SELECT offense.id
                       FROM offense)) OR (tpa.position_2 IN ( SELECT offense.id
                       FROM offense)) OR (tpa.position_3 IN ( SELECT offense.id
                       FROM offense)) THEN true
                    ELSE false
                END AS "case") AS is_offense,
    ( WITH defense AS (
                 SELECT go_position_type.id
                   FROM go_position_type
                  WHERE go_position_type.go_platoon_type_id = 2
                )
         SELECT
                CASE
                    WHEN (tpa.position_1 IN ( SELECT defense.id
                       FROM defense)) OR (tpa.position_2 IN ( SELECT defense.id
                       FROM defense)) OR (tpa.position_3 IN ( SELECT defense.id
                       FROM defense)) THEN true
                    ELSE false
                END AS "case") AS is_defense,
        CASE
            WHEN tpa.position_st IS NULL THEN false
            ELSE true
        END AS is_special_teams,
    ( SELECT tpcs.id
           FROM team_player_career_stat tpcs
          WHERE tpa.team_player_id = tpcs.team_player_id AND tpcs.go_team_id = tpa.go_team_id) AS team_player_career_stat,
    ARRAY( SELECT tpss.id
           FROM team_player_season_stat tpss
          WHERE tpa.team_player_id = tpss.team_player_id AND tpss.go_team_id = tpa.go_team_id
          ORDER BY tpss.season DESC) AS team_player_season_stats
   FROM team_player_assignment tpa
  WHERE tpa.is_prospect = false;

--rollback DROP VIEW view_team_player_assignment;