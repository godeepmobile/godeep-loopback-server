--liquibase formatted sql

--changeset rambert:152 runOnChange:true stripComments:false splitStatements:false
--comment create view_player_rank_overall_at_position

CREATE OR REPLACE VIEW view_player_rank_overall_at_position_season_stat AS
  SELECT
    position_season_stat.go_team_id,
    position_season_stat.team_player_id,
    position_season_stat.season,
    position_season_stat.go_position_type_id,
    position_season_stat.overall_eval_avg,
    position_season_stat.target_match,
    position_season_stat.num_evaluations_at_post,
    position_season_stat.rank_at_post
  FROM godeepmobile.player_rank_overall_at_position_season_stat position_season_stat
      JOIN godeepmobile.view_team_player player
      ON position_season_stat.team_player_id = player.id;

CREATE OR REPLACE VIEW view_prospect_rank_overall_at_position_season_stat AS
  SELECT
    position_season_stat.go_team_id,
    position_season_stat.team_player_id AS team_prospect_id,
    position_season_stat.season,
    position_season_stat.go_position_type_id,
    position_season_stat.overall_eval_avg,
    position_season_stat.target_match,
    position_season_stat.num_evaluations_at_post,
    position_season_stat.rank_at_post
  FROM godeepmobile.player_rank_overall_at_position_season_stat position_season_stat
      JOIN godeepmobile.view_team_prospect prospect
      ON position_season_stat.team_player_id = prospect.id;

--rollback DROP VIEW view_player_rank_overall_at_position_season_stat; DROP VIEW view_prospect_rank_overall_at_position_season_stat;
