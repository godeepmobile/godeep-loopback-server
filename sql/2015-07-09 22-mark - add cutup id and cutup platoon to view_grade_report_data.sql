--liquibase formatted sql

--changeset mark:22 runOnChange:true stripComments:false splitStatements:false
--comment add cutup id and cutup platoon columns to view_grade_report_data view

--modify table by adding a new column for short name
DROP VIEW IF EXISTS view_grade_report_data;
CREATE OR REPLACE VIEW view_grade_report_data AS
 SELECT tpg.id,
    tpd.go_team_id,
    te.season,
    te.date,
    te.go_field_condition_id,
    te.go_surface_type_id,
    gge.home_team_id,
    gge.away_team_id,
    gge.home_team_id = tpd.go_team_id AS at_home,
    tpd.team_event_id,
    tpd.play_number,
    tpd.quarter,
    tpd.game_down,
    tpd.game_distance,
    tpd.yard_line,
    tpd.go_play_type_id,
    tpd.game_possession,
    tpd.go_play_result_type_id,
    tpg.id AS team_play_grade_id,
    tpg.team_player_id,
    tpg.position_played AS team_position_type_id,
    tpg.go_platoon_type_id,
    tpg.team_position_group_id,
    tpg.go_user_id,
    tpg.hard,
    tpg.poa,
    tpg.cat_1_grade AS cat1_grade,
    tpg.cat_2_grade AS cat2_grade,
    tpg.cat_3_grade AS cat3_grade,
    tpg.cat_1_grade + tpg.cat_2_grade + tpg.cat_3_grade AS sum_overall_grade,
    (tpg.cat_1_grade + tpg.cat_2_grade + tpg.cat_3_grade) / 3 AS avg_overall_grade,
    abs(tpd.yard_line) <= 20 AS red_zone,
    tc.id AS team_cutup_id,
    gpt.short_name AS cutup_platoon_short_name,
    tpg.factor_in_play
   FROM team_play_data tpd
     JOIN team_event te ON tpd.team_event_id = te.id
     JOIN team_play_grade tpg ON tpd.id = tpg.team_play_data_id
     JOIN go_game_event gge ON te.go_game_event_id = gge.id
     JOIN team_cutup tc ON tpd.team_cutup_id = tc.id
     JOIN go_platoon_type gpt ON tc.which_platoon = gpt.id;


--rollback DROP VIEW view_grade_report_data;CREATE OR REPLACE VIEW view_grade_report_data AS SELECT tpg.id, tpd.go_team_id, te.season, te.date, te.go_field_condition_id, te.go_surface_type_id, gge.home_team_id, gge.away_team_id, gge.home_team_id = tpd.go_team_id AS at_home, tpd.team_event_id, tpd.play_number, tpd.quarter, tpd.game_down, tpd.game_distance, tpd.yard_line, tpd.go_play_type_id, tpd.game_possession, tpd.go_play_result_type_id, tpg.id AS team_play_grade_id, tpg.team_player_id, tpg.position_played AS team_position_type_id, tpg.go_platoon_type_id, tpg.team_position_group_id, tpg.go_user_id, tpg.hard, tpg.poa, tpg.cat_1_grade AS cat1_grade, tpg.cat_2_grade AS cat2_grade, tpg.cat_3_grade AS cat3_grade, tpg.cat_1_grade + tpg.cat_2_grade + tpg.cat_3_grade AS sum_overall_grade, (tpg.cat_1_grade + tpg.cat_2_grade + tpg.cat_3_grade) / 3 AS avg_overall_grade, abs(tpd.yard_line) <= 20 AS red_zone FROM team_play_data tpd JOIN team_event te ON tpd.team_event_id = te.id JOIN team_play_grade tpg ON tpd.id = tpg.team_play_data_id JOIN go_game_event gge ON te.go_game_event_id = gge.id;
