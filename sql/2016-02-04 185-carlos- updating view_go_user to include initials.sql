﻿--liquibase formatted sql

--changeset carlos:185 runOnChange:true splitStatements:false stripComments:false
--comment updated view_go_user to include initials

DROP VIEW view_go_user;

CREATE OR REPLACE VIEW view_go_user AS 
	SELECT *, CONCAT(LEFT(first_name, 1), LEFT(last_name, 1)) AS initials FROM go_user WHERE logical_delete = false;

--rollback DROP VIEW view_go_user;