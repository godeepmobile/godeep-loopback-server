--liquibase formatted sql

--changeset carlos:25 runOnChange:true
--comment updated go_play_factor_id on team_play_grade

UPDATE godeepmobile.team_play_grade SET go_play_factor_id = (SELECT id FROM godeepmobile.go_play_factor WHERE value = factor_in_play);

--rollback UPDATE godeepmobile.team_play_grade SET go_play_factor_id = null;