--liquibase formatted sql

--changeset rambert:175 runOnChange:true stripComments:false splitStatements:false
--comment droping team_target_profile table

DROP VIEW IF EXISTS view_team_target_scouting_eval_criteria;
DROP TABLE IF EXISTS team_target_profile;
