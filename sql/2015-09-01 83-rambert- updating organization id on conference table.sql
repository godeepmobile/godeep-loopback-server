--liquibase formatted sql

--changeset rambert:83 runOnChange:true stripComments:false splitStatements:false
--comment change organization id on conference table so that only NFL levels have 3 and the rest have 2

UPDATE godeepmobile.go_conference
SET go_organization_type_id = 2
WHERE id IN (21,22,23,24,25,26,27,28);

--rollback UPDATE godeepmobile.go_conference SET go_organization_type_id = 3 WHERE id IN (21,22,23,24,25,26,27,28);