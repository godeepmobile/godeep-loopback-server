﻿--liquibase formatted sql

--changeset rambert:102 runOnChange:true stripComments:false splitStatements:false
--comment create go_scouting_eval_criteria table

CREATE TABLE godeepmobile.go_scouting_eval_criteria (
  id                                  integer  NOT NULL,
  name                                varchar(100)  NOT NULL,
  weight                              smallint NOT NULL DEFAULT 6,
  go_scouting_eval_criteria_type_id   integer  NOT NULL,
  CONSTRAINT pk_go_scouting_eval_criteria PRIMARY KEY ( id ),
  CONSTRAINT fk_go_scouting_eval_criteria_go_scouting_eval_criteria_type FOREIGN KEY (go_scouting_eval_criteria_type_id)
      REFERENCES go_scouting_eval_criteria_type (id)
);

INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_scouting_eval_criteria_type_id, name, weight)
  VALUES  ( 1, 1, 'Personal/Behavior', 6);
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_scouting_eval_criteria_type_id, name, weight)
  VALUES  ( 2, 1, 'Athletic Ability', 6);
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_scouting_eval_criteria_type_id, name, weight)
  VALUES  ( 3, 1, 'Strength & Explosion', 6);
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_scouting_eval_criteria_type_id, name, weight)
  VALUES  ( 4, 1, 'Competitiveness', 6);
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_scouting_eval_criteria_type_id, name, weight)
  VALUES  ( 5, 1, 'Toughness', 6);
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_scouting_eval_criteria_type_id, name, weight)
  VALUES  ( 6, 1, 'Mental/Learning', 6);
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_scouting_eval_criteria_type_id, name, weight)
  VALUES  ( 7, 1, 'Injury/Durability', 6);
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_scouting_eval_criteria_type_id, name, weight)
  VALUES  ( 8, 2, 'Leadership', 6);
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_scouting_eval_criteria_type_id, name, weight)
  VALUES  ( 9, 2, 'Decision Making', 6);
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_scouting_eval_criteria_type_id, name, weight)
  VALUES  ( 10, 2, 'Overall Accuracy', 6);
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_scouting_eval_criteria_type_id, name, weight)
  VALUES  ( 11, 2, 'Clutch Production', 6);
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_scouting_eval_criteria_type_id, name, weight)
  VALUES  ( 12, 3, 'Accuracy Short', 6);
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_scouting_eval_criteria_type_id, name, weight)
  VALUES  ( 13, 3, 'Accuracy Long', 6);
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_scouting_eval_criteria_type_id, name, weight)
  VALUES  ( 14, 3, 'Arm Strength', 6);
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_scouting_eval_criteria_type_id, name, weight)
  VALUES  ( 15, 3, 'Mechanics', 6);
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_scouting_eval_criteria_type_id, name, weight)
  VALUES  ( 16, 3, 'Mobility', 6);
INSERT INTO godeepmobile.go_scouting_eval_criteria
          ( id, go_scouting_eval_criteria_type_id, name, weight)
  VALUES  ( 17, 3, 'Type of Production - System / Skills', 6);


--rollback DROP TABLE IF EXISTS  godeepmobile.go_scouting_eval_criteria;
