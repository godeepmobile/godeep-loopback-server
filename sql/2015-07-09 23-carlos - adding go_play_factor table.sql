--liquibase formatted sql

--changeset carlos:23 runOnChange:true
--comment added go_play_factor table

CREATE TABLE godeepmobile.go_play_factor (
  id integer NOT NULL,
  name varchar(100) NOT NULL,
  value smallint,
  CONSTRAINT pk_go_play_factor PRIMARY KEY ( id )
);

INSERT INTO godeepmobile.go_play_factor VALUES (1, 'Neutral', 0);
INSERT INTO godeepmobile.go_play_factor VALUES (2, 'Negative', -1);
INSERT INTO godeepmobile.go_play_factor VALUES (3, 'Positive', 1);

--rollback DROP TABLE godeepmobile.go_play_factor;