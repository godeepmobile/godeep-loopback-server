--liquibase formatted sql

--changeset carlos:32 runOnChange:true splitStatements:false
--comment added view_game_stat_ids_for_players_with_cutup_plays

DROP VIEW IF EXISTS godeepmobile.view_game_stat_ids_for_players_with_cutup_plays;

CREATE OR REPLACE VIEW godeepmobile.view_game_stat_ids_for_players_with_cutup_plays AS 
 SELECT grades.team AS go_team_id,
    grades."teamPlayer" AS team_player_id,
    grades."teamCutup" AS team_cutup_id,
    grades."numPlays" AS num_plays,
    stats.id AS team_player_game_cutup_stat_id,
    grades.season,
    grades.date
   FROM godeepmobile.view_team_player_event_grades_by_cutup grades
     LEFT JOIN team_player_game_cutup_stat stats ON stats.team_player_id = grades."teamPlayer"
  ORDER BY grades."teamPlayer";

--rollback DROP VIEW godeepmobile.view_game_stat_ids_for_players_with_cutup_plays;

