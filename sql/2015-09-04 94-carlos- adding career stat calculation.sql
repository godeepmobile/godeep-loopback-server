﻿--liquibase formatted sql

--changeset carlos:94 runOnChange:true splitStatements:false stripComments:false
--comment added career stat calculation

-- making the score column to be numeric on team_player_career_stat
DROP VIEW view_team_player_career_stat;

ALTER TABLE team_player_career_stat ALTER COLUMN cat1_grade_allevent TYPE numeric(1000, 2) USING cat1_grade_allevent::numeric;
ALTER TABLE team_player_career_stat ALTER COLUMN cat2_grade_allevent TYPE numeric(1000, 2) USING cat2_grade_allevent::numeric;
ALTER TABLE team_player_career_stat ALTER COLUMN cat3_grade_allevent TYPE numeric(1000, 2) USING cat3_grade_allevent::numeric;
ALTER TABLE team_player_career_stat ALTER COLUMN overall_grade_allevent TYPE numeric(1000, 2) USING overall_grade_allevent::numeric;
ALTER TABLE team_player_career_stat ALTER COLUMN impact_posgroup_grade_allevent TYPE numeric(1000, 2) USING impact_platoon_grade_allevent::numeric;
ALTER TABLE team_player_career_stat ALTER COLUMN impact_platoon_grade_allevent TYPE numeric(1000, 2) USING impact_platoon_grade_allevent::numeric;

ALTER TABLE team_player_career_stat ALTER COLUMN cat1_grade_practice TYPE numeric(1000, 2) USING cat1_grade_practice::numeric;
ALTER TABLE team_player_career_stat ALTER COLUMN cat2_grade_practice TYPE numeric(1000, 2) USING cat2_grade_practice::numeric;
ALTER TABLE team_player_career_stat ALTER COLUMN cat3_grade_practice TYPE numeric(1000, 2) USING cat3_grade_practice::numeric;
ALTER TABLE team_player_career_stat ALTER COLUMN overall_grade_practice TYPE numeric(1000, 2) USING overall_grade_practice::numeric;
ALTER TABLE team_player_career_stat ALTER COLUMN impact_posgroup_grade_practice TYPE numeric(1000, 2) USING impact_platoon_grade_practice::numeric;
ALTER TABLE team_player_career_stat ALTER COLUMN impact_platoon_grade_practice TYPE numeric(1000, 2) USING impact_platoon_grade_practice::numeric;

ALTER TABLE team_player_career_stat ALTER COLUMN cat1_grade_game TYPE numeric(1000, 2) USING cat1_grade_game::numeric;
ALTER TABLE team_player_career_stat ALTER COLUMN cat2_grade_game TYPE numeric(1000, 2) USING cat2_grade_game::numeric;
ALTER TABLE team_player_career_stat ALTER COLUMN cat3_grade_game TYPE numeric(1000, 2) USING cat3_grade_game::numeric;
ALTER TABLE team_player_career_stat ALTER COLUMN overall_grade_game TYPE numeric(1000, 2) USING overall_grade_game::numeric;
ALTER TABLE team_player_career_stat ALTER COLUMN impact_posgroup_grade_game TYPE numeric(1000, 2) USING impact_platoon_grade_game::numeric;
ALTER TABLE team_player_career_stat ALTER COLUMN impact_platoon_grade_game TYPE numeric(1000, 2) USING impact_platoon_grade_game::numeric;

CREATE OR REPLACE VIEW view_team_player_career_stat AS
	SELECT * FROM team_player_career_stat
	WHERE team_player_id IN (SELECT id FROM team_player WHERE end_date IS NULL);

-- view for career stats ids
CREATE OR REPLACE VIEW view_career_stat_ids_for_players AS 
	SELECT 
		player.go_team_id,
		player.id AS team_player_id,
		stats.id AS team_player_career_stat_id,
		player.season,
		player.is_veteran
	FROM view_team_player player
	LEFT JOIN team_player_career_stat stats ON stats.team_player_id = player.id
	ORDER BY player.id;

-- helper function to create career stat record for each player on the team
CREATE OR REPLACE FUNCTION create_career_stats_for_team(
    team_id_parm uuid
 )
  RETURNS void AS
$BODY$
DECLARE
	mviews RECORD;
	stat_id integer;
BEGIN
	FOR mviews IN SELECT * FROM godeepmobile.view_career_stat_ids_for_players where go_team_id = team_id_parm AND team_player_career_stat_id IS NULL LOOP
		RAISE NOTICE 'creating career stats for team player %', quote_ident(mviews.team_player_id::text);
		insert into
			godeepmobile.team_player_career_stat (
			  go_team_id,
			  team_player_id,
			  is_veteran
		  )
		values (
			mviews.go_team_id,
			mviews.team_player_id,
			mviews.is_veteran
		)
		;
	END LOOP;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

-- function to refresh the career stats
CREATE OR REPLACE FUNCTION refresh_career_stats(
    team_id_parm uuid)
  RETURNS void AS
$BODY$
DECLARE
    season_scores RECORD;
BEGIN
	perform godeepmobile.create_career_stats_for_team(team_id_parm);
	FOR season_scores IN ( 
		SELECT
			team_player_id AS "teamPlayer",
			COUNT(season) AS "numSeasons",
			SUM(num_games) AS "numGames",
			SUM(num_plays_game) AS "numPlaysGames",
			ROUND(AVG(cat1_grade_game), 2) AS "cat1GradeGame",
			ROUND(AVG(cat2_grade_game), 2) AS "cat2GradeGame",
			ROUND(AVG(cat3_grade_game), 2) AS "cat3GradeGame",
			ROUND(AVG(overall_grade_game), 2) AS "overallGradeGame",
			ROUND(AVG(impact_posgroup_grade_game), 2) AS "impactPosgroupGradeGame",
			ROUND(AVG(impact_platoon_grade_game), 2) AS "impactPlatoonGradeGame",
			SUM(num_practices) AS "numPractices",
			SUM(num_plays_practice) AS "numPlaysPractice",
			ROUND(AVG(cat1_grade_practice), 2) AS "cat1GradePractice",
			ROUND(AVG(cat2_grade_practice), 2) AS "cat2GradePractice",
			ROUND(AVG(cat3_grade_practice), 2) AS "cat3GradePractice",
			ROUND(AVG(overall_grade_practice), 2) AS "overallGradePractice",
			ROUND(AVG(impact_posgroup_grade_practice), 2) AS "impactPosgroupGradePractice",
			ROUND(AVG(impact_platoon_grade_practice), 2) AS "impactPlatoonGradePractice",
			SUM(num_allevents) AS "numAllEvents",
			SUM(num_plays_allevent) AS "numPlaysAllEvents",
			ROUND(AVG(cat1_grade_allevent), 2) AS "cat1GradeAllEvent",
			ROUND(AVG(cat2_grade_allevent), 2) AS "cat2GradeAllEvent",
			ROUND(AVG(cat3_grade_allevent), 2) AS "cat3GradeAllEvent",
			ROUND(AVG(overall_grade_allevent), 2) AS "overallGradeAllEvent",
			ROUND(AVG(impact_posgroup_grade_allevent), 2) AS "impactPosgroupGradeAllEvent",
			ROUND(AVG(impact_platoon_grade_allevent), 2) AS "impactPlatoonGradeAllEvent"
		FROM godeepmobile.team_player_season_stat 
		WHERE go_team_id = '00000000-0000-0000-0000-000000000006' -- CAHNGE HERE team_id_parm
		GROUP BY team_player_id
	) 
	LOOP

		WITH stat_id AS (SELECT id FROM godeepmobile.team_player_career_stat WHERE team_player_id = season_scores."teamPlayer")
		UPDATE
			godeepmobile.team_player_career_stat
		SET
			num_seasons = season_scores."numSeasons",
			num_games = season_scores."numGames",
			num_plays_game = season_scores."numPlaysGames",
			cat1_grade_game = season_scores."cat1GradeGame",
			cat2_grade_game = season_scores."cat2GradeGame",
			cat3_grade_game = season_scores."cat3GradeGame",
			overall_grade_game = season_scores."overallGradeGame",
			impact_posgroup_grade_game = season_scores."impactPosgroupGradeGame",
			impact_platoon_grade_game = season_scores."impactPlatoonGradeGame",
			num_practices = season_scores."numPractices",
			num_plays_practice = season_scores."numPlaysPractice",
			cat1_grade_practice = season_scores."cat1GradePractice",
			cat2_grade_practice = season_scores."cat2GradePractice",
			cat3_grade_practice = season_scores."cat3GradePractice",
			overall_grade_practice = season_scores."overallGradePractice",
			impact_posgroup_grade_practice = season_scores."impactPosgroupGradePractice",
			impact_platoon_grade_practice = season_scores."impactPlatoonGradePractice",
			num_allevents = season_scores."numAllEvents",
			num_plays_allevent = season_scores."numPlaysAllEvents",
			cat1_grade_allevent = season_scores."cat1GradeAllEvent",
			cat2_grade_allevent = season_scores."cat2GradeAllEvent",
			cat3_grade_allevent = season_scores."cat3GradeAllEvent",
			overall_grade_allevent = season_scores."overallGradeAllEvent",
			impact_posgroup_grade_allevent = season_scores."impactPosgroupGradeAllEvent",
			impact_platoon_grade_allevent = season_scores."impactPlatoonGradeAllEvent"
		WHERE
		    id = (select id from stat_id);
	END LOOP;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

--rollback DROP VIEW IF EXISTS view_career_stat_ids_for_players; DROP FUNCTION IF EXISTS create_career_stats_for_team(uuid); DROP FUNCTION IF EXISTS refresh_career_stats(uuid);