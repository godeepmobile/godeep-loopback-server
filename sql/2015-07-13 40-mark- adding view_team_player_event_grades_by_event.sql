--liquibase formatted sql

--changeset mark:40 runOnChange:true stripComments:false splitStatements:false
--comment create view_team_player_event_grades_by_event

DROP VIEW IF EXISTS view_team_player_event_grades_by_event;
CREATE OR REPLACE VIEW view_team_player_event_grades_by_event AS
SELECT
    concat(player.id, '-', event.id) AS id,
    player.go_team_id,
    player.id AS team_player_id,
    event.id AS team_event_id,
--    event.date
    player.season,
    count(grade.id)::integer AS num_plays,
    sum(grade.cat_1_grade)::integer AS sum_cat1_grade,
    sum(grade.cat_2_grade)::integer AS sum_cat2_grade,
    sum(grade.cat_3_grade)::integer AS sum_cat3_grade,
    sum(grade.cat_1_grade + grade.cat_2_grade + grade.cat_3_grade)::integer AS sum_overall_grade,
    avg(grade.cat_1_grade)::integer AS avg_cat1_grade,
    avg(grade.cat_2_grade)::integer AS avg_cat2_grade,
    avg(grade.cat_3_grade)::integer AS avg_cat3_grade,
    (avg(grade.cat_1_grade + grade.cat_2_grade + grade.cat_3_grade) / 3::numeric)::integer AS avg_overall_grade
   FROM team_player player
     JOIN team_play_grade grade ON grade.team_player_id = player.id
     JOIN team_play_data play ON play.id = grade.team_play_data_id
     JOIN team_event event ON play.team_event_id = event.id
  GROUP BY player.id, event.id
  ORDER BY player.id;

  DROP VIEW IF EXISTS view_team_event;
  CREATE OR REPLACE VIEW view_team_event AS
   SELECT event.id,
      event.go_team_id,
      event.date,
      event.go_event_type_id,
      event.go_field_condition_id,
      event.go_surface_type_id,
      event.description,
      event.go_game_event_id,
      event.grade_base,
      event.grade_increment,
      event.data,
      event.season,
      event.name,
      ( SELECT
                  CASE
                      WHEN hometeam.id = event.go_team_id THEN awayteam.abbreviation::text
                      WHEN awayteam.id = event.go_team_id THEN hometeam.abbreviation::text
                      ELSE 'n/a'::text
                  END AS "case"
             FROM team_event event1
               LEFT JOIN go_game_event gge ON gge.id = event.go_game_event_id
               LEFT JOIN go_team hometeam ON gge.home_team_id = hometeam.id
               LEFT JOIN go_team awayteam ON gge.away_team_id = awayteam.id
            WHERE event1.id = event.id) AS opponent_name
     FROM team_event event;

--rollback DROP VIEW view_team_player_event_grades_by_event; DROP VIEW view_team_event;
