--liquibase formatted sql

--changeset rambert:110 runOnChange:true stripComments:false splitStatements:false
--comment adding CONSTRAINT to score values

ALTER TABLE godeepmobile.team_scouting_eval_score ADD CONSTRAINT score CHECK(score >= 1 AND score <= 9);
ALTER TABLE godeepmobile.go_scouting_eval_criteria ADD CONSTRAINT weight CHECK(weight >= 1 AND weight <= 9);
ALTER TABLE godeepmobile.team_scouting_eval_criteria ADD CONSTRAINT weight CHECK(weight >= 1 AND weight <= 9);

--rollback ALTER TABLE godeepmobile.team_scouting_eval_score DROP CONSTRAINT score; ALTER TABLE godeepmobile.go_scouting_eval_criteria DROP CONSTRAINT weight; ALTER TABLE godeepmobile.team_scouting_eval_criteria DROP CONSTRAINT weight;
