--liquibase formatted sql

--changeset carlos:15 runOnChange:true splitStatements:false
--comment added reference to team_cutup on team_event_grade_status_view
DROP VIEW view_team_event_grade_status;
CREATE OR REPLACE VIEW view_team_event_grade_status AS 
 SELECT team_event_grade_status.id,
    team_event_grade_status.go_team_id,
    team_event_grade_status.go_user_id,
    team_event_grade_status.team_event_id,
    team_event_grade_status.team_cutup_id,
    team_event_grade_status.completed_date,
    team_event_grade_status.started_date,
        CASE
            WHEN team_event_grade_status.started_date IS NULL THEN 'not started'::text
            WHEN team_event_grade_status.completed_date IS NOT NULL THEN 'completed'::text
            ELSE 'in process'::text
        END AS status
   FROM team_event_grade_status;

--rollback DROP VIEW view_team_event_grade_status; CREATE OR REPLACE VIEW view_team_event_grade_status AS 
-- SELECT team_event_grade_status.id,
--    team_event_grade_status.go_team_id,
--    team_event_grade_status.go_user_id,
--    team_event_grade_status.team_event_id,
--    team_event_grade_status.completed_date,
--    team_event_grade_status.started_date,
--        CASE
--            WHEN team_event_grade_status.started_date IS NULL THEN 'not started'::text
--            WHEN team_event_grade_status.completed_date IS NOT NULL THEN 'completed'::text
--            ELSE 'in process'::text
--        END AS status
--  FROM team_event_grade_status;