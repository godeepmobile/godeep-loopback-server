--liquibase formatted sql

--changeset carlos:35 runOnChange:true splitStatements:false
--comment added refresh_cutup_stats function

CREATE OR REPLACE FUNCTION godeepmobile.refresh_cutup_stats(cutup_id_parm integer)
  RETURNS integer AS
$BODY$
DECLARE
    mviews RECORD;
    stat godeepmobile.team_player_game_cutup_stat%ROWTYPE;
BEGIN
    --make sure that game stats exist for each player graded during this game
    perform godeepmobile.create_game_stats_for_cutup(cutup_id_parm);
    FOR mviews IN SELECT * FROM godeepmobile.view_team_player_event_grades_by_cutup where "teamCutup" = cutup_id_parm LOOP
        --RAISE NOTICE 'mviews is %', mviews::text;
        --RAISE NOTICE 'updating game stats for team player %', quote_ident(mviews."teamPlayer"::text);
        --select * into stat from godeepmobile.team_player_game_cutup_stat where team_player_id = mviews."teamPlayer" and team_cutup_id = cutup_id_parm;
        --RAISE NOTICE 'stat is %', stat::text;
        with stat_id as (select id from godeepmobile.team_player_game_cutup_stat where team_player_id = mviews."teamPlayer" and team_cutup_id = cutup_id_parm)
        update
            godeepmobile.team_player_game_cutup_stat
        SET
            num_plays = mviews."numPlays",
            cat1_grade = mviews."avgCat1Grade",
            cat2_grade = mviews."avgCat2Grade",
            cat3_grade = mviews."avgCat3Grade",
            overall_grade = mviews."avgOverallGrade",
            last_update = now()
        WHERE
            id = (select id from stat_id);
    END LOOP;
    RETURN 1;
END;
$BODY$
  LANGUAGE plpgsql;

--rollback DROP FUNCTION godeepmobile.refresh_cutup_stats(integer);