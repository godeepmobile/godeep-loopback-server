--liquibase formatted sql

--changeset mark:7 runOnChange:true stripComments:false
--comment add go_position_type_to_group_assignment table
DROP TABLE IF EXISTS go_position_type_to_group_assignment CASCADE;
DROP SEQUENCE IF EXISTS go_position_type_to_group_assignment_id_seq;
CREATE SEQUENCE go_position_type_to_group_assignment_id_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;

CREATE TABLE go_position_type_to_group_assignment (
	  id integer NOT NULL DEFAULT nextval('go_position_type_to_group_assignment_id_seq'::regclass),
	  go_position_type_id integer NOT NULL,
	  go_position_group_id integer NOT NULL,
	  go_platoon_type_id integer DEFAULT 0,
	  CONSTRAINT pk_go_position_type_to_group_assignment PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
--rollback DROP TABLE go_position_type_to_group_assignment; DROP SEQUENCE go_position_type_to_group_assignment_id_seq;

INSERT INTO go_position_type_to_group_assignment(go_position_type_id,go_position_group_id,go_platoon_type_id) VALUES ( 11,12,2 );
INSERT INTO go_position_type_to_group_assignment(go_position_type_id,go_position_group_id,go_platoon_type_id) VALUES ( 11,6,2 );
INSERT INTO go_position_type_to_group_assignment(go_position_type_id,go_position_group_id,go_platoon_type_id) VALUES ( 13,6,2 );
INSERT INTO go_position_type_to_group_assignment(go_position_type_id,go_position_group_id,go_platoon_type_id) VALUES ( 14,6,2 );
INSERT INTO go_position_type_to_group_assignment(go_position_type_id,go_position_group_id,go_platoon_type_id) VALUES ( 12,6,2 );
INSERT INTO go_position_type_to_group_assignment(go_position_type_id,go_position_group_id,go_platoon_type_id) VALUES ( 21,8,2 );
INSERT INTO go_position_type_to_group_assignment(go_position_type_id,go_position_group_id,go_platoon_type_id) VALUES ( 20,8,2 );
INSERT INTO go_position_type_to_group_assignment(go_position_type_id,go_position_group_id,go_platoon_type_id) VALUES ( 19,8,2 );
INSERT INTO go_position_type_to_group_assignment(go_position_type_id,go_position_group_id,go_platoon_type_id) VALUES ( 22,8,2 );
INSERT INTO go_position_type_to_group_assignment(go_position_type_id,go_position_group_id,go_platoon_type_id) VALUES ( 2,9,2 );
INSERT INTO go_position_type_to_group_assignment(go_position_type_id,go_position_group_id,go_platoon_type_id) VALUES ( 16,9,2 );
INSERT INTO go_position_type_to_group_assignment(go_position_type_id,go_position_group_id,go_platoon_type_id) VALUES ( 17,9,2 );
INSERT INTO go_position_type_to_group_assignment(go_position_type_id,go_position_group_id,go_platoon_type_id) VALUES ( 28,13,3 );
INSERT INTO go_position_type_to_group_assignment(go_position_type_id,go_position_group_id,go_platoon_type_id) VALUES ( 23,13,3 );
INSERT INTO go_position_type_to_group_assignment(go_position_type_id,go_position_group_id,go_platoon_type_id) VALUES ( 24,13,3 );
INSERT INTO go_position_type_to_group_assignment(go_position_type_id,go_position_group_id,go_platoon_type_id) VALUES ( 25,13,3 );
INSERT INTO go_position_type_to_group_assignment(go_position_type_id,go_position_group_id,go_platoon_type_id) VALUES ( 26,13,3 );
INSERT INTO go_position_type_to_group_assignment(go_position_type_id,go_position_group_id,go_platoon_type_id) VALUES ( 27,13,3 );
INSERT INTO go_position_type_to_group_assignment(go_position_type_id,go_position_group_id,go_platoon_type_id) VALUES ( 15,7,2 );
INSERT INTO go_position_type_to_group_assignment(go_position_type_id,go_position_group_id,go_platoon_type_id) VALUES ( 2,7,2 );
INSERT INTO go_position_type_to_group_assignment(go_position_type_id,go_position_group_id,go_platoon_type_id) VALUES ( 18,7,2 );
INSERT INTO go_position_type_to_group_assignment(go_position_type_id,go_position_group_id,go_platoon_type_id) VALUES ( 17,7,2 );
INSERT INTO go_position_type_to_group_assignment(go_position_type_id,go_position_group_id,go_platoon_type_id) VALUES ( 16,7,2 );
INSERT INTO go_position_type_to_group_assignment(go_position_type_id,go_position_group_id,go_platoon_type_id) VALUES ( 9,4,1 );
INSERT INTO go_position_type_to_group_assignment(go_position_type_id,go_position_group_id,go_platoon_type_id) VALUES ( 31,4,1 );
INSERT INTO go_position_type_to_group_assignment(go_position_type_id,go_position_group_id,go_platoon_type_id) VALUES ( 32,4,1 );
INSERT INTO go_position_type_to_group_assignment(go_position_type_id,go_position_group_id,go_platoon_type_id) VALUES ( 33,4,1 );
INSERT INTO go_position_type_to_group_assignment(go_position_type_id,go_position_group_id,go_platoon_type_id) VALUES ( 8,4,1 );
INSERT INTO go_position_type_to_group_assignment(go_position_type_id,go_position_group_id,go_platoon_type_id) VALUES ( 10,4,1 );
INSERT INTO go_position_type_to_group_assignment(go_position_type_id,go_position_group_id,go_platoon_type_id) VALUES ( 30,4,1 );
INSERT INTO go_position_type_to_group_assignment(go_position_type_id,go_position_group_id,go_platoon_type_id) VALUES ( 18,10,2 );
INSERT INTO go_position_type_to_group_assignment(go_position_type_id,go_position_group_id,go_platoon_type_id) VALUES ( 15,10,2 );
INSERT INTO go_position_type_to_group_assignment(go_position_type_id,go_position_group_id,go_platoon_type_id) VALUES ( 1,1,1 );
INSERT INTO go_position_type_to_group_assignment(go_position_type_id,go_position_group_id,go_platoon_type_id) VALUES ( 29,1,1 );
INSERT INTO go_position_type_to_group_assignment(go_position_type_id,go_position_group_id,go_platoon_type_id) VALUES ( 4,2,1 );
INSERT INTO go_position_type_to_group_assignment(go_position_type_id,go_position_group_id,go_platoon_type_id) VALUES ( 5,2,1 );
INSERT INTO go_position_type_to_group_assignment(go_position_type_id,go_position_group_id,go_platoon_type_id) VALUES ( 6,2,1 );
INSERT INTO go_position_type_to_group_assignment(go_position_type_id,go_position_group_id,go_platoon_type_id) VALUES ( 12,11,2 );
INSERT INTO go_position_type_to_group_assignment(go_position_type_id,go_position_group_id,go_platoon_type_id) VALUES ( 13,11,2 );
INSERT INTO go_position_type_to_group_assignment(go_position_type_id,go_position_group_id,go_platoon_type_id) VALUES ( 14,11,2 );
INSERT INTO go_position_type_to_group_assignment(go_position_type_id,go_position_group_id,go_platoon_type_id) VALUES ( 7,5,1 );
INSERT INTO go_position_type_to_group_assignment(go_position_type_id,go_position_group_id,go_platoon_type_id) VALUES ( 3,3,1 );
