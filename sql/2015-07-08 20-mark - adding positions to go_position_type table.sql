--liquibase formatted sql

--changeset mark:20 runOnChange:true stripComments:false splitStatements:false
--comment add missing positions to go_position_type table

-- insert 3 new positions into global position types table in Defense platoon
INSERT INTO godeepmobile.go_position_type( id, name, go_sport_type_id, short_name, go_platoon_type_id ) VALUES ( 34, 'Defensive Back', 1, 'DB', 2 );
INSERT INTO godeepmobile.go_position_type( id, name, go_sport_type_id, short_name, go_platoon_type_id ) VALUES ( 35, 'Linebacker',     1, 'LB', 2 );
INSERT INTO godeepmobile.go_position_type( id, name, go_sport_type_id, short_name, go_platoon_type_id ) VALUES ( 36, 'Defensive Back', 1, 'DL', 2 );

-- insert these 3 new positions into appropriate global position group
INSERT INTO go_position_type_to_group_assignment(go_position_type_id,go_position_group_id,go_platoon_type_id) VALUES ( 34,6,2 ); -- add to defensive backs group
INSERT INTO go_position_type_to_group_assignment(go_position_type_id,go_position_group_id,go_platoon_type_id) VALUES ( 35,7,2 ); -- add to linebackers group
INSERT INTO go_position_type_to_group_assignment(go_position_type_id,go_position_group_id,go_platoon_type_id) VALUES ( 36,7,2 ); -- add to linebackers group

--rollback DELETE FROM go_position_type_to_group_assignment where go_position_type_id in (34,35,36); DELETE FROM go_position_type where id in (34,35,36);
