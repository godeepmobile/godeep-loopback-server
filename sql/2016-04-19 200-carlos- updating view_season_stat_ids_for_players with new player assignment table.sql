﻿--liquibase formatted sql

--changeset carlos:200 runOnChange:true splitStatements:false stripComments:false
--comment updated view_season_stat_ids_for_players with new player assignment table

CREATE OR REPLACE VIEW view_season_stat_ids_for_players AS
	SELECT player.go_team_id,
		player.team_player_id,
		stats.id AS team_player_season_stat_id,
		player.season,
		player.is_veteran
	FROM view_team_player_assignment player
	LEFT JOIN team_player_season_stat stats ON stats.team_player_id = player.team_player_id AND stats.season = player.season
	ORDER BY player.id;

--rollback DROP VIEW view_season_stat_ids_for_players;