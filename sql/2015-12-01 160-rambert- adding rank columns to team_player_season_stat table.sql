--liquibase formatted sql

--changeset rambert:160 runOnChange:true splitStatements:false stripComments:false
--comment add rank columns to team_player_season_stat table

ALTER TABLE godeepmobile.team_player_season_stat ADD rank_overall_practice smallint;

--rollback ALTER TABLE godeepmobile.team_player_season_stat DROP COLUMN rank_overall_practice;
