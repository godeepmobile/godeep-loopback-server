--liquibase formatted sql

--changeset mark:41 runOnChange:true stripComments:false splitStatements:false
--comment modify team_event table and view

--add columns to team_event table
ALTER TABLE team_event ADD COLUMN opponent_organization_id integer;
ALTER TABLE team_event ADD COLUMN score smallint;
ALTER TABLE team_event ADD COLUMN opponent_score smallint;
ALTER TABLE team_event ADD COLUMN "time" time without time zone;
ALTER TABLE team_event ADD COLUMN location character varying(256);
ALTER TABLE team_event ADD COLUMN city character varying(256);
ALTER TABLE team_event ADD COLUMN go_state_id integer;
ALTER TABLE team_event ADD COLUMN is_home_game boolean;

--copy data from go_game_event table into team_event table
update
	team_event event
set
	location = gge.location,
	city = gge.city,
	go_state_id = gge.go_state_id,
	"time" = gge."time",
	is_home_game = (gge.home_team_id = event1.go_team_id),
	opponent_organization_id = case
		WHEN gge.home_team_id = event1.go_team_id THEN away_team.go_organization_id
		WHEN gge.away_team_id = event1.go_team_id THEN home_team.go_organization_id
		ELSE NULL
		END,
	opponent_score = case
		WHEN gge.home_team_id = event1.go_team_id THEN gge.away_score
		WHEN gge.away_team_id = event1.go_team_id THEN gge.home_score
		ELSE 0
		END,
	score = case
		WHEN gge.home_team_id = event1.go_team_id THEN gge.home_score
		WHEN gge.away_team_id = event1.go_team_id THEN gge.away_score
		ELSE 0
		END
from
	team_event event1
	join go_game_event gge on gge.id = event1.go_game_event_id
	join go_team home_team on gge.home_team_id = home_team.id
	join go_team away_team on gge.away_team_id = away_team.id
where
	event.id = event1.id;

--make sure every team-event has a name before making it required
UPDATE team_event
	set
			name=concat('team-event-',id)::text
		where
			name IS NULL;

ALTER TABLE team_event ALTER COLUMN name SET NOT NULL;

--rollback ALTER TABLE team_event DROP COLUMN opponent_organization_id; ALTER TABLE team_event DROP COLUMN score; ALTER TABLE team_event DROP COLUMN opponent_score; ALTER TABLE team_event DROP COLUMN "time"; ALTER TABLE team_event DROP COLUMN location; ALTER TABLE team_event DROP COLUMN city; ALTER TABLE team_event DROP COLUMN go_state_id; ALTER TABLE team_event DROP COLUMN is_home_game;ALTER TABLE team_event ALTER COLUMN name name character varying(256);
