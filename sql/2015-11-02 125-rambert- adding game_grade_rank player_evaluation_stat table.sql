--changeset rambert:125 runOnChange:true stripComments:false splitStatements:false
--comment add game_grade_rank to player_evaluation_stat table

-- add game_grade_rank constraint to go_scouting_eval_criteria
ALTER TABLE godeepmobile.player_evaluation_stat ADD rank_game_grade smallint;
ALTER TABLE godeepmobile.player_evaluation_stat ADD rank_player_evaluation_avg smallint;

ALTER TABLE godeepmobile.player_evaluation_stat RENAME COLUMN overall_eval_avg_ranking TO overall_eval_avg_rank;

-- is_completed flag is not used anymore
ALTER TABLE godeepmobile.team_scouting_eval_score_group DROP COLUMN is_completed;
--rollback ALTER TABLE godeepmobile.player_evaluation_stat DROP COLUMN rank_game_grade; ALTER TABLE godeepmobile.player_evaluation_stat DROP COLUMN rank_player_evaluation_avg;
