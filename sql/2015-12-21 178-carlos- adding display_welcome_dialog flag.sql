﻿--liquibase formatted sql

--changeset carlos:178 runOnChange:true splitStatements:false stripComments:false
--comment added display_welcome_dialog flag

ALTER TABLE go_user ADD display_welcome_dialog BOOLEAN DEFAULT true;

DROP VIEW IF EXISTS view_go_user;

CREATE OR REPLACE VIEW view_go_user AS 
 SELECT user_.id,
    user_.user_name,
    user_.pass_word,
    user_.first_name,
    user_.middle_name,
    user_.last_name,
    user_.credentials,
    user_.challenges,
    user_.email,
    user_.emailverified,
    user_.verificationtoken,
    user_.status,
    user_.created,
    user_.lastupdated,
    user_.superuser,
    user_.job_title,
    user_.office_phone,
    user_.mobile_phone,
    user_.has_changed_password,
    user_.first_time_login,
    user_.logical_delete,
    user_.display_welcome_dialog
   FROM go_user user_
  WHERE user_.logical_delete = false;

--rollbar DROP VIEW view_go_user; ALTER TABLE go_user DROP COLUMN display_welcome_dialog;