﻿--liquibase formatted sql

--changeset carlos:231 runOnChange:true splitStatements:false stripComments:false
--comment fixed refresh_career_stats function

CREATE OR REPLACE FUNCTION refresh_career_stats(
    team_id_parm uuid)
  RETURNS void AS
$BODY$
DECLARE
    season_scores RECORD;
BEGIN
	perform godeepmobile.create_career_stats_for_team(team_id_parm);
	FOR season_scores IN ( 
		SELECT
			team_player_id AS "teamPlayer",
			COUNT(season) AS "numSeasons",
			SUM(num_games) AS "numGames",
			SUM(num_plays_game) AS "numPlaysGames",
			ROUND(SUM(cat1_grade_game) / (CASE WHEN SUM(num_games) <> 0 THEN SUM(num_games) ELSE 1 END), 2) AS "cat1GradeGame",
			ROUND(SUM(cat2_grade_game) / (CASE WHEN SUM(num_games) <> 0 THEN SUM(num_games) ELSE 1 END), 2) AS "cat2GradeGame",
			ROUND(SUM(cat3_grade_game) / (CASE WHEN SUM(num_games) <> 0 THEN SUM(num_games) ELSE 1 END), 2) AS "cat3GradeGame",
			ROUND(SUM(overall_grade_game) / (CASE WHEN SUM(num_games) <> 0 THEN SUM(num_games) ELSE 1 END), 2) AS "overallGradeGame",
			ROUND(SUM(impact_posgroup_grade_game) / (CASE WHEN SUM(num_games) <> 0 THEN SUM(num_games) ELSE 1 END), 2) AS "impactPosgroupGradeGame",
			ROUND(SUM(impact_platoon_grade_game) / (CASE WHEN SUM(num_games) <> 0 THEN SUM(num_games) ELSE 1 END), 2) AS "impactPlatoonGradeGame",
			SUM(num_practices) AS "numPractices",
			SUM(num_plays_practice) AS "numPlaysPractice",
			ROUND(SUM(cat1_grade_practice) / (CASE WHEN SUM(num_practices) <> 0 THEN SUM(num_practices) ELSE 1 END), 2) AS "cat1GradePractice",
			ROUND(SUM(cat2_grade_practice) / (CASE WHEN SUM(num_practices) <> 0 THEN SUM(num_practices) ELSE 1 END), 2) AS "cat2GradePractice",
			ROUND(SUM(cat3_grade_practice) / (CASE WHEN SUM(num_practices) <> 0 THEN SUM(num_practices) ELSE 1 END), 2) AS "cat3GradePractice",
			ROUND(SUM(overall_grade_practice) / (CASE WHEN SUM(num_practices) <> 0 THEN SUM(num_practices) ELSE 1 END), 2) AS "overallGradePractice",
			ROUND(SUM(impact_posgroup_grade_practice) / (CASE WHEN SUM(num_practices) <> 0 THEN SUM(num_practices) ELSE 1 END), 2) AS "impactPosgroupGradePractice",
			ROUND(SUM(impact_platoon_grade_practice) / (CASE WHEN SUM(num_practices) <> 0 THEN SUM(num_practices) ELSE 1 END), 2) AS "impactPlatoonGradePractice",
			SUM(num_allevents) AS "numAllEvents",
			SUM(num_plays_allevent) AS "numPlaysAllEvents",
			ROUND(AVG(cat1_grade_allevent), 2) AS "cat1GradeAllEvent",
			ROUND(AVG(cat2_grade_allevent), 2) AS "cat2GradeAllEvent",
			ROUND(AVG(cat3_grade_allevent), 2) AS "cat3GradeAllEvent",
			ROUND(AVG(overall_grade_allevent), 2) AS "overallGradeAllEvent",
			ROUND(AVG(impact_posgroup_grade_allevent), 2) AS "impactPosgroupGradeAllEvent",
			ROUND(AVG(impact_platoon_grade_allevent), 2) AS "impactPlatoonGradeAllEvent"
		FROM godeepmobile.team_player_season_stat 
		WHERE go_team_id = team_id_parm
		GROUP BY team_player_id
	) 
	LOOP

		WITH stat_id AS (SELECT id FROM godeepmobile.team_player_career_stat WHERE team_player_id = season_scores."teamPlayer")
		UPDATE
			godeepmobile.team_player_career_stat
		SET
			num_seasons = season_scores."numSeasons",
			num_games = season_scores."numGames",
			num_plays_game = season_scores."numPlaysGames",
			cat1_grade_game = season_scores."cat1GradeGame",
			cat2_grade_game = season_scores."cat2GradeGame",
			cat3_grade_game = season_scores."cat3GradeGame",
			overall_grade_game = season_scores."overallGradeGame",
			impact_posgroup_grade_game = season_scores."impactPosgroupGradeGame",
			impact_platoon_grade_game = season_scores."impactPlatoonGradeGame",
			num_practices = season_scores."numPractices",
			num_plays_practice = season_scores."numPlaysPractice",
			cat1_grade_practice = season_scores."cat1GradePractice",
			cat2_grade_practice = season_scores."cat2GradePractice",
			cat3_grade_practice = season_scores."cat3GradePractice",
			overall_grade_practice = season_scores."overallGradePractice",
			impact_posgroup_grade_practice = season_scores."impactPosgroupGradePractice",
			impact_platoon_grade_practice = season_scores."impactPlatoonGradePractice",
			num_allevents = season_scores."numAllEvents",
			num_plays_allevent = season_scores."numPlaysAllEvents",
			cat1_grade_allevent = season_scores."cat1GradeAllEvent",
			cat2_grade_allevent = season_scores."cat2GradeAllEvent",
			cat3_grade_allevent = season_scores."cat3GradeAllEvent",
			overall_grade_allevent = season_scores."overallGradeAllEvent",
			impact_posgroup_grade_allevent = season_scores."impactPosgroupGradeAllEvent",
			impact_platoon_grade_allevent = season_scores."impactPlatoonGradeAllEvent"
		WHERE
		    id = (select id from stat_id);
	END LOOP;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

 --rollback DROP FUNCTION refresh_career_stats;