--liquibase formatted sql

--changeset rambert:156 runOnChange:true splitStatements:false stripComments:false
--comment updating refresh_player_evaluation function

CREATE OR REPLACE FUNCTION godeepmobile.refresh_player_evaluation(
  team_id_parm uuid,
  evaluation_id_parm integer)
  RETURNS json AS
$BODY$
DECLARE
	target_match_value numeric;
	player_score_overall numeric;
  score_major_factors_eval_avg numeric;
  score_critical_factors_eval_avg numeric;
  score_position_skills_eval_avg numeric;
  evaluation_exists integer;
  current_season integer;
  current_position_type_id integer;
  is_prospect_value boolean;
  _records jsonb;
  ranks jsonb;
  evaluation RECORD;
  evaluation_stat RECORD;
BEGIN

  perform godeepmobile.refresh_evaluation_stat_values(team_id_parm, evaluation_id_parm);

  SELECT eval_stat.* INTO evaluation_stat
  FROM godeepmobile.player_evaluation_stat eval_stat
  WHERE eval_stat.go_team_id = team_id_parm::uuid
    AND eval_stat.team_scouting_eval_score_group_id = evaluation_id_parm;

  SELECT array_to_json(array_agg(row_to_json(records))) INTO _records
    FROM (
      SELECT criteria_type.id,
        criteria_type.name,
        ROUND(avg(eval_score.score), 2) AS "scoreAvg",
        ROUND(avg(
        ( CASE WHEN team_criteria.use_calculated_target_score
              THEN team_criteria.calculated_target_score
              ELSE team_criteria.custom_target_score
         END)
        ), 2) AS "targetScoreAvg",
        array_agg(team_criteria.name ORDER BY team_criteria.sort_index ASC) AS "categories",
        array_agg((CASE WHEN team_criteria.is_critical THEN team_criteria.name ELSE null END)
                  ORDER BY team_criteria.sort_index ASC) AS "criticals",
        array_agg(eval_score.score ORDER BY team_criteria.sort_index ASC) AS "playerScores",
        array_agg(eval_score.note ORDER BY team_criteria.sort_index ASC) AS "notes",
        array_agg(( CASE WHEN team_criteria.use_calculated_target_score
              THEN team_criteria.calculated_target_score
              ELSE team_criteria.custom_target_score
         END) ORDER BY team_criteria.sort_index ASC) AS "targetScores"
      FROM godeepmobile.team_scouting_eval_score eval_score,
        godeepmobile.team_scouting_eval_criteria team_criteria,
        godeepmobile.team_scouting_eval_score_group score_group,
        godeepmobile.go_scouting_eval_criteria_type criteria_type
      WHERE eval_score.go_team_id = team_id_parm::uuid
        AND eval_score.team_scouting_eval_score_group_id = score_group.id
        AND eval_score.team_scouting_eval_criteria_id = team_criteria.id
        AND score_group.id = evaluation_id_parm
        AND eval_score.go_scouting_eval_criteria_type_id = criteria_type.id
      GROUP BY criteria_type.id
      ORDER BY criteria_type.id ASC) records;

  -- is prospect
  SELECT player.is_prospect INTO is_prospect_value
  FROM godeepmobile.team_scouting_eval_score_group score_group,
         godeepmobile.team_player player
  WHERE score_group.id = evaluation_id_parm
    AND score_group.go_team_id = team_id_parm::uuid
    AND score_group.go_team_id = player.go_team_id
    AND player.id = score_group.team_player_id;

  SELECT season INTO current_season
  FROM godeepmobile.team_scouting_eval_score_group
  WHERE go_team_id = team_id_parm::uuid
    AND id = evaluation_id_parm;

  SELECT go_position_type_id INTO current_position_type_id
  FROM godeepmobile.team_scouting_eval_score_group
  WHERE go_team_id = team_id_parm::uuid
    AND id = evaluation_id_parm;

  IF is_prospect_value THEN
    perform godeepmobile.refresh_prospect_evaluation_ranking(team_id_parm, current_season);
    perform godeepmobile.refresh_prospect_evaluation_season_ranking(team_id_parm, current_season);
    perform godeepmobile.refresh_prospect_evaluation_rank_at_position(team_id_parm, current_season, current_position_type_id);
    perform godeepmobile.refresh_prospect_season_evaluation_rank_at_position(team_id_parm, current_season, current_position_type_id);
    SELECT row_to_json(rank) INTO ranks
      FROM (SELECT eval_stat.rank_overall AS "rankOverall", overall_at_position.rank_at_post AS "rankAtPost"
        FROM view_prospect_evaluation_stat eval_stat,
          view_prospect_rank_overall_at_position overall_at_position
        WHERE eval_stat.season = 2015
          AND overall_at_position.season = eval_stat.season
          AND eval_stat.team_prospect_id = overall_at_position.team_prospect_id
          AND eval_stat.go_team_id = team_id_parm::uuid
          AND eval_stat.go_team_id = overall_at_position.go_team_id
          AND eval_stat.team_scouting_eval_score_group_id = evaluation_id_parm
          AND eval_stat.team_scouting_eval_score_group_id = overall_at_position.team_scouting_eval_score_group_id
    ) rank;
  ELSE
    perform godeepmobile.refresh_player_evaluation_ranking(team_id_parm, current_season);
    perform godeepmobile.refresh_player_evaluation_season_ranking(team_id_parm, current_season);
    perform godeepmobile.refresh_player_evaluation_rank_at_position(team_id_parm, current_season, current_position_type_id);
    perform godeepmobile.refresh_player_season_evaluation_rank_at_position(team_id_parm, current_season, current_position_type_id);

    SELECT row_to_json(rank) INTO ranks
      FROM (SELECT eval_stat.rank_overall AS "rankOverall", overall_at_position.rank_at_post AS "rankAtPost"
        FROM view_player_evaluation_stat eval_stat,
          view_player_rank_overall_at_position overall_at_position
        WHERE eval_stat.season = current_season
          AND overall_at_position.season = eval_stat.season
          AND eval_stat.team_player_id = overall_at_position.team_player_id
          AND eval_stat.go_team_id = team_id_parm::uuid
          AND eval_stat.go_team_id = overall_at_position.go_team_id
          AND eval_stat.team_scouting_eval_score_group_id = overall_at_position.team_scouting_eval_score_group_id
          AND eval_stat.team_scouting_eval_score_group_id = evaluation_id_parm
    ) rank;

  END IF;

  RETURN json_build_object('targetMatch', evaluation_stat.target_match,
                           'playerScoreOverall', evaluation_stat.overall_eval_avg,
                           'records', _records,
                           'ranks', ranks);
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

--rollback DROP FUNCTION godeepmobile.refresh_player_evaluation(uuid, integer);
