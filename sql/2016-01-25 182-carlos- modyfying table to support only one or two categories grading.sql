﻿--liquibase formatted sql

--changeset carlos:182 runOnChange:true splitStatements:false stripComments:false
--comment modyfying table to support only one or two categories grading 

--adding new property to config table
ALTER TABLE team_configuration ADD num_grade_categories SMALLINT NOT NULL DEFAULT 3;

--updating related views
CREATE OR REPLACE VIEW view_team_player_event_grades AS 
	WITH 
		team_config AS (SELECT go_team_id, num_grade_categories FROM team_configuration)
	SELECT player.id AS "teamPlayer",
		event.season,
		count(grade.id)::integer AS "numPlays",
		sum(grade.cat_1_grade)::integer AS "sumCat1Grade",
		sum(grade.cat_2_grade)::integer AS "sumCat2Grade",
		sum(grade.cat_3_grade)::integer AS "sumCat3Grade",
		sum(grade.cat_1_grade + COALESCE(grade.cat_2_grade, 0) + COALESCE(grade.cat_3_grade, 0))::integer AS "sumOverallGrade",
		round(avg(grade.cat_1_grade), 2) AS "avgCat1Grade",
		round(avg(grade.cat_2_grade), 2) AS "avgCat2Grade",
		round(avg(grade.cat_3_grade), 2) AS "avgCat3Grade",
		round(avg(grade.cat_1_grade + COALESCE(grade.cat_2_grade, 0) + COALESCE(grade.cat_3_grade, 0)) / (SELECT num_grade_categories FROM team_config WHERE go_team_id = player.go_team_id)::numeric, 2) AS "avgOverallGrade",
		event.id AS "teamEvent",
		event.date,
		player.go_team_id AS team
	FROM team_player player
	JOIN team_play_grade grade ON grade.team_player_id = player.id
	JOIN team_play_data play ON play.id = grade.team_play_data_id
	JOIN team_event event ON play.team_event_id = event.id
	GROUP BY player.id, event.id
	ORDER BY player.id;

CREATE OR REPLACE VIEW view_team_player_event_grades_by_cutup AS 
	WITH 
		team_config AS (SELECT go_team_id, num_grade_categories FROM team_configuration)
	SELECT player.id AS "teamPlayer",
		player.season,
		count(grade.id)::integer AS "numPlays",
		sum(grade.cat_1_grade)::integer AS "sumCat1Grade",
		sum(grade.cat_2_grade)::integer AS "sumCat2Grade",
		sum(grade.cat_3_grade)::integer AS "sumCat3Grade",
		sum(grade.cat_1_grade + COALESCE(grade.cat_2_grade, 0) + COALESCE(grade.cat_3_grade, 0))::integer AS "sumOverallGrade",
		round(avg(grade.cat_1_grade), 2) AS "avgCat1Grade",
		round(avg(grade.cat_2_grade), 2) AS "avgCat2Grade",
		round(avg(grade.cat_3_grade), 2) AS "avgCat3Grade",
		round(avg(grade.cat_1_grade + COALESCE(grade.cat_2_grade, 0) + COALESCE(grade.cat_3_grade, 0)) / (SELECT num_grade_categories FROM team_config WHERE go_team_id = player.go_team_id)::numeric, 2) AS "avgOverallGrade",
		play.team_cutup_id AS "teamCutup",
		event.date,
		player.go_team_id AS team
	FROM team_player player
	JOIN team_play_grade grade ON grade.team_player_id = player.id
	JOIN team_play_data play ON play.id = grade.team_play_data_id
	JOIN team_event event ON play.team_event_id = event.id
	GROUP BY player.id, event.date, play.team_cutup_id
	ORDER BY player.id;

CREATE OR REPLACE VIEW view_team_player_event_grades_by_event AS 
	WITH 
		team_config AS (SELECT go_team_id, num_grade_categories FROM team_configuration)
	SELECT concat(player.id, '-', event.id) AS id,
		player.go_team_id,
		player.id AS team_player_id,
		event.id AS team_event_id,
		player.season,
		count(grade.id)::integer AS num_plays,
		sum(grade.cat_1_grade)::integer AS sum_cat1_grade,
		sum(grade.cat_2_grade)::integer AS sum_cat2_grade,
		sum(grade.cat_3_grade)::integer AS sum_cat3_grade,
		sum(grade.cat_1_grade + COALESCE(grade.cat_2_grade, 0) + COALESCE(grade.cat_3_grade, 0))::integer AS sum_overall_grade,
		avg(grade.cat_1_grade)::integer AS avg_cat1_grade,
		avg(grade.cat_2_grade)::integer AS avg_cat2_grade,
		avg(grade.cat_3_grade)::integer AS avg_cat3_grade,
		round(avg(grade.cat_1_grade + COALESCE(grade.cat_2_grade, 0) + COALESCE(grade.cat_3_grade, 0)) / (SELECT num_grade_categories FROM team_config WHERE go_team_id = player.go_team_id)::numeric, 2)::integer AS avg_overall_grade
	FROM team_player player
	JOIN team_play_grade grade ON grade.team_player_id = player.id
	JOIN team_play_data play ON play.id = grade.team_play_data_id
	JOIN team_event event ON play.team_event_id = event.id
	GROUP BY player.id, event.id
	ORDER BY player.id;

CREATE OR REPLACE VIEW view_team_player_event_grades_by_quarter AS 
	WITH 
		team_config AS (SELECT go_team_id, num_grade_categories FROM team_configuration)
	SELECT concat(player.id, '-', play.team_event_id, '-', play.quarter) AS id,
		player.go_team_id,
		player.id AS team_player_id,
		play.team_event_id,
		play.quarter,
		count(grade.id) AS num_plays,
		sum(grade.cat_1_grade)::integer AS sum_cat1_grade,
		sum(grade.cat_2_grade)::integer AS sum_cat2_grade,
		sum(grade.cat_3_grade)::integer AS sum_cat3_grade,
		sum(grade.cat_1_grade + COALESCE(grade.cat_2_grade, 0) + COALESCE(grade.cat_3_grade, 0))::integer AS sum_overall_grade,
		avg(grade.cat_1_grade)::integer AS avg_cat1_grade,
		avg(grade.cat_2_grade)::integer AS avg_cat2_grade,
		avg(grade.cat_3_grade)::integer AS avg_cat3_grade,
		round(avg(grade.cat_1_grade + COALESCE(grade.cat_2_grade, 0) + COALESCE(grade.cat_3_grade, 0)) / (SELECT num_grade_categories FROM team_config WHERE go_team_id = player.go_team_id)::numeric, 2)::integer AS avg_overall_grade
	FROM team_player player
	JOIN team_play_grade grade ON grade.team_player_id = player.id
	JOIN team_play_data play ON play.id = grade.team_play_data_id
	JOIN team_event event ON play.team_event_id = event.id
	GROUP BY player.id, play.team_event_id, play.quarter
	ORDER BY player.id, play.team_event_id, play.quarter;

CREATE OR REPLACE VIEW view_grade_report_data AS 
	WITH 
		team_config AS (SELECT go_team_id, num_grade_categories FROM team_configuration)
	SELECT tpg.id,
		te.go_team_id,
		te.season,
		te.date,
		te.go_field_condition_id,
		te.go_surface_type_id,
		te.opponent_organization_id,
		te.is_home_game,
		te.id AS team_event_id,
		tpd.play_number,
		tpd.quarter,
		tpd.game_down,
		tpd.game_distance,
		tpd.yard_line,
		tpd.go_play_type_id,
		tpd.game_possession,
		tpd.go_play_result_type_id,
		tpg.id AS team_play_grade_id,
		tpg.team_player_id,
		tpg.position_played AS team_position_type_id,
		tpg.go_platoon_type_id,
		tpg.team_position_group_id,
		tpg.go_user_id,
		tpg.hard,
		tpg.poa,
		tpg.cat_1_grade AS cat1_grade,
		tpg.cat_2_grade AS cat2_grade,
		tpg.cat_3_grade AS cat3_grade,
		(tpg.cat_1_grade + COALESCE(tpg.cat_2_grade, 0) + COALESCE(tpg.cat_3_grade, 0))::smallint AS sum_overall_grade,
		(tpg.cat_1_grade + COALESCE(tpg.cat_2_grade, 0) + COALESCE(tpg.cat_3_grade, 0)) / (SELECT num_grade_categories FROM team_config WHERE go_team_id = te.go_team_id) AS avg_overall_grade,
		abs(tpd.yard_line) <= 20 AS red_zone,
		tc.id AS team_cutup_id,
		gpt.short_name AS cutup_platoon_short_name,
		tpg.factor_in_play
	FROM team_play_data tpd
	JOIN team_event te ON tpd.team_event_id = te.id
	JOIN team_play_grade tpg ON tpd.id = tpg.team_play_data_id
	JOIN team_cutup tc ON tpd.team_cutup_id = tc.id
	JOIN go_platoon_type gpt ON tc.which_platoon = gpt.id;

CREATE OR REPLACE VIEW view_platoon_event_grades AS 
	WITH 
		team_config AS (SELECT go_team_id, num_grade_categories FROM team_configuration)
	SELECT event.go_team_id AS team,
		event.season,
		grade.go_platoon_type_id AS "platoonType",
		event.date,
		event.id AS "teamEvent",
		count(grade.id)::integer AS "numPlays",
		sum(grade.cat_1_grade)::integer AS "sumCat1Grade",
		sum(grade.cat_2_grade)::integer AS "sumCat2Grade",
		sum(grade.cat_3_grade)::integer AS "sumCat3Grade",
		sum(grade.cat_1_grade + COALESCE(grade.cat_2_grade, 0) + COALESCE(grade.cat_3_grade, 0))::integer AS "sumOverallGrade",
		avg(grade.cat_1_grade)::integer AS "avgCat1Grade",
		avg(grade.cat_2_grade)::integer AS "avgCat2Grade",
		avg(grade.cat_3_grade)::integer AS "avgCat3Grade",
		(avg(grade.cat_1_grade + COALESCE(grade.cat_2_grade, 0) + COALESCE(grade.cat_3_grade, 0)) / (SELECT num_grade_categories FROM team_config WHERE go_team_id = event.go_team_id)::numeric)::integer AS "avgOverallGrade"
	FROM team_play_grade grade
	JOIN team_play_data play ON play.id = grade.team_play_data_id
	JOIN team_event event ON play.team_event_id = event.id
	GROUP BY event.id, grade.go_platoon_type_id
	ORDER BY event.id, grade.go_platoon_type_id;

CREATE OR REPLACE VIEW view_platoon_event_grades_by_quarter AS 
	WITH 
		team_config AS (SELECT go_team_id, num_grade_categories FROM team_configuration)
	SELECT concat(event.id, '-', grade.go_platoon_type_id, '-', play.quarter) AS id,
		event.go_team_id,
		event.season,
		grade.go_platoon_type_id,
		event.date,
		event.id AS team_event_id,
		play.quarter,
		count(grade.id) AS num_plays,
		sum(grade.cat_1_grade)::integer AS sum_cat1_grade,
		sum(grade.cat_2_grade)::integer AS sum_cat2_grade,
		sum(grade.cat_3_grade)::integer AS sum_cat3_grade,
		sum(grade.cat_1_grade + COALESCE(grade.cat_2_grade, 0) + COALESCE(grade.cat_3_grade, 0))::integer AS sum_overall_grade,
		round(avg(grade.cat_1_grade), 2) AS avg_cat1_grade,
		round(avg(grade.cat_2_grade), 2) AS avg_cat2_grade,
		round(avg(grade.cat_3_grade), 2) AS avg_cat3_grade,
		round(avg(grade.cat_1_grade + COALESCE(grade.cat_2_grade, 0) + COALESCE(grade.cat_3_grade, 0)) / (SELECT num_grade_categories FROM team_config WHERE go_team_id = event.go_team_id)::numeric, 2) AS avg_overall_grade
	FROM team_play_grade grade
	JOIN team_play_data play ON play.id = grade.team_play_data_id
	JOIN team_event event ON play.team_event_id = event.id
	GROUP BY event.id, grade.go_platoon_type_id, play.quarter
	ORDER BY event.id, grade.go_platoon_type_id, play.quarter;

CREATE OR REPLACE VIEW view_position_type_event_grades AS 
	WITH 
		team_config AS (SELECT go_team_id, num_grade_categories FROM team_configuration)
	SELECT event.go_team_id AS team,
		event.season,
		grade.position_played AS "positionType",
		event.date,
		event.id AS "teamEvent",
		count(grade.id)::integer AS "numPlays",
		sum(grade.cat_1_grade)::integer AS "sumCat1Grade",
		sum(grade.cat_2_grade)::integer AS "sumCat2Grade",
		sum(grade.cat_3_grade)::integer AS "sumCat3Grade",
		sum(grade.cat_1_grade + COALESCE(grade.cat_2_grade, 0) + COALESCE(grade.cat_3_grade, 0))::integer AS "sumOverallGrade",
		avg(grade.cat_1_grade)::integer AS "avgCat1Grade",
		avg(grade.cat_2_grade)::integer AS "avgCat2Grade",
		avg(grade.cat_3_grade)::integer AS "avgCat3Grade",
		(avg(grade.cat_1_grade + COALESCE(grade.cat_2_grade, 0) + COALESCE(grade.cat_3_grade, 0)) / (SELECT num_grade_categories FROM team_config WHERE go_team_id = event.go_team_id)::numeric)::integer AS "avgOverallGrade"
	FROM team_play_grade grade
	JOIN team_play_data play ON play.id = grade.team_play_data_id
	JOIN team_event event ON play.team_event_id = event.id
	GROUP BY event.id, grade.position_played
	ORDER BY event.id, grade.position_played;

CREATE OR REPLACE VIEW view_position_type_event_grades_by_quarter AS 
	WITH 
		team_config AS (SELECT go_team_id, num_grade_categories FROM team_configuration)
	SELECT concat(grade.position_played, '-', event.id, '-', play.quarter) AS id,
		event.go_team_id,
		event.season,
		grade.position_played AS go_position_type_id,
		event.date,
		event.id AS team_event_id,
		play.quarter,
		count(grade.id) AS num_plays,
		sum(grade.cat_1_grade)::integer AS sum_cat1_grade,
		sum(grade.cat_2_grade)::integer AS sum_cat2_grade,
		sum(grade.cat_3_grade)::integer AS sum_cat3_grade,
		sum(grade.cat_1_grade + COALESCE(grade.cat_2_grade, 0) + COALESCE(grade.cat_3_grade, 0))::integer AS sum_overall_grade,
		avg(grade.cat_1_grade)::integer AS avg_cat1_grade,
		avg(grade.cat_2_grade)::integer AS avg_cat2_grade,
		avg(grade.cat_3_grade)::integer AS avg_cat3_grade,
		(avg(grade.cat_1_grade + COALESCE(grade.cat_2_grade, 0) + COALESCE(grade.cat_3_grade, 0)) / (SELECT num_grade_categories FROM team_config WHERE go_team_id = event.go_team_id)::numeric)::integer AS avg_overall_grade
	FROM team_play_grade grade
	JOIN team_play_data play ON play.id = grade.team_play_data_id
	JOIN team_event event ON play.team_event_id = event.id
	GROUP BY event.id, grade.position_played, play.quarter
	ORDER BY event.id, grade.position_played, play.quarter;

CREATE OR REPLACE VIEW view_team_play_grade_detail AS 
	WITH 
		team_config AS (SELECT go_team_id, num_grade_categories FROM team_configuration)
	SELECT play.play_number AS "playNumber",
		play.quarter,
		(play.game_down || '/'::text) || play.game_distance AS "dnDist",
		play.yard_line AS "yardLine",
		( SELECT go_play_result_type.name
		   FROM go_play_result_type
		  WHERE go_play_result_type.id = play.go_play_result_type_id) AS "resultType",
		( SELECT go_position_type.short_name
		   FROM go_position_type
		  WHERE go_position_type.id = grade.position_played) AS "position",
		( SELECT go_platoon_type.name
		   FROM go_platoon_type
		  WHERE go_platoon_type.id = grade.go_platoon_type_id) AS platoon,
		grade.cat_1_grade AS "cat1Grade",
		grade.cat_2_grade AS "cat2Grade",
		grade.cat_3_grade AS "cat3Grade",
		round((grade.cat_1_grade + COALESCE(grade.cat_2_grade, 0) + COALESCE(grade.cat_3_grade, 0))::numeric / (SELECT num_grade_categories FROM team_config WHERE go_team_id = grade.go_team_id)::numeric, 2) AS "overallGrade",
		( SELECT go_play_factor.name
		   FROM go_play_factor
		  WHERE go_play_factor.id = grade.go_play_factor_id) AS "factorInPlay",
		grade.notes,
		grade.players_can_see_notes AS "playersCanSeeNotes",
		grade.position_played,
		grade.go_platoon_type_id,
		grade.team_player_id,
		grade.go_team_id,
		event.id AS event_id,
		event.season,
		event.date AS event_date
	FROM team_play_grade grade,
		team_play_data play,
		team_event event
	WHERE grade.team_play_data_id = play.id AND play.team_event_id = event.id
	ORDER BY event.date, event.id, play.play_number;

--updating related functions
CREATE OR REPLACE FUNCTION calculate_evaluation_target_scores(go_team_id_parm text)
  RETURNS integer AS
$BODY$
DECLARE
	exists integer;
	team_config record;
	head_coach_start_date date;
	num_graded_games integer;
	position record;
	eval_criteria record;
	minimun_required_graded_games integer := 5; --n1
	minimun_required_games_with_grades_for_position integer := 5; --n2
	num_top_players integer := 2; --n3
BEGIN
	--check validity of parameters
	select count(team.id) into exists from godeepmobile.go_team team where team.id = go_team_id_parm::uuid;
	if exists != 1 then
		RAISE EXCEPTION 'Team % does not exist', go_team_id_parm USING HINT = 'Please check your team id parameter';
		return 0;
	end if;

	--get the current head coach start date
	select start_date into head_coach_start_date from go_user_team_assignment where go_team_id = go_team_id_parm::uuid and end_date is null and go_user_role_type_id = 2; --n1

	--get the team grades
	select count(distinct event.id) into num_graded_games from team_play_grade grade, team_play_data play, view_team_event event where grade.team_play_data_id = play.id and play.team_event_id = event.id and grade.go_team_id = go_team_id_parm::uuid and event.is_practice = false and event.date > head_coach_start_date;

	--check if the team has the minimun number of graded games, if true proceed to calculate the target scores
	if num_graded_games > minimun_required_graded_games then
		--get the team configuraion
		select * into team_config from team_configuration where go_team_id = go_team_id_parm::uuid;
		
		--get the positions with at least three graded players for the minimun number of graded games
		for position in (select position_played as id from
			(select grade.position_played, event.id as event_id, 
				count(distinct grade.team_player_id) as num_graded_players 
			from team_play_grade grade, team_play_data play, view_team_event event 
			where grade.team_play_data_id = play.id and 
			play.team_event_id = event.id and 
			grade.go_team_id = go_team_id_parm::uuid and 
			event.is_practice = false and 
			event.date > head_coach_start_date
			group by grade.position_played, event.id) x 
		where num_graded_players > 3 
		group by position_played 
		having count(event_id) > minimun_required_games_with_grades_for_position)

		loop
			--get the top two players for the position
			for eval_criteria in (select team_scouting_eval_criteria_id as criteria_id, round(avg(score), 2) as target_score from team_scouting_eval_score where team_scouting_eval_score_group_id in (
				select evaluation_id from (select distinct on (eval.team_player_id) eval.id as evaluation_id, round(avg(eval_score.score), 2) as eval_overall 
				from team_scouting_eval_score_group eval, team_scouting_eval_score eval_score 
				where eval_score.team_scouting_eval_score_group_id = eval.id and
				eval.date > head_coach_start_date and
				eval.go_team_id = go_team_id_parm::uuid and
				team_player_id in (select team_player_id from (select grade.team_player_id, 
						avg(round((grade.cat_1_grade + COALESCE(grade.cat_2_grade, 0) + COALESCE(grade.cat_3_grade, 0))/team_config.num_grade_categories::numeric, 2)) as overall 
					from team_play_grade grade, team_play_data play, view_team_event event 
					where grade.team_play_data_id = play.id and 
						play.team_event_id = event.id and 
						grade.position_played = 1 and
						grade.go_team_id = go_team_id_parm::uuid and 
						event.is_practice = false and 
						event.date > head_coach_start_date
					group by grade.team_player_id
					having avg(round((grade.cat_1_grade + COALESCE(grade.cat_2_grade, 0) + COALESCE(grade.cat_3_grade, 0))/team_config.num_grade_categories::numeric, 2)) > team_config.grade_base
					order by overall desc
					limit num_top_players) y)
				group by eval.id order by eval.team_player_id, eval_overall desc) z
			) group by team_scouting_eval_criteria_id)
			
			loop
				
				--RAISE NOTICE 'criteria_tpe % = %', eval_criteria.criteria_id, eval_criteria.target_score;
				update team_scouting_eval_criteria set calculated_target_score = eval_criteria.target_score where id = eval_criteria.criteria_id;
				
			end loop;
		end loop;
	end if;
	
	return 1;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

CREATE OR REPLACE FUNCTION refresh_season_stats(
    team_id_parm uuid,
    season_parm integer)
  RETURNS void AS
$BODY$
DECLARE
    game_scores RECORD;
BEGIN
	perform godeepmobile.create_season_stats_for_team(team_id_parm, season_parm);
	FOR game_scores IN (
		WITH 
		team_config AS (SELECT num_grade_categories FROM team_configuration WHERE go_team_id = team_id_parm)
		SELECT
			grades.team
			,grades."teamPlayer"
			,grades.season
			,count(grades."teamEvent")     as "numEvents"
			,sum(grades."numPlays")        as "numPlays"

			,count(case when event.is_practice = false then grades."teamEvent" else NULL end) as "numGames"
			,count(case when event.is_practice then grades."teamEvent" else NULL end) as "numPractices"

			,sum(case when event.is_practice = false then grades."numPlays" else 0 end) as "numPlaysGame"
			,sum(case when event.is_practice then grades."numPlays" else 0 end) as "numPlaysPractice"

			,sum(grades."sumCat1Grade")    as "sumCat1Grade"
			,sum(grades."sumCat2Grade")    as "sumCat2Grade"
			,sum(grades."sumCat3Grade")    as "sumCat3Grade"
			,sum(grades."sumOverallGrade") as "sumOverallGrade"
			,ROUND((sum(grades."sumCat1Grade")::numeric / sum(grades."numPlays"))::numeric, 2)        as "avgCat1Grade"
			,ROUND((sum(grades."sumCat2Grade")::numeric / sum(grades."numPlays"))::numeric, 2)        as "avgCat2Grade"
			,ROUND((sum(grades."sumCat3Grade")::numeric / sum(grades."numPlays"))::numeric, 2)        as "avgCat3Grade"
			,ROUND((sum(grades."sumOverallGrade")::numeric / ((SELECT num_grade_categories FROM team_config)*sum(grades."numPlays")))::numeric, 2) as "avgOverallGrade"

			,ROUND((sum(case when event.is_practice = false then grades."sumCat1Grade" else 0 end)::numeric / COALESCE(NULLIF(sum(case when event.is_practice = false then grades."numPlays" else 0 end), 0), 1))::numeric, 2)        as "avgCat1GradeGame"
			,ROUND((sum(case when event.is_practice = false then grades."sumCat2Grade" else 0 end)::numeric / COALESCE(NULLIF(sum(case when event.is_practice = false then grades."numPlays" else 0 end), 0), 1))::numeric, 2)        as "avgCat2GradeGame"
			,ROUND((sum(case when event.is_practice = false then grades."sumCat3Grade" else 0 end)::numeric / COALESCE(NULLIF(sum(case when event.is_practice = false then grades."numPlays" else 0 end), 0), 1))::numeric, 2)        as "avgCat3GradeGame"
			,ROUND((sum(case when event.is_practice = false then grades."sumOverallGrade" else 0 end)::numeric / ((SELECT num_grade_categories FROM team_config)*COALESCE(NULLIF(sum(case when event.is_practice = false then grades."numPlays" else 0 end), 0), 1)))::numeric, 2) as "avgOverallGradeGame"

			,ROUND((sum(case when event.is_practice then grades."sumCat1Grade" else 0 end)::numeric / COALESCE(NULLIF(sum(case when event.is_practice then grades."numPlays" else 0 end), 0), 1))::numeric, 2)        as "avgCat1GradePractice"
			,ROUND((sum(case when event.is_practice then grades."sumCat2Grade" else 0 end)::numeric / COALESCE(NULLIF(sum(case when event.is_practice then grades."numPlays" else 0 end), 0), 1))::numeric, 2)        as "avgCat2GradePractice"
			,ROUND((sum(case when event.is_practice then grades."sumCat3Grade" else 0 end)::numeric / COALESCE(NULLIF(sum(case when event.is_practice then grades."numPlays" else 0 end), 0), 1))::numeric, 2)        as "avgCat3GradePractice"
			,ROUND((sum(case when event.is_practice then grades."sumOverallGrade" else 0 end)::numeric / ((SELECT num_grade_categories FROM team_config)*COALESCE(NULLIF(sum(case when event.is_practice then grades."numPlays" else 0 end), 0), 1)))::numeric, 2) as "avgOverallGradePractice"
		FROM
			godeepmobile.view_team_player_event_grades grades, godeepmobile.view_team_event event
		WHERE
			grades."teamEvent" = event.id and
			grades.team = team_id_parm and grades.season = season_parm
		GROUP BY
			grades.team, grades."teamPlayer", grades.season
		ORDER BY
			grades."teamPlayer"
	)
	LOOP



        with
		season_stat_id as (select id from godeepmobile.team_player_season_stat where team_player_id = game_scores."teamPlayer" and game_scores.season = season_parm),
		last_game as (select *, (SELECT is_practice FROM view_team_event where id = "teamEvent") as "isPractice" from godeepmobile.view_team_player_event_grades where "teamPlayer" = game_scores."teamPlayer" and game_scores.season = season_parm ORDER BY date DESC LIMIT 1),
		team_conf as (select num_grade_categories, grade_base, player_multiplier_1, player_multiplier_2, player_multiplier_3, platoon_multiplier_1, platoon_multiplier_2, platoon_multiplier_3 from godeepmobile.team_configuration where go_team_id = team_id_parm),

		plays_with_player as (SELECT grade.team_play_data_id
			FROM team_play_grade grade,
				team_play_data play,
				view_team_event evt
			WHERE grade.team_player_id = game_scores."teamPlayer" AND
				grade.go_team_id = team_id_parm AND
				grade.team_play_data_id = play.id AND
				play.team_event_id = evt.id AND
				evt.is_practice = false AND
				evt.season = season_parm),

		player_primary_posgroup_platoon as (SELECT position_type.team_position_group_id AS "positionGroupId", position_type.go_platoon_type_id AS "platoonTypeId"
			FROM (SELECT grade.position_played, COUNT(*) AS num_grades
				FROM godeepmobile.team_play_grade grade,
					godeepmobile.view_team_play_with_event play
				WHERE grade.team_play_data_id = play.id AND
					play.is_practice = false AND
					grade.team_player_id = game_scores."teamPlayer" AND
					play.season = season_parm
				GROUP BY grade.position_played
				ORDER BY num_grades DESC LIMIT 1) AS team_player_primary_position,
				godeepmobile.view_team_position_type position_type
			WHERE position_type.go_team_id = team_id_parm AND
				team_player_primary_position.position_played = position_type.go_position_type_id AND
				position_type.end_date IS NULL),

		overall_grades_without_player as (SELECT ROUND((grade.cat_1_grade + COALESCE(grade.cat_2_grade, 0) + COALESCE(grade.cat_3_grade, 0))/(SELECT num_grade_categories FROM team_conf)::numeric, 2) AS overall_grade,
				pt.team_position_group_id,
				pt.go_platoon_type_id
			FROM team_play_grade grade,
				team_play_data play,
				view_team_event evt,
				view_team_position_type pt
			WHERE grade.go_team_id = team_id_parm AND
				grade.team_play_data_id = play.id AND
				play.team_event_id = evt.id AND
				evt.is_practice = false AND
				evt.season = season_parm AND
				grade.position_played = pt.go_position_type_id AND
				pt.go_team_id = team_id_parm AND
				grade.team_play_data_id NOT IN (SELECT team_play_data_id FROM plays_with_player)),

		impact_posgroup as (SELECT ROUND(AVG(CASE WHEN team_player_id = game_scores."teamPlayer"
					THEN
						CASE
							WHEN overall > (select grade_base from team_conf) AND go_play_factor_id = 3 THEN
								CASE
									WHEN hard THEN overall * (select player_multiplier_2 from team_conf)
								ELSE
									overall * (select player_multiplier_1 from team_conf)
								END
							WHEN overall < (select grade_base from team_conf) AND go_play_factor_id = 2 THEN
								overall * (select player_multiplier_3 from team_conf)
						ELSE
							overall
						END
					ELSE
						overall
				END), 2) - (SELECT ROUND(AVG(overall_grade), 2) FROM overall_grades_without_player WHERE team_position_group_id IN (SELECT "positionGroupId" FROM player_primary_posgroup_platoon)) AS "impactPosgroupGradeGame"
			FROM (
				SELECT g.team_player_id,
					ROUND((g.cat_1_grade + COALESCE(g.cat_2_grade, 0) + COALESCE(g.cat_3_grade, 0))/(SELECT num_grade_categories FROM team_conf)::numeric, 2) AS overall,
					g.go_play_factor_id,
					g.hard
				FROM godeepmobile.team_play_grade g,
					godeepmobile.view_team_position_type pt
				WHERE g.go_team_id = team_id_parm AND
					g.position_played = pt.go_position_type_id AND
					pt.team_position_group_id IN (SELECT "positionGroupId" FROM player_primary_posgroup_platoon) AND
					g.team_play_data_id IN (SELECT team_play_data_id FROM plays_with_player)
			) AS grade_overalls),
		impact_platoon as (SELECT ROUND(AVG(CASE WHEN team_player_id = game_scores."teamPlayer"
					THEN
						CASE
							WHEN overall > (select grade_base from team_conf) AND go_play_factor_id = 3 THEN
								CASE
									WHEN hard THEN overall * (select platoon_multiplier_2 from team_conf)
								ELSE
									overall * (select platoon_multiplier_1 from team_conf)
								END
							WHEN overall < (select grade_base from team_conf) AND go_play_factor_id = 2 THEN
								overall * (select platoon_multiplier_3 from team_conf)
						ELSE
							overall
						END
					ELSE
						overall
				END), 2) - (SELECT ROUND(AVG(overall_grade), 2) FROM overall_grades_without_player WHERE go_platoon_type_id IN (SELECT "platoonTypeId" FROM player_primary_posgroup_platoon)) AS "impactPlatoonGradeGame"
			FROM (
				SELECT g.team_player_id,
					ROUND((g.cat_1_grade + COALESCE(g.cat_2_grade, 0) + COALESCE(g.cat_3_grade, 0))/(SELECT num_grade_categories FROM team_conf)::numeric, 2) AS overall,
					g.go_play_factor_id,
					g.hard
				FROM godeepmobile.team_play_grade g,
					godeepmobile.view_team_position_type pt
				WHERE g.go_team_id = team_id_parm AND
					g.position_played = pt.go_position_type_id AND
					pt.go_platoon_type_id IN (SELECT "platoonTypeId" FROM player_primary_posgroup_platoon) AND
					g.team_play_data_id IN (SELECT team_play_data_id FROM plays_with_player)
			) AS grade_overalls)
        update
		godeepmobile.team_player_season_stat
        SET
		num_games                   = game_scores."numGames"
		,num_plays_game             = game_scores."numPlaysGame"
		,cat1_grade_game            = game_scores."avgCat1GradeGame"
		,cat2_grade_game            = game_scores."avgCat2GradeGame"
		,cat3_grade_game            = game_scores."avgCat3GradeGame"
		,overall_grade_game         = game_scores."avgOverallGradeGame"

                ,num_practices              = game_scores."numPractices"
		,num_plays_practice         = game_scores."numPlaysPractice"
		,cat1_grade_practice        = game_scores."avgCat1GradePractice"
		,cat2_grade_practice        = game_scores."avgCat2GradePractice"
		,cat3_grade_practice        = game_scores."avgCat3GradePractice"
		,overall_grade_practice     = game_scores."avgOverallGradePractice"

		,num_allevents              = game_scores."numEvents"
		,num_plays_allevent         = game_scores."numPlays"
		,cat1_grade_allevent        = game_scores."avgCat1Grade"
		,cat2_grade_allevent        = game_scores."avgCat2Grade"
		,cat3_grade_allevent        = game_scores."avgCat3Grade"
		,overall_grade_allevent     = game_scores."avgOverallGrade"

		,num_plays_last_game        = case when last_game."isPractice" = false then last_game."numPlays" else 0 end
		,cat1_grade_last_game       = case when last_game."isPractice" = false then last_game."avgCat1Grade" else 0 end
		,cat2_grade_last_game       = case when last_game."isPractice" = false then last_game."avgCat2Grade" else 0 end
		,cat3_grade_last_game       = case when last_game."isPractice" = false then last_game."avgCat3Grade" else 0 end
		,overall_grade_last_game    = case when last_game."isPractice" = false then last_game."avgOverallGrade" else 0 end

		,impact_posgroup_grade_game = (select "impactPosgroupGradeGame" from impact_posgroup)
		,impact_platoon_grade_game  = (select "impactPlatoonGradeGame" from impact_platoon)
		,unit_impact_grade_game     = ROUND(((select "impactPosgroupGradeGame" from impact_posgroup) + (select "impactPlatoonGradeGame" from impact_platoon)) / 2::numeric, 2)
	FROM
		last_game
        WHERE
		godeepmobile.team_player_season_stat.id = (select id from season_stat_id);
    END LOOP;

    --refreshing career stats
    perform godeepmobile.refresh_career_stats(team_id_parm);
    --refreshing player overall grade game ranking
    perform godeepmobile.refresh_player_grading_season_ranking(team_id_parm, season_parm);

    --refreshing player grade position ranking
    perform godeepmobile.refresh_player_grade_position_season_ranking(team_id_parm, season_parm);

    --refreshing player overall position ranking
    perform godeepmobile.refresh_player_overall_position_season_ranking(team_id_parm, season_parm);
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

--rollback ALTER TABLE team_configuration DROP COLUMN num_grade_categories; DROP VIEW view_team_player_event_grades; DROP VIEW view_team_player_event_grades_by_cutup; DROP VIEW view_team_player_event_grades_by_event; DROP VIEW view_team_player_event_grades_by_quarter; DROP VIEW view_grade_report_data; DROP VIEW view_platoon_event_grades; DROP VIEW view_platoon_event_grades_by_quarter; DROP VIEW view_position_type_event_grades; DROP VIEW view_position_type_event_grades_by_quarter; DROP VIEW view_team_play_grade_detail; DROP FUNCTION refresh_season_stats(uuid, integer); DROP FUNCTION calculate_evaluation_target_scores(text);