--liquibase formatted sql

--changeset rambert:93 runOnChange:true stripComments:false splitStatements:false
--comment making mascot column nullable in go_organization table

ALTER TABLE godeepmobile.go_organization ALTER COLUMN mascot DROP NOT NULL;

--rollback ALTER TABLE godeepmobile.go_organization ALTER COLUMN mascot SET NOT NULL;