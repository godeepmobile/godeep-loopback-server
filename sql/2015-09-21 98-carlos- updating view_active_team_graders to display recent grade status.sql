﻿--liquibase formatted sql

--changeset carlos:98 runOnChange:true splitStatements:false stripComments:false
--comment updated view_active_team_graders to display recent grade status

CREATE OR REPLACE VIEW view_active_team_graders AS
SELECT guta.id,
    guta.go_team_id,
    guta.go_user_id,
    gurt.name AS role,
    ( SELECT MAX("row".completed_date)
           FROM ( SELECT team_event_grade_status.go_user_id,
                    team_event_grade_status.go_team_id,
                    team_event_grade_status.team_event_id,
                    team_event_grade_status.completed_date
                   FROM team_event_grade_status
                  WHERE team_event_grade_status.completed_date IS NOT NULL
                  ORDER BY team_event_grade_status.completed_date DESC
                 ) "row"
          WHERE "row".go_user_id = guta.go_user_id) AS last_submit_date,
    concat(gu.first_name, ' ', gu.last_name) AS user_name
   FROM go_user_team_assignment guta,
    go_user gu,
    go_user_role_type gurt,
    go_role_permission grp
  WHERE gu.id = guta.go_user_id AND gurt.id = guta.go_user_role_type_id AND gurt.id = grp.go_user_role_type_id AND grp.go_permission_id::text = 'GRADER'::text AND guta.end_date IS NULL;

--rollback DROP VIEW view_active_team_graders;