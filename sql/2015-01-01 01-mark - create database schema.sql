--liquibase formatted sql

--changeset mark:1
SET client_min_messages TO WARNING;

drop schema if exists godeepmobile cascade;
drop schema if exists extensions cascade;
drop extension if exists "uuid-ossp" cascade;

CREATE SCHEMA godeepmobile;
CREATE SCHEMA extensions;
grant all on schema extensions to public;

DO $$ BEGIN
  execute 'ALTER DATABASE '||current_database()||' SET search_path = godeepmobile,extensions,public';
END; $$ ;


CREATE EXTENSION "uuid-ossp"
  SCHEMA extensions
  VERSION "1.0";

CREATE EXTENSION  IF NOT EXISTS plpgsql
  SCHEMA pg_catalog
  VERSION "1.0";

CREATE EXTENSION IF NOT EXISTS pg_stat_statements
  SCHEMA extensions
  VERSION "1.2";

CREATE TABLE godeepmobile.go_body_type (
  id                   integer  NOT NULL,
  name                 varchar(100)  NOT NULL,
  description          varchar(256)  ,
  CONSTRAINT pk_go_body_type PRIMARY KEY ( id )
 );

CREATE TABLE godeepmobile.go_event_type (
  id                   integer  NOT NULL,
  name                 varchar(100)  NOT NULL,
  description          varchar(256)  ,
  short_name           varchar(20)  ,
  CONSTRAINT pk_go_event_type PRIMARY KEY ( id )
 );

CREATE TABLE godeepmobile.go_field_condition (
  id                   integer  NOT NULL,
  name                 varchar(100)  NOT NULL,
  CONSTRAINT pk_go_field_condition PRIMARY KEY ( id )
 );

CREATE TABLE godeepmobile.go_organization_type (
  id                   integer  NOT NULL,
  name                 varchar(100)  NOT NULL,
  CONSTRAINT pk_go_organization_type PRIMARY KEY ( id )
 );

COMMENT ON TABLE godeepmobile.go_organization_type IS 'organizations are grouped by type: college, high school, pro, et al.';

CREATE TABLE godeepmobile.go_permission (
  id                   varchar(16)  NOT NULL,
  description          varchar(512)  ,
  CONSTRAINT pk_go_permission PRIMARY KEY ( id )
 );

COMMENT ON COLUMN godeepmobile.go_permission.id IS 'text value representing which permission. examples:
GRADER, PLAYER, REPORT_VIEWER, ';

CREATE TABLE godeepmobile.go_platoon_type (
  id                   integer  NOT NULL,
  name                 varchar(100)  NOT NULL,
  description          varchar(256)  ,
  CONSTRAINT pk_go_platoon_type PRIMARY KEY ( id )
 );

CREATE TABLE godeepmobile.go_play_result_type (
  id                   integer  NOT NULL,
  name                 varchar(100)  NOT NULL,
  description          varchar(256)  ,
  CONSTRAINT pk_go_play_result PRIMARY KEY ( id )
 );

CREATE TABLE godeepmobile.go_play_type (
  id                   integer  NOT NULL,
  name                 varchar(100)  NOT NULL,
  description          varchar(256)  ,
  CONSTRAINT pk_go_play_type PRIMARY KEY ( id )
 );

CREATE TABLE godeepmobile.go_sport_type (
  id                   integer  NOT NULL,
  name                 varchar(100)  NOT NULL,
  description          varchar(256)  ,
  CONSTRAINT pk_go_sport_type PRIMARY KEY ( id )
 );

CREATE TABLE godeepmobile.go_state (
  id                   integer  NOT NULL,
  name                 varchar(100)  NOT NULL,
  CONSTRAINT pk_go_state PRIMARY KEY ( id )
 );

CREATE TABLE godeepmobile.go_surface_type (
  id                   integer  NOT NULL,
  name                 varchar(100)  NOT NULL,
  CONSTRAINT pk_go_surface_type PRIMARY KEY ( id )
 );

CREATE TABLE godeepmobile.go_user (
  id                   serial  NOT NULL,
  user_name            varchar(1024)  NOT NULL, -- may be same as email address
  pass_word            varchar(256)  NOT NULL,
  first_name           varchar(100)  NOT NULL,
  middle_name          varchar(100)  ,
  last_name            varchar(100)  NOT NULL,
  credentials          varchar(1024)  ,
  challenges           varchar(1024)  ,
  email                varchar(1024)  ,
  emailverified        bool  ,
  verificationtoken    varchar(1024)  ,
  status               varchar(1024)  ,
  created              timestamptz  ,
  lastupdated          timestamptz  ,
  superuser            bool DEFAULT false ,
  job_title            varchar(250)  ,
  office_phone         varchar(15)  ,
  mobile_phone         varchar(15)  ,
  CONSTRAINT pk_go_user PRIMARY KEY ( id ),
  CONSTRAINT idx_go_user_unique UNIQUE ( user_name )
 );
COMMENT ON COLUMN godeepmobile.go_user.user_name IS 'might be email address';
COMMENT ON COLUMN godeepmobile.go_user.pass_word IS 'user`s encrypted password';

CREATE TABLE godeepmobile.go_user_email (
  email_address        varchar(256)  NOT NULL,
  go_user_id           integer  NOT NULL,
  CONSTRAINT pk_go_user_email PRIMARY KEY ( email_address )
 );

CREATE INDEX idx_go_user_email ON godeepmobile.go_user_email ( go_user_id );

CREATE TABLE godeepmobile.go_user_role_type (
  id                   integer  NOT NULL,
  name                 varchar(100)  NOT NULL,
  description          varchar(256)  ,
  created              timestamptz  ,
  modified             timestamptz  ,
  CONSTRAINT pk_go_person_role_type PRIMARY KEY ( id )
 );

COMMENT ON TABLE godeepmobile.go_user_role_type IS 'what role did a person play in a team or organization';

CREATE TABLE godeepmobile.go_conference (
  id                   integer  NOT NULL,
  name                 varchar(10)  NOT NULL,
  description          varchar(256)  ,
  go_organization_type_id integer  NOT NULL,
  level                varchar(8)  ,
  CONSTRAINT pk_go_conference PRIMARY KEY ( id )
 );

CREATE INDEX idx_go_conference ON godeepmobile.go_conference ( go_organization_type_id );

CREATE TABLE godeepmobile.go_organization (
  id                   serial  NOT NULL,
  go_conference_id     integer  ,
  go_organization_type_id integer  NOT NULL,
  name                 varchar(512)  NOT NULL,
  short_name           varchar(128)  NOT NULL,
  espn_name            varchar(8)  ,
  mascot               varchar(128)  NOT NULL,
  created_at           timestamptz  ,
  updated_at           timestamptz  ,
  asset_base_url       varchar(512) DEFAULT ''::character varying ,
  CONSTRAINT pk_go_organization PRIMARY KEY ( id )
 );

CREATE INDEX idx_go_team ON godeepmobile.go_organization ( go_conference_id );

CREATE INDEX idx_go_organization ON godeepmobile.go_organization ( go_organization_type_id );

COMMENT ON TABLE godeepmobile.go_organization IS 'GoOrganization
system-wide / public data for organizations: colleges, high schools, professional teams';

CREATE TABLE godeepmobile.go_position_type (
  id                   integer  NOT NULL,
  name                 varchar(100)  ,
  short_name           varchar(5)  ,
  go_sport_type_id     integer  NOT NULL,
  go_platoon_type_id   integer DEFAULT 0 ,
  CONSTRAINT pk_go_position_type PRIMARY KEY ( id )
 );

CREATE INDEX idx_go_position_type ON godeepmobile.go_position_type ( go_sport_type_id );

CREATE TABLE godeepmobile.go_role_permission (
  id                   integer  NOT NULL,
  go_user_role_type_id integer  NOT NULL,
  go_permission_id     varchar(16)  NOT NULL,
  CONSTRAINT pk_go_role_permission PRIMARY KEY ( id )
 );

CREATE INDEX idx_go_role_permission ON godeepmobile.go_role_permission ( go_user_role_type_id );

CREATE INDEX idx_go_role_permission_0 ON godeepmobile.go_role_permission ( go_permission_id );

COMMENT ON COLUMN godeepmobile.go_role_permission.go_permission_id IS 'permission will be sentinel values for specific permissions like: GRADER, or PLAYER, or VIEW_REPORTS, or ADMIN';

CREATE TABLE godeepmobile.go_team (
  id                   uuid DEFAULT extensions.uuid_generate_v4() NOT NULL,
  short_name           varchar(128)  ,
  go_organization_id   integer  NOT NULL,
  go_sport_type_id     integer  NOT NULL,
  go_surface_type_id   integer  ,
  go_conference_id     integer  ,
  abbreviation         varchar(8)  ,
  mascot               varchar(128)  ,
  asset_base_url       varchar(512)  ,
  addr_street1         varchar(256)  ,
  addr_street2         varchar(256)  ,
  addr_city            varchar(256)  ,
  addr_state_id        integer  ,
  addr_postal_code     varchar(16)  ,
  addr_phone_main      varchar(20)  ,
  addr_phone_fax       varchar(20)  ,
  home_stadium         varchar(128)  ,
  film_system          varchar(16)  ,
  CONSTRAINT pk_go_team PRIMARY KEY ( id )
 );

CREATE INDEX idx_organization ON godeepmobile.go_team ( go_organization_id );

CREATE INDEX idx_organization_0 ON godeepmobile.go_team ( go_sport_type_id );

CREATE INDEX idx_go_team_0 ON godeepmobile.go_team ( addr_state_id );

CREATE INDEX idx_go_team_1 ON godeepmobile.go_team ( go_surface_type_id );

CREATE INDEX idx_go_team_2 ON godeepmobile.go_team ( go_conference_id );

COMMENT ON TABLE godeepmobile.go_team IS 'public data for a team that plays a single sport as part of a larger organization.
example: Georgia Tech Yellow Jackets Football';

CREATE TABLE godeepmobile.go_user_team_assignment (
  id                   serial  NOT NULL,
  go_team_id           uuid  NOT NULL,
  go_user_id           integer  NOT NULL,
  go_user_role_type_id integer  NOT NULL,
  start_date           date  NOT NULL,
  end_date             date  ,
  principal_type       varchar(1024)  ,
  CONSTRAINT pk_go_user_team_assignment PRIMARY KEY ( id )
 );

CREATE INDEX idx_go_user_team_assignment ON godeepmobile.go_user_team_assignment ( go_user_id );

CREATE INDEX idx_go_user_team_assignment_0 ON godeepmobile.go_user_team_assignment ( go_team_id );

CREATE INDEX idx_go_user_team_assignment_1 ON godeepmobile.go_user_team_assignment ( go_user_role_type_id );

CREATE TABLE godeepmobile.team_configuration (
  id                   serial  NOT NULL,
  go_team_id           uuid  NOT NULL,
  grade_cat1_label     varchar(20) DEFAULT 'Mental' NOT NULL,
  grade_cat2_label     varchar(20) DEFAULT 'Technique' NOT NULL,
  grade_cat3_label     varchar(20) DEFAULT 'Effort' NOT NULL,
  grade_base           smallint DEFAULT 3 NOT NULL,
  grade_increment      smallint DEFAULT 1 NOT NULL,
  CONSTRAINT pk_team_configuration PRIMARY KEY ( id )
 );

CREATE TABLE godeepmobile.team_player (
  id                   uuid DEFAULT extensions.uuid_generate_v4() NOT NULL,
  go_team_id           uuid  NOT NULL,
  go_user_team_assignment_id integer  ,
  first_name           varchar(100)  NOT NULL,
  middle_name          varchar(100)  ,
  last_name            varchar(100)  NOT NULL,
  class_year           char(2)  ,
  birth_date           date  ,
  jersey_number        smallint  ,
  data                 jsonb  ,
  season               integer DEFAULT 2015 ,
  graduation_year      integer  ,
  go_body_type_id      integer  ,
  height               varchar(10)  ,
  weight               varchar(10)  ,
  wingspan             varchar(10)  ,
  forty_yard           varchar(10)  ,
  hand_size            varchar(10)  ,
  vertical_jump        varchar(10)  ,
  email                varchar(250)  ,
  sat                  numeric  ,
  act                  numeric  ,
  gpa                  numeric  ,
  go_state_id          integer  ,
  team_position_type_id_pos_1 integer  ,
  team_position_type_id_pos_2 integer  ,
  team_position_type_id_pos_3 integer  ,
  team_position_type_id_pos_st integer  ,
  high_school          varchar(200)  ,
  coach                varchar(100)  ,
  coach_phone          varchar(20)  ,
  level                varchar(10)  ,
  is_veteran           bool DEFAULT false ,
  start_date            timestamp DEFAULT ('now'::text)::timestamp NOT NULL ,
  end_date              timestamp ,
  CONSTRAINT pk_team_player PRIMARY KEY ( id )
 );

CREATE INDEX idx_team_player ON godeepmobile.team_player ( go_team_id );

CREATE INDEX idx_team_player_0 ON godeepmobile.team_player ( go_state_id );

CREATE INDEX idx_team_player_1 ON godeepmobile.team_player ( go_body_type_id );

CREATE TABLE godeepmobile.team_player_career_stat (
  id                   serial  NOT NULL,
  go_team_id           uuid  NOT NULL,
  team_player_id       uuid  NOT NULL,
  num_seasons          integer DEFAULT 0 ,
  num_games            integer DEFAULT 0 ,
  num_plays_game       integer DEFAULT 0 ,
  cat1_grade_game      integer DEFAULT 0 ,
  cat2_grade_game      integer DEFAULT 0 ,
  cat3_grade_game      integer DEFAULT 0 ,
  overall_grade_game   integer DEFAULT 0 ,
  impact_posgroup_grade_game integer DEFAULT 0 ,
  impact_platoon_grade_game integer DEFAULT 0 ,
  num_practices        integer DEFAULT 0 ,
  num_plays_practice   integer DEFAULT 0 ,
  cat1_grade_practice  integer DEFAULT 0 ,
  cat2_grade_practice  integer DEFAULT 0 ,
  cat3_grade_practice  integer DEFAULT 0 ,
  overall_grade_practice integer DEFAULT 0 ,
  impact_posgroup_grade_practice integer DEFAULT 0 ,
  impact_platoon_grade_practice integer DEFAULT 0 ,
  num_allevents        integer DEFAULT 0 ,
  num_plays_allevent   integer DEFAULT 0 ,
  cat1_grade_allevent  integer DEFAULT 0 ,
  cat2_grade_allevent  integer DEFAULT 0 ,
  cat3_grade_allevent  integer DEFAULT 0 ,
  overall_grade_allevent integer DEFAULT 0 ,
  impact_posgroup_grade_allevent integer DEFAULT 0 ,
  impact_platoon_grade_allevent integer DEFAULT 0 ,
  is_veteran           bool DEFAULT false ,
  CONSTRAINT pk_team_player_career_stat PRIMARY KEY ( id )
 );

CREATE INDEX idx_team_player_career_stat_0 ON godeepmobile.team_player_career_stat ( team_player_id );

COMMENT ON COLUMN godeepmobile.team_player_career_stat.cat1_grade_game IS 'total raw points, not an average.';

COMMENT ON COLUMN godeepmobile.team_player_career_stat.cat2_grade_game IS 'total raw points, not an average.';

COMMENT ON COLUMN godeepmobile.team_player_career_stat.cat3_grade_game IS 'total raw points, not an average.';

CREATE TABLE godeepmobile.team_player_season_stat (
  id                   serial  NOT NULL,
  go_team_id           uuid  NOT NULL,
  team_player_id       uuid  NOT NULL,
  season               integer DEFAULT 2015 NOT NULL,
  num_games            integer DEFAULT 0 ,
  num_plays_game       integer DEFAULT 0 ,
  cat1_grade_game      integer DEFAULT 0 ,
  cat2_grade_game      integer DEFAULT 0 ,
  cat3_grade_game      integer DEFAULT 0 ,
  overall_grade_game   integer DEFAULT 0 ,
  unit_impact_grade_game integer DEFAULT 0 ,
  impact_platoon_grade_game integer DEFAULT 0 ,
  impact_posgroup_grade_game integer DEFAULT 0 ,
  num_practices        integer DEFAULT 0 ,
  num_plays_practice   integer DEFAULT 0 ,
  cat1_grade_practice  integer DEFAULT 0 ,
  cat2_grade_practice  integer DEFAULT 0 ,
  cat3_grade_practice  integer DEFAULT 0 ,
  overall_grade_practice integer DEFAULT 0 ,
  impact_platoon_grade_practice integer DEFAULT 0 ,
  impact_posgroup_grade_practice integer DEFAULT 0 ,
  num_allevents        integer DEFAULT 0 ,
  num_plays_allevent   integer DEFAULT 0 ,
  cat1_grade_allevent  integer DEFAULT 0 ,
  cat2_grade_allevent  integer DEFAULT 0 ,
  cat3_grade_allevent  integer DEFAULT 0 ,
  overall_grade_allevent integer DEFAULT 0 ,
  impact_platoon_grade_allevent integer DEFAULT 0 ,
  impact_posgroup_grade_allevent integer DEFAULT 0 ,
  num_plays_last_game  integer DEFAULT 0 ,
  cat1_grade_last_game integer DEFAULT 0 ,
  cat2_grade_last_game integer DEFAULT 0 ,
  cat3_grade_last_game integer DEFAULT 0 ,
  overall_grade_last_game integer DEFAULT 0 ,
  impact_platoon_grade_last_game integer DEFAULT 0 ,
  impact_posgroup_grade_last_game integer DEFAULT 0 ,
  is_veteran           bool DEFAULT false ,
  CONSTRAINT pk_team_player_season_stat PRIMARY KEY ( id )
 );

CREATE INDEX idx_team_player_season_stat_0 ON godeepmobile.team_player_season_stat ( team_player_id );

CREATE INDEX fki_team_player_season_stat_go_stat_type ON godeepmobile.team_player_season_stat ( go_team_id );

COMMENT ON TABLE godeepmobile.team_player_season_stat IS 'season statistics for a single player in a single season
category grades are total raw points for all plays in all games in the season
to get average, divide category grade by number of plays';

COMMENT ON COLUMN godeepmobile.team_player_season_stat.cat1_grade_game IS 'total raw points, not an average.';

COMMENT ON COLUMN godeepmobile.team_player_season_stat.cat2_grade_game IS 'total raw points, not an average.';

COMMENT ON COLUMN godeepmobile.team_player_season_stat.cat3_grade_game IS 'total raw points, not an average.';

COMMENT ON COLUMN godeepmobile.team_player_season_stat.is_veteran IS 'true when a player is a veteran and not a first year player.';

CREATE TABLE godeepmobile.team_position_group (
  id                   serial  NOT NULL,
  go_team_id           uuid  NOT NULL,
  name                 varchar(100)  NOT NULL,
  go_platoon_type_id   integer  ,
  start_date           timestamp DEFAULT ('now'::text)::timestamp NOT NULL ,
  end_date             timestamp ,
  CONSTRAINT pk_team_position_group PRIMARY KEY ( id )
 );

CREATE INDEX idx_team_position_group_0 ON godeepmobile.team_position_group ( go_team_id );

CREATE INDEX idx_team_position_group ON godeepmobile.team_position_group ( go_platoon_type_id );

CREATE TABLE godeepmobile.team_position_type (
  id                     serial  NOT NULL,
  go_team_id             uuid  NOT NULL,
  name                   varchar(100)  NOT NULL,
  short_name             varchar(5)  ,
  go_position_type_id    integer  NOT NULL,
  team_position_group_id integer  NOT NULL,
  go_platoon_type_id     integer DEFAULT 0 ,
  start_date             timestamp DEFAULT ('now'::text)::timestamp NOT NULL ,
  end_date               timestamp ,
  CONSTRAINT pk_team_position_type PRIMARY KEY ( id )
 );

CREATE INDEX idx_team_position_type ON godeepmobile.team_position_type ( go_position_type_id );

CREATE INDEX idx_team_position_type_0 ON godeepmobile.team_position_type ( go_team_id );

CREATE INDEX idx_team_position_type_1 ON godeepmobile.team_position_type ( team_position_group_id );

COMMENT ON TABLE godeepmobile.team_position_type IS 'team-specific player position definitions';

CREATE TABLE godeepmobile.team_practice_drill_type (
  id                   serial  NOT NULL,
  go_team_id           uuid  NOT NULL,
  name                 varchar(100)  NOT NULL,
  description          varchar(256)  ,
  team_position_group_id integer  ,
  CONSTRAINT pk_team_practice_type PRIMARY KEY ( id )
 );

CREATE INDEX idx_team_practice_type ON godeepmobile.team_practice_drill_type ( team_position_group_id );

CREATE INDEX idx_team_practice_type_0 ON godeepmobile.team_practice_drill_type ( go_team_id );

CREATE TABLE godeepmobile.team_user_position_group_assignment (
  id                   serial  NOT NULL,
  go_team_id           uuid  NOT NULL,
  go_user_id           integer  NOT NULL,
  team_position_group_id integer  NOT NULL,
  start_date           date  NOT NULL,
  end_date             date  ,
  CONSTRAINT pk_team_user_position_group_assignment PRIMARY KEY ( id )
 );

ALTER TABLE godeepmobile.team_user_position_group_assignment ADD CONSTRAINT end_date_later CHECK ( end_date > start_date );

CREATE INDEX idx_team_user_position_group_assignment ON godeepmobile.team_user_position_group_assignment ( go_user_id );

CREATE INDEX idx_team_user_position_group_assignment_0 ON godeepmobile.team_user_position_group_assignment ( team_position_group_id );

CREATE INDEX idx_team_user_position_group_assignment_1 ON godeepmobile.team_user_position_group_assignment ( go_team_id );

CREATE TABLE godeepmobile.go_game_event (
  id                   serial  NOT NULL,
  season               integer DEFAULT 2015 NOT NULL,
  home_team_id         uuid  NOT NULL,
  away_team_id         uuid  NOT NULL,
  home_score           smallint DEFAULT 0 ,
  away_score           smallint DEFAULT 0 ,
  date                 date  NOT NULL,
  CONSTRAINT go_event_pkey PRIMARY KEY ( id )
 );

CREATE INDEX idx_go_event ON godeepmobile.go_game_event ( home_team_id );

CREATE INDEX idx_go_event_0 ON godeepmobile.go_game_event ( away_team_id );

COMMENT ON COLUMN godeepmobile.go_game_event.home_team_id IS 'home team';

COMMENT ON COLUMN godeepmobile.go_game_event.away_team_id IS 'away team';

CREATE TABLE godeepmobile.team_event (
  id                   serial  NOT NULL,
  go_team_id           uuid  NOT NULL,
  date                 date  NOT NULL,
  go_event_type_id     integer  NOT NULL,
  go_field_condition_id integer  ,
  go_surface_type_id   integer  ,
  description          varchar(256)  ,
  go_game_event_id     integer  ,
  grade_base           smallint  ,
  grade_increment      smallint  ,
  data                 jsonb  ,
  season               integer DEFAULT 2015 ,
  CONSTRAINT pk_team_event PRIMARY KEY ( id )
 );

CREATE INDEX idx_team_event ON godeepmobile.team_event ( go_event_type_id );

CREATE INDEX idx_team_event_0 ON godeepmobile.team_event ( go_team_id );

CREATE INDEX idx_team_event_1 ON godeepmobile.team_event ( go_game_event_id );

CREATE INDEX idx_team_event_2 ON godeepmobile.team_event ( go_field_condition_id );

CREATE INDEX idx_team_event_3 ON godeepmobile.team_event ( go_surface_type_id );

COMMENT ON COLUMN godeepmobile.team_event.data IS 'unstructured JSON data for an event';

CREATE TABLE godeepmobile.team_cutup (
	id                   serial  NOT NULL,
	go_team_id           uuid  NOT NULL,
	team_event_id        integer  NOT NULL,
	which_platoon        integer  NOT NULL,
	file_uri             varchar(500),
	num_plays            integer  ,
	CONSTRAINT pk_team_cutup PRIMARY KEY ( id )
 );

CREATE INDEX idx_team_cutup ON godeepmobile.team_cutup ( team_event_id );

CREATE INDEX idx_team_cutup_0 ON godeepmobile.team_cutup ( go_team_id );

COMMENT ON COLUMN godeepmobile.team_cutup.which_platoon IS 'offense, defense, special teams, ALL';

COMMENT ON COLUMN godeepmobile.team_cutup.file_uri IS 'where is the play data file? might be s3 bucket identifier';

ALTER TABLE godeepmobile.team_cutup ADD CONSTRAINT fk_team_cutup_team_event FOREIGN KEY ( team_event_id ) REFERENCES godeepmobile.team_event( id );

ALTER TABLE godeepmobile.team_cutup ADD CONSTRAINT fk_team_cutup_go_team FOREIGN KEY ( go_team_id ) REFERENCES godeepmobile.go_team( id );

CREATE TABLE godeepmobile.team_event_grade_status (
  id                   serial  NOT NULL,
  go_team_id           uuid  NOT NULL,
  go_user_id           integer  NOT NULL,
  team_event_id        integer  NOT NULL,
  completed_date       timestamp  ,
  started_date         timestamp  ,
  CONSTRAINT pk_team_event_grade_status PRIMARY KEY ( id )
 );

CREATE INDEX idx_team_event_grade_status ON godeepmobile.team_event_grade_status ( go_team_id );

CREATE INDEX idx_team_event_grade_status_0 ON godeepmobile.team_event_grade_status ( go_user_id );

CREATE INDEX idx_team_event_grade_status_1 ON godeepmobile.team_event_grade_status ( team_event_id );

CREATE TABLE godeepmobile.team_play_data (
  id                   serial  NOT NULL,
  go_team_id           uuid  NOT NULL,
  team_event_id        integer  NOT NULL,
  play_number          smallint  NOT NULL,
  game_down            smallint  ,
  game_distance        smallint  ,
  game_possession      varchar(20)  ,
  go_play_result_type_id integer  ,
  team_practice_drill_type_id integer  ,
  data                 jsonb  ,
  go_play_type_id      integer  ,
  quarter              smallint  ,
  yard_line            smallint  ,
  CONSTRAINT pk_team_event_game_play_data PRIMARY KEY ( id )
 );

CREATE INDEX idx_team_event_game_play_data ON godeepmobile.team_play_data ( go_play_result_type_id );

CREATE INDEX idx_team_event_game_play_data_0 ON godeepmobile.team_play_data ( team_event_id );

CREATE INDEX idx_team_play_data ON godeepmobile.team_play_data ( team_practice_drill_type_id );

CREATE INDEX idxgin_play_data ON godeepmobile.team_play_data ( data );

COMMENT ON TABLE godeepmobile.team_play_data IS 'variant play data
put game specific fields in JSON column';

COMMENT ON COLUMN godeepmobile.team_play_data.data IS 'unstructured JSON data for a play';

CREATE TABLE godeepmobile.team_play_grade (
  id                     serial  NOT NULL,
  go_team_id             uuid  NOT NULL,
  team_player_id         uuid  NOT NULL,
  team_play_data_id      integer  NOT NULL,
  cat_1_grade            smallint  ,
  cat_2_grade            smallint  ,
  cat_3_grade            smallint  ,
  notes                  varchar(1024)  ,
  poa                    bool DEFAULT false ,
  hard                   bool DEFAULT false ,
  position_played        integer  NOT NULL,
  go_user_id             integer  NOT NULL,
  players_can_see_notes  bool DEFAULT false,
  go_platoon_type_id     integer,
  team_position_group_id integer,
  CONSTRAINT pk_team_event_play_grade PRIMARY KEY ( id )
 );

CREATE INDEX idx_team_play_grade ON godeepmobile.team_play_grade ( team_play_data_id );

CREATE INDEX idx_team_play_grade_0 ON godeepmobile.team_play_grade ( go_user_id );

CREATE INDEX idx_team_play_grade_1 ON godeepmobile.team_play_grade ( go_platoon_type_id );

CREATE INDEX idx_team_play_grade_2 ON godeepmobile.team_play_grade ( team_position_group_id );

CREATE TABLE godeepmobile.team_player_game_stat (
  id                   serial  NOT NULL,
  go_team_id           uuid  NOT NULL,
  team_player_id       uuid  NOT NULL,
  season               integer DEFAULT 2015 NOT NULL,
  num_plays            integer DEFAULT 0 ,
  cat1_grade           integer DEFAULT 0 ,
  cat2_grade           integer DEFAULT 0 ,
  cat3_grade           integer DEFAULT 0 ,
  overall_grade        integer DEFAULT 0 ,
  team_event_id        integer  ,
  date                 date  ,
  last_update          timestamp  ,
  CONSTRAINT pk_team_player_game_stat PRIMARY KEY ( id ),
  CONSTRAINT unique_team_player_id_team_event_id UNIQUE ( team_event_id, team_player_id )
 );

CREATE INDEX idx_team_player_season_stat ON godeepmobile.team_player_game_stat ( team_player_id );

CREATE INDEX idx_team_player_game_stat ON godeepmobile.team_player_game_stat ( team_event_id );

CREATE INDEX fki_team_player_game_stat_go_stat_type ON godeepmobile.team_player_game_stat ( go_team_id );

COMMENT ON TABLE godeepmobile.team_player_game_stat IS 'game statistics for a single player for a single game
category grades for total raw points for all plays in a single game. to get average category grade, divide catn_grade by num_plays';

COMMENT ON COLUMN godeepmobile.team_player_game_stat.cat1_grade IS 'total raw points, not an average.';

COMMENT ON COLUMN godeepmobile.team_player_game_stat.cat2_grade IS 'total raw points, not an average.';

COMMENT ON COLUMN godeepmobile.team_player_game_stat.cat3_grade IS 'total raw points, not an average.';

COMMENT ON COLUMN godeepmobile.team_player_game_stat.overall_grade IS 'total raw points of category grades, not an average.';

COMMENT ON COLUMN godeepmobile.team_player_game_stat.last_update IS 'last time this record was updated';

CREATE VIEW godeepmobile.view_active_team_graders AS  SELECT guta.id,
    guta.go_team_id,
    guta.go_user_id,
    gurt.name AS role,
    ( SELECT "row".completed_date
           FROM ( SELECT team_event_grade_status.go_user_id,
                    team_event_grade_status.go_team_id,
                    team_event_grade_status.team_event_id,
                    team_event_grade_status.completed_date
                   FROM godeepmobile.team_event_grade_status
                  WHERE (team_event_grade_status.completed_date IS NOT NULL)
                  ORDER BY team_event_grade_status.completed_date DESC
                 LIMIT 1) "row"
          WHERE ("row".go_user_id = guta.go_user_id)) AS last_submit_date,
    concat(gu.first_name, ' ', gu.last_name) AS user_name
   FROM godeepmobile.go_user_team_assignment guta,
    godeepmobile.go_user gu,
    godeepmobile.go_user_role_type gurt,
    godeepmobile.go_role_permission grp
  WHERE (((((gu.id = guta.go_user_id) AND (gurt.id = guta.go_user_role_type_id)) AND (gurt.id = grp.go_user_role_type_id)) AND ((grp.go_permission_id)::text = 'GRADER'::text)) AND (guta.end_date IS NULL));;

CREATE VIEW godeepmobile.view_active_team_players AS
 SELECT player.go_team_id,
    player.id AS team_player_id,
    player.season,
    player.go_user_team_assignment_id,
    assign.go_user_id,
    assign.start_date,
    assign.end_date
   FROM godeepmobile.team_player player
     LEFT JOIN godeepmobile.go_user_team_assignment assign ON player.go_user_team_assignment_id = assign.id
     LEFT JOIN godeepmobile.go_user_role_type role ON assign.go_user_role_type_id = role.id
     LEFT JOIN godeepmobile.go_role_permission permission ON role.id = permission.go_user_role_type_id
  WHERE permission.go_permission_id::text = 'PLAYER'::text AND assign.end_date IS NULL
  ORDER BY player.id;

CREATE VIEW godeepmobile.view_active_user_non_players AS
 SELECT
    ( SELECT
        guta.id
      FROM
        godeepmobile.go_user_team_assignment guta,
        godeepmobile.go_user_role_type gurt
      WHERE
        gu.id = guta.go_user_id AND
        guta.go_user_role_type_id = gurt.id AND
        gurt.id <> 1 AND
        guta.end_date IS NULL
    ) AS go_user_team_assignment_id,
    ( SELECT
        guta.go_team_id
      FROM
        godeepmobile.go_user_team_assignment guta,
        godeepmobile.go_user_role_type gurt
      WHERE
        gu.id = guta.go_user_id AND
        guta.go_user_role_type_id = gurt.id AND
        gurt.id <> 1 AND
        guta.end_date IS NULL
    ) AS go_team_id,
    ( SELECT
        gurt.name
      FROM
        godeepmobile.go_user_team_assignment guta,
        godeepmobile.go_user_role_type gurt
      WHERE
        gu.id = guta.go_user_id AND
        guta.go_user_role_type_id = gurt.id AND
        gurt.id <> 1 AND
        guta.end_date IS NULL
    ) AS role,
    ( SELECT
        gurt.id
      FROM
        godeepmobile.go_user_team_assignment guta,
        godeepmobile.go_user_role_type gurt
      WHERE
        gu.id = guta.go_user_id AND
        guta.go_user_role_type_id = gurt.id AND
        gurt.id <> 1 AND
        guta.end_date IS NULL
    ) AS go_user_role_type_id,
    gu.id,
    gu.user_name,
    gu.first_name,
    gu.middle_name,
    gu.last_name,
    gu.email,
    gu.job_title,
    gu.office_phone,
    gu.mobile_phone,
    (gu.last_name::text || ', '::text) || gu.first_name::text AS full_name
 FROM
    godeepmobile.go_user gu
 WHERE (
      gu.id IN (
        SELECT
          gu.id
        FROM
          godeepmobile.go_user_team_assignment guta,
          godeepmobile.go_user_role_type gurt
        WHERE
          gu.id = guta.go_user_id AND
          guta.go_user_role_type_id = gurt.id AND
          gurt.id <> 1 AND
          guta.end_date IS NULL
      )
  )
  ORDER BY gu.last_name, gu.first_name;

CREATE VIEW godeepmobile.view_go_event AS  SELECT gge.id,
    gge.home_score,
    gge.away_score,
    gge.date,
    go1.short_name AS home_team,
    go.short_name AS away_team,
    concat(go.short_name, '(', gge.away_score, ') at ', go1.short_name, '(', gge.home_score, ')') AS description
   FROM ((((godeepmobile.go_game_event gge
     JOIN godeepmobile.go_team gt ON ((gge.home_team_id = gt.id)))
     JOIN godeepmobile.go_organization go1 ON ((gt.go_organization_id = go1.id)))
     JOIN godeepmobile.go_team gt1 ON ((gge.away_team_id = gt1.id)))
     JOIN godeepmobile.go_organization go ON ((gt1.go_organization_id = go.id)))
  ORDER BY gge.date, go.short_name;;

CREATE VIEW godeepmobile.view_go_team AS  SELECT gt.id,
    ( SELECT go.name
           FROM godeepmobile.go_organization go
          WHERE (go.id = gt.go_organization_id)) AS name,
    gt.short_name,
    gt.abbreviation,
    gt.mascot,
    ( SELECT gc.name
           FROM godeepmobile.go_conference gc
          WHERE (gc.id = gt.go_conference_id)) AS conference_name,
    gt.go_sport_type_id,
    gt.addr_street1,
    gt.addr_street2,
    gt.addr_city,
    gt.addr_state_id,
    gt.addr_postal_code,
    gt.addr_phone_main,
    gt.addr_phone_fax,
    gt.home_stadium,
    gt.go_surface_type_id,
    gt.film_system,
    gt.go_conference_id,
    gt.asset_base_url
   FROM godeepmobile.go_team gt
  ORDER BY gt.id;;

CREATE VIEW godeepmobile.view_grade_report_data AS
  SELECT
    tpg.id,
    tpd.go_team_id,
    te.season,
    te.date,
    te.go_field_condition_id,
    te.go_surface_type_id,
    gge.home_team_id,
    gge.away_team_id,
    (gge.home_team_id = tpd.go_team_id) AS at_home,
    tpd.team_event_id,
    tpd.play_number,
    tpd.quarter,
    tpd.game_down,
    tpd.game_distance,
    tpd.yard_line,
    tpd.go_play_type_id,
    tpd.game_possession,
    tpd.go_play_result_type_id,
    tpg.id AS team_play_grade_id,
    tpg.team_player_id,
    tpg.position_played as team_position_type_id,
    tpg.go_platoon_type_id,
    tpg.team_position_group_id,
    tpg.go_user_id,
    tpg.hard,
    tpg.poa,
    tpg.cat_1_grade as cat1_grade,
    tpg.cat_2_grade as cat2_grade,
    tpg.cat_3_grade as cat3_grade,
    ((tpg.cat_1_grade + tpg.cat_2_grade) + tpg.cat_3_grade) AS sum_overall_grade,
    (((tpg.cat_1_grade + tpg.cat_2_grade) + tpg.cat_3_grade) / 3) AS avg_overall_grade,
    (abs(tpd.yard_line) <= 20) AS red_zone
   FROM (((godeepmobile.team_play_data tpd
     JOIN godeepmobile.team_event te ON ((tpd.team_event_id = te.id)))
     JOIN godeepmobile.team_play_grade tpg ON ((tpd.id = tpg.team_play_data_id)))
     JOIN godeepmobile.go_game_event gge ON ((te.go_game_event_id = gge.id)));;

CREATE VIEW godeepmobile.view_team_event_grade_status AS  SELECT team_event_grade_status.id,
    team_event_grade_status.go_team_id,
    team_event_grade_status.go_user_id,
    team_event_grade_status.team_event_id,
    team_event_grade_status.completed_date,
    team_event_grade_status.started_date,
        CASE
            WHEN (team_event_grade_status.started_date IS NULL) THEN 'not started'::text
            WHEN (team_event_grade_status.completed_date IS NOT NULL) THEN 'completed'::text
            ELSE 'in process'::text
        END AS status
   FROM godeepmobile.team_event_grade_status;;

CREATE VIEW godeepmobile.view_team_play_data AS  SELECT tpd.id,
    ('Play #'::text || tpd.play_number) AS play_number,
    te.date,
    te.description
   FROM (godeepmobile.team_play_data tpd
     JOIN godeepmobile.team_event te ON ((tpd.team_event_id = te.id)))
  ORDER BY tpd.id;;

CREATE VIEW godeepmobile.view_team_play_with_event AS  SELECT tpd.id,
    tpd.play_number,
    tpd.game_down,
    tpd.game_distance,
    tpd.game_possession,
    tpd.go_play_result_type_id,
    tpd.team_event_id,
    tpd.team_practice_drill_type_id,
    tpd.data,
    tpd.go_team_id,
    te.date,
    te.season
   FROM (godeepmobile.team_play_data tpd
     JOIN godeepmobile.team_event te ON ((tpd.team_event_id = te.id)));;
-- View: view_team_player

-- DROP VIEW view_team_player;

CREATE VIEW godeepmobile.view_team_player AS
SELECT team_player.id,
    team_player.last_name,
    team_player.middle_name,
    team_player.first_name,
    team_player.go_team_id,
    team_player.data,
    team_player.go_user_team_assignment_id,
    team_player.class_year,
    team_player.birth_date,
    team_player.jersey_number,
    team_player.season,
    team_player.height,
    team_player.weight,
    team_player.graduation_year,
    team_player.wingspan,
    team_player.forty_yard,
    team_player.go_body_type_id,
    team_player.hand_size,
    team_player.vertical_jump,
    team_player.email,
    team_player.sat,
    team_player.act,
    team_player.gpa,
    team_player.go_state_id,
    team_player.team_position_type_id_pos_1,
    team_player.team_position_type_id_pos_2,
    team_player.team_position_type_id_pos_3,
    team_player.team_position_type_id_pos_st,
    team_player.high_school,
    team_player.coach,
    team_player.coach_phone,
    team_player.level,
    team_player.is_veteran,
    team_player.start_date,
    team_player.end_date,
        CASE
            WHEN (team_player.team_position_type_id_pos_st IS NULL) THEN false
            ELSE true
        END AS is_special_teams,
    ( WITH offense AS (
                 SELECT team_position_type.id
                   FROM godeepmobile.team_position_type
                  WHERE (team_position_type.go_platoon_type_id = 1)
                )
         SELECT
                CASE
                    WHEN (((team_player.team_position_type_id_pos_1 IN ( SELECT offense.id
                       FROM offense)) OR (team_player.team_position_type_id_pos_2 IN ( SELECT offense.id
                       FROM offense))) OR (team_player.team_position_type_id_pos_3 IN ( SELECT offense.id
                       FROM offense))) THEN true
                    ELSE false
                END AS "case") AS is_offense,
    ( WITH defense AS (
                 SELECT team_position_type.id
                   FROM godeepmobile.team_position_type
                  WHERE (team_position_type.go_platoon_type_id = 2)
                )
         SELECT
                CASE
                    WHEN (((team_player.team_position_type_id_pos_1 IN ( SELECT defense.id
                       FROM defense)) OR (team_player.team_position_type_id_pos_2 IN ( SELECT defense.id
                       FROM defense))) OR (team_player.team_position_type_id_pos_3 IN ( SELECT defense.id
                       FROM defense))) THEN true
                    ELSE false
                END AS "case") AS is_defense,
    ( SELECT tpcs.id
           FROM godeepmobile.team_player_career_stat tpcs
          WHERE (team_player.id = tpcs.team_player_id)) AS team_player_career_stat,
    ARRAY( SELECT tpss.id
           FROM godeepmobile.team_player_season_stat tpss
          WHERE (team_player.id = tpss.team_player_id)
          ORDER BY tpss.season DESC) AS team_player_season_stats
   FROM godeepmobile.team_player
  WHERE team_player.end_date IS NULL
  ORDER BY team_player.jersey_number;

CREATE VIEW godeepmobile.view_team_player_event_grades AS
 SELECT player.id AS "teamPlayer",
    player.season,
    count(grade.id)::integer AS "numPlays",
    sum(grade.cat_1_grade)::integer AS "sumCat1Grade",
    sum(grade.cat_2_grade)::integer AS "sumCat2Grade",
    sum(grade.cat_3_grade)::integer AS "sumCat3Grade",
    sum(grade.cat_1_grade + grade.cat_2_grade + grade.cat_3_grade)::integer AS "sumOverallGrade",
    avg(grade.cat_1_grade)::integer AS "avgCat1Grade",
    avg(grade.cat_2_grade)::integer AS "avgCat2Grade",
    avg(grade.cat_3_grade)::integer AS "avgCat3Grade",
   (avg(grade.cat_1_grade + grade.cat_2_grade + grade.cat_3_grade)/3)::integer AS "avgOverallGrade",
     play.team_event_id AS "teamEvent",
    event.date,
    player.go_team_id AS team
   FROM godeepmobile.team_player player
     JOIN godeepmobile.team_play_grade grade ON grade.team_player_id = player.id
     JOIN godeepmobile.team_play_data play ON play.id = grade.team_play_data_id
     JOIN godeepmobile.team_event event ON play.team_event_id = event.id
  GROUP BY player.id, event.date, play.team_event_id
  ORDER BY player.id;

CREATE VIEW godeepmobile.view_game_stat_ids_for_players_with_event_plays AS
 SELECT grades.team AS go_team_id,
    grades."teamPlayer" AS team_player_id,
    grades."teamEvent" AS team_event_id,
    grades."numPlays" AS num_plays,
    stats.id AS team_player_game_stat_id,
    grades.season,
    grades.date
   FROM godeepmobile.view_team_player_event_grades grades
     LEFT JOIN godeepmobile.team_player_game_stat stats ON stats.team_player_id = grades."teamPlayer"
  ORDER BY grades."teamPlayer";

CREATE VIEW godeepmobile.view_team_player_event_grades_by_quarter AS
  SELECT concat(player.id, '-', play.team_event_id, '-', play.quarter) AS id,
      player.go_team_id,
      player.id AS team_player_id,
      play.team_event_id,
      play.quarter,
      count(grade.id) AS num_plays,
      (sum(grade.cat_1_grade))::integer AS sum_cat1_grade,
      (sum(grade.cat_2_grade))::integer AS sum_cat2_grade,
      (sum(grade.cat_3_grade))::integer AS sum_cat3_grade,
      (sum(((grade.cat_1_grade + grade.cat_2_grade) + grade.cat_3_grade)))::integer AS sum_overall_grade,
      (avg(grade.cat_1_grade))::integer AS avg_cat1_grade,
      (avg(grade.cat_2_grade))::integer AS avg_cat2_grade,
      (avg(grade.cat_3_grade))::integer AS avg_cat3_grade,
      ((avg(((grade.cat_1_grade + grade.cat_2_grade) + grade.cat_3_grade)) / (3)::numeric))::integer AS avg_overall_grade
     FROM (((godeepmobile.team_player player
       JOIN godeepmobile.team_play_grade grade ON ((grade.team_player_id = player.id)))
       JOIN godeepmobile.team_play_data play ON ((play.id = grade.team_play_data_id)))
       JOIN godeepmobile.team_event event ON ((play.team_event_id = event.id)))
    GROUP BY player.id, play.team_event_id, play.quarter
    ORDER BY player.id, play.team_event_id, play.quarter;

CREATE OR REPLACE VIEW godeepmobile.view_team_event_plays_by_platoon_type_count AS
	SELECT
		team_event_id,
		COUNT(*) AS num_plays,
		SUM(CASE WHEN platoon = 1 THEN 1 ELSE 0 END) AS offense_plays,
		SUM(CASE WHEN platoon = 2 THEN 1 ELSE 0 END) AS defense_plays,
		SUM(CASE WHEN platoon = 3 THEN 1 ELSE 0 END) AS special_teams_plays,
		SUM(CASE WHEN platoon = 0 THEN 1 ELSE 0 END) AS unknown_unassigned_plays
	FROM (
		SELECT
			play.team_event_id,
			COALESCE(
				CASE
					WHEN play.game_down >= 1 AND play.game_down <= 3 THEN
						CASE WHEN play.game_possession = team.abbreviation THEN 1 ELSE 2 END
					WHEN play.game_down = 4 THEN 3
				END,
				CASE
					WHEN play_type.id IN (6, 7, 8, 9, 10) THEN 3
				END,
				(SELECT
					CASE
						WHEN SUM(CASE WHEN platoon_type.id = 3 THEN 1 ELSE 0 END) >= 2 THEN 3
						WHEN SUM(CASE WHEN platoon_type.id = 1 THEN 1 ELSE 0 END) >= 2 THEN 1
						WHEN SUM(CASE WHEN platoon_type.id = 2 THEN 1 ELSE 0 END) >= 2 THEN 2
					END
				FROM
					godeepmobile.team_play_grade grade,
					godeepmobile.team_player player,
					godeepmobile.team_position_type position_played,
					godeepmobile.go_platoon_type platoon_type
				WHERE
					grade.team_player_id = player.id AND
					grade.position_played = position_played.id AND
					position_played.go_platoon_type_id = platoon_type.id AND
					grade.team_play_data_id = play.id),
				0
			) AS platoon
		FROM
			godeepmobile.team_play_data play
		INNER JOIN
			godeepmobile.go_team team ON play.go_team_id = team.id
		LEFT OUTER JOIN
			godeepmobile.go_play_type play_type ON play.go_play_type_id = play_type.id
	) AS plays GROUP BY team_event_id;

ALTER TABLE godeepmobile.go_conference ADD CONSTRAINT fk_go_conference_go_organization_type FOREIGN KEY ( go_organization_type_id ) REFERENCES godeepmobile.go_organization_type( id );

ALTER TABLE godeepmobile.go_game_event ADD CONSTRAINT fk_go_event_home_org FOREIGN KEY ( home_team_id ) REFERENCES godeepmobile.go_team( id );

ALTER TABLE godeepmobile.go_game_event ADD CONSTRAINT fk_go_event_away_org FOREIGN KEY ( away_team_id ) REFERENCES godeepmobile.go_team( id );

ALTER TABLE godeepmobile.go_organization ADD CONSTRAINT fk_go_organization_go_conference_id FOREIGN KEY ( go_conference_id ) REFERENCES godeepmobile.go_conference( id );

ALTER TABLE godeepmobile.go_organization ADD CONSTRAINT fk_go_organization_go_organization_type FOREIGN KEY ( go_organization_type_id ) REFERENCES godeepmobile.go_organization_type( id );

ALTER TABLE godeepmobile.go_position_type ADD CONSTRAINT fk_go_position_type FOREIGN KEY ( go_sport_type_id ) REFERENCES godeepmobile.go_sport_type( id );

ALTER TABLE godeepmobile.go_position_type ADD CONSTRAINT fk_go_position_type_go_platoon_type FOREIGN KEY ( go_platoon_type_id ) REFERENCES godeepmobile.go_platoon_type( id );

ALTER TABLE godeepmobile.go_role_permission ADD CONSTRAINT fk_go_role_permissions_go_user_role_type FOREIGN KEY ( go_user_role_type_id ) REFERENCES godeepmobile.go_user_role_type( id );

ALTER TABLE godeepmobile.go_role_permission ADD CONSTRAINT fk_go_role_permission_go_permission FOREIGN KEY ( go_permission_id ) REFERENCES godeepmobile.go_permission( id );

ALTER TABLE godeepmobile.go_team ADD CONSTRAINT fk_organization_go_sport FOREIGN KEY ( go_sport_type_id ) REFERENCES godeepmobile.go_sport_type( id ) ON DELETE SET NULL;

ALTER TABLE godeepmobile.go_team ADD CONSTRAINT fk_go_team_go_organization FOREIGN KEY ( go_organization_id ) REFERENCES godeepmobile.go_organization( id );

ALTER TABLE godeepmobile.go_team ADD CONSTRAINT fk_go_team_addr_go_state FOREIGN KEY ( addr_state_id ) REFERENCES godeepmobile.go_state( id );

ALTER TABLE godeepmobile.go_team ADD CONSTRAINT fk_go_team_go_surface_type FOREIGN KEY ( go_surface_type_id ) REFERENCES godeepmobile.go_surface_type( id );

ALTER TABLE godeepmobile.go_team ADD CONSTRAINT fk_go_team_go_conference FOREIGN KEY ( go_conference_id ) REFERENCES godeepmobile.go_conference( id );

ALTER TABLE godeepmobile.go_user_email ADD CONSTRAINT fk_go_user_email FOREIGN KEY ( go_user_id ) REFERENCES godeepmobile.go_user( id );

ALTER TABLE godeepmobile.go_user_team_assignment ADD CONSTRAINT fk_go_user_team_assignment FOREIGN KEY ( go_user_id ) REFERENCES godeepmobile.go_user( id );

ALTER TABLE godeepmobile.go_user_team_assignment ADD CONSTRAINT fk_go_user_team_assignment_go_team FOREIGN KEY ( go_team_id ) REFERENCES godeepmobile.go_team( id );

ALTER TABLE godeepmobile.go_user_team_assignment ADD CONSTRAINT fk_go_user_team_assignment_0 FOREIGN KEY ( go_user_role_type_id ) REFERENCES godeepmobile.go_user_role_type( id );

ALTER TABLE godeepmobile.team_configuration ADD CONSTRAINT fk_team_configuration_go_team FOREIGN KEY ( go_team_id ) REFERENCES godeepmobile.go_team( id );

ALTER TABLE godeepmobile.team_event ADD CONSTRAINT fk_team_event_event_type FOREIGN KEY ( go_event_type_id ) REFERENCES godeepmobile.go_event_type( id );

ALTER TABLE godeepmobile.team_event ADD CONSTRAINT fk_team_event FOREIGN KEY ( go_game_event_id ) REFERENCES godeepmobile.go_game_event( id );

ALTER TABLE godeepmobile.team_event ADD CONSTRAINT fk_team_event_0 FOREIGN KEY ( go_field_condition_id ) REFERENCES godeepmobile.go_field_condition( id );

ALTER TABLE godeepmobile.team_event ADD CONSTRAINT fk_team_event_1 FOREIGN KEY ( go_surface_type_id ) REFERENCES godeepmobile.go_surface_type( id );

ALTER TABLE godeepmobile.team_event ADD CONSTRAINT fk_team_event_go_team FOREIGN KEY ( go_team_id ) REFERENCES godeepmobile.go_team( id );

ALTER TABLE godeepmobile.team_event_grade_status ADD CONSTRAINT fk_team_event_grade_status_go_team FOREIGN KEY ( go_team_id ) REFERENCES godeepmobile.go_team( id );

ALTER TABLE godeepmobile.team_event_grade_status ADD CONSTRAINT fk_team_event_grade_status_go_user FOREIGN KEY ( go_user_id ) REFERENCES godeepmobile.go_user( id );

ALTER TABLE godeepmobile.team_event_grade_status ADD CONSTRAINT fk_team_event_grade_status_team_event FOREIGN KEY ( team_event_id ) REFERENCES godeepmobile.team_event( id );

ALTER TABLE godeepmobile.team_play_data ADD CONSTRAINT fk_team_event_game_play_data_0 FOREIGN KEY ( team_event_id ) REFERENCES godeepmobile.team_event( id );

ALTER TABLE godeepmobile.team_play_data ADD CONSTRAINT fk_team_play_data FOREIGN KEY ( team_practice_drill_type_id ) REFERENCES godeepmobile.team_practice_drill_type( id );

ALTER TABLE godeepmobile.team_play_data ADD CONSTRAINT fk_team_play_data_play_result_type FOREIGN KEY ( go_play_result_type_id ) REFERENCES godeepmobile.go_play_result_type( id );

ALTER TABLE godeepmobile.team_play_data ADD CONSTRAINT fk_team_play_data_go_team FOREIGN KEY ( go_team_id ) REFERENCES godeepmobile.go_team( id );

ALTER TABLE godeepmobile.team_play_data ADD CONSTRAINT fk_team_play_data_play_type FOREIGN KEY ( go_play_type_id ) REFERENCES godeepmobile.go_play_type( id );

ALTER TABLE godeepmobile.team_play_grade ADD CONSTRAINT fk_team_play_grade FOREIGN KEY ( team_play_data_id ) REFERENCES godeepmobile.team_play_data( id );

ALTER TABLE godeepmobile.team_play_grade ADD CONSTRAINT fk_team_play_grade_go_team FOREIGN KEY ( go_team_id ) REFERENCES godeepmobile.go_team( id );

ALTER TABLE godeepmobile.team_play_grade ADD CONSTRAINT fk_team_play_grade_team_player FOREIGN KEY ( team_player_id ) REFERENCES godeepmobile.team_player( id );

ALTER TABLE godeepmobile.team_play_grade ADD CONSTRAINT fk_team_play_grade_position_played_team_position_type FOREIGN KEY ( position_played ) REFERENCES godeepmobile.team_position_type( id );

ALTER TABLE godeepmobile.team_play_grade ADD CONSTRAINT fk_team_play_grade_go_user FOREIGN KEY ( go_user_id ) REFERENCES godeepmobile.go_user( id );

ALTER TABLE godeepmobile.team_play_grade ADD CONSTRAINT fk_team_play_grade_team_position_group FOREIGN KEY ( team_position_group_id ) REFERENCES godeepmobile.team_position_group( id );

ALTER TABLE godeepmobile.team_play_grade ADD CONSTRAINT fk_team_play_grade_go_platoon_type FOREIGN KEY ( go_platoon_type_id ) REFERENCES godeepmobile.go_platoon_type( id );

ALTER TABLE godeepmobile.team_player ADD CONSTRAINT fk_team_player_go_team FOREIGN KEY ( go_team_id ) REFERENCES godeepmobile.go_team( id );

ALTER TABLE godeepmobile.team_player ADD CONSTRAINT fk_team_player_go_body_type FOREIGN KEY ( go_body_type_id ) REFERENCES godeepmobile.go_body_type( id );

ALTER TABLE godeepmobile.team_player ADD CONSTRAINT fk_team_player_go_state FOREIGN KEY ( go_state_id ) REFERENCES godeepmobile.go_state( id );

ALTER TABLE godeepmobile.team_player_career_stat ADD CONSTRAINT fk_team_player_career_stat_team_player FOREIGN KEY ( team_player_id ) REFERENCES godeepmobile.team_player( id );

ALTER TABLE godeepmobile.team_player_career_stat ADD CONSTRAINT fk_team_player_career_stat_go_team FOREIGN KEY ( go_team_id ) REFERENCES godeepmobile.go_team( id );

ALTER TABLE godeepmobile.team_player_game_stat ADD CONSTRAINT fk_team_player_game_stat_team_player FOREIGN KEY ( team_player_id ) REFERENCES godeepmobile.team_player( id );

ALTER TABLE godeepmobile.team_player_game_stat ADD CONSTRAINT fk_team_player_game_stat_team_event FOREIGN KEY ( team_event_id ) REFERENCES godeepmobile.team_event( id );

ALTER TABLE godeepmobile.team_player_game_stat ADD CONSTRAINT fk_team_player_game_stat_go_team FOREIGN KEY ( go_team_id ) REFERENCES godeepmobile.go_team( id );

ALTER TABLE godeepmobile.team_player_season_stat ADD CONSTRAINT fk_team_player_season_stat_team_player FOREIGN KEY ( team_player_id ) REFERENCES godeepmobile.team_player( id );

ALTER TABLE godeepmobile.team_player_season_stat ADD CONSTRAINT fk_team_player_season_stat_go_team FOREIGN KEY ( go_team_id ) REFERENCES godeepmobile.go_team( id );

ALTER TABLE godeepmobile.team_position_group ADD CONSTRAINT fk_team_position_group_go_team FOREIGN KEY ( go_team_id ) REFERENCES godeepmobile.go_team( id );

ALTER TABLE godeepmobile.team_position_group ADD CONSTRAINT fk_team_position_group_go_platoon_type FOREIGN KEY ( go_platoon_type_id ) REFERENCES godeepmobile.go_platoon_type( id );

ALTER TABLE godeepmobile.team_position_type ADD CONSTRAINT fk_team_position_type FOREIGN KEY ( go_position_type_id ) REFERENCES godeepmobile.go_position_type( id );

ALTER TABLE godeepmobile.team_position_type ADD CONSTRAINT fk_team_position_type_0 FOREIGN KEY ( team_position_group_id ) REFERENCES godeepmobile.team_position_group( id );

ALTER TABLE godeepmobile.team_position_type ADD CONSTRAINT fk_team_position_type_go_team FOREIGN KEY ( go_team_id ) REFERENCES godeepmobile.go_team( id );

ALTER TABLE godeepmobile.team_position_type ADD CONSTRAINT fk_team_position_type_go_platoon_type FOREIGN KEY ( go_platoon_type_id ) REFERENCES godeepmobile.go_platoon_type( id );

ALTER TABLE godeepmobile.team_practice_drill_type ADD CONSTRAINT fk_team_practice_type FOREIGN KEY ( team_position_group_id ) REFERENCES godeepmobile.team_position_group( id );

ALTER TABLE godeepmobile.team_practice_drill_type ADD CONSTRAINT fk_team_practice_drill_type_go_team FOREIGN KEY ( go_team_id ) REFERENCES godeepmobile.go_team( id );

ALTER TABLE godeepmobile.team_user_position_group_assignment ADD CONSTRAINT fk_team_user_position_group_assignment_position_group FOREIGN KEY ( team_position_group_id ) REFERENCES godeepmobile.team_position_group( id );

ALTER TABLE godeepmobile.team_user_position_group_assignment ADD CONSTRAINT fk_team_user_position_group_assignment FOREIGN KEY ( go_user_id ) REFERENCES godeepmobile.go_user( id );

ALTER TABLE godeepmobile.team_user_position_group_assignment ADD CONSTRAINT fk_team_user_position_group_assignment_go_team FOREIGN KEY ( go_team_id ) REFERENCES godeepmobile.go_team( id );


CREATE OR REPLACE FUNCTION godeepmobile.get_player_event_grade_stats(player_id uuid, event_id integer)
 RETURNS record
 LANGUAGE sql
AS $function$
SELECT
	*
FROM
	godeepmobile.view_team_player_event_grades
WHERE
	"teamPlayer" = player_id AND
	"teamEvent" = event_id
$function$;

CREATE OR REPLACE FUNCTION godeepmobile.refresh_event_stats(event_id_parm integer)
 RETURNS integer AS
$BODY$
DECLARE
    mviews RECORD;
    stat godeepmobile.team_player_game_stat%ROWTYPE;
BEGIN
    --make sure that game stats exist for each player graded during this game
    perform godeepmobile.create_game_stats_for_event(event_id_parm);
    FOR mviews IN SELECT * FROM godeepmobile.view_team_player_event_grades where "teamEvent" = event_id_parm LOOP
        --RAISE NOTICE 'mviews is %', mviews::text;
        --RAISE NOTICE 'updating game stats for team player %', quote_ident(mviews."teamPlayer"::text);
        --select * into stat from godeepmobile.team_player_game_stat where team_player_id = mviews."teamPlayer" and team_event_id = event_id_parm;
        --RAISE NOTICE 'stat is %', stat::text;
        with stat_id as (select id from godeepmobile.team_player_game_stat where team_player_id = mviews."teamPlayer" and team_event_id = event_id_parm)
        update
            godeepmobile.team_player_game_stat
        SET
            num_plays = mviews."numPlays",
            cat1_grade = mviews."avgCat1Grade",
            cat2_grade = mviews."avgCat2Grade",
            cat3_grade = mviews."avgCat3Grade",
            overall_grade = mviews."avgOverallGrade",
            last_update = now()
        WHERE
            id = (select id from stat_id);
    END LOOP;
    RETURN 1;
END;
$BODY$
LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION godeepmobile.create_game_stats_for_event(event_id_parm integer)
  RETURNS integer AS
$BODY$
DECLARE
	mviews RECORD;
	stat_id integer;
BEGIN
	FOR mviews IN SELECT * FROM godeepmobile.view_game_stat_ids_for_players_with_event_plays where team_event_id = event_id_parm AND team_player_game_stat_id IS NULL LOOP
		--RAISE NOTICE 'creating game stats for team player %', quote_ident(mviews.team_player_id::text);
		insert into
			godeepmobile.team_player_game_stat (
			  go_team_id,
			  team_player_id,
			  season,
			  team_event_id,
			  date,
			  last_update
		  )
		values (
			mviews.go_team_id,
			mviews.team_player_id,
			mviews.season,
			event_id_parm,
			mviews.date,
			now()
		)
		;
	END LOOP;
	RETURN 1;
END;
$BODY$
LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION godeepmobile.create_empty_plays_for_event(
    how_many_parm integer,
    team_event_id_parm integer,
    go_team_id_parm text)
   RETURNS VOID AS
$BODY$
DECLARE
	max_play_number integer;
	exists integer;
BEGIN
	if how_many_parm <= 0 then
		RAISE EXCEPTION 'How many is not positive non-zero number %', how_many_parm USING HINT = 'Please check your how many parameter';
	end if;

	select count(event.id) into exists from godeepmobile.team_event event where event.go_team_id = go_team_id_parm::uuid and event.id = team_event_id_parm;
	if exists != 1 then
		RAISE EXCEPTION 'Team event % does not exist for team %', team_event_id_parm, go_team_id_parm USING HINT = 'Please check your event and team parameters';
	end if;

	select max(play.play_number) into max_play_number from godeepmobile.team_play_data play where play.go_team_id = go_team_id_parm::uuid and play.team_event_id = team_event_id_parm;
	if (max_play_number IS NULL) then
		max_play_number := 0;
	end if;
	FOR i IN (max_play_number + 1)..(max_play_number + how_many_parm) LOOP
		INSERT INTO godeepmobile.team_play_data (play_number, team_event_id, go_team_id ) VALUES (i, team_event_id_parm,go_team_id_parm::uuid );
	END LOOP;
END;
$BODY$
  LANGUAGE plpgsql;



CREATE OR REPLACE FUNCTION create_season_stats_for_team(
    team_id_parm uuid,
    season_parm integer)
  RETURNS void AS
$BODY$
DECLARE
	mviews RECORD;
	stat_id integer;
BEGIN
	FOR mviews IN SELECT * FROM godeepmobile.view_season_stat_ids_for_players where go_team_id = team_id_parm and season = season_parm AND team_player_season_stat_id IS NULL LOOP
		RAISE NOTICE 'creating season stats for team player %', quote_ident(mviews.team_player_id::text);
		insert into
			godeepmobile.team_player_season_stat (
			  go_team_id,
			  team_player_id,
			  season,
			  is_veteran
		  )
		values (
			mviews.go_team_id,
			mviews.team_player_id,
			mviews.season,
			mviews.is_veteran
		)
		;
	END LOOP;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;


CREATE OR REPLACE FUNCTION refresh_season_stats(
    team_id_parm uuid,
    season_parm integer)
  RETURNS void AS
$BODY$
DECLARE
    game_scores RECORD;
BEGIN
	-- ensure that there is a season_stats record for every active player on the team for the season
	perform godeepmobile.create_season_stats_for_team(team_id_parm, season_parm);

	-- aggregate game stats for the season for each active player
	FOR game_scores IN (
		SELECT
			team
			,"teamPlayer"
			,season
			,count("teamEvent")     as "numGames"
			,sum("numPlays")        as "numPlays"
			,sum("sumCat1Grade")    as "sumCat1Grade"
			,sum("sumCat2Grade")    as "sumCat2Grade"
			,sum("sumCat3Grade")    as "sumCat3Grade"
			,sum("sumOverallGrade") as "sumOverallGrade"
			,(sum("sumCat1Grade")::numeric / sum("numPlays"))::integer        as "avgCat1Grade"
			,(sum("sumCat2Grade")::numeric / sum("numPlays"))::integer        as "avgCat2Grade"
			,(sum("sumCat3Grade")::numeric / sum("numPlays"))::integer        as "avgCat3Grade"
			,(sum("sumOverallGrade")::numeric / (3*sum("numPlays")))::integer as "avgOverallGrade"
		FROM
			godeepmobile.view_team_player_event_grades
		WHERE
			team = team_id_parm and season = season_parm
		GROUP BY
			team, "teamPlayer", season
		ORDER BY
			"teamPlayer"
	)
	LOOP
        --RAISE NOTICE 'game_scores is %', game_scores::text;
        --RAISE NOTICE 'updating season stats for team player %', quote_ident(game_scores."teamPlayer"::text);
        --select * into stat from godeepmobile.team_player_game_stat where team_player_id = mviews."teamPlayer" and team_event_id = event_id_parm;
        --RAISE NOTICE 'stat is %', stat::text;
        with
		season_stat_id as (select id from godeepmobile.team_player_season_stat where team_player_id = game_scores."teamPlayer" and game_scores.season = season_parm),
		last_game as (select * from godeepmobile.view_team_player_event_grades where "teamPlayer" = game_scores."teamPlayer" and game_scores.season = season_parm ORDER BY date DESC LIMIT 1)
        update
		godeepmobile.team_player_season_stat
        SET
		num_games                = game_scores."numGames"
		,num_plays_game          = game_scores."numPlays"
		,cat1_grade_game         = game_scores."avgCat1Grade"
		,cat2_grade_game         = game_scores."avgCat2Grade"
		,cat3_grade_game         = game_scores."avgCat3Grade"
		,overall_grade_game      = game_scores."avgOverallGrade"
--need to update this when we have practice data also
		,num_plays_allevent      = game_scores."numPlays"
		,cat1_grade_allevent     = game_scores."avgCat1Grade"
		,cat2_grade_allevent     = game_scores."avgCat2Grade"
		,cat3_grade_allevent     = game_scores."avgCat3Grade"
		,overall_grade_allevent  = game_scores."avgOverallGrade"
		,num_plays_last_game     = last_game."numPlays"
		,cat1_grade_last_game    = last_game."avgCat1Grade"
		,cat2_grade_last_game    = last_game."avgCat2Grade"
		,cat3_grade_last_game    = last_game."avgCat3Grade"
		,overall_grade_last_game = last_game."avgOverallGrade"
	FROM
		last_game
        WHERE
		godeepmobile.team_player_season_stat.id = (select id from season_stat_id);
    END LOOP;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;


CREATE OR REPLACE VIEW view_season_stat_ids_for_players AS
 SELECT player.go_team_id,
    player.id AS team_player_id,
    stats.id AS team_player_season_stat_id,
    player.season,
    player.is_veteran
   FROM view_team_player player
     LEFT JOIN team_player_season_stat stats ON stats.team_player_id = player.id
  ORDER BY player.id;
