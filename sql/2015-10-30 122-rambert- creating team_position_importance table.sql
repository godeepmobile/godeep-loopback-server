--liquibase formatted sql

--changeset rambert:122 runOnChange:true stripComments:false splitStatements:false
--comment creat team_position_importance table

CREATE TABLE godeepmobile.team_position_importance (
  id                   serial  NOT NULL,
  go_team_id           uuid NOT NULL,
  go_position_type_id  integer NOT NULL,
  go_importance_id     integer DEFAULT 3,
  CONSTRAINT pk_team_position_importance PRIMARY KEY ( id, go_team_id, go_position_type_id ),
  CONSTRAINT fk_team_position_importance_go_team FOREIGN KEY ( go_team_id )
      REFERENCES go_team (id),
  CONSTRAINT fk_team_position_importance_go_position_type FOREIGN KEY ( go_position_type_id )
      REFERENCES go_position_type (id)
);

-- populating team_position_importance table
INSERT INTO godeepmobile.team_position_importance
             (go_team_id, go_position_type_id)
   SELECT team.id, position.id
   FROM godeepmobile.go_position_type position, go_team team
   ORDER BY team.id, position.id;

--rollback DROP TABLE IF EXISTS  godeepmobile.team_position_importance;
