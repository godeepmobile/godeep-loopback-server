﻿--liquibase formatted sql

--changeset carlos:77 runOnChange:true
--comment added view_team_play_grade

CREATE OR REPLACE VIEW view_team_play_grade AS
	SELECT * FROM team_play_grade WHERE team_player_id IN (SELECT id FROM team_player where end_date IS NULL);

--rollback DROP VIEW IF EXISTS view_team_play_grade;