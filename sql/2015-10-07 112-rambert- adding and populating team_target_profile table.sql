--liquibase formatted sql

--changeset rambert:112 runOnChange:true stripComments:false splitStatements:false
--comment create team_target_profile table

CREATE TABLE godeepmobile.team_target_profile (
  id                                serial  NOT NULL,
  go_team_id                        uuid NOT NULL,
  go_position_type_id               integer NOT NULL,
  team_scouting_eval_criteria_id    integer NOT NULL,
  go_scouting_eval_criteria_type_id integer NOT NULL,
  calculated_target_score           numeric(10, 2) CHECK(calculated_target_score >= 1 AND calculated_target_score <= 9),
  UNIQUE (go_team_id, team_scouting_eval_criteria_id, go_position_type_id),
  CONSTRAINT pk_team_target_profile PRIMARY KEY ( id ),
  CONSTRAINT fk_team_target_profile_go_team FOREIGN KEY (go_team_id)
      REFERENCES go_team (id),
  CONSTRAINT fk_team_target_profile_go_position_type FOREIGN KEY (go_position_type_id)
      REFERENCES go_position_type (id),
  CONSTRAINT fk_team_target_profile_team_scouting_eval_criteria FOREIGN KEY (team_scouting_eval_criteria_id)
      REFERENCES team_scouting_eval_criteria (id),
  CONSTRAINT fk_team_target_profile_go_scouting_eval_criteria_type FOREIGN KEY (go_scouting_eval_criteria_type_id)
          REFERENCES go_scouting_eval_criteria_type (id)
);

INSERT INTO godeepmobile.team_target_profile
             (go_team_id, go_position_type_id, team_scouting_eval_criteria_id, go_scouting_eval_criteria_type_id, calculated_target_score)
  SELECT  team_eval_criteria.go_team_id, team_eval_criteria.go_position_type_id, team_eval_criteria.id, team_eval_criteria.go_scouting_eval_criteria_type_id, 4 as calculated_target_score
  FROM team_scouting_eval_criteria team_eval_criteria
  ORDER BY team_eval_criteria.go_team_id, team_eval_criteria.go_position_type_id, team_eval_criteria.go_scouting_eval_criteria_type_id, team_eval_criteria.id;

--rollback DROP TABLE IF EXISTS  godeepmobile.team_target_profile;
