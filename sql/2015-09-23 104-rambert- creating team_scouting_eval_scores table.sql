--liquibase formatted sql

--changeset rambert:104 runOnChange:true stripComments:false splitStatements:false
--comment create team_scouting_eval_scores table

CREATE TABLE godeepmobile.team_scouting_eval_score (
  id                                serial  NOT NULL,
  go_team_id                        uuid NOT NULL,
  team_player_id                    uuid NOT NULL,
  go_user_id                        integer NOT NULL,
  team_scouting_eval_criteria_id    integer NOT NULL,
  name                              varchar(100)  NOT NULL,
  team_position_type_id             integer NOT NULL,
  score                             smallint NOT NULL,
  comment                           varchar(100),
  CONSTRAINT pk_team_scouting_eval_score PRIMARY KEY ( id ),
  CONSTRAINT fk_team_scouting_eval_score_go_team FOREIGN KEY (go_team_id)
      REFERENCES go_team (id),
  CONSTRAINT fk_team_scouting_eval_score_team_player FOREIGN KEY (team_player_id)
      REFERENCES team_player (id),
  CONSTRAINT fk_team_scouting_eval_score_team_scouting_eval_criteria FOREIGN KEY (team_scouting_eval_criteria_id)
      REFERENCES team_scouting_eval_criteria (id),
  CONSTRAINT fk_team_scouting_eval_score_team_go_user FOREIGN KEY (go_user_id)
      REFERENCES go_user (id),
  CONSTRAINT fk_team_scouting_eval_score_team_position_type FOREIGN KEY (team_position_type_id)
      REFERENCES team_position_type (id)
);

--rollback DROP TABLE IF EXISTS  godeepmobile.team_scouting_eval_score;
