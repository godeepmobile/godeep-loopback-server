--liquibase formatted sql

--changeset rambert:105 runOnChange:true stripComments:false splitStatements:false
--comment add prospect role and permission

INSERT INTO godeepmobile.go_user_role_type( id, name, description )
                                   VALUES ( 8, 'Prospect', 'team prospect' );
INSERT INTO godeepmobile.go_permission( id, description )
                               VALUES ( 'PROSPECT', 'Prospect functions' );

--rollback DELETE FROM godeepmobile.go_user_role_type WHERE id = 8; DELETE FROM godeepmobile.go_permission WHERE id = 'PROSPECT';
