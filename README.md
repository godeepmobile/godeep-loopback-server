# Godeep Mobile - LoopBack framework PoC

This project is a "Proof of Concepts" app to check the [LoopBack](http://loopback.io) framework as the API generator for the GoDeep mobile app.

## Prerequisites

You will need the following things properly installed on your computer.

* [Git](http://git-scm.com/)
* [Node.js](http://nodejs.org/) (with NPM)
* [LoopBack](http://loopback.io)

## Installation

* `git clone <repository-url>` this repository
* change into the new directory

## Running / Development

* `slc run`
* Visit your app at [http://localhost:3000](http://localhost:3000).

## Further Reading / Useful Links

* [node-inspector](https://github.com/node-inspector/node-inspector)