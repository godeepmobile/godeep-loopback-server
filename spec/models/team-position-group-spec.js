var frisby    = require('frisby')
  , host      = process.env.host || 'localhost'
  , port      = process.env.port || '3000'
  , _username = process.env.user || 'admin'
  , _password = process.env.pass || 'admin'
  , endpoint  = 'teamPositionGroups'
  , validId = -1
  , teamId = -1
  , accessToken = -1
  ;

frisby.create('Login').
  post('http://' + host + ':' + port + '/api/gousers/login', {username : _username, password : _password})
  .afterJSON(function(result) {
    accessToken = result.id;
    teamId = result.teamId;

    frisby.create('TeamPositionGroups API should get a valid json response with a 200 code')
      .get('http://' + host + ':' + port + '/api/' + endpoint + '?access_token=' + accessToken)
      .expectHeaderContains('content-type', 'application/json')
      .expectStatus(200)
      .toss();

    frisby.create('TeamPositionGroups API should return the pagination info with default values when none provided')
      .get('http://' + host + ':' + port + '/api/' + endpoint + '?access_token=' + accessToken)
      .expectStatus(200)
      .expectJSON({
        meta : {
          offset : 0
          , limit  : 30
        }
      })
      .toss();

    frisby.create('TeamPositionGroups API should return the pagination info with the provided values')
      .get('http://' + host + ':' + port + '/api/' + endpoint + '?access_token=' + accessToken + '&limit=10&offset=0')
      .expectStatus(200)
      .expectJSON({
        meta : {
          offset : 0
          , limit  : 10
        }
      })
      .toss();

    frisby.create('TeamPositionGroups API should rise an error when malformed parameters sent')
      .post('http://' + host + ':' + port + '/api/' + endpoint + '?access_token=' + accessToken, {test_parameter : 'test_value'})
      .expectStatus(422)
      .toss();

    frisby.create('TeamPositionGroups API should rise an forbidden error when the access token hasn\'t the right permissions')
      .get('http://' + host + ':' + port + '/api/' + endpoint + '?access_token=CtVkdPE900DEAvaFlHhq50nW18z6SbKSuF48QwvUSjHAxFH9PWUW5PszuexYlshg')
      .expectStatus(403)
      .toss();

    frisby.create('TeamPositionGroups API should rise an unathorized error when no access token is provided')
      .get('http://' + host + ':' + port + '/api/' + endpoint + '')
      .expectStatus(401)
      .toss();

    frisby.create('TeamPositionGroups API should return the records wrapped on a root node called TeamPositionGroups')
      .get('http://' + host + ':' + port + '/api/' + endpoint + '?access_token=' + accessToken)
      .expectStatus(200)
      .expectBodyContains('TeamPositionGroups')
      .toss();

    frisby.create('TeamPositionGroups API all records returned should accomplish the provided criteria')
      .get('http://' + host + ':' + port + '/api/' + endpoint + '?filter[where][name][like]=%Backs%&access_token=' + accessToken)
      .expectStatus(200)
      .expectJSON({
        TeamPositionGroups : function(records) {
          var allAccomplish = true;
          records.forEach(function(record) {
            if(!~record.name.indexOf('Backs')) {
              allAccomplish = false;
            }
          });
          expect(allAccomplish).toBeTruthy();
        }
      })
      .toss();

    // need to test team filtering here
    frisby.create('TeamPositionGroups API, only records from same team as user should be returned')
      .get('http://' + host + ':' + port + '/api/' + endpoint + '?access_token=' + accessToken)
      .expectStatus(200)
      .expectJSON({
        TeamPositionGroups : function(records) {
          var allAccomplish = true;
          records.forEach(function(record) {
            if(record.team != teamId) {
              console.log('record team id is %d, expected %d', record.team, teamId);
              allAccomplish = false;
            }
          });
          expect(allAccomplish).toBeTruthy();
        }
      })
      .toss();

    frisby.create('TeamPositionGroups API, Team Filtering')
      .get('http://' + host + ':' + port + '/api/' + endpoint + '/findOne?access_token=' + accessToken)
      .expectStatus(200)
      .afterJSON(function(result) {
        validId = result.id;

        //TODO update with current model definition
        frisby.create('TeamPositionGroups API, Models should return fields of correct type')
          .get('http://' + host + ':' + port + '/api/' + endpoint + '/' + validId + '?access_token=' + accessToken)
          .expectStatus(200)
          .expectJSONTypes('TeamPositionGroup', {
            name: String,
            team: String,
            id: Number
          })
          .toss();

        frisby.create('TeamPositionGroups API should return one record when an id is provided')
          .get('http://' + host + ':' + port + '/api/' + endpoint + '/' + validId + '?access_token=' + accessToken)
          .expectStatus(200)
          .expectJSONLength(1)
          .toss();

        frisby.create('TeamPositionGroups API should return one record wrapped on a root node called with the model\'s name when an Id is provided')
          .get('http://' + host + ':' + port + '/api/' + endpoint + '/' + validId + '?access_token=' + accessToken)
          .expectStatus(200)
          .expectBodyContains('TeamPositionGroup')
          .toss();

      }).toss();


    frisby.create('TeamPositionGroups API should rise a not found error when requesting none existing records')
      .get('http://' + host + ':' + port + '/api/' + endpoint + '/99999999999?access_token=' + accessToken)
      .expectStatus(404)
      .toss();
  }).toss();

frisby.create('Logout').post('http://' + host + ':' + port + '/api/gousers/logout?access_token=' + accessToken).toss();
