var frisby = require('frisby'),
  host = process.env.host || 'localhost',
  port = process.env.port || '3000',
  _username = process.env.user || 'admin',
  _password = process.env.pass || 'admin',
  endpoint = 'http://' + host + ':' + port + '/api/GoUsers',
  validId = -1,
  teamId = -1,
  accessToken = -1;

function dump500(result) {
  if (result && result.error && result.error.status === 500) {
    if (result.error && result.error.stack) {
      console.log('\n500 status: ',result.error.stack);
    } else if (result.error)
      console.log('\n500 status: ',result.error);
    else
      console.log('\n500 status: ',result);
  }
}

frisby.globalSetup({
  timeout: 10000
});

frisby.create('Login')
  .post('http://' + host + ':' + port + '/api/gousers/login', {
    username: _username,
    password: _password
  })
  .afterJSON(function (result) {
    accessToken = result.id;
    teamId = result.teamId; // remember team id for current user for team-filtering tests

    frisby.create('GoUsers API should get a valid json response with a 200 code')
      .get(endpoint + '?access_token=' + accessToken)
      .expectHeaderContains('content-type', 'application/json')
      .expectStatus(200)
      .toss();

    frisby.create('GoUsers API should raise an error when malformed parameters sent')
      .post(endpoint + '?access_token=' + accessToken, {
        test_parameter: 'test_value'
      })
      .expectStatus(422)
      .toss();

    frisby.create('GoUsers API should raise an forbidden error when the access token hasn\'t the right permissions')
      .get(endpoint + '?access_token=CtVkdPE900DEAvaFlHhq50nW18z6SbKSuF48QwvUSjHAxFH9PWUW5PszuexYlshg')
      .expectStatus(403)
      .toss();

    frisby.create('GoUsers API should raise an unauthorized error when no access token is provided')
      .get(endpoint + '')
      .expectStatus(401)
      .toss();

    frisby.create('GoUsers API should raise a not found error when requesting none existing records')
      .get(endpoint + '/-1?access_token=' + accessToken)
      .expectStatus(404)
      .afterJSON(function (result) { dump500(result); })
      .toss();

    // these test require a valid record id, get that first
    frisby.create('GoUsers API, valid ID')
      .get(endpoint + '/findOne?access_token=' + accessToken)
      .expectStatus(200)
      .afterJSON(function (result) {
        var validId = result.id;
        var username = result.username;

        frisby.create('GoUsers API all records returned should match the provided filter criteria')
          .get(endpoint + '?filter[where][username]=' + username + '&access_token=' + accessToken)
          .expectStatus(200)
          .expectJSON({
            GoUsers: function (records) {
              var allAccomplish = true;
              records.forEach(function (record) {
                if (record.username.indexOf(username) !== 0) {
                  allAccomplish = false;
                }
              });
              expect(allAccomplish).toBeTruthy();
            }
          })
          .toss(); // all records returned should accomplish the provided criteria

        // update to latest model definition
        frisby.create('GoUsers API, Model\'s relations needs to include field list')
          .get(endpoint + '/' + validId + '?access_token=' + accessToken)
          .expectStatus(200)
          .expectJSONTypes('GoUser', {
            id: Number,
            username: String,
            firstTimeLogin: Boolean,
            displayWelcomeDialog: Boolean,
            lastName: function (val) {
              expect(val).toBeTypeOrNull(String);
            },
            firstName: function (val) {
              expect(val).toBeTypeOrNull(String);
            },
            middleName: function (val) {
              expect(val).toBeTypeOrNull(String);
            }
          })
          .toss();

        frisby.create('GoUsers API should return one record when an id is provided')
          .get(endpoint + '/' + validId + '?access_token=' + accessToken)
          .expectStatus(200)
          .expectJSON({
            GoUser: function (record) {
              var allAccomplish = true;
              if (record.id !== validId) {
                allAccomplish = false;
              }
              expect(allAccomplish).toBeTruthy();
            }
          })
          .toss();

      }).toss(); // Team Filtering, get validId

    frisby.create('GoUsers API should return a valid user profile')
      .get(endpoint + '/profiles/1?access_token=' + accessToken)
      .expectStatus(200)
      .expectBodyContains('profile')
      .toss();

    frisby.create('GoUsers API delete a test user account')
      .get(endpoint + '/findOne?filter={"where":{"username":"testuser"}}&access_token=' + accessToken)
      .delete(endpoint + '/' + validId + '?access_token=' + accessToken)
      .expectStatus(204)
      .toss();

    frisby.create('GoUsers API create a new test user account')
      .post(endpoint + '?access_token=' + accessToken, {
        username: '  TestUser',
        password: 'TestUserPassword',
        email: ' TestUser@gmail.com ',
        firstName: 'Test',
        lastName: 'User'
      })
      .expectStatus(200)
      .expectJSON({
        username: 'testuser',
        email: 'testuser@gmail.com'
      })
      .afterJSON(function (result) {
        validId = result.id;
      })
      .after(function() {
        frisby.create('GoUsers API delete test user account after create test')
          .get(endpoint + '/findOne?filter={"where":{"username":"testuser"}}&access_token=' + accessToken)
          .delete(endpoint + '/' + validId + '?access_token=' + accessToken)
          .expectStatus(204)
          .toss();
      })
      .toss();


  }).toss(); //login

frisby.create('Logout').post('http://' + host + ':' + port + '/api/gousers/logout?access_token=' + accessToken).toss();
