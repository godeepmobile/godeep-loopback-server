var frisby    = require('frisby')
  , host      = process.env.host || 'localhost'
  , port      = process.env.port || '3000'
  , _username = process.env.user || 'admin'
  , _password = process.env.pass || 'admin'
  //, endpoint  = 'TeamProspectAssignments'
  , endpoint  = 'http://' + host + ':' + port + '/api/TeamProspectAssignments'
  , validId = -1
  , teamId = -1
  , accessToken = -1
  ;

frisby.create('Login')
  .post('http://' + host + ':' + port + '/api/gousers/login', {username : _username, password : _password})
  .afterJSON(function(result) {
    accessToken = result.id;
    teamId = result.teamId; // remember team id for current user for team-filtering tests

    frisby.create('TeamProspectAssignments API should get a valid json response with a 200 code')
      .get(endpoint + '?access_token=' + accessToken)
      .expectHeaderContains('content-type', 'application/json')
      .expectStatus(200)
      .toss();

    frisby.create('TeamProspectAssignments API should return the pagination info with default values when none provided')
      .get(endpoint + '?access_token=' + accessToken)
      .expectStatus(200)
      .expectJSON({
        meta : {
          offset : 0
          , limit  : 30
        }
      })
      .toss();

    frisby.create('TeamProspectAssignments API should return the pagination info with the provided values')
      .get(endpoint + '?access_token=' + accessToken + '&limit=10&offset=5')
      .expectStatus(200)
      .expectJSON({
        meta : {
          offset : 5
          , limit  : 10
        }
      })
      .toss();

    frisby.create('TeamProspectAssignments API should rise an error when malformed parameters sent')
      .post(endpoint + '?access_token=' + accessToken, {test_parameter : 'test_value'})
      .expectStatus(422)
      .toss();

    //TODO: verify why TeamProspectAssignments don't make the accessToken validation.
    frisby.create('TeamProspectAssignments API should rise an forbidden error when the access token hasn\'t the right permissions')
      .get(endpoint + '?access_token=CtVkdPE900DEAvaFlHhq50nW18z6SbKSuF48QwvUSjHAxFH9PWUW5PszuexYlshg')
      .expectStatus(403)
      .toss();

    //TODO: verify why TeamProspectAssignments don't need accessToken to retrieve them.
    frisby.create('TeamProspectAssignments API should rise an unathorized error when no access token is provided')
      .get(endpoint)
      .expectStatus(401)
      .toss();

    frisby.create('TeamProspectAssignments API should rise a not found error when requesting none existing records')
      .get(endpoint + '/99999999?access_token=' + accessToken)
      .expectStatus(404)
      .toss();

    frisby.create('TeamProspectAssignments API should return the records wrapped on a root node called TeamProspectAssignments')
      .get(endpoint + '?access_token=' + accessToken)
      .expectStatus(200)
      .expectBodyContains('TeamProspectAssignments')
      .toss();

    frisby.create('TeamProspectAssignments API all records returned should accomplish the provided criteria')
      .get(endpoint + '?filter[where][isVeteran]=false&access_token=' + accessToken)
      .expectStatus(200)
      .expectJSON({
        TeamProspectAssignments : function(records) {
          var allAccomplish = true;
          records.forEach(function(record) {
            if(record.isVeteran) {
              allAccomplish = false;
            }
          });
          expect(allAccomplish).toBeTruthy();
        }
      })
      .toss();

    // need to test team filtering here
    frisby.create('TeamProspectAssignments API, only records from same team as user should be returned')
      .get(endpoint + '?access_token=' + accessToken)
      .expectStatus(200)
      .expectJSON({
        TeamProspectAssignments : function(records) {
          var allAccomplish = true;
          records.forEach(function(record) {
            if(record.team != teamId) {
              allAccomplish = false;
            }
          });
          expect(allAccomplish).toBeTruthy();
        }
      })
      .toss();

    // these test require a valid record id, get that first
    frisby.create('TeamProspectAssignments API, Team Filtering get valid ID')
      .get(endpoint + '/findOne?access_token=' + accessToken)
      .expectStatus(200)
      .afterJSON(function(result) {
        validId = result.id;

        frisby.create('TeamProspectAssignments API should return one record when an id is provided')
          .get(endpoint + '/' + validId + '?access_token=' + accessToken)
          .expectStatus(200)
          .expectJSONLength(1)
          .toss();

        frisby.create('TeamProspectAssignments API should return one record wrapped on a root node called with the model\'s name when an Id is provided')
          .get(endpoint + '/' + validId + '?access_token=' + accessToken)
          .expectStatus(200)
          .expectBodyContains('TeamProspectAssignment')
          .toss();

        frisby.create('TeamProspectAssignments API, returned fields need to be of correct type')
          .get(endpoint + '/' + validId + '?access_token=' + accessToken)
          .expectStatus(200)
          .expectJSONTypes('TeamProspectAssignment', {
            id: Number,
            teamProspect: String,
            team: String,
            userTeamAssignment: function(val) { expect(val).toBeTypeOrNull(Number); },
            jerseyNumber: function(val) { expect(val).toBeTypeOrNull(Number); },
            season: function(val) { expect(val).toBeTypeOrNull(Number); },
            projectedPosition: function(val) { expect(val).toBeTypeOrNull(Number); },
            currentPosition: function(val) { expect(val).toBeTypeOrNull(Number); },
            position3: function(val) { expect(val).toBeTypeOrNull(Number); },
            positionST: function(val) { expect(val).toBeTypeOrNull(Number); }
          })
          .toss(); // returned fields need to be of correct type
      }).toss();  // Team Filtering, get validId

      frisby.create('TeamProspectAssignments API should rise an error when malformed parameters sent')
        .post(endpoint + '?access_token=' + accessToken, {test_parameter : 'test_value'})
        .expectStatus(422)
        .toss();
  }).toss(); // login

// logout after login test finishes to avoid race conditions
frisby.create('Logout').post('http://' + host + ':' + port + '/api/gousers/logout?access_token=' + accessToken).toss();
