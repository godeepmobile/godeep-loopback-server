var frisby    = require('frisby')
  , host      = process.env.host || 'localhost'
  , port      = process.env.port || '3000'
  , _username = process.env.user || 'admin'
  , _password = process.env.pass || 'admin'
  , endpoint  = 'states';

frisby.create('Login').post('http://' + host + ':' + port + '/api/gousers/login', {username : _username, password : _password}).afterJSON(function(result) {
	var accessToken = result.id;
	
	frisby.create('States API should get a valid json response with a 200 code')
		.get('http://' + host + ':' + port + '/api/' + endpoint + '?access_token=' + accessToken)
		.expectHeaderContains('content-type', 'application/json')
 		.expectStatus(200)
	.toss();

	frisby.create('States API should return the pagination info with default values when none provided')
		.get('http://' + host + ':' + port + '/api/' + endpoint + '?access_token=' + accessToken)
		.expectJSON({
			meta : {
				offset : 0
			  , limit  : 30
			}
		})
	.toss();

	frisby.create('States API should return the pagination info with the provided values')
		.get('http://' + host + ':' + port + '/api/' + endpoint + '?access_token=' + accessToken + '&limit=10&offset=5')
		.expectJSON({
			meta : {
				offset : 5
			  , limit  : 10
			}
		})
	.toss();

	frisby.create('States API should rise an error when malformed parameters sent')
		.post('http://' + host + ':' + port + '/api/' + endpoint + '?access_token=' + accessToken, {test_parameter : 'test_value'})
		.expectStatus(422)
	.toss();

	frisby.create('States API should rise an forbidden error when the access token hasn\'t the right permissions')
		.get('http://' + host + ':' + port + '/api/' + endpoint + '?access_token=CtVkdPE900DEAvaFlHhq50nW18z6SbKSuF48QwvUSjHAxFH9PWUW5PszuexYlshg')
 		.expectStatus(403)
	.toss();

	frisby.create('States API should rise an unathorized error when no access token is provided')
		.get('http://' + host + ':' + port + '/api/' + endpoint + '')
 		.expectStatus(401)
	.toss();

	frisby.create('States API should rise a not found error when requesting none existing records')
		.get('http://' + host + ':' + port + '/api/' + endpoint + '/99999999999?access_token=' + accessToken)
		.expectStatus(404)
	.toss();

	frisby.create('States API, Model\'s relations needs to include field list')
    	.get('http://' + host + ':' + port + '/api/' + endpoint + '/1?access_token=' + accessToken)
    	.expectJSONTypes('State', {
        	name: String,
        	id: Number
        })
    .toss();

    frisby.create('States API should return the records wrapped on a root node called States')
		.get('http://' + host + ':' + port + '/api/' + endpoint + '?access_token=' + accessToken)
		.expectBodyContains('States')
	.toss();

	frisby.create('States API should return one record when an id is provided')
		.get('http://' + host + ':' + port + '/api/' + endpoint + '/1?access_token=' + accessToken)
		.expectJSONLength(1)
	.toss();

	frisby.create('States API should return one record wrapped on a root node called with the model\'s name when an Id is provided')
		.get('http://' + host + ':' + port + '/api/' + endpoint + '/1?access_token=' + accessToken)
		.expectBodyContains('State')
	.toss();

	frisby.create('States API all records returned should accomplish the provided criteria')
		.get('http://' + host + ':' + port + '/api/' + endpoint + '?filter[where][name][like]=N%&access_token=' + accessToken)
 		.expectJSON({
 			States : function(records) {
 				var allAccomplish = true;
 				records.forEach(function(record) {
 					if(record.name.indexOf('N') !== 0) {
 						allAccomplish = false;
 					}
 				});
 				expect(allAccomplish).toBeTruthy();
 			}
 		})
	.toss();

	frisby.create('Logout').post('http://' + host + ':' + port + '/api/gousers/logout?access_token=' + accessToken).toss();

}).toss();