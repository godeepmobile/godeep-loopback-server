var frisby    = require('frisby')
  , host      = process.env.host || 'localhost'
  , port      = process.env.port || '3000'
  , _username = process.env.user || 'admin'
  , _password = process.env.pass || 'admin'
  //, endpoint  = 'TeamPlayers'
  , endpoint  = 'http://' + host + ':' + port + '/api/TeamPlayers'
  , validId = -1
  , teamId = -1
  , accessToken = -1
  ;

frisby.create('Login')
  .post('http://' + host + ':' + port + '/api/gousers/login', {username : _username, password : _password})
  .afterJSON(function(result) {
    accessToken = result.id;
    teamId = result.teamId; // remember team id for current user for team-filtering tests

    frisby.create('TeamPlayers API should get a valid json response with a 200 code')
      .get(endpoint + '?access_token=' + accessToken)
      .expectHeaderContains('content-type', 'application/json')
      .expectStatus(200)
      .toss();

    frisby.create('TeamPlayers API should return the pagination info with default values when none provided')
      .get(endpoint + '?access_token=' + accessToken)
      .expectStatus(200)
      .expectJSON({
        meta : {
          offset : 0
          , limit  : 30
        }
      })
      .toss();

    frisby.create('TeamPlayers API should return the pagination info with the provided values')
      .get(endpoint + '?access_token=' + accessToken + '&limit=10&offset=5')
      .expectStatus(200)
      .expectJSON({
        meta : {
          offset : 5
          , limit  : 10
        }
      })
      .toss();

    frisby.create('TeamPlayers API should rise an error when malformed parameters sent')
      .post(endpoint + '?access_token=' + accessToken, {test_parameter : 'test_value'})
      .expectStatus(422)
      .toss();

    //TODO: verify why TeamPlayers don't make the accessToken validation.
    frisby.create('TeamPlayers API should rise an forbidden error when the access token hasn\'t the right permissions')
      .get(endpoint + '?access_token=CtVkdPE900DEAvaFlHhq50nW18z6SbKSuF48QwvUSjHAxFH9PWUW5PszuexYlshg')
      .expectStatus(403)
      .toss();

    //TODO: verify why TeamPlayers don't need accessToken to retrieve them.
    frisby.create('TeamPlayers API should rise an unathorized error when no access token is provided')
      .get(endpoint)
      .expectStatus(401)
      .toss();

    frisby.create('TeamPlayers API should rise a not found error when requesting none existing records')
      .get(endpoint + '/99999999-9999-9999-9999-999999999999?access_token=' + accessToken)
      .expectStatus(404)
      .toss();

    frisby.create('TeamPlayers API should return the records wrapped on a root node called TeamPlayers')
      .get(endpoint + '?access_token=' + accessToken)
      .expectStatus(200)
      .expectBodyContains('TeamPlayers')
      .toss();

    frisby.create('TeamPlayers API all records returned should accomplish the provided criteria')
      .get(endpoint + '?filter[where][classYear]=SR&access_token=' + accessToken)
      .expectStatus(200)
      .expectJSON({
        TeamPlayers : function(records) {
          var allAccomplish = true;
          records.forEach(function(record) {
            if(record.classYear !== 'SR') {
              allAccomplish = false;
            }
          });
          expect(allAccomplish).toBeTruthy();
        }
      })
      .toss();

    // need to test team filtering here
    frisby.create('TeamPlayers API, only records from same team as user should be returned')
      .get(endpoint + '?access_token=' + accessToken)
      .expectStatus(200)
      .expectJSON({
        TeamPlayers : function(records) {
          var allAccomplish = true;
          records.forEach(function(record) {
            if(record.team != teamId) {
              allAccomplish = false;
            }
          });
          expect(allAccomplish).toBeTruthy();
        }
      })
      .toss();

    // these test require a valid record id, get that first
    frisby.create('TeamPlayers API, Team Filtering get valid ID')
      .get(endpoint + '/findOne?access_token=' + accessToken)
      .expectStatus(200)
      .afterJSON(function(result) {
        validId = result.id;

        frisby.create('TeamPlayers API should return one record when an id is provided')
          .get(endpoint + '/' + validId + '?access_token=' + accessToken)
          .expectStatus(200)
          .expectJSONLength(1)
          .toss();

        frisby.create('TeamPlayers API should return one record wrapped on a root node called with the model\'s name when an Id is provided')
          .get(endpoint + '/' + validId + '?access_token=' + accessToken)
          .expectStatus(200)
          .expectBodyContains('TeamPlayer')
          .toss();

        frisby.create('TeamPlayers API, returned fields need to be of correct type')
          .get(endpoint + '/' + validId + '?access_token=' + accessToken)
          .expectStatus(200)
          .expectJSONTypes('TeamPlayer', {
            id: String,
            lastName: String,
            middleName: function(val) { expect(val).toBeTypeOrNull(String); },
            firstName: String,
            team: String,
            data: function(val) { expect(val).toBeTypeOrNull(String); },
            userTeamAssignment: function(val) { expect(val).toBeTypeOrNull(Number); },
            classYear: function(val) { expect(val).toBeTypeOrNull(String); },
            birthDate: function(val) { expect(val).toBeTypeOrNull(String); },
            jerseyNumber: function(val) { expect(val).toBeTypeOrNull(Number); },
            season: function(val) { expect(val).toBeTypeOrNull(Number); },
            graduationYear: function(val) { expect(val).toBeTypeOrNull(Number); },
            bodyType: function(val) { expect(val).toBeTypeOrNull(Number); },
            height: function(val) { expect(val).toBeTypeOrNull(String); },
            weight: function(val) { expect(val).toBeTypeOrNull(String); },
            wingspan: function(val) { expect(val).toBeTypeOrNull(String); },
            fortyYard: function(val) { expect(val).toBeTypeOrNull(String); },
            handSize: function(val) { expect(val).toBeTypeOrNull(String); },
            verticalJump: function(val) { expect(val).toBeTypeOrNull(String); },
            email: function(val) { expect(val).toBeTypeOrNull(String); },
            sat: function(val) { expect(val).toBeTypeOrNull(Number); },
            act: function(val) { expect(val).toBeTypeOrNull(Number); },
            gpa: function(val) { expect(val).toBeTypeOrNull(Number); },
            level: function(val) { expect(val).toBeTypeOrNull(Number); },
            position1: function(val) { expect(val).toBeTypeOrNull(Number); },
            position2: function(val) { expect(val).toBeTypeOrNull(Number); },
            position3: function(val) { expect(val).toBeTypeOrNull(Number); },
            positionST: function(val) { expect(val).toBeTypeOrNull(Number); },
            state: function(val) { expect(val).toBeTypeOrNull(Number); },
            highSchool: function(val) { expect(val).toBeTypeOrNull(String); },
            coach: function(val) { expect(val).toBeTypeOrNull(String); },
            coachPhone: function(val) { expect(val).toBeTypeOrNull(String); }
          })
          .toss(); // returned fields need to be of correct type
      }).toss();  // Team Filtering, get validId

      frisby.create('TeamPlayers API should rise an error when malformed parameters sent')
        .post(endpoint + '?access_token=' + accessToken, {test_parameter : 'test_value'})
        .expectStatus(422)
        .toss();

      var firstName    = 'Will'
        , lastName     = 'Smith'
        , jerseyNumber = 10
        , height       = "5'10''"
        , newRecordId  = -1
        ;
      frisby.create('TeamPlayers API create a new record')
        .post(endpoint + '?access_token=' + accessToken,
        {
          firstName   : firstName,
          lastName    : lastName,
          jerseyNumber: jerseyNumber,
          height      : height,
          team        : teamId
        })
        .expectStatus(200)
        .expectJSON(
        {
          firstName   : firstName,
          lastName    : lastName,
          jerseyNumber: jerseyNumber,
          height      : height,
          //team        : teamId
        })
        .afterJSON(function(newRecord) {
          newRecordId = newRecord.id;
          frisby.create('TeamPlayers API gets the new record created')
            .get(endpoint + '/' + newRecordId + '?access_token=' + accessToken)
            .expectStatus(200)
            .expectJSONLength(1)
            .expectJSONTypes('TeamPlayer', {
              id          : String,
              firstName   : String,
              lastName    : String,
              jerseyNumber: Number,
              height      : String,
              team        : String
            })
            .expectJSON(
            {
              TeamPlayer: {
                id          : newRecordId,
                firstName   : firstName,
                lastName    : lastName,
                jerseyNumber: jerseyNumber,
                height      : height,
                team        : teamId
              }
            })
            .toss();

          var firstNameTmp    = firstName + 'tmp'
            , lastNameTmp     = lastName + 'tmp'
            , jerseyNumberTmp = jerseyNumber + 10
            ;
          frisby.create('TeamPlayers API Updates the new record just created')
            .put(endpoint + '?access_token=' + accessToken,
            {
              id          : newRecordId,
              firstName   : firstNameTmp,
              lastName    : lastNameTmp,
              jerseyNumber: jerseyNumberTmp
            })
            .expectStatus(200)
            .expectJSON(
            {
              id          : newRecordId,
              firstName   : firstNameTmp,
              lastName    : lastNameTmp,
              jerseyNumber: jerseyNumberTmp,
              height      : height,
              //team        : teamId
            })
            .toss();

          // Verifies that record was updated successfully
          frisby.create('TeamPlayer API returns the record updated')
            .get(endpoint + '/' + newRecordId + '?access_token=' + accessToken)
            .expectStatus(200)
            .expectJSONLength(1)
            .expectJSON(
            {
              TeamPlayer: {
                id          : newRecordId,
                firstName   : firstNameTmp,
                lastName    : lastNameTmp,
                jerseyNumber: jerseyNumberTmp,
                height      : height,
                team        : teamId
              }
            })
            .toss();

          frisby.create('TeamPlayers API deletes the record')
            .delete(endpoint + '/' + newRecordId + '?access_token=' + accessToken)
            .expectStatus(204)
            .toss();

          frisby.create('TeamPlayers API, Verifies that record was deleted')
            .get(endpoint + '/' + newRecordId + '?access_token=' + accessToken)
            .expectStatus(404)
            .toss();
        }).toss();
  }).toss(); // login

// logout after login test finishes to avoid race conditions
frisby.create('Logout').post('http://' + host + ':' + port + '/api/gousers/logout?access_token=' + accessToken).toss();
