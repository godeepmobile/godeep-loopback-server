var frisby      = require('frisby')
  , host        = process.env.host || 'localhost'
  , port        = process.env.port || '3000'
  , _username   = process.env.user || 'admin'
  , _password   = process.env.pass || 'admin'
  , endpoint    = 'http://' + host + ':' + port + '/api/TeamPlayerGameStats'
  , validId     = -1
  , teamId      = -1
  , accessToken = -1
  ;

frisby.globalSetup({
  timeout: 10000
});

frisby.create('Login')
  .post('http://' + host + ':' + port + '/api/gousers/login', {username : _username, password : _password})
  .afterJSON(function(result) {
    accessToken = result.id;
    teamId = result.teamId; // remember team id for current user for team-filtering tests

    frisby.create('TeamPlayerGameStats API should get a valid json response with a 200 code')
      .get(endpoint + '?access_token=' + accessToken)
      .expectHeaderContains('content-type', 'application/json')
      .expectStatus(200)
      .toss();

    frisby.create('TeamPlayerGameStats API should return the pagination info with default values when none provided')
      .get(endpoint + '?access_token=' + accessToken)
      .expectStatus(200)
      .expectJSON({
        meta : {
          offset : 0
          , limit  : 30
        }
      })
      .toss();

    frisby.create('TeamPlayerGameStats API should return the pagination info with the provided values')
      .get(endpoint + '?access_token=' + accessToken + '&limit=10&offset=0')
      .expectStatus(200)
      .expectJSON({
        meta : {
          offset : 0
          , limit  : 10
        }
      })
      .toss();

    frisby.create('TeamPlayerGameStats API should rise an error when malformed parameters sent')
      .post(endpoint + '?access_token=' + accessToken, {test_parameter : 'test_value'})
      .expectStatus(422)
      .toss();

    frisby.create('TeamPlayerGameStats API should rise an forbidden error when the access token hasn\'t the right permissions')
      .get(endpoint + '?access_token=CtVkdPE900DEAvaFlHhq50nW18z6SbKSuF48QwvUSjHAxFH9PWUW5PszuexYlshg')
      .expectStatus(403)
      .toss();

    frisby.create('TeamPlayerGameStats API should rise an unathorized error when no access token is provided')
      .get(endpoint + '')
      .expectStatus(401)
      .toss();

    frisby.create('TeamPlayerGameStats API should rise a not found error when requesting none existing records')
      .get(endpoint + '/99999999999?access_token=' + accessToken)
      .expectStatus(404)
      .toss();

    frisby.create('TeamPlayerGameStats API should return the records wrapped on a root node called TeamPlayerGameStats')
      .get(endpoint + '?access_token=' + accessToken)
      .expectStatus(200)
      .expectBodyContains('TeamPlayerGameStats')
      .toss();

    // need to test team filtering here
    frisby.create('TeamPlayerGameStats API, only records from same team as user should be returned')
      .get(endpoint + '?access_token=' + accessToken)
      .expectStatus(200)
      .expectJSON({
        TeamPlayerGameStats : function(records) {
          var allAccomplish = true;
          records.forEach(function(record) {
            if (record.team !== teamId) {
              console.log('record team id is %d, expected %d', record.team, teamId);
              allAccomplish = false;
            }
          });
          expect(allAccomplish).toBeTruthy();
        }
      })
      .toss();


    // these test require a valid record id, get that first
    frisby.create('TeamPlayerGameStats API, Team Filtering get valid ID')
      .get(endpoint + '/findOne?access_token=' + accessToken)
      .expectStatus(200)
      .afterJSON(function(result) {
        validId = result.id;
        var season = result.season;
        frisby.create('TeamPlayerGameStats API all records returned should match the provided filter criteria')
          .get(endpoint + '?filter={"where":{"season":"'+season+'"}}&access_token=' + accessToken)
          .expectStatus(200)
          .expectJSON({
            TeamPlayerGameStats : function(records) {
              var allAccomplish = true;
              records.forEach(function(record) {
                if(isNaN(season)) {
                  allAccomplish = false;
                }
              });
              expect(allAccomplish).toBeTruthy();
            }
          })
          .toss(); // all records returned should accomplish the provided criteria

        // update to latest model definition
        frisby.create('TeamPlayerGameStats API, Model\'s relations needs to include field list')
          .get(endpoint + '/' + validId + '?access_token=' + accessToken)
          .expectStatus(200)
          .expectJSONTypes('TeamPlayerGameStat', {
            teamPlayer: String,
            season: Number,
            numPlays: function(val) { expect(val).toBeTypeOrNull(Number); },
            avgCat1Grade: function(val) { expect(val).toBeTypeOrNull(Number); },
            avgCat2Grade: function(val) { expect(val).toBeTypeOrNull(Number); },
            avgCat3Grade: function(val) { expect(val).toBeTypeOrNull(Number); },
            avgOverallGrade: function(val) { expect(val).toBeTypeOrNull(Number); },
            date: function(val) { expect(val).toBeTypeOrNull(String); },
            teamEvent: Number,
            id: Number,
            team: String
          })
          .toss();

        frisby.create('TeamPlayerGameStats API should return one record when an id is provided')
          .get(endpoint + '/' + validId + '?access_token=' + accessToken)
          .expectStatus(200)
          .expectJSONLength(1)
          .toss();

        frisby.create('TeamPlayerGameStats API should return one record wrapped on a root node called with the model\'s name when an Id is provided')
          .get(endpoint + '/' + validId + '?access_token=' + accessToken)
          .expectStatus(200)
          .expectBodyContains('TeamPlayerGameStat')
          .toss();

      }).toss();  // Team Filtering, get validId

    // these tests will create/update/delete a record
    frisby.create('TeamPlayerGameStats API, Event Filtering get a valid ID')
      .get('http://' + host + ':' + port + '/api/TeamEvents/findOne?access_token=' + accessToken)
      .expectStatus(200)
      .afterJSON(function(result) {
        var teamEventId = result.id;

        frisby.create('TeamPlayerGameStats API, Player Filtering get a valid ID')
          .get('http://' + host + ':' + port + '/api/TeamPlayers/findOne?access_token=' + accessToken)
          .expectStatus(200)
          .afterJSON(function(result) {
            var teamPlayerId    = result.id
              , currentYear     = (new Date()).getFullYear()
              , numPlays        = 30
              , avgCat1Grade       = 45
              , avgCat2Grade       = 30
              , avgCat3Grade       = 45
              , avgOverallGrade    = 120
              , date            = new Date()
              , newRecordId     = -1
              ;

            frisby.create('TeamPlayerGameStats API create a new record')
              .post(endpoint + '?access_token=' + accessToken,
              {
                teamPlayer     : teamPlayerId,
                season         : currentYear,
                numPlays       : numPlays,
                avgCat1Grade      : avgCat1Grade,
                avgCat2Grade      : avgCat2Grade,
                avgCat3Grade      : avgCat3Grade,
                avgOverallGrade   : avgOverallGrade,
                teamEvent      : teamEventId,
                date           : date,
                team           : teamId
              })
              .expectStatus(200)
              .expectJSON(
              {
                teamPlayer     : teamPlayerId,
                season         : currentYear,
                numPlays       : numPlays,
                avgCat1Grade      : avgCat1Grade,
                avgCat2Grade      : avgCat2Grade,
                avgCat3Grade      : avgCat3Grade,
                avgOverallGrade   : avgOverallGrade,
                teamEvent      : teamEventId,
                team           : undefined
              })
              .afterJSON(function(newRecord) {
                newRecordId = newRecord.id;
                frisby.create('TeamPlayerGameStats API gets the new record created')
                  .get(endpoint + '/' + newRecordId + '?access_token=' + accessToken)
                  .expectStatus(200)
                  .expectJSONLength(1)
                  .expectJSONTypes('TeamPlayerGameStat', {
                    id             : Number,
                    teamPlayer     : String,
                    season         : Number,
                    numPlays       : Number,
                    avgCat1Grade      : Number,
                    avgCat2Grade      : Number,
                    avgCat3Grade      : Number,
                    avgOverallGrade   : Number,
                    teamEvent      : Number,
                    team           : String
                  })
                  .expectJSON(
                  {
                    TeamPlayerGameStat: {
                      id              : newRecordId,
                      teamPlayer      : teamPlayerId.toString(),
                      season          : currentYear,
                      numPlays        : numPlays,
                      avgCat1Grade       : avgCat1Grade,
                      avgCat2Grade       : avgCat2Grade,
                      avgCat3Grade       : avgCat3Grade,
                      avgOverallGrade    : avgOverallGrade,
                      teamEvent       : teamEventId,
                      team            : teamId
                    }
                  })
                  .toss();

                var numPlaysTmp   = numPlays + 10
                  , avgCat1GradeTmp    = avgCat1Grade + 10
                  , avgOverallGradeTmp = avgOverallGrade +10
                  ;
                frisby.create('TeamPlayerGameStats API Updates the new record just created')
                  .put(endpoint + '?access_token=' + accessToken,
                  {
                    id             : newRecordId,
                    teamPlayer     : teamPlayerId,
                    season         : currentYear,
                    numPlays       : numPlaysTmp,
                    avgCat1Grade      : avgCat1GradeTmp,
                    avgCat2Grade      : avgCat2Grade,
                    avgCat3Grade      : avgCat3Grade,
                    avgOverallGrade   : avgOverallGradeTmp,
                    teamEvent      : teamEventId,
                    team           : teamId
                  })
                  .expectStatus(200)
                  .expectJSON(
                  {
                    id              : newRecordId,
                    teamPlayer      : teamPlayerId,
                    season          : currentYear,
                    numPlays        : numPlaysTmp,
                    avgCat1Grade       : avgCat1GradeTmp,
                    avgCat2Grade       : avgCat2Grade,
                    avgCat3Grade       : avgCat3Grade,
                    avgOverallGrade    : avgOverallGradeTmp,
                    teamEvent       : teamEventId,
                    team            : undefined
                  })
                  .toss();

                // Verifies that record was updated successfully
                frisby.create('TeamPlayerGameStats API returns the record updated')
                  .get(endpoint + '/' + newRecordId + '?access_token=' + accessToken)
                  .expectStatus(200)
                  .expectJSONLength(1)
                  .expectJSON(
                  {
                    TeamPlayerGameStat: {
                      id              : newRecordId,
                      teamPlayer      : teamPlayerId.toString(),
                      season          : currentYear,
                      numPlays        : numPlaysTmp,
                      avgCat1Grade       : avgCat1GradeTmp,
                      avgCat2Grade       : avgCat2Grade,
                      avgCat3Grade       : avgCat3Grade,
                      avgOverallGrade    : avgOverallGradeTmp,
                      teamEvent       : teamEventId,
                      team            : teamId
                    }
                  })
                  .toss();

                frisby.create('TeamPlayerGameStats API deletes the record')
                  .delete(endpoint + '/' + newRecordId + '?access_token=' + accessToken)
                  .expectStatus(204)
                  .toss();

                frisby.create('TeamPlayerGameStats API, Verifies that record was deleted')
                  .get(endpoint + '/' + newRecordId + '?access_token=' + accessToken)
                  .expectStatus(404)
                  .toss();

              }).toss();
          }).toss();
      }).toss();
  }).toss(); //login

frisby.create('Logout').post('http://' + host + ':' + port + '/api/gousers/logout?access_token=' + accessToken).toss();
