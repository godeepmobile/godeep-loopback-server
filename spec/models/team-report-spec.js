var frisby    = require('frisby')
  , host      = process.env.host || 'localhost'
  , port      = process.env.port || '3000'
  , _username = process.env.user || 'admin'
  , _password = process.env.pass || 'admin'
  , endpoint  = 'http://' + host + ':' + port + '/api/TeamReports'
  , validId = -1
  , teamId = -1
  , accessToken = -1
  ;

frisby.create('Login')
  .post('http://' + host + ':' + port + '/api/gousers/login', {username : _username, password : _password})
  .afterJSON(function(result) {
    accessToken = result.id;
    teamId = result.teamId; // remember team id for current user for team-filtering tests

    frisby.create('TeamReport API should get a valid json response with a 200 code')
      .get(endpoint + '?access_token=' + accessToken)
      .expectHeaderContains('content-type', 'application/json')
      .expectStatus(200)
      .toss();

    frisby.create('TeamReport API should return the pagination info with default values when none provided')
      .get(endpoint + '?access_token=' + accessToken)
      .expectStatus(200)
      .expectJSON({
        meta : {
          offset : 0
          , limit  : 30
        }
      })
      .toss();

    frisby.create('TeamReport API should return the pagination info with the provided values')
      .get(endpoint + '?access_token=' + accessToken + '&limit=10&offset=0')
      .expectStatus(200)
      .expectJSON({
        meta : {
          offset : 0
          , limit  : 10
        }
      })
      .toss();

    frisby.create('TeamReport API should rise an error when malformed parameters sent')
      .post(endpoint + '?access_token=' + accessToken, {test_parameter : 'test_value'})
      .expectStatus(422)
      .toss();

    frisby.create('TeamReport API should rise an forbidden error when the access token hasn\'t the right permissions')
      .get(endpoint + '?access_token=CtVkdPE900DEAvaFlHhq50nW18z6SbKSuF48QwvUSjHAxFH9PWUW5PszuexYlshg')
      .expectStatus(403)
      .toss();

    frisby.create('TeamReport API should rise an unathorized error when no access token is provided')
      .get(endpoint + '/1')
      .expectStatus(401)
      .toss();

    frisby.create('TeamReport API should rise a not found error when requesting none existing records')
      .get(endpoint + '/99999999999?access_token=' + accessToken)
      .expectStatus(404)
      .toss();

    frisby.create('TeamReport API should return the records wrapped on a root node called TeamReport')
      .get(endpoint + '?access_token=' + accessToken)
      .expectStatus(200)
      .expectBodyContains('TeamReports')
      .toss();

    // need to test team filtering here
    frisby.create('TeamReport API, only records from same team as user should be returned')
      .get(endpoint + '?access_token=' + accessToken)
      .expectStatus(200)
      .expectJSON({
        TeamReports : function(records) {
          var allAccomplish = true;
          records.forEach(function(record) {
            if(record.team !== teamId) {
              allAccomplish = false;
            }
          });
          expect(allAccomplish).toBeTruthy();
        }
      })
      .toss();

    // these test require a valid record id, get that first
    //TODO create a valid record to ensure that there is data to test
    frisby.create('TeamReport API, Team Filtering get valid ID')
      .get(endpoint + '/findOne?access_token=' + accessToken)
      .expectStatus(200)
      .afterJSON(function(result) {
        validId = result.id;
        var numPlays = result.numPlays;

        /*frisby.create('TeamReport API all records returned should match the provided filter criteria')
          .get(endpoint + '?filter={"where":{"numPlays":"'+numPlays+'"}}&access_token=' + accessToken)
          .expectStatus(200)
          .expectJSON({
            TeamReports : function(records) {
              var allAccomplish = true;
              records.forEach(function(record) {
                if(record.numPlays !== numPlays) {
                  allAccomplish = false;
                }
              });
              expect(allAccomplish).toBeTruthy();
            }
          })
          .toss(); // all records returned should accomplish the provided criteria*/

        frisby.create('TeamReport API, Model\'s relations needs to include field list')
          .get(endpoint + '/' + validId + '?access_token=' + accessToken)
          .expectStatus(200)
          .expectJSONTypes('TeamReport', {
            id: function(val) { expect(val).toBeTypeOrNull(Number); },
            team: function(val) { expect(val).toBeTypeOrNull(String); },
            user: function(val) { expect(val).toBeTypeOrNull(Number); },
            reportType: function(val) { expect(val).toBeTypeOrNull(Number); },
            criteria: function(val) { expect(val).toBeTypeOrNull(String); },
            criteriaRange: function(val) { expect(val).toBeTypeOrNull(String); },
            scope: function(val) { expect(val).toBeTypeOrNull(String); },
            scopeRange: function(val) { expect(val).toBeTypeOrNull(String); },
            condition: function(val) { expect(val).toBeTypeOrNull(String); },
            conditionRange: function(val) { expect(val).toBeTypeOrNull(String); },
            name: function(val) { expect(val).toBeTypeOrNull(String); },
            isPublic: function(val) { expect(val).toBeTypeOrNull(Boolean); },
            eventType: function(val) { expect(val).toBeTypeOrNull(Number); }
          })
          .toss();

        frisby.create('TeamReport API should return one record when an id is provided')
          .get(endpoint + '/' + validId + '?access_token=' + accessToken)
          .expectStatus(200)
          .expectJSONLength(1)
          .toss();

        frisby.create('TeamReport API should return one record wrapped on a root node called with the model\'s name when an Id is provided')
          .get(endpoint + '/' + validId + '?access_token=' + accessToken)
          .expectStatus(200)
          .expectBodyContains('TeamReport')
          .toss();

      }).toss();  // Team Filtering, get validId

  }).toss(); // login
frisby.create('Logout').post('http://' + host + ':' + port + '/api/gousers/logout?access_token=' + accessToken).toss();
