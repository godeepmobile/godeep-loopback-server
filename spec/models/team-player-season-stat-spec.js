var frisby    = require('frisby')
  , host      = process.env.host || 'localhost'
  , port      = process.env.port || '3000'
  , _username = process.env.user || 'admin'
  , _password = process.env.pass || 'admin'
  , endpoint  = 'http://' + host + ':' + port + '/api/TeamPlayerSeasonStats'
  , validId = -1
  , teamId = -1
  , accessToken = -1
  ;

frisby.create('Login')
  .post('http://' + host + ':' + port + '/api/gousers/login', {username : _username, password : _password})
  .afterJSON(function(result) {
    accessToken = result.id;
    teamId = result.teamId; // remember team id for current user for team-filtering tests

    frisby.create('TeamPlayerSeasonStats API should get a valid json response with a 200 code')
      .get(endpoint + '?access_token=' + accessToken)
      .expectHeaderContains('content-type', 'application/json')
      .expectStatus(200)
      .toss();

    frisby.create('TeamPlayerSeasonStats API should return the pagination info with default values when none provided')
      .get(endpoint + '?access_token=' + accessToken)
      .expectStatus(200)
      .expectJSON({
        meta : {
          offset : 0
          , limit  : 30
        }
      })
      .toss();

    frisby.create('TeamPlayerSeasonStats API should return the pagination info with the provided values')
      .get(endpoint + '?access_token=' + accessToken + '&limit=10&offset=5')
      .expectStatus(200)
      .expectJSON({
        meta : {
          offset : 5
          , limit  : 10
        }
      })
      .toss();

    frisby.create('TeamPlayerSeasonStats API should rise an error when malformed parameters sent')
      .post(endpoint + '?access_token=' + accessToken, {test_parameter : 'test_value'})
      .expectStatus(422)
      .toss();

    //TODO: verify why TeamPlayerSeasonStats don't make the accessToken validation.
    frisby.create('TeamPlayerSeasonStats API should rise an forbidden error when the access token hasn\'t the right permissions')
      .get(endpoint + '?access_token=CtVkdPE900DEAvaFlHhq50nW18z6SbKSuF48QwvUSjHAxFH9PWUW5PszuexYlshg')
      .expectStatus(403)
      .toss();

    //TODO: verify why TeamPlayerSeasonStats don't need accessToken to retrieve them.
    frisby.create('TeamPlayerSeasonStats API should rise an unauthorized error when no access token is provided')
      .get(endpoint + '')
      .expectStatus(401)
      .toss();

    frisby.create('TeamPlayerSeasonStats API should rise a not found error when requesting non-existing records')
      .get(endpoint + '/99999999999?access_token=' + accessToken)
      .expectStatus(404)
      .toss();

    frisby.create('TeamPlayerSeasonStats API should return the records wrapped on a root node called TeamPlayerSeasonStats')
      .get(endpoint + '?access_token=' + accessToken)
      .expectStatus(200)
      .expectBodyContains('TeamPlayerSeasonStats')
      .toss();

    frisby.create('TeamPlayerSeasonStats API all records returned should match the filter criteria')
      .get(endpoint + '?filter[where][season]=2015&access_token=' + accessToken)
      .expectStatus(200)
      .expectJSON({
        TeamPlayerSeasonStats : function(records) {
          var allAccomplish = true;
          records.forEach(function(record) {
            if(record.season !== 2015) {
              allAccomplish = false;
            }
          });
          expect(allAccomplish).toBeTruthy();
        }
      })
      .toss(); //match filter criteria

    // need to test team filtering here
    frisby.create('TeamPlayerSeasonStats API, only records from same team as user should be returned')
      .get(endpoint + '?access_token=' + accessToken)
      .expectStatus(200)
      .expectJSON({
        TeamPlayerSeasonStats : function(records) {
          var allAccomplish = true;
          records.forEach(function(record) {
            if(record.team != teamId) {
              console.log('record team id is %d, expected %d', record.team, teamId);
              allAccomplish = false;
            }
          });
          expect(allAccomplish).toBeTruthy();
        }
      })
      .toss();

    // these test require a valid record id, get that first
    frisby.create('TeamPlayerSeasonStats API, Team Filtering get valid ID')
      .get(endpoint + '/findOne?access_token=' + accessToken)
      .expectStatus(200)
      .afterJSON(function(result) {
        validId = result.id;
        var name = result.name;

        frisby.create('TeamPlayerSeasonStats API, returned fields should have correct data types')
          .get(endpoint + '/' + validId + '?access_token=' + accessToken)
          .expectStatus(200)
          .expectJSONTypes('TeamPlayerSeasonStat', {
            id: Number,
            team: String,
            season: Number,
            gameNumPlays: Number,
            gameCount: Number,
            gameCat1Grade: Number,
            gameCat2Grade: Number,
            gameCat3Grade: Number,
            gameOverallGrade: Number,
            gameUnitImpactGrade: Number,
            gameImpactPosGroupGrade: Number,
            gameImpactPlatoonGrade: Number,
            lastGameNumPlays: Number,
            lastGameCat1Grade: Number,
            lastGameCat2Grade: Number,
            lastGameCat3Grade: Number,
            lastGameOverallGrade: Number,
            lastGameImpactPosGroupGrade: Number,
            lastGameImpactPlatoonGrade: Number,
            practiceNumPlays: Number,
            practiceCount: Number,
            practiceCat1Grade: Number,
            practiceCat2Grade: Number,
            practiceCat3Grade: Number,
            practiceOverallGrade: Number,
            practiceImpactPosGroupGrade: Number,
            practiceImpactPlatoonGrade: Number,
            allEventNumPlays: Number,
            allEventCount: Number,
            allEventCat1Grade: Number,
            allEventCat2Grade: Number,
            allEventCat3Grade: Number,
            allEventOverallGrade: Number,
            allEventImpactPosGroupGrade: Number,
            allEventImpactPlatoonGrade: Number,
            isVeteran: Boolean,
            teamPlayer: String
          })
          .toss();

        frisby.create('TeamPlayerSeasonStats API should return one record when an id is provided')
          .get(endpoint + '/' + validId + '?access_token=' + accessToken)
          .expectStatus(200)
          .expectJSONLength(1)
          .toss();

        frisby.create('TeamPlayerSeasonStats API should return one record wrapped on a root node called with the model\'s name when an Id is provided')
          .get(endpoint + '/' + validId + '?access_token=' + accessToken)
          .expectStatus(200)
          .expectBodyContains('TeamPlayerSeasonStat')
          .toss();

      }).toss();  // Team Filtering, get validId


  }).toss(); // login
frisby.create('Logout').post('http://' + host + ':' + port + '/api/gousers/logout?access_token=' + accessToken).toss();
