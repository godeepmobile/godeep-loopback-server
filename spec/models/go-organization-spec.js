var frisby    = require('frisby')
  , dumpError   = require('../frisby-utils').dumpError
  , host      = process.env.host || 'localhost'
  , port      = process.env.port || '3000'
  , _username = process.env.user || 'admin'
  , _password = process.env.pass || 'admin'
  , endpoint  = 'organizations';

frisby.create('Login').post('http://' + host + ':' + port + '/api/gousers/login', {username : _username, password : _password}).afterJSON(function(result) {
	var accessToken = result.id;

	frisby.create('Organizations API should get a valid json response with a 200 code')
		.get('http://' + host + ':' + port + '/api/' + endpoint + '?access_token=' + accessToken)
		.expectHeaderContains('content-type', 'application/json')
 		.expectStatus(200)
	.toss();

	frisby.create('Organizations API should return the pagination info with default values when none provided')
		.get('http://' + host + ':' + port + '/api/' + endpoint + '?access_token=' + accessToken)
		.expectJSON({
			meta : {
				offset : 0
			  , limit  : 30
			}
		})
	.toss();

	frisby.create('Organizations API should return the pagination info with the provided values')
		.get('http://' + host + ':' + port + '/api/' + endpoint + '?access_token=' + accessToken + '&limit=10&offset=5')
		.expectJSON({
			meta : {
				offset : 5
			  , limit  : 10
			}
		})
	.toss();

	frisby.create('Organizations API should rise an error when malformed parameters sent')
		.post('http://' + host + ':' + port + '/api/' + endpoint + '?access_token=' + accessToken, {test_parameter : 'test_value'})
		.expectStatus(422)
	.toss();

	frisby.create('Organizations API should rise a not found error when requesting none existing records')
		.get('http://' + host + ':' + port + '/api/' + endpoint + '/99999999999?access_token=' + accessToken)
		.expectStatus(404)
	.toss();

  // these test require a valid record id, get that first
  frisby.create('Organizations API, find one valid Organization')
    .get('http://' + host + ':' + port + '/api/' + endpoint + '/findOne?access_token=' + accessToken)
    .expectStatus(200)
    .afterJSON(function(result) {
      var validId = result.id;
      var name = result.name;

    	frisby.create('Organizations API, Model\'s relations needs to include field list')
          .expectStatus(200)
        	.get('http://' + host + ':' + port + '/api/' + endpoint + '/'+validId+'?access_token=' + accessToken)
          .expectJSONTypes('Organization', {
            	name: String,
            	shortName: String,
            	espnName: String,
            	mascot: String,
            	id: Number,
            	organizationType: Object,
            	conference: Object
            })
          .afterJSON(function (result) { dumpError(result); })
        .toss();
    }).toss();  // Team Filtering, get validId

    frisby.create('Organizations API should return the records wrapped on a root node called Organizations')
		.get('http://' + host + ':' + port + '/api/' + endpoint + '?access_token=' + accessToken)
		.expectBodyContains('Organizations')
	.toss();

	frisby.create('Organizations API should return one record when an id is provided')
		.get('http://' + host + ':' + port + '/api/' + endpoint + '/1?access_token=' + accessToken)
		.expectJSONLength(1)
	.toss();

	frisby.create('Organizations API should return one record wrapped on a root node called with the model\'s name when an Id is provided')
		.get('http://' + host + ':' + port + '/api/' + endpoint + '/1?access_token=' + accessToken)
		.expectBodyContains('Organization')
	.toss();

	frisby.create('Organizations API all records returned should accomplish the provided criteria')
		.get('http://' + host + ':' + port + '/api/' + endpoint + '?filter[where][name][like]=University%&access_token=' + accessToken)
 		.expectJSON({
 			Organizations : function(records) {
 				var allAccomplish = true;
 				records.forEach(function(record) {
 					if(record.name.indexOf('University') !== 0) {
 						allAccomplish = false;
 					}
 				});
 				expect(allAccomplish).toBeTruthy();
 			}
 		})
	.toss();

	frisby.create('Logout').post('http://' + host + ':' + port + '/api/gousers/logout?access_token=' + accessToken).toss();

}).toss();
