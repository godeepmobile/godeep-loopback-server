var frisby    = require('frisby')
  , host      = process.env.host || 'localhost'
  , port      = process.env.port || '3000'
  , _username = process.env.user || 'admin'
  , _password = process.env.pass || 'admin'
  , endpoint  = 'gameEvents';

frisby.create('Login').post('http://' + host + ':' + port + '/api/gousers/login', {username : _username, password : _password}).afterJSON(function(result) {
	var accessToken = result.id;

	frisby.create('GameEvents API should get a valid json response with a 200 code')
		.get('http://' + host + ':' + port + '/api/' + endpoint + '?access_token=' + accessToken)
		.expectHeaderContains('content-type', 'application/json')
 		.expectStatus(200)
	.toss();

	frisby.create('GameEvents API should return the pagination info with default values when none provided')
		.get('http://' + host + ':' + port + '/api/' + endpoint + '?access_token=' + accessToken)
    .expectStatus(200)
		.expectJSON({
			meta : {
				offset : 0
			  , limit  : 30
			}
		})
	.toss();

	frisby.create('GameEvents API should return the pagination info with the provided values')
		.get('http://' + host + ':' + port + '/api/' + endpoint + '?access_token=' + accessToken + '&limit=10&offset=0')
    .expectStatus(200)
		.expectJSON({
			meta : {
				offset : 0
			  , limit  : 10
			}
		})
	.toss();

	frisby.create('GameEvents API should rise an error when malformed parameters sent')
		.post('http://' + host + ':' + port + '/api/' + endpoint + '?access_token=' + accessToken, {test_parameter : 'test_value'})
		.expectStatus(422)
	.toss();

	frisby.create('GameEvents API should rise an forbidden error when the access token hasn\'t the right permissions')
		.get('http://' + host + ':' + port + '/api/' + endpoint + '?access_token=CtVkdPE900DEAvaFlHhq50nW18z6SbKSuF48QwvUSjHAxFH9PWUW5PszuexYlshg')
 		.expectStatus(403)
	.toss();

	frisby.create('GameEvents API should rise an unathorized error when no access token is provided')
		.get('http://' + host + ':' + port + '/api/' + endpoint + '')
 		.expectStatus(401)
	.toss();

	frisby.create('GameEvents API should rise a not found error when requesting none existing records')
		.get('http://' + host + ':' + port + '/api/' + endpoint + '/99999999999?access_token=' + accessToken)
		.expectStatus(404)
	.toss();

  //TODO update with current model definition
  //TODO get a valid id before calling this. id 1 is not guaranteed to exist
	frisby.create('GameEvents API, Model\'s relations needs to include field list')
   	.get('http://' + host + ':' + port + '/api/' + endpoint + '/1?access_token=' + accessToken)
    .expectStatus(200)
   	.expectJSONTypes('GameEvent', {
        	season: Number,
        	homeTeam: String,
        	awayTeam: String,
        	homeScore: Number,
        	awayScore: Number,
        	date: String,
        	id: Number,
        	time: function(val) { expect(val).toBeTypeOrNull(String); },
        	location: function(val) { expect(val).toBeTypeOrNull(String); },
        	city: function(val) { expect(val).toBeTypeOrNull(String); },
        	state: function(val) { expect(val).toBeTypeOrNull(Number); }
        })
    .toss();

    frisby.create('GameEvents API should return the records wrapped on a root node called GameEvents')
		.get('http://' + host + ':' + port + '/api/' + endpoint + '?access_token=' + accessToken)
      .expectStatus(200)
      .expectBodyContains('GameEvents')
	.toss();

  //TODO get a valid id before calling this. id 1 is not guaranteed to exist
	frisby.create('GameEvents API should return one record when an id is provided')
		.get('http://' + host + ':' + port + '/api/' + endpoint + '/1?access_token=' + accessToken)
    .expectStatus(200)
		.expectJSONLength(1)
	.toss();

  //TODO get a valid id before calling this. id 1 is not guaranteed to exist
	frisby.create('GameEvents API should return one record wrapped on a root node called with the model\'s name when an Id is provided')
		.get('http://' + host + ':' + port + '/api/' + endpoint + '/1?access_token=' + accessToken)
    .expectStatus(200)
		.expectBodyContains('GameEvent')
	.toss();

	// frisby.create('GameEvents API all records returned should accomplish the provided criteria')
	// 	.get('http://' + host + ':' + port + '/api/' + endpoint + '?filter[where][season][like]=%2014%&access_token=' + accessToken)
 // 		.expectJSON({
 // 			GameEvents : function(records) {
 // 				var allAccomplish = true;
 // 				records.forEach(function(record) {
 // 					if(!~record.season.indexOf('2014')) {
 // 						allAccomplish = false;
 // 					}
 // 				});
 // 				expect(allAccomplish).toBeTruthy();
 // 			}
 // 		})
	// .toss();

	frisby.create('Logout').post('http://' + host + ':' + port + '/api/gousers/logout?access_token=' + accessToken).toss();

}).toss();
