var frisby      = require('frisby')
  , host        = process.env.host || 'localhost'
  , port        = process.env.port || '3000'
  , _username   = process.env.user || 'admin'
  , _password   = process.env.pass || 'admin'
  , endpoint    = 'http://' + host + ':' + port + '/api/TeamConfigurations'
  , validId     = -1
  , teamId      = -1
  , accessToken = -1
  ;

frisby.globalSetup({
  timeout: 10000
});

frisby.create('Login')
  .post('http://' + host + ':' + port + '/api/gousers/login', {username : _username, password : _password})
  .afterJSON(function(result) {
    accessToken = result.id;
    teamId = result.teamId; // remember team id for current user for team-filtering tests

    frisby.create('TeamConfigurations API should get a valid json response with a 200 code')
      .get(endpoint + '?access_token=' + accessToken)
      .expectHeaderContains('content-type', 'application/json')
      .expectStatus(200)
      .toss();

    frisby.create('TeamConfigurations API should return the pagination info with default values when none provided')
      .get(endpoint + '?access_token=' + accessToken)
      .expectStatus(200)
      .expectJSON({
        meta : {
          offset : 0
          , limit  : 30
        }
      })
      .toss();

    frisby.create('TeamConfigurations API should return the pagination info with the provided values')
      .get(endpoint + '?access_token=' + accessToken + '&limit=10&offset=0')
      .expectStatus(200)
      .expectJSON({
        meta : {
          offset : 0
          , limit  : 10
        }
      })
      .toss();

    frisby.create('TeamConfigurations API should rise an error when malformed parameters sent')
      .post(endpoint + '?access_token=' + accessToken, {test_parameter : 'test_value'})
      .expectStatus(422)
      .toss();

    frisby.create('TeamConfigurations API should rise an forbidden error when the access token hasn\'t the right permissions')
      .get(endpoint + '?access_token=CtVkdPE900DEAvaFlHhq50nW18z6SbKSuF48QwvUSjHAxFH9PWUW5PszuexYlshg')
      .expectStatus(403)
      .toss();

    frisby.create('TeamConfigurations API should rise an unathorized error when no access token is provided')
      .get(endpoint + '')
      .expectStatus(401)
      .toss();

    frisby.create('TeamConfigurations API should rise a not found error when requesting none existing records')
      .get(endpoint + '/99999999999?access_token=' + accessToken)
      .expectStatus(404)
      .toss();

    frisby.create('TeamConfigurations API should return the records wrapped on a root node called TeamConfigurations')
      .get(endpoint + '?access_token=' + accessToken)
      .expectStatus(200)
      .expectBodyContains('TeamConfigurations')
      .toss();

    // need to test team filtering here
    frisby.create('TeamConfigurations API, only records from same team as user should be returned')
      .get(endpoint + '?access_token=' + accessToken)
      .expectStatus(200)
      .expectJSON({
        TeamConfigurations : function(records) {
          var allAccomplish = true;
          records.forEach(function(record) {
            if (record.team != teamId) {
              console.log('record team id is %d, expected %d', record.team, teamId);
              allAccomplish = false;
            }
          });
          expect(allAccomplish).toBeTruthy();
        }
      })
      .toss();


    // these test require a valid record id, get that first
    frisby.create('TeamConfigurations API, Team Filtering get valid ID')
      .get(endpoint + '/findOne?access_token=' + accessToken)
      .expectStatus(200)
      .afterJSON(function(result) {
        validId = result.id;
        var gradeCat1Label = result.gradeCat1Label;
        frisby.create('TeamConfigurations API all records returned should match the provided filter criteria')
          .get(endpoint + '?filter={"where":{"gradeCat1Label":"'+gradeCat1Label+'"}}&access_token=' + accessToken)
          .expectStatus(200)
          .expectJSON({
            TeamConfigurations : function(records) {
              var allAccomplish = true;
              records.forEach(function(record) {
                if(record.gradeCat1Label.indexOf(gradeCat1Label) !== 0) {
                  allAccomplish = false;
                }
              });
              expect(allAccomplish).toBeTruthy();
            }
          })
          .toss(); // all records returned should accomplish the provided criteria

        // update to latest model definition
        frisby.create('TeamConfigurations API, Model\'s relations needs to include field list')
          .get(endpoint + '/' + validId + '?access_token=' + accessToken)
          .expectStatus(200)
          .expectJSONTypes('TeamConfiguration', {
            gradeCat1Label: String,
            gradeCat2Label: String,
            gradeCat3Label: String,
            gradeBase: function(val) { expect(val).toBeTypeOrNull(Number); },
            gradeIncrement: function(val) { expect(val).toBeTypeOrNull(Number); },
            data: function(val) { expect(val).toBeTypeOrNull(String); },
            id: Number,
            team: String,
            numGradeCategories: Number,
            createCutupForEachPlatoon: Boolean
          })
          .toss();
        frisby.create('TeamConfigurations API should return one record when an id is provided')
          .get(endpoint + '/' + validId + '?access_token=' + accessToken)
          .expectStatus(200)
          .expectJSONLength(1)
          .toss();

        frisby.create('TeamConfigurations API should return one record wrapped on a root node called with the model\'s name when an Id is provided')
          .get(endpoint + '/' + validId + '?access_token=' + accessToken)
          .expectStatus(200)
          .expectBodyContains('TeamConfiguration')
          .toss();

      }).toss();  // Team Filtering, get validId

    //TODO add validation when grades have been entered
    // these test require a valid record id to update the record
    /*frisby.create('TeamConfigurations API, Team Filtering get valid ID')
      .get(endpoint + '/findOne?access_token=' + accessToken)
      .expectStatus(200)
      .afterJSON(function(result) {
        validId = result.id;
        var gradeCat1LabelTemp = result.gradeCat1Label + 'temp'
        , gradeCat2LabelTemp   = result.gradeCat2Label + 'temp'
        , gradeCat3LabelTemp   = result.gradeCat3Label + 'temp'
        , gradeBaseTemp        = result.gradeBase + 10
        ;

        frisby.create('TeamConfigurations API a record should be updated')
          .put(endpoint + '?access_token=' + accessToken,
            {
              id: validId,
              gradeCat1Label: gradeCat1LabelTemp,
              gradeCat2Label: gradeCat2LabelTemp,
              gradeCat3Label: gradeCat3LabelTemp,
              gradeBase: gradeBaseTemp,
              gradeIncrement: result.gradeIncrement
            })
          .expectStatus(200)
          .expectJSON(
            {
              id: validId,
              gradeCat1Label: gradeCat1LabelTemp,
              gradeCat2Label: gradeCat2LabelTemp,
              gradeCat3Label: gradeCat3LabelTemp,
              gradeBase: gradeBaseTemp,
              gradeIncrement: result.gradeIncrement,
              team: undefined
            })
          .toss();

        frisby.create('TeamConfigurations API should return updated record when the id is provided')
          .get(endpoint + '/' + validId + '?access_token=' + accessToken)
          .expectStatus(200)
          .expectJSONLength(1)
          .expectJSON(
            {
              TeamConfiguration: {
                id: validId,
                gradeCat1Label: gradeCat1LabelTemp,
                gradeCat2Label: gradeCat2LabelTemp,
                gradeCat3Label: gradeCat3LabelTemp,
                gradeBase: gradeBaseTemp,
                gradeIncrement: result.gradeIncrement
              }
            }
          ).toss();

        frisby.create('TeamConfigurations API the record should get back to original values')
          .put(endpoint + '?access_token=' + accessToken,
            {
              id: validId,
              gradeCat1Label: result.gradeCat1Label,
              gradeCat2Label: result.gradeCat2Label,
              gradeCat3Label: result.gradeCat3Label,
              gradeBase: result.gradeBase,
              gradeIncrement: result.gradeIncrement
            })
          .expectStatus(200)
          .expectJSON(
            {
              id: validId,
              gradeCat1Label: result.gradeCat1Label,
              gradeCat2Label: result.gradeCat2Label,
              gradeCat3Label: result.gradeCat3Label,
              gradeBase: result.gradeBase,
              gradeIncrement: result.gradeIncrement,
              team: undefined
            })
          .toss();
      }).toss();*/
  }).toss(); //login

frisby.create('Logout').post('http://' + host + ':' + port + '/api/gousers/logout?access_token=' + accessToken).toss();
