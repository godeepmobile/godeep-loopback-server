var frisby      = require('frisby')
  , host        = process.env.host || 'localhost'
  , port        = process.env.port || '3000'
  , _username   = process.env.user || 'admin'
  , _password   = process.env.pass || 'admin'
  , endpoint    = 'http://' + host + ':' + port + '/api/UserRoles'
  , validId     = -1
  , teamId      = -1
  , accessToken = -1
  ;

frisby.globalSetup({
  timeout: 10000
});

frisby.create('Login')
  .post('http://' + host + ':' + port + '/api/gousers/login', {username : _username, password : _password})
  .afterJSON(function(result) {
    accessToken = result.id;
    teamId = result.teamId; // remember team id for current user for team-filtering tests

    frisby.create('UserRoles API should get a valid json response with a 200 code')
      .get(endpoint + '?access_token=' + accessToken)
      .expectHeaderContains('content-type', 'application/json')
      .expectStatus(200)
      .toss();

    frisby.create('UserRoles API should return the pagination info with default values when none provided')
      .get(endpoint + '?access_token=' + accessToken)
      .expectStatus(200)
      .expectJSON({
        meta : {
          offset : 0
          , limit  : 30
        }
      })
      .toss();

    frisby.create('UserRoles API should return the pagination info with the provided values')
      .get(endpoint + '?access_token=' + accessToken + '&limit=10&offset=0')
      .expectStatus(200)
      .expectJSON({
        meta : {
          offset : 0
          , limit  : 10
        }
      })
      .toss();

    frisby.create('UserRoles API should rise an error when malformed parameters sent')
      .post(endpoint + '?access_token=' + accessToken, {test_parameter : 'test_value'})
      .expectStatus(422)
      .toss();

    frisby.create('UserRoles API should rise an forbidden error when the access token hasn\'t the right permissions')
      .get(endpoint + '?access_token=CtVkdPE900DEAvaFlHhq50nW18z6SbKSuF48QwvUSjHAxFH9PWUW5PszuexYlshg')
      .expectStatus(403)
      .toss();

    frisby.create('UserRoles API should rise an unathorized error when no access token is provided')
      .get(endpoint + '')
      .expectStatus(401)
      .toss();

    frisby.create('UserRoles API should rise a not found error when requesting a record with an invalid id')
      .get(endpoint + '/-1?access_token=' + accessToken)
      .expectStatus(404)
      .toss();

    frisby.create('UserRoles API should return the records wrapped on a root node called UserRoles')
      .get(endpoint + '?access_token=' + accessToken)
      .expectStatus(200)
      .expectBodyContains('UserRoles')
      .toss();

    // these test require a valid record id, get that first
    frisby.create('UserRoles API, Team Filtering get valid ID')
      .get(endpoint + '/findOne?access_token=' + accessToken)
      .expectStatus(200)
      .afterJSON(function(result) {
        validId = result.id;
        var fullName = result.fullName;

        frisby.create('UserRoles API all records returned should match the provided filter criteria')
          .get(endpoint + '?filter={"where":{"fullName":"'+fullName+'"}}&access_token=' + accessToken)
          .expectStatus(200)
          .expectJSON({
            UserRoles : function(records) {
              var allAccomplish = true;
              records.forEach(function(record) {
                if(record.fullName.indexOf(fullName) !== 0) {
                  allAccomplish = false;
                }
              });
              expect(allAccomplish).toBeTruthy();
            }
          })
          .toss(); // all records returned should accomplish the provided criteria

        // update to latest model definition
        frisby.create('UserRoles API, Model\'s relations needs to include field list')
          .get(endpoint + '/' + validId + '?access_token=' + accessToken)
          .expectStatus(200)
          .expectJSONTypes('UserRole', {
            id: Number,
            fullName: String,
            role: String
          })
          .toss();

        frisby.create('UserRoles API should return one record when an id is provided')
          .get(endpoint + '/' + validId + '?access_token=' + accessToken)
          .expectStatus(200)
          .expectJSONLength(1)
          .toss();

        frisby.create('UserRoles API should return one record wrapped on a root node called with the model\'s name when an Id is provided')
          .get(endpoint + '/' + validId + '?access_token=' + accessToken)
          .expectStatus(200)
          .expectBodyContains('UserRole')
          .toss();

      }).toss();  // UserRole Filtering, get validId

  }).toss(); //login

frisby.create('Logout').post('http://' + host + ':' + port + '/api/gousers/logout?access_token=' + accessToken).toss();
