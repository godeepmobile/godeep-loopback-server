module.exports = {
  version: '1.0',

  dumpError: function(result) {
    if (result && result.error) {
      if (result.error.stack) {
        console.log('\ndumpError stack: ', result.error.stack);
      } else {
        console.log('\ndumpError error: ', result.error);
      }
    }
  }
};
