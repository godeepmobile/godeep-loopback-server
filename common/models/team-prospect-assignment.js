var loopback  = require('loopback');

module.exports = function(TeamProspectAssignment) {
	TeamProspectAssignment.observe('before save', function(ctx, next) {
	    if (ctx.instance) {
	      console.log('TeamProspectAssignment.observe before save has a new instance');
	      ctx.instance.isProspect = true;
	    }
	    next();
	  });

	  /*
	   * Deletes logically a team prospect and persist it into the data source.
	   */
	  TeamProspectAssignment.delete = function(_id, cb) {
	    console.log('TeamProspectAssignment.delete');
	    TeamProspectAssignment.findById(_id, function(err, teamProspectAssignment) {
	      if (err) {
	        console.log('TeamProspectAssignment.delete findById() got error ', err);
	        return cb(err);
	      }
	      if ( teamProspectAssignment ) {
	        console.log('TeamProspectAssignment.delete TeamProspectAssignment found');
	        var endDate = new Date();
	        teamProspectAssignment.updateAttribute('endDate', endDate, function(err, obj) {
	          if (err) {
	            console.log('TeamProspectAssignment.delete updateAttribute got error ', err);
	            return cb(err);
	          }
	          console.log('TeamProspectAssignment.delete TeamProspectAssignment deleted');
	        });
	      }
	      return cb();
	    });
	  };

	  /*
	   * Gets prospect stat data.
	   */
	  TeamProspectAssignment.evaluationStats = function(_id, cb) {
	    var requestQuery = 'SELECT (SELECT row_to_json(career_stat.*) \
	      FROM view_prospect_evaluation_career_stat career_stat \
	      WHERE career_stat.team_prospect_id = $2 \
	         AND career_stat.go_team_id = $1) as career_data, \
	      array_to_json(array_agg(row_to_json(seasons))) as seasons_data \
	      FROM (  SELECT \
	      row_to_json(rank_overall.*) as season_data, \
	      ( \
	      SELECT array_to_json(array_agg(row_to_json(evaluation))) as evaluations_data \
	      FROM ( \
	        SELECT users.first_name, users.last_name, eval_stat.*, overall_at_position.go_position_type_id, overall_at_position.rank_at_post, overall_at_position.short_name \
	          FROM view_prospect_evaluation_stat eval_stat, view_active_user_non_players users, view_prospect_rank_overall_at_position overall_at_position \
	          WHERE eval_stat.season = rank_overall.season \
	            AND overall_at_position.season = eval_stat.season \
	            AND eval_stat.team_prospect_id = $2 \
	            AND eval_stat.team_prospect_id = overall_at_position.team_prospect_id \
	            AND eval_stat.go_team_id = $1 \
	            AND eval_stat.go_team_id = overall_at_position.go_team_id \
	            AND eval_stat.team_scouting_eval_score_group_id = overall_at_position.team_scouting_eval_score_group_id \
	            AND users.go_team_id = eval_stat.go_team_id \
	            AND users.id = eval_stat.go_user_id \
	      ) evaluation \
	      ) \
	      FROM view_prospect_evaluation_season_stat rank_overall \
	      WHERE rank_overall.team_prospect_id = $2 \
	        AND rank_overall.go_team_id = $1 \
	      GROUP BY rank_overall.season, rank_overall.* \
	      ) seasons;';
	    var connector = TeamProspectAssignment.getDataSource().connector
	      , response;
	    console.log('TeamProspectAssignment.evaluationStats');
	    var context           = loopback.getCurrentContext()
	      , teamId            = context.get('accessToken').teamId;
	    
	    TeamProspectAssignment.findById(_id, function(err, teamProspectAssignment){
	    	if (err) {
	    		console.log('TeamProspectAssignment.evaluationStats, find got error ', err);
	    		return cb(err);
	    	}

	    	connector.executeSQL(requestQuery, [teamId, teamProspectAssignment.teamProspect], function(err, records) {
		      if (err) {
		        console.log('TeamProspectAssignment.evaluationStats, execute got error ', err);
		        return cb(err);
		      } else{
		        console.log('TeamProspectAssignment.evaluationStats execute done');
		        cb(null, records);
		      }
		    });
	    });	
	  };

	TeamProspectAssignment.remoteMethod(
	    'delete', // local function that implements the RESTful API method
	    {
	      description: "Delete a team prospect by id from the data source",
	      notes      : "Delete a team prospect logically",
	      accepts    : [
	                    {arg: 'id', type: 'string', required: true, description:"You must pass an team prospect id."}
	                    ],
	      http       : {path: '/:id/logicalDelete', verb: 'del'},
	      returns    : {arg: 'teamProspectAssignment', type: 'object'}
	    }
	  );

	TeamProspectAssignment.remoteMethod(
	    'evaluationStats', // local function that implements the RESTful API method
	    {
	      description : 'Gets prospect evaluation stats',
	      notes       : 'prospect evaluation stats',
	      accepts    : [
	                    {arg: 'id', type: 'string', required: true, description:"You must pass an team player id."}
	                    ],
	      http       : {path: '/:id/evaluationStats', verb: 'get'},
	      returns    : {arg: 'stat', type: 'object'}
    });
};