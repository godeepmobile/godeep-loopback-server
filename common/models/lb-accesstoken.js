module.exports = function(LbAccesstoken) {

LbAccesstoken.observe('before save', function(ctx, next) {
  var RoleMapping = LbAccesstoken.app.models.UserTeamAssignment
    , self        = ctx.instance
    , User        = LbAccesstoken.app.models.GoUser
    , connector   = User.getDataSource().connector;

  User.findById(self.userId, function(err,user) {
    if (err) {
      console.log('LbAccesstoken: err ', err);
      return next(err);
    }
    self.superUser              = user.superUser;
    self.hasUserChangedPassword = user.hasChangedPassword;
    // load RoleMappings for this user
    RoleMapping.find({
        where : {
          and : [
            {principalType : RoleMapping.USER},
            {principalId   : self.userId},
            {startDate     : {lt: Date.now()}},
            {or : [
              {endDate   : {gt: Date.now()}},
              {endDate   : null}
            ]}
          ]
        },
        include : ['user', {role: 'permissions'}]
      }, function(err, roleMappings) {
        var roles       = []
        , permissions   = []
        , teamIds       = []
        , playerRole    = {id: 1, name: 'Player'}
        , role
        , permission;

        if (err) {
          console.log('LbAccesstoken: err ', err);
          return next(err);
        }

        // ok, we've got roleMappings. let's get permissions tied to this role.
        roleMappings.forEach(function(roleMapping) {
          role = roleMapping.role();
          roles.push({
              id   : role.id
            , name : role.name
          });

          role.permissions().forEach(function(rolePermission) {
            // each permission will be identified with the go_permission_id field
            // keep track of that for resolving permissions for this user later
            permission = rolePermission.go_permission_id;
            if (permissions.indexOf(permission) === -1) {
              permissions.push(permission);
            }
          });

          teamIds.push(roleMapping.go_team_id);

          if (role.id === playerRole.id && role.name === playerRole.name) {
            self.playerId = roleMapping.playerId;
          }
        }); // role.permissions().forEach()
        self.userRoles       = roles;
        // these are the actual permission for this user, based on their role
        self.userPermissions = permissions;
        self.teamId          = teamIds[0];

        connector.executeSQL('SELECT can_grade AS "canGrade", can_evaluate AS "canEvaluate", players_can_see_ranks AS "playersCanSeeRanks" FROM team_configuration WHERE go_team_id = $1' , [self.teamId], function(err, teamConfiguration) {

          if (err) {
            console.log('LbAccesstoken: err ', err);
            return next(err);
          }

          if (teamConfiguration[0]) {

            self.teamAccount = {
              canGrade           : teamConfiguration[0].canGrade,
              canEvaluate        : teamConfiguration[0].canEvaluate,
              playersCanSeeRanks : teamConfiguration[0].playersCanSeeRanks
            };

          } else {
            if (user.superUser) {
              self.teamAccount = {
                canGrade    : true,
                canEvaluate : true
              };
            } else {
              return next(new Error('LbAccesstoken: team configuration for the user team was not found'));
            }
          }

          return next();
        }); // connector.executeSQL()
      }); // roleMapping.find()
    }); // User.findById()
  }); // beforeSave

};