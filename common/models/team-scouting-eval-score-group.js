var loopback  = require('loopback');

module.exports = function(TeamScoutingEvalScoreGroup) {

  TeamScoutingEvalScoreGroup.finishEvaluation = function(id, cb) {
    var connector   = TeamScoutingEvalScoreGroup.getDataSource().connector
      , ctx         = loopback.getCurrentContext()
      , accessToken = ctx.get('accessToken');

    console.log('TeamScoutingEvalScoreGroup.finishEvaluation');
    TeamScoutingEvalScoreGroup.findById(id, function(err, group) {
      if (err) {
        console.log('TeamScoutingEvalScoreGroup.finishEvaluation findById() got error ', err);
        return cb(err);
      } else {
        if (group) {
          var now = new Date();
          if (!group.date) {
            group.updateAttribute('date', now, function(err, savedGroup) {
              if (err) {
                console.log('TeamScoutingEvalScoreGroup.finishEvaluation updateAttribute() got error ', err);
                return cb(err);
              } else {
                console.log('TeamScoutingEvalScoreGroup.finishEvaluation updateAttribute() done');
                console.log('TeamScoutingEvalScoreGroup.finishEvaluation updating target scores ...');

                connector.executeSQL('SELECT calculate_evaluation_target_scores($1)', [accessToken.teamId], function (err) {
                  if (err) {
                    console.log('TeamScoutingEvalScoreGroup.finishEvaluation: calculate target scores has error ', err);
                    return cb(err);
                  }

                  console.log('TeamScoutingEvalScoreGroup.finishEvaluation updating target scores done');
                  return cb(null, savedGroup);
                });
              }
            });
          } else {
            return cb(null, group);
          }
        } else {
          console.log('TeamScoutingEvalScoreGroup.saveEvaluationNotes findById did not find any element');
          var error = new Error('Evaluation not found.');
          return cb(error);
        }
      }
    });
  };

  TeamScoutingEvalScoreGroup.playerEvaluationReport = function (_evaluationId,  cb) {
    var connector = TeamScoutingEvalScoreGroup.getDataSource().connector;
    var ctx = loopback.getCurrentContext();
    var accessToken = ctx.get('accessToken');
    console.log('TeamScoutingEvalScoreGroup.playerEvaluationReport');
    var requestQuery = 'SELECT godeepmobile.refresh_player_evaluation($1,$2)';
    connector.executeSQL(requestQuery, [accessToken.teamId, _evaluationId], function (err, records) {
      if (err) {
        console.log('TeamScoutingEvalScoreGroup.playerEvaluationReport: execute has error ', err);
        return cb(err);
      } else {
        var result = null;
        if (records.length) {
          result = records[0].refresh_player_evaluation;
        }
        return cb(null, result);
      }
    });
  };
  TeamScoutingEvalScoreGroup.deleteEvaluation = function (_id,  cb) {
    var connector = TeamScoutingEvalScoreGroup.getDataSource().connector;
    var ctx = loopback.getCurrentContext();
    var accessToken = ctx.get('accessToken');
    console.log('TeamScoutingEvalScoreGroup.deleteEvaluation');
    var requestQuery = 'DELETE FROM godeepmobile.team_scouting_eval_score \
                          WHERE team_scouting_eval_score_group_id = $2 \
                            AND go_team_id = $1;';

    var requestQuery2 = 'DELETE FROM godeepmobile.team_scouting_eval_score_group \
                              WHERE id = $2 \
                                AND go_team_id = $1;';
    connector.executeSQL(requestQuery, [accessToken.teamId, _id], function (err, result) {
      if (err) {
        console.log('TeamScoutingEvalScoreGroup.deleteEvaluation: execute has error ', err);
        return cb(err);
      } else {
        connector.executeSQL(requestQuery2, [accessToken.teamId, _id], function (err, result) {
          if (err) {
            console.log('TeamScoutingEvalScoreGroup.deleteEvaluation: execute has error ', err);
            return cb(err);
          } else {
            console.log('TeamScoutingEvalScoreGroup.deleteEvaluation: done ');
            return cb(null, true);
          }
        });
      }
    });
  };

  TeamScoutingEvalScoreGroup.countNotScoredCriteria = function (_id,  cb) {
    var connector = TeamScoutingEvalScoreGroup.getDataSource().connector;
    var ctx = loopback.getCurrentContext();
    var accessToken = ctx.get('accessToken');
    console.log('TeamScoutingEvalScoreGroup.countNotScoredCriteria');
    var requestQuery = ' SELECT SUM(CASE WHEN eval_score.score IS NULL THEN 1 ELSE 0 END) AS notScoredCriteria \
                        FROM godeepmobile.team_scouting_eval_score eval_score \
                        WHERE eval_score.go_team_id = $1 \
                        AND eval_score.team_scouting_eval_score_group_id = $2 ;';
    connector.executeSQL(requestQuery, [accessToken.teamId, _id], function (err, records) {
      if (err) {
        console.log('TeamScoutingEvalScoreGroup.countNotScoredCriteria: execute has error ', err);
        return cb(err);
      } else {
        var result = null;
        if (records.length) {
          result = records[0];
        }
        return cb(null, result);
      }
    });
  };

  TeamScoutingEvalScoreGroup.createEvaluation = function(data, cb) {
    console.log('TeamScoutingEvalScoreGroup.createEvaluation');
    var connector = TeamScoutingEvalScoreGroup.getDataSource().connector
      , teamId    = loopback.getCurrentContext().get('accessToken').teamId;
    TeamScoutingEvalScoreGroup.create(data, function(err, group) {
      if (err) {
        console.log('TeamScoutingEvalScoreGroup.createEvaluation create() got error ', err);
        return cb(err);
      } else {
        console.log('TeamScoutingEvalScoreGroup.createEvaluation create() done ');
        var query = 'INSERT INTO godeepmobile.team_scouting_eval_score \
                                (go_team_id, team_scouting_eval_criteria_id, team_scouting_eval_score_group_id, go_scouting_eval_criteria_type_id) \
                     SELECT team_eval_criteria.go_team_id, team_eval_criteria.id AS team_scouting_eval_criteria_id, $3 AS team_scouting_eval_score_group_id, team_eval_criteria.go_scouting_eval_criteria_type_id \
                     FROM team_scouting_eval_criteria team_eval_criteria \
                     WHERE team_eval_criteria.go_position_type_id = $2 \
                        AND team_eval_criteria.go_team_id = $1 \
                     ORDER BY team_eval_criteria.go_team_id, team_eval_criteria.go_position_type_id, team_eval_criteria.go_scouting_eval_criteria_type_id, team_eval_criteria.id;';
        console.log('TeamScoutingEvalScoreGroup.createEvaluation executeSQL');
        connector.executeSQL(query, [teamId, group.positionType, group.id], function (err, records) {
          if (err) {
            console.log('TeamScoutingEvalScoreGroup.createEvaluation executeSQL got error:', err);
            console.log('TeamScoutingEvalScoreGroup.createEvaluation executeSQL query:', query);
            return cb(err);
          } else {
            console.log('TeamScoutingEvalScoreGroup.createEvaluation executeSQL done:');
            return cb(null, group);
          }
        });
      }
    });
  };

  TeamScoutingEvalScoreGroup.remoteMethod(
    'finishEvaluation', // local function that implements the RESTful API method
    {
      description: "Finishes player evaluation.",
      notes      : "Finishes player evaluation.",
      accepts    : [
                    {arg: 'id', type: 'Number', required: true, description:"You must pass a TeamScoutingEvalScoreGroup id."}
                    ],
      http       : {path: '/:id/finishEvaluation', verb: 'post'},
      returns    : {arg: 'teamScoutingEvalScoreGroup', type: 'object'}
    }
  );

  TeamScoutingEvalScoreGroup.remoteMethod(
    'playerEvaluationReport', // local function that implements the RESTful API method
    {
      description: "Gets player evaluation summary report.",
      notes      : "Gets player evaluation summary report.",
      accepts    : [
                    {arg: 'id', type: 'Number', required: true, description:"You must pass a TeamScoutingEvalScoreGroup id."}
                    ],
      http       : {path: '/:id/playerEvaluationReport', verb: 'get'},
      returns    : {arg: 'data', type: 'array'}
    }
  );

  TeamScoutingEvalScoreGroup.remoteMethod(
    'createEvaluation', // local function that implements the RESTful API method
    {
      description: "Creates player evaluation.",
      notes      : "Creates player evaluation",
      accepts    : [{arg: 'data', type: 'object', description: 'TeamScoutingEvalScoreGroup', http: {source: 'body'}}],
      http       : {path: '/createEvaluation', verb: 'post'},
      returns    : {arg: 'teamScoutingEvalScoreGroup', type: 'object'}
    }
  );
  TeamScoutingEvalScoreGroup.remoteMethod(
    'countNotScoredCriteria', // local function that implements the RESTful API method
    {
      description: "Gets number of criteria that have not been evaluated yet.",
      notes      : "Gets number of criteria that have not been evaluated yet",
      accepts    : [
                    {arg: 'id', type: 'Number', required: true, description:"You must pass a TeamScoutingEvalScoreGroup id."}
                    ],
      http       : {path: '/:id/countNotScoredCriteria', verb: 'get'},
      returns    : {arg: 'data', type: 'Number'}
    }
  );
  TeamScoutingEvalScoreGroup.remoteMethod(
    'deleteEvaluation', // local function that implements the RESTful API method
    {
      description: "Deletes evaluation with all the scores.",
      notes      : "Deletes evaluation with all the scores.",
      accepts    : [
                    {arg: 'id', type: 'Number', required: true, description:"You must pass a TeamScoutingEvalScoreGroup id."}
                    ],
      http       : {path: '/:id/deleteEvaluation', verb: 'del'},
      returns    : {arg: 'data', type: 'Boolean'}
    }
  );
};
