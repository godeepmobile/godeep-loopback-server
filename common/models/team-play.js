module.exports = function(TeamPlay) {
	TeamPlay.observe('before save', function(ctx, next) {
		// The connector is having troubles when trying to save nested objects
		// so the object is parse to string in order to be included on the query
		console.log('TeamPlay.observe before save');
		if (ctx.data && ctx.data.playData) {
			console.log('TeamPlay.observe before save has playData');
			ctx.data.playData = JSON.stringify(ctx.data.playData);
		}
		next();
	});
};
