var loopback = require('loopback');

module.exports = function(PlayerRankOverall) {
  PlayerRankOverall.updateSeasonRankingStats = function(_season, cb) {
    var connector = PlayerRankOverall.getDataSource().connector;
    var ctx = loopback.getCurrentContext();
    var accessToken = ctx.get('accessToken');
    var requestQuery = 'SELECT godeepmobile.refresh_player_evaluation_season_stat_values($1,$2)';

    console.log('PlayerRankOverall.updateSeasonRankingStats');
    connector.executeSQL(requestQuery, [accessToken.teamId, _season ], function(err, records) {
      if (err) {
        console.log("PlayerRankOverall.updateSeasonRankingStats: refresh_player_evaluation_season_stat_values got error \nquery: %s \nhas error %s\n\n", requestQuery, JSON.stringify(err));
        return cb(err);
      }
      console.log('PlayerRankOverall.updateSeasonRankingStats: refresh_player_evaluation_season_stat_values execute done');
      return cb();
    });
  };

  PlayerRankOverall.remoteMethod(
    'updateSeasonRankingStats',
    {
      description: "Update the season rank statistics for all players for a season",
      accepts: [
        {arg: 'season', type: 'Number', required: true, description:"Season"}
      ],
      http: {path: '/updateSeasonRankingStats', verb: 'post'}
    }
  );
};
