var loopback = require('loopback');
var assert = require('assert');
/*
 Predefined remote methods
 By default, for a model backed by a data source that supports it, LoopBack
 exposes a REST API that provides all the standard create, read, update, and
 delete (CRUD) operations.

 As an example, consider a simple model called Location (that provides business
 locations) to illustrate the REST API exposed by LoopBack.  LoopBack
 automatically creates the following endpoints:
 Model API                        HTTP Method     Example Path
 ============================     ===========     =====================
 create()	                        POST	          /locations
 upsert()	                        PUT	            /locations
 exists()	                        GET	            /locations/:id/exists
 findById()	                      GET	            /locations/:id
 find()	                          GET	            /locations
 findOne()	                      GET	            /locations/findOne
 deleteById()	                    DELETE	        /locations/:id
 count()	                        GET	            /locations/count
 prototype.updateAttributes()	    PUT	            /locations/:id

 The above API follows the standard LoopBack model REST API that most built-in models extend.  See PersistedModel REST API for more details.
 */
module.exports = function(BaseTeamModel) {
  BaseTeamModel.setup = function() {
    var model = this;

    BaseTeamModel.base.setup.apply(this, arguments);

    /*
     filter access based on the current user's associated teamId from the current session
     */
    this.observe('access', function limitToTenant(ctx, next) {
      //console.log('%s.access()',ctx.Model.modelName);
      var context = loopback.getCurrentContext();
      var accessToken = context.get('accessToken');
      assert(accessToken, 'accessToken is not null');
      assert(accessToken.teamId, 'teamId is not null');

      var query = ctx.query;
      //console.log('  accessToken is %o', accessToken);
      //console.log('  query.where is %o', ctx.query);
      if (!query.where) {
        query.where = {};
      }

      // add team id filters on several ways of naming the team id, just in case
      query.where["team"] = accessToken.teamId;
      query.where["go_team_id"] = accessToken.teamId;
      query.where["teamId"] = accessToken.teamId;
      query.where["goTeamId"] = accessToken.teamId;
      //console.log('  new query.where is %o', query);
      next();
    }.bind(this)); // this.observe('access')

    this.observe('before save', function addTeamId(ctx, next) {
      var context = loopback.getCurrentContext();
      var accessToken = context.get('accessToken');

      assert(accessToken, 'accessToken is not null');
      assert(accessToken.teamId, 'teamId is not null');
      var modelName = ctx.Model.modelName;

      // set team id before saving data to database
      if (ctx.Model.settings.teamFilter.addTeam) {
        if (ctx.instance) {
          ctx.instance.team = accessToken.teamId;
        } else {
          ctx.data.team = accessToken.teamId;
        }
      }
      next();
    }.bind(this)); // this.observe('before save')

    this.observe('after save', function removeTeamId(ctx, next) {
      var context = loopback.getCurrentContext();
      var accessToken = context.get('accessToken');

      assert(accessToken, 'accessToken is not null');
      assert(accessToken.teamId, 'teamId is not null');

      // remove team id before returning data to user
      if (ctx.instance) {
        ctx.instance.unsetAttribute('team');
      } else {
        delete ctx.data['team'];
      }
      next();
    }.bind(this)); // this.observe('after save')
/*
    this.observe('before delete', function addTeamId(ctx, next) {
      var context = loopback.getCurrentContext();
      var accessToken = context.get('accessToken');

      assert(accessToken, 'accessToken is not null');
      assert(accessToken.teamId, 'teamId is not null');
      var modelName = ctx.Model.modelName;
      console.log('%s before delete, teamId is %o', modelName, accessToken.teamId);

      // set team id before deleting data from database
      if (ctx.instance) {
        console.log('%s before delete, ctx.instance is %o', modelName, ctx.instance);
        ctx.instance.team = accessToken.teamId;
      } else {
        console.log('%s before delete, ctx.data is %o', modelName, ctx.data);
        ctx.data.team = accessToken.teamId;
      }
      next();
    }.bind(this)); // this.observe('before delete')
*/
  }; // BaseTeamModel.setup()

  BaseTeamModel.setup();
};
