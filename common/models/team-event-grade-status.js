module.exports = function(TeamEventGradeStatus) {
	TeamEventGradeStatus.numCompletedPlays = function(id, cb) {
		var response
		  , connector    = TeamEventGradeStatus.getDataSource().connector
		  , requestQuery = 'SELECT COUNT(DISTINCT team_play_data_id) AS "completedPlays" \
				FROM team_play_grade \
				WHERE team_play_data_id IN (SELECT id FROM team_play_data WHERE team_cutup_id = (SELECT team_cutup_id FROM team_event_grade_status WHERE id = $1)) AND \
					go_user_id = (SELECT go_user_id FROM team_event_grade_status WHERE id = $1)';

		console.log('TeamEventGradeStatus.numCompletedPlays');

		connector.executeSQL(requestQuery, [id], function(err, info) {
			if (err) {
				console.log('TeamEventGradeStatus.numCompletedPlays, execute got error ', err);
				return cb(err);
			}
			console.log('TeamEventGradeStatus.numCompletedPlays execute done');

			if (info[0] && info[0].completedPlays) {
				response = info[0].completedPlays;
			} else {
				response = 0;
				console.log('TeamEventGradeStatus.numCompletedPlays there is no completed plays');
			}

			console.log('TeamEventGradeStatus.numCompletedPlays done');
			cb(null, response);
		});
	};

	TeamEventGradeStatus.remoteMethod('numCompletedPlays', {
		description : 'Get information about the number of plays completed on gradind',
		http        : {path: '/:id/numCompletedPlays', verb: 'get'},
		returns     : {arg: 'completedPlays', type: 'Number'},
		accepts     : [{
			arg: 'id',
			type: 'Number',
			required: true,
			description:"The grade status id"
		}]
	});

};
