var loopback  = require('loopback');
var assert    = require('assert');
var sendEmail = require('../../server/middleware/sendEmail');
var util      = require('util');

module.exports = function(TeamPlayer) {

  var createNewUserAccount = function(player, cb) {
    var UserTeamAssignment = TeamPlayer.app.models.UserTeamAssignment;
    var GoUser           = TeamPlayer.app.models.GoUser;
    player.email     = player.email.toLowerCase().trim();
    console.log('TeamPlayer.observe before save has email, user account will be created');
    var random           = Math.floor(Math.random()*1000) + 1000;
    player.plainPassword = (player.firstName + random).toLowerCase().trim();
    var newGoUser        = {
      email     : player.email,
      username  : player.email,
      password  : player.plainPassword,
      firstName : player.firstName,
      middleName: player.middleName,
      lastName  : player.lastName,
    };

    GoUser.create(newGoUser, function(err, goUser) {
      if (err) {
        console.log('TeamPlayer.observe before save GoUser.create() got error ', err);
        return cb(err);
      } else {
        console.log('TeamPlayer.observe before save GoUser.create() done');
        var context           = loopback.getCurrentContext()
          , teamId            = context.get('accessToken').teamId
          , newTeamAssignment = {
          startDate           : new Date(),
          principalType       : 'USER',
          go_user_role_type_id: 1, // Team player's role
          go_team_id          : teamId,
          go_user_id          : goUser.id
        };
        UserTeamAssignment.create(newTeamAssignment, function(err, teamAssignment) {
          if (err) {
            console.log('TeamPlayer.observe before save UserTeamAssignment.create() got error ', err);
            return cb(err);
          } else {
            console.log('TeamPlayer.observe before save UserTeamAssignment.create() done');
            player.userTeamAssignment = teamAssignment.id;
            return cb(null, teamAssignment);
          }
        });
      }
    });
  };
  var createOrUpdatePlayerAccount = function (player, playerDb, cb) {
    var UserTeamAssignment = TeamPlayer.app.models.UserTeamAssignment;
    var GoUser           = TeamPlayer.app.models.GoUser;
    if (playerDb && playerDb.id) {
      UserTeamAssignment.findOne({
        where : {
            and : [
                {playerId: playerDb.id},
                {endDate : null}
            ]
        }}
        , function(err, teamAssignment) {
        if (err) {
          console.log('TeamPlayer.observe after: Error finding player team assignment by id', err);
          return cb(err);
        } else {

          if (teamAssignment) {
            // the user already has a godeep account, the email is mandatory from now on
            if (player.email) {
              player.email = player.email.toLowerCase().trim();
              var user        = {
                id        : teamAssignment.go_user_id,
                email     : player.email,
                username  : player.email,
                password  : player.plainPassword,
                firstName : player.firstName,
                middleName: player.middleName,
                lastName  : player.lastName
              };

              GoUser.upsert(user, function(err, goUser) {
                if (err) {
                  console.log('TeamPlayer.observe after: Error finding player team assignment by id', err);
                  return cb(err);
                }
              });
              return cb(null, teamAssignment);
            } else {
              return cb(new Error("Email can't be null because this player already has a GoDeep user account"));
            }
          } else {
            // the user does not have a godeep account, if an email was provided then proceed to create one
            if (player.email) {
              createNewUserAccount(player, function(err, assignment) {
                if (err) {
                  console.log('TeamPlayer.observe after: Error finding player team assignment by id', err);
                  return cb(err);
                } else {
                  return cb(null, assignment);
                }
              });
            } else {
              cb();
            }
          }
        }
      });
    } else {
      if (player.email) {
        createNewUserAccount(player, function(err, assignment) {
          if (err) {
            console.log('TeamPlayer.observe after: Error finding player team assignment by id', err);
            return cb(err);
          } else {
            return cb(null, assignment);
          }
        });
      } else {
        return cb(null);
      }
    }
  };

  TeamPlayer.observe('before save', function(ctx, next) {
    var createAssignemnt = false
      , player           = null
      , playerFromDb     = null;

    console.log('TeamPlayer.observe before save');
    if (ctx.instance) {
      console.log('TeamPlayer.observe before save has a new instance');
      player = ctx.instance;
    } else {
      console.log('TeamPlayer.observe before save record is being updated');
      playerFromDb = ctx.currentInstance;
      player       = ctx.data;
      if (player.email) {
        if (playerFromDb.userTeamAssignment) {
          if (ctx.currentInstance.email && player.email !== ctx.currentInstance.email) {
            player.emailChanged = true;
          }
        }
      }
      /*else {
        return next(new Error("Email can't be null because this player already has a GoDeep user account"));
      }*/
    }

    createOrUpdatePlayerAccount(player, playerFromDb, function(err, teamAssignment) {
      if (err) {
        console.log('TeamPlayer.observe before save UserTeamAssignment.create() got error ', err);
        return next(err);
      } else {
        if( teamAssignment ) {
          console.log('TeamPlayer.observe before save UserTeamAssignment.create() done');
          player.userTeamAssignment = teamAssignment.id;
        }
        next();
      }
    });
  });

  TeamPlayer.observe('after save', function(ctx, next) {
    console.log('TeamPlayer.observe after save');
    var to, url, mUrl, subject, mailOptions, slackbot = TeamPlayer.app.slackbot, GoTeam = TeamPlayer.getDataSource().models.Team;
    var player = null;
    if (ctx.instance) {
      console.log('TeamPlayer.observe after save has an instance');
      player = ctx.instance;
    } else {
      console.log('TeamPlayer.observe after save player is being updated');
      player = ctx.data;
    }
    if (player.emailChanged) {
      to      = player.email;
      url     = "http://app.godeepmobile.com";    //TODO get server url
      subject = player.firstName +", Your GoDeep username has been changed!";
      mailOptions = {
        template: 'username-changed',
        context : {
          firstName: player.firstName,
          username : player.email,
          url      : url
        }
      };
      console.log('TeamUser.observe.after save: sending email to user with new username');
      sendEmail(to, subject, mailOptions);
    }

    if (player.plainPassword) {
      console.log('TeamPlayer.observe after save has a new password');
      to      = player.email;
      url     = "http://app.godeepmobile.com";
      mUrl    = 'http://m.godeepmobile.com';
      subject = player.firstName +", GoDeep account created!";
      mailOptions = {
        template: 'player-account-created',
        context : {
          firstName: player.firstName,
          username : player.email,
          password : player.plainPassword,
          url      : url,
          mUrl     : mUrl
        }
      };
      console.log('TeamPlayer.observe after save send email to player account');
      sendEmail(to, subject, mailOptions);

      if (slackbot) {
        GoTeam.findById(loopback.getCurrentContext().get('accessToken').teamId, function(err, team) {
          if (err) {
            console.log('TeamPlayer.observe after save send user notification: An error happened trying to recorver the team:', err);
            return;
          }
          console.log('TeamPlayer.observe after save sending new user notification');
          slackbot.sendNewUserNotification({
            firstName: player.firstName,
            lastName : player.lastName,
            username : player.username,
            email    : player.email,
            team     : team.shortName
          });
        });
      }

      var UserTeamAssignment = TeamPlayer.app.models.UserTeamAssignment;
      UserTeamAssignment.findById(player.userTeamAssignment, function(err, teamAssignment) {
        if (err) {
          console.log('TeamPlayer.observe after: Error finding player team assignment by id', err);
          return next(err);
        }

        if (!teamAssignment) {
          console.log('TeamPlayer.observe after save: No user team assignment returned by find');
          return next(new Error('No user team assignment found'));
        }

        console.log('TeamPlayer.observe after save: ending current team player assignment');
        teamAssignment.updateAttribute('playerId', player.id, function(err, obj) {
          if (err) {
            console.log("TeamPlayer.observe after save: error updating UserTeamAssignment attributes ", err);
            return next(err);
          }
        });
      });
    }
    console.log('TeamPlayer.observe after save done');
    next();
  });

  /*
   * Deletes logically a team player and persist it into the data source.
   */
  TeamPlayer.delete = function(_id, cb) {

    var query1 = 'UPDATE \
                     godeepmobile.team_player \
                  SET \
                     end_date = now() \
                  WHERE go_team_id = $2 \
                    AND id = $1 \
                    AND end_date IS NULL;';
  var query2 = 'UPDATE \
                  go_user_team_assignment \
                SET \
                   end_date = now() \
                FROM (SELECT go_user_team_assignment_id AS id \
                FROM godeepmobile.team_player player \
                WHERE player.id = $1 \
                  AND player.go_team_id = $2 ) AS subquery \
                WHERE go_user_team_assignment.id = subquery.id;';
  var query3 = 'UPDATE \
                  go_user \
                SET \
                   logical_delete = true \
                FROM (SELECT assignment.go_user_id \
                  FROM godeepmobile.team_player player, godeepmobile.go_user_team_assignment assignment \
                  WHERE player.id = $1 \
                    AND assignment.id = player.go_user_team_assignment_id \
                    AND player.go_team_id = $2) AS subquery \
                WHERE go_user.id = subquery.go_user_id;';
    var connector = TeamPlayer.getDataSource().connector
      , response;
    console.log('TeamPlayer.delete');
    var context           = loopback.getCurrentContext()
      , teamId            = context.get('accessToken').teamId;
    connector.executeSQL(query1, [_id, teamId], function(err, records) {
      if (err) {
        console.log('TeamPlayer.delete, execute got error ', err);
        return cb(err);
      } else{
        console.log('TeamPlayer.delete execute done');
        connector.executeSQL(query2, [_id, teamId], function(err, records) {
          if (err) {
            console.log('TeamPlayer.delete, delete assignment execute got error ', err);
            return cb(err);
          } else{
            console.log('TeamPlayer.delete, delete assignment execute done');
            connector.executeSQL(query3, [_id, teamId], function(err, records) {
              if (err) {
                console.log('TeamPlayer.delete, go_user logical delete execute got error ', err);
                return cb(err);
              } else{
                console.log('TeamPlayer.delete, go_user logical delete execute done');
                cb();
              }
            });
          }
        });
      }
    });
  };

  /*
   * Gets players assigned to position type.
   */
  TeamPlayer.playersAssignedToPosition = function(_positionTypeId, cb) {
    var queryCountGroups = 'SELECT COUNT(_group.id) AS "numGroups" \
                            FROM team_position_group _group, team_position_type position \
                            WHERE _group.go_team_id = $2 \
                              AND _group.go_team_id = position.go_team_id \
                              AND _group.id = position.team_position_group_id \
                              AND position.go_position_type_id = $1 \
                              AND position.end_date IS NULL ;';
    var requestQuery = 'SELECT player.id, player.go_team_id AS "teamId", \
                               player.first_name AS "firstName", \
                               player.last_name AS "lastName" \
                          FROM godeepmobile.team_player AS player \
                          WHERE player.go_team_id = $2 \
                              AND player.end_date IS NULL \
                              AND ( \
                                player.team_position_type_id_pos_1 = $1 \
                                OR player.team_position_type_id_pos_2 = $1 \
                                OR player.team_position_type_id_pos_3 = $1 \
                                OR player.team_position_type_id_pos_ST = $1) \
                          ORDER BY player.first_name, player.last_name;';
    var connector = TeamPlayer.getDataSource().connector
      , response;
    console.log('TeamPlayer.playersAssignedToPosition');
    var context           = loopback.getCurrentContext()
      , teamId            = context.get('accessToken').teamId;
    connector.executeSQL(queryCountGroups, [_positionTypeId, teamId], function(err, numGroups) {
      if (err) {
        console.log('TeamPlayer.playersAssignedToPosition, count groups execute got error ', err);
        return cb(err);
      } else{
        console.log('TeamPlayer.playersAssignedToPosition count groups execute done');
        if (numGroups.length && numGroups[0].numGroups == 1) {
          connector.executeSQL(requestQuery, [_positionTypeId, teamId], function(err1, players) {
            if (err1) {
              console.log('TeamPlayer.playersAssignedToPosition, execute got error ', err1);
              return cb(err1);
            } else{
              console.log('TeamPlayer.playersAssignedToPosition execute done');
              cb(null, players);
            }
          });
        } else {
            cb(null, []);
        }
      }
    });
  };

  /*
   * Gets players stat data.
   */
  TeamPlayer.evaluationStats = function(_id, cb) {
    var requestQuery = 'SELECT (SELECT row_to_json(career_stat.*) \
      FROM view_player_evaluation_career_stat career_stat \
      WHERE career_stat.team_player_id = $2 \
         AND career_stat.go_team_id = $1) as career_data, \
      array_to_json(array_agg(row_to_json(seasons))) as seasons_data \
      FROM (  SELECT \
      row_to_json(rank_overall.*) as season_data, \
      ( \
      SELECT array_to_json(array_agg(row_to_json(evaluation))) as evaluations_data \
      FROM ( \
        SELECT users.first_name, users.last_name, eval_stat.*, overall_at_position.go_position_type_id, overall_at_position.rank_at_post, overall_at_position.short_name \
          FROM view_player_evaluation_stat eval_stat, view_active_user_non_players users, view_player_rank_overall_at_position overall_at_position \
          WHERE eval_stat.season = rank_overall.season \
            AND overall_at_position.season = eval_stat.season \
            AND eval_stat.team_player_id = $2 \
            AND eval_stat.team_player_id = overall_at_position.team_player_id \
            AND eval_stat.go_team_id = $1 \
            AND eval_stat.go_team_id = overall_at_position.go_team_id \
            AND eval_stat.team_scouting_eval_score_group_id = overall_at_position.team_scouting_eval_score_group_id \
            AND users.go_team_id = eval_stat.go_team_id \
            AND users.id = eval_stat.go_user_id \
      ) evaluation \
      ) \
      FROM view_player_evaluation_season_stat rank_overall \
      WHERE rank_overall.team_player_id = $2 \
        AND rank_overall.go_team_id = $1 \
      GROUP BY rank_overall.season, rank_overall.* \
      ) seasons;';
    var connector = TeamPlayer.getDataSource().connector
      , response;
    console.log('TeamPlayer.evaluationStats');
    var context           = loopback.getCurrentContext()
      , teamId            = context.get('accessToken').teamId;
    connector.executeSQL(requestQuery, [teamId, _id], function(err, records) {
      if (err) {
        console.log('TeamPlayer.evaluationStats, execute got error ', err);
        return cb(err);
      } else{
        console.log('TeamPlayer.evaluationStats execute done');
        cb(null, records);
      }
    });
  };

  TeamPlayer.upload = function(data, cb) {
    var connector          = TeamPlayer.getDataSource().connector
      , TeamPositionType   = TeamPlayer.app.models.TeamPositionType
      , teamId             = loopback.getCurrentContext().get('accessToken').teamId
      , currentDate        = new Date()
      , currentSeason      = currentDate.getMonth() < 2 ? currentDate.getFullYear() - 1 : currentDate.getFullYear()
      , assignmentClauses  = []
      , statsCreationQuery = ''
      , error
      , recordNumber
      , position2
      , positionST
      , positionsMap       = {'T':'OT', 'CB':'DC', 'SE':'WR', 'G':'OG', 'C':'OC', 'PK':'K'} // used to map some positions that are the same but with different codes
      , positionTypesAbbrs = []
      , queryString        = ''
      , query              = 'WITH player AS (INSERT INTO godeepmobile.team_player (jersey_number,first_name,last_name,height,weight,team_position_type_id_pos_1,team_position_type_id_pos_2,team_position_type_id_pos_st,high_school,go_state_id,birth_date,go_team_id) \
                                     VALUES (%s,\'%s\',\'%s\',%s,%s,\
                                      (SELECT id FROM godeepmobile.go_position_type WHERE short_name = \'%s\'),\
                                      (SELECT id FROM godeepmobile.go_position_type WHERE short_name = %s),\
                                      (SELECT id FROM godeepmobile.go_position_type WHERE short_name = %s),%s,\
                                      (SELECT id FROM godeepmobile.go_state WHERE name = %s),%s,\'' + teamId + '\') RETURNING id,go_team_id,jersey_number,team_position_type_id_pos_1,team_position_type_id_pos_2,team_position_type_id_pos_st) \
                              INSERT INTO godeepmobile.team_player_assignment (team_player_id,go_team_id,jersey_number,position_1,position_2,position_st,start_date) %%__assignment_clauses__%%;';

    console.log('TeamPlayer.upload');

    if (data && data.players && data.players.length > 0 && data.seasons && data.seasons.length > 0) {

      data.seasons.forEach(function(season) {
        if (season === currentSeason) {
          assignmentClauses.push('SELECT *,now() FROM player')
        } else {
          assignmentClauses.push('SELECT *,\'' + season + '-03-01\' FROM player')
        }

        statsCreationQuery += 'SELECT godeepmobile.create_season_stats_for_team(\'' + teamId + '\', ' + season + ');'

      });

      query = query.replace('%%__assignment_clauses__%%', assignmentClauses.join(' UNION '));

      TeamPositionType.find(function(errFindPositionTypes, teamPositionTypes) {
        if (errFindPositionTypes) {
          console.log('TeamPlayer.upload got error, unable to find the team\'s position types', errFindPositionTypes);
          return cb(errFindPositionTypes);
        }

        teamPositionTypes.forEach(function(positionType) {
          positionTypesAbbrs.push(positionType.shortName);
        });

        data.players.every(function(player, index) {
          //Validating required fields
          recordNumber = index + 1;
          if (!player.firstName) {
            console.log('TeamPlayer.upload got error, required field "first_name" was not provided for record %d', recordNumber);
            error = new Error('Required field "First Name" was not provided for player ' + recordNumber);
            return false;
          }

          if (!player.lastName) {
            console.log('TeamPlayer.upload got error, required field "last_name" was not provided for record %d', recordNumber);
            error = new Error('Required field "Last Name" was not provided for player ' + recordNumber);
            return false;
          }

          if (!player.jerseyNumber) {
            console.log('TeamPlayer.upload got error, required field "jersey_number" was not provided for record %d', recordNumber);
            error = new Error('Required field "Jersey No" was not provided for player ' + recordNumber);
            return false;
          } else {
            if (isNaN(player.jerseyNumber)) {
              player.jerseyNumber = null;
            }
          }

          /*if (!player.height) {
            console.log('TeamPlayer.upload got error, required field "height" was not provided for record %d', recordNumber);
            error = new Error('Required field "Height" was not provided for player ' + recordNumber);
            return false;
          }

          if (!player.weight) {
            console.log('TeamPlayer.upload got error, required field "weight" was not provided for record %d', recordNumber);
            error = new Error('Required field "Weight" was not provided for player ' + recordNumber);
            return false;
          }*/

          if (!player.position1) {
            console.log('TeamPlayer.upload got error, required field "position1" was not provided for record %d', recordNumber);
            error = new Error('Required field "Position" was not provided for player ' + recordNumber);
            return false;
          }

          if (positionTypesAbbrs.indexOf(player.position1) === -1) {

            if (positionsMap[player.position1]) {
              // need to be mapped
              player.position1 = positionsMap[player.position1];
            } else {
              console.log('TeamPlayer.upload got error, field "position1" has a wrong value ("%s") for record %d', player.position1, recordNumber);
              error = new Error('Position uploaded in line ' + recordNumber + ' is not valid');
              return false;
            }
          }

          if (player.position2 && player.position2.trim()) {

            if (positionTypesAbbrs.indexOf(player.position2) === -1) {

              if (positionsMap[player.position2]) {
                // need to be mapped
                player.position2 = positionsMap[player.position2];
              } else {
                console.log('TeamPlayer.upload got error, field "position2" has a wrong value ("%s") for record %d', player.position2, recordNumber);
                error = new Error('2nd Position uploaded in line ' + recordNumber + ' is not valid');
                return false;
              }
            }

            position2 = '\'' + player.position2 + '\'';
          } else {
            position2 = 'null';
          }

          if (player.positionST && player.positionST.trim()) {

            if (positionTypesAbbrs.indexOf(player.positionST) === -1) {

              if (positionsMap[player.positionST]) {
                // need to be mapped
                player.positionST = positionsMap[player.positionST];
              } else {
                console.log('TeamPlayer.upload got error, field "positionST" has a wrong value ("%s") for record %d', player.positionST, recordNumber);
                error = new Error('ST Position uploaded in line ' + recordNumber + ' is not valid');
                return false;
              }
            }

            positionST = '\'' + player.positionST + '\'';
          } else {
            positionST = 'null';
          }

          queryString += util.format(query,
            player.jerseyNumber,
            player.firstName.replace(/'/g, "''"),
            player.lastName.replace(/'/g, "''"),
            //player.height.replace(/'/g, "''"),
            //player.weight.replace(/'/g, "''"),
            player.height && player.height.trim() ? '\'' + player.height.replace(/'/g, "''") + '\'' : 'null',
            player.weight && player.weight.trim() ? '\'' + player.weight.replace(/'/g, "''") + '\'' : 'null',
            player.position1,
            position2,
            positionST,
            player.highSchool && player.highSchool.trim() ? '\'' + player.highSchool.replace(/'/g, "''") + '\'' : 'null',
            player.state && player.state.trim() ? '\'' + player.state + '\'' : 'null',
            player.birthDate && player.birthDate.trim() ? '\'' + player.birthDate + '\'' : 'null'
          );

          return true;
        });

        if (error) {
          return cb(error);
        }

        queryString += statsCreationQuery;

        connector.executeSQL(queryString, [], function(err) {
          if (err) {
            console.log('TeamPlayer.upload insert query got error ', err);
            console.log('TeamPlayer.upload insert query is ', queryString);
            return cb(err);
          }

          cb();
        });
      });

    } else {
      if (!data.seasons || data.seasons.length === 0) {
        console.log('TeamPlayer.upload got error, no seasons were provided');
        return cb(new Error('No seasons were provided'));
      }
      console.log('TeamPlayer.upload got error, no players data was provided');
      return cb(new Error('No players data was provided'));
    }
  };

  TeamPlayer.remoteMethod(
    'delete', // local function that implements the RESTful API method
    {
      description: "Delete a team player by id from the data source",
      notes      : "Delete a team player logically",
      accepts    : [
                    {arg: 'id', type: 'string', required: true, description:"You must pass an team player id."}
                    ],
      http       : {path: '/:id/logicalDelete', verb: 'del'},
      returns    : {arg: 'teamPlayer', type: 'object'}
    }
  );

  TeamPlayer.remoteMethod(
    'playersAssignedToPosition', // local function that implements the RESTful API method
    {
      description: "Players assigned to position",
      notes      : "Players assigned to position",
      accepts    : [
                    {arg: 'positionTypeId', type: 'Number', required: true, description:"You must pass a position type id."}
                    ],
      http       : {path: '/playersAssignedToPosition/:positionTypeId/', verb: 'get'},
      returns    : {arg: 'teamPlayer', type: 'object'}
    }
  );

  TeamPlayer.remoteMethod('upload', {
    description : 'Add multiple players to the roster',
    notes       : 'Bulk save for team players',
    http        : {path: '/upload', verb: 'post'},
      returns     : {arg: 'upload', type: 'object'},
    accepts     : [{
      arg: 'data',
      type: 'Object',
      required: true,
      description:"The roster data",
      http: {source: 'body'}
    }]
  });

  TeamPlayer.remoteMethod(
    'evaluationStats', // local function that implements the RESTful API method
    {
      description : 'Gets player evaluation stats',
      notes       : 'player evaluation stats',
      accepts    : [
                    {arg: 'id', type: 'string', required: true, description:"You must pass an team player id."}
                    ],
      http       : {path: '/:id/evaluationStats', verb: 'get'},
      returns    : {arg: 'stat', type: 'object'}
    }
  );
};
