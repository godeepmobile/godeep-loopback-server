var loopback  = require('loopback');

module.exports = function(GoPositionType) {
	GoPositionType.hasEvaluations = function(positionId, cb) {
		var connector = GoPositionType.getDataSource().connector
	      , teamId    = loopback.getCurrentContext().get('accessToken').teamId
	      , query     = 'SELECT count(score.score) > 0 AS "criteriaHasEvaluations" \
	      			FROM godeepmobile.team_scouting_eval_score_group evaluation, godeepmobile.team_scouting_eval_score score \
					WHERE score.team_scouting_eval_score_group_id = evaluation.id AND evaluation.go_position_type_id = $1 AND score.go_team_id = $2';

		connector.executeSQL(query, [positionId, teamId], function(err, result) {
			if (err) {
				console.log('Error: ', err);
        		console.log('There was an error running the query: %s', query);
				return cb(err);
			}

			if (result && result[0]) {
				cb(null, result[0].criteriaHasEvaluations);
			} else {
				console.log('GoPositionType.hasEvaluations - No data was returned to perform the validation');
          		return cb(new Error('No data was returned to perform the validation'));
			}

		});
	};

	GoPositionType.remoteMethod('hasEvaluations', {
		description : 'Checks for evaluations on the desired position',
	    notes       : 'This API is used to return an auxiliar flag to indicate if the position has evaluation scores recorded',
	    http        : {path: '/:id/hasEvaluations', verb: 'get'},
	   	returns     : {arg: 'hasEvaluations', type: 'boolean'},
		accepts     : [{
			arg: 'id',
			type: 'Number',
			required: true,
			description:"The position id"
		}]
	});
};
