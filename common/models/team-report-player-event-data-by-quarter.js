module.exports = function(TeamReportPlayerEventDataByQuarter) {
  var isStatic = true;
  TeamReportPlayerEventDataByQuarter.disableRemoteMethod('create', isStatic);
  TeamReportPlayerEventDataByQuarter.disableRemoteMethod('update', isStatic);
  TeamReportPlayerEventDataByQuarter.disableRemoteMethod('updateAll', isStatic);
  TeamReportPlayerEventDataByQuarter.disableRemoteMethod('upsert', isStatic);
  TeamReportPlayerEventDataByQuarter.disableRemoteMethod('exists', isStatic);
  TeamReportPlayerEventDataByQuarter.disableRemoteMethod('findById', isStatic);
  TeamReportPlayerEventDataByQuarter.disableRemoteMethod('deleteById', isStatic);
  isStatic = false;
  TeamReportPlayerEventDataByQuarter.disableRemoteMethod('updateAttribute', isStatic);
  TeamReportPlayerEventDataByQuarter.disableRemoteMethod('updateAttributes', isStatic);
};
