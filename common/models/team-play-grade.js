var loopback = require('loopback');
var assert = require('assert');
var redisPubClient = require('../../server/redisPubClient');
var emitter = require('socket.io-emitter')

module.exports = function(TeamPlayGrade) {

  // TeamPlayGrade.observe('before save', function(ctx, next) {
  //   // The connector is having troubles when trying to save nested objects
  //   // so the object is parsed to string in order to be included on the query
  //   console.log('TeamPlayGrade.observe before save');
  //   if (ctx.data && ctx.data.customFields) {
  //     console.log('TeamPlayGrade.observe before save has customFields');
  //     ctx.data.customFields = JSON.stringify(ctx.data.customFields);
  //   }
  //   next();
  // });

  /*TeamPlayGrade.observe('after save', function updateGameStats(ctx, next) {
    var modelName = ctx.Model.modelName;
    //var TeamPlayer = TeamPlayGrade.app.models.TeamPlayer;
    var TeamPlay = TeamPlayGrade.app.models.TeamPlay;
    var TeamPlayerGameStat = TeamPlayGrade.app.models.TeamPlayerGameStat;
    var context = loopback.getCurrentContext();
    var accessToken = context.get('accessToken');
    var eventId;
    var play;
    var teamPlayerId;
    var teamPlayId;

    assert(TeamPlay,'TeamPlay is defined');
    assert(context, 'context is defined');
    assert(accessToken, 'accessToken is defined');
    assert(accessToken.teamId, 'teamId is defined');
    if (ctx.instance) {
      teamPlayerId = ctx.instance.teamPlayer;
      teamPlayId = ctx.instance.teamPlay;
    } else {
      teamPlayerId = ctx.data.teamPlayer;
      teamPlayId = ctx.data.teamPlay;
    }

    var connector = TeamPlay.getDataSource().connector;
    var requestQuery = 'SELECT  * from godeepmobile.view_team_play_with_event where id = $1';

    console.log('TeamPlayGrade.observe after save execute query');
    connector.executeSQL(requestQuery, [teamPlayId], function(err, records) {
      if (err) {
        console.log('TeamPlayGrade.observe after save, execute got error ', err);
        return next(err);
      }
      console.log('TeamPlayGrade.observe after save execute done');
      if (!records || records.length < 1) {
        console.log('TeamPlayGrade.observe after save execute query returned 0 plays. bailing.');
        return next(new Error('no plays found'));
      }
      play = records[0];
      eventId = play.team_event_id;

      // after saving grades for a player, make sure that a game stats record exists
      TeamPlayerGameStat.find({where:{teamPlayer:teamPlayerId,teamEvent:eventId}}, function(err, results) {
        if (err) {
          console.log('TeamPlayGrade.observe after save TeamPlayerGameStat.find() got error ', err);
          return next(err);
        }
        if (!results || results.length === 0) {
          // none found
          console.log('TeamPlayGrade.observe after save TeamPlayerGameStat.find(), data not found');
          var stat = {
            teamPlayer: teamPlayerId,
            season: play.season,
            numPlays: 0,
            cat1Grade: 0,
            cat2Grade: 0,
            cat3Grade: 0,
            overallGrade: 0,
            teamEvent: eventId,
            date: play.date
          };
          TeamPlayerGameStat.create(stat, function(err, result) {
            if (err) {
              console.log('TeamPlayGrade.observe after save TeamPlayerGameStat.create() got error ', err);
              return next(err);
            }
            console.log('TeamPlayGrade.observe after save TeamPlayerGameStat.create() done');
            return next();
          });
        } else {
          console.log('TeamPlayGrade.observe after save TeamPlayerGameStat.find() done');
          return next();
        }
      });
    });
  }); // this.observe('after save')*/

  TeamPlayGrade.notifyUpsert = function(gradeStatusId, socketId, cb) {
    var io = emitter(redisPubClient);

    if (gradeStatusId && socketId) {
      console.log('TeamPlayGrade.notifyUpsert sending notification.');
      io.to(socketId).emit('refresh', 'grade::' + gradeStatusId);
      console.log('TeamPlayGrade.notifyUpsert notification sent to ' + socketId);
    }

    cb();
  };

  TeamPlayGrade.remoteMethod('notifyUpsert', {
    description : 'Sends a WS message that grades were saved',
    notes       : 'Notification when saving grades',
    http        : {path: '/notifyUpsert', verb: 'post'},
    accepts     : [{
      arg: 'gradeStatus',
      type: 'number'
    }, {
      arg: 'socketId',
      type: 'string'
    }]
  });
};
