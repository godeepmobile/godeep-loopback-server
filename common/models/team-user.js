var loopback = require('loopback');
var assert = require('assert');
var sendEmail = require('../../server/middleware/sendEmail');

module.exports = function(TeamUser) {

  TeamUser.observe('before save', function(ctx, next) {
    var user   = null;
    var GoUser = TeamUser.app.models.GoUser;
    if (ctx.instance) {
      console.log('TeamUser.observe.before save: got ctx instance');
      user = ctx.instance;
      if (user.email) {
        user.email    = user.email.toLowerCase().trim();
        user.username = user.email;
      }
      var random         = Math.floor(Math.random()*1000) + 1000;
      user.plainPassword = (user.firstName.replace(/\s/g, '') + random).toLowerCase();
      user.password      = GoUser.hashPassword(user.plainPassword);
    } else {
      console.log('TeamUser.observe.before save: got ctx data');
      user = ctx.data;
      if (user.email) {
        user.email = user.email.toLowerCase().trim();
        user.username = user.email;
        if (user.email !== ctx.currentInstance.email) {
          user.emailChanged = true;
        }
      }
    }
    next();
  });

  TeamUser.observe('after save', function(ctx, next) {
    var user = ctx.instance, slackbot = TeamUser.app.slackbot, GoTeam = TeamUser.getDataSource().models.Team;
    var to, url,subject,mailOptions;

    if (user.emailChanged) {
      to      = user.email;
      url     = "http://app.godeepmobile.com";    //TODO get server url
      subject = user.firstName +", Your GoDeep username has been changed!";
      mailOptions = {
        template: 'username-changed',
        context : {
          firstName: user.firstName,
          username : user.username,
          url      : url
        }
      };
      console.log('TeamUser.observe.after save: sending email to user with new username');
      sendEmail(to, subject, mailOptions);
    }
    if (user && user.plainPassword) {
      to      = user.email;
      url     = "http://app.godeepmobile.com";    //TODO get server url
      subject = user.firstName +", GoDeep account created!";
      mailOptions = {
        template: 'account-created',
        context : {
          firstName: user.firstName,
          username : user.username,
          password : user.plainPassword,
          url      : url
        }
      };
      console.log('TeamUser.observe.after save: sending email to user');
      sendEmail(to, subject, mailOptions);

      if (slackbot) {
        GoTeam.findById(loopback.getCurrentContext().get('accessToken').teamId, function(err, team) {
          if (err) {
            console.log('TeamUser.observe after save send user notification: An error happened trying to recorver the team:', err);
            return;
          }
          console.log('TeamUser.observe after save sending new user notification');
          slackbot.sendNewUserNotification({
            firstName: user.firstName,
            lastName : user.lastName,
            username : user.username,
            email    : user.email,
            team     : team.shortName
          });
        });
      }
    }
    next();
  });

  /*
   * Updates role attribute for User instance and persist it into the data source.
   */
  TeamUser.role = function(_id, _roleId, cb) {
    TeamUser.findById(_id, function(err, teamUser) {
      if (err) {
        console.log('TeamUser.role: Error finding team user by id ', err);
        return cb(err);
      }
      var UserTeamAssignment = TeamUser.app.models.UserTeamAssignment;
      if (teamUser) {
        console.log('TeamUser.observe.before save: team user already has a role assigned');
          if (teamUser.role !== _roleId) {
            console.log('TeamUser.observe.before save: team user is changing roles');
            UserTeamAssignment.findById(teamUser.teamAssignment, function(err, teamAssignment) {
              if (err) {
                console.log('TeamUser.role: Error finding user team assignment by id', err);
                return cb(err);
              }
              var endDate = new Date();
              if (!teamAssignment) {
                console.log('TeamUser.role: No user team assignment returned by find');
                return cb(new Error('No user team assignment found'));
              }
              console.log('TeamUser.observe.before save: ending current role assignment');
              teamAssignment.updateAttribute('endDate', endDate, function(err, obj) {
                if (err) {
                  console.log("\nTeamUser.role: error updating UserTeamAssignment attributes ", err);
                  return cb(err);
                }
                console.log('TeamUser.observe.before save: starting new role assignment');
                TeamUser.createNewTeamAssignment(_roleId, teamAssignment.go_team_id, _id,cb);
              });
            });
          }
      } else {
        console.log('TeamUser.observe.before save: team user has no role assigned, assign a new role');
        var context = loopback.getCurrentContext();
        var teamId = context.get('accessToken').teamId;
        TeamUser.createNewTeamAssignment(_roleId, teamId, _id, cb);
      }
      return cb();
    });
  };

  TeamUser.createNewTeamAssignment = function(roleId, teamId, userId, cb) {
    var UserTeamAssignment = TeamUser.app.models.UserTeamAssignment;
    var newTeamAssignment = {
      startDate           : new Date(),
      principalType       : 'USER',
      go_user_role_type_id: roleId,
      go_team_id          : teamId,
      go_user_id          : userId
    };
    UserTeamAssignment.create(newTeamAssignment, function(err, result) {
      if (err) {
        console.log('TeamUser.createNewTeamAssignment: Error creating new role assignment ', err);
        return cb(err);
      }
    });
  };



  /*
   * Updates TeamUserPositionGroupAssignments for User instance and
   * persist it into the data source.
   */
  TeamUser.positionGroupAssignments = function(_id, data, cb) {
    if (_id) {
      var TeamUserPositionGroupAssignment = TeamUser.app.models.TeamUserPositionGroupAssignment;
      TeamUserPositionGroupAssignment.find({
            where : {
                and : [
                    {user   : _id},
                    {endDate: null}
                ]
            }
        }, function(err, groupAssignments) {
          if (err) {
            console.log('TeamUser.positionGroupAssignments: error finding TeamUserPositionGroupAssignment ', err);
            return cb(err);
          }

          if (!groupAssignments) {
            console.log('TeamUser.positionGroupAssignments: no TeamUserPositionGroupAssignment found for team user');
            return cb(new Error('no position group assignment found for team user'));
          }

          var positionGroupsToBeUpdated = groupAssignments.filter(function(value) {
            var index = arrayObjectIndexOf(data, value.teamPositionGroup, 'id');
            return arrayObjectIndexOf(data, value.teamPositionGroup, 'id') === -1;
          });
          var positionGroupsToBeAdded = data.filter(function(value) {
            return arrayObjectIndexOf(groupAssignments, value.id, 'teamPositionGroup') === -1;
          });

          var errors = [];
          var endDate = new Date();
          positionGroupsToBeUpdated.forEach(function(groupAssignment) {
            groupAssignment.updateAttribute('endDate', endDate, function(err, obj) {
              if (err) {
                console.log("\nTeamUser.positionGroupAssignments: TeamUserPositionGroupAssignment updateAttribute has error ", err);
                errors.push("TeamUserPositionGroupAssignment updateAttribute has error: " + err);
              }
            });
          });

          //TODO save a list at once, createUpdates does not work
          var teamId  = loopback.getCurrentContext().get('accessToken').teamId;
          var newTeamPostionsGroups = [];
          positionGroupsToBeAdded.forEach(function(teamPositionGroup) {
            newTeamPostionsGroups.push({
              team             : teamId,
              teamPositionGroup: teamPositionGroup.id,
              user             : _id
            });
          });
          TeamUserPositionGroupAssignment.create(newTeamPostionsGroups, function(err, obj) {
            if (err) {
              console.log("\nTeamUserPositionGroupAssignment create has error \n%j\n", err);
              return cb(err);
            }
            if (errors.length > 0) {
              return cb(new Error(errors.toString()));
            } else {
              cb(null, data);
            }
          });
      });
    } else {
      return cb(new Error('Invalid user id'));
    }
  };

  /*
   * Returns the index of the first occurrence of the specified element in array list,
   * or -1 if this list does not contain the element.
   */
  var arrayObjectIndexOf = function(array, value, property) {
    for(var i = 0, len = array.length; i < len; i++) {
        if (parseInt(array[i][property]) === parseInt(value))
          return i;
    }
    return -1;
  };


  /*
   * Deletes logically a teamUser and persist it into the data source.
   */
  TeamUser.delete = function(_id, cb) {
    var query = 'UPDATE \
                     godeepmobile.go_user \
                  SET \
                     logical_delete = true \
                  WHERE id = $1;';
    var UserTeamAssignment = TeamUser.app.models.UserTeamAssignment;
    UserTeamAssignment.find({
          where : {
              and : [
                  {go_user_id: _id},
                  {endDate   : null}
              ]
          }
      }, function(err, teamAssignments) {
      if (err) {
        console.log('TeamUser.delete Error %j finding user team assignment', err);
        return cb(err);
      } else {
        if (teamAssignments && teamAssignments.length > 0) {
          var teamAssignment = teamAssignments[0]
            ,   endDate      = new Date();
          teamAssignment.updateAttribute('endDate', endDate, function(err, obj) {
            if (err) {
              console.log("TeamUser.delete UserTeamAssignment updateAttribute has error \n%j\n", err);
              return cb(err);
            } else {
              console.log('TeamUser.delete, delete assignment done');
              var connector = TeamUser.getDataSource().connector;
              connector.executeSQL(query, [_id], function(err, records) {
                if (err) {
                  console.log('TeamUser.delete, go_user logical delete execute got error ', err);
                  return cb(err);
                } else{
                  console.log('TeamUser.delete, go_user logical delete execute done');
                  cb();
                }
              });
            }
          });
        } else {
          return cb(new Error('No user team assignment found'));
        }
      }
    });
  };

  TeamUser.remoteMethod(
    'role', // local function that implements the RESTful API method
    {
      description: "Updates role attribute for User instance and persist it into the data source.",
      notes      : "Update User's role information.",
      accepts    : [
                    {arg: 'id', type: 'Number', required: true, description:"You must pass an user id."},
                    {arg: 'role_id', type: 'Number', required: true, description:"You must pass an role id."}
                    ],
      http       : {path: '/:id/role/:role_id', verb: 'put'},
      returns    : {arg: 'role', type: 'object'}
    }
  );

  TeamUser.remoteMethod(
    'positionGroupAssignments', // local function that implements the RESTful API method
    {
      description: "Updates TeamUserPositionGroupAssignments for User instance and persist it into the data source.",
      notes      : "Update User's TeamUserPositionGroupAssignments information.",
      accepts    : [
                    {arg: 'id', type: 'Number', required: true, description:"You must pass an user id."},
                    {arg: 'data', type: 'object', description: 'teamPositionGroup list', http: {source: 'body'}}
                    ],
      http       : {path: '/:id/positionGroupAssignments', verb: 'put'},
      returns    : {arg: 'teamPositionGroup', type: 'object'}
    }
  );

  TeamUser.remoteMethod(
    'delete', // local function that implements the RESTful API method
    {
      description: "Delete a TeamUser by id from the data source",
      notes      : "Delete a TeamUser logically",
      accepts    : [
                    {arg: 'id', type: 'string', required: true, description:"You must pass a TeamUser id."}
                    ],
      http       : {path: '/:id/logicalDelete', verb: 'del'},
      returns    : {arg: 'teamUser', type: 'object'}
    }
  );
};
