var loopback  = require('loopback');

module.exports = function(TeamScoutingEvalScore) {
  TeamScoutingEvalScore.saveScore = function(data, cb) {
    console.log('TeamScoutingEvalScore.saveScore');
    var teamId = loopback.getCurrentContext().get('accessToken').teamId;
    TeamScoutingEvalScore.find({
      where: {
        and: [
          {teamScoutingEvalCriteria   : data.teamScoutingEvalCriteria},
          {scoutingEvalCriteriaType   : data.scoutingEvalCriteriaType},
          {teamScoutingEvalScoreGroup : data.teamScoutingEvalScoreGroup},
          {team                       : teamId}
        ]
      }
    }, function(err, evalScores) {
      if (err) {
        console.log('TeamScoutingEvalScore.saveScore TeamScoutingEvalScore.find', err);
      } else {
        if (evalScores.length) {
          var evalScoreDb = evalScores[0];
          evalScoreDb.updateAttribute('score', data.score, function(err, obj) {
            if (err) {
              console.log('TeamScoutingEvalScore.saveScore error updating score attribute', err);
              return cb(err);
            } else {
              cb(null, obj);
            }
          });
        } else {
          data.team = teamId;
          TeamScoutingEvalScore.create(data, function(err, obj) {
            if (err) {
              console.log('TeamScoutingEvalScore.saveScore TeamScoutingEvalScore.create got error', err);
              return cb(err);
            } else {
              cb(null, obj);
            }
          });
        }
      }
    });
  };

  TeamScoutingEvalScore.remoteMethod(
    'saveScore', // local function that implements the RESTful API method
    {
      description: "Saves evaluation score.",
      notes      : "Saves evaluation score.",
      accepts    : [
                    {arg: 'data', type: 'object', description: 'TeamScoutingEvalScore object', http: {source: 'body'}}
                    ],
      http       : {path: '/saveScore', verb: 'post'},
      returns    : {arg: 'teamScoutingEvalScore', type: 'object'}
    }
  );
};
