module.exports = function(LbUploadStatus) {

	LbUploadStatus.UPLOADED = 'Uploaded';
	LbUploadStatus.IMPORTED = 'Imported';
	LbUploadStatus.FAILED   = 'Failed';
	LbUploadStatus.STORED   = 'Stored'; // Moved to S3 for been stored

};
