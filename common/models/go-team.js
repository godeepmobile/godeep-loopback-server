var loopback = require('loopback');
module.exports = function (Team) {
  Team.remoteMethod(
    'reportAggregateGrades', // local function that implements the RESTful API method
    {
      description: "Get aggregated grades for reporting",
      accepts: [
        {
        arg: 'report',
        type: 'String',
        required: true,
        description: "report: one of game-performance, red-zone or third-down"
      }, {
        arg: 'dimension',
        type: 'String',
        required: true,
        description: "dimension: one of player, position or platoon"
      }, {
        arg: 'dimensionId',
        type: 'String',
        required: true,
        description: "dimension ID"
      }, {
        arg: 'period',
        type: 'String',
        required: true,
        description: "period: one of season or event"
      }, {
        arg: 'periodId',
        type: 'Number',
        required: true,
        description: "period ID"
      }],
      returns: {
        arg: 'data',
        type: 'array'
      },
      http: {
        path: '/reportAggregateGrades',
        verb: 'get'
      }
    }
  ); // remoteMethod()
  Team.reportAggregateGrades = function (_report, _dimension, _dimensionId, _period, _periodId, cb) {
    var connector = Team.getDataSource().connector;
    var ctx = loopback.getCurrentContext();
    var accessToken = ctx.get('accessToken');
    var requestQuery = 'select * from rpt_aggregate_grades($1, $2, $3, $4, $5, $6)';
    console.log('Team.reportAggregateGrades');
    connector.executeSQL(requestQuery, [accessToken.teamId, _report, _dimension, _dimensionId, _period, _periodId], function (err, records) {
      if (err) {
        console.log('Team.reportAggregateGrades: execute has error ', err);
        return cb(err);
      }
      var result;
      if (records && records.length > 0) {
        result = records[0].rpt_aggregate_grades;
      }
      if (result.detailGrades && result.detailGrades.length) {
        console.log('Team.reportAggregateGrades: execute query has %d grades ', result.detailGrades.length);
      } else {
        //result = {};
        console.log('Team.reportAggregateGrades: execute returned no grades');
      }
      return cb(null, result);
    }); // execute()
  }; // TeamPlayerGameCutupStat.updatePlayerCutupStats
};
