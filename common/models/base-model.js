var loopback = require('loopback');
/*
 Predefined remote methods
 By default, for a model backed by a data source that supports it, LoopBack
 exposes a REST API that provides all the standard create, read, update, and
 delete (CRUD) operations.

 As an example, consider a simple model called Location (that provides business
 locations) to illustrate the REST API exposed by LoopBack.  LoopBack automatically 
 creates the following endpoints:
 Model API                        HTTP Method     Example Path
 ============================     ===========     =====================
 create()	                        POST	          /locations
 upsert()	                        PUT	            /locations
 exists()	                        GET	            /locations/:id/exists
 findById()	                      GET	            /locations/:id
 find()	                          GET	            /locations
 findOne()	                      GET	            /locations/findOne
 deleteById()	                    DELETE	        /locations/:id
 count()	                        GET	            /locations/count
 prototype.updateAttributes()	    PUT	            /locations/:id

 The above API follows the standard LoopBack model REST API that most built-in models extend.  See PersistedModel REST API for more details.
 */

module.exports = function(BaseModel) {
  BaseModel.setup = function() {
    var model = this;

    BaseModel.base.setup.apply(this, arguments);

    /*
      modify the remote find():
        1   allow specifying an order by clause that uses loopbackModelPropertyName
            instead of table_column_name.
        2   validate the filter definition and return a 422 status if it is malformed
        3   add a default limit clause if it is not specified
     */
    this.beforeRemote('find', function(ctx, result, next) {
      var query       = ctx.req.query
        , filter      = ctx.args.filter || {}
        , connector   = model.getDataSource().connector
        , order       = query.order || filter.order
        , accessToken = ctx.req.accessToken
        , tableName;

      function fixOrder( original ) {
        var orderSplit = original.split(' ');
        var columnName = connector.columnEscaped(model.modelName, orderSplit[0]);
        var result = (tableName + '.' + columnName).replace(/"/g, '');
        if (orderSplit[1]){
          result = result + ' ' + orderSplit[1];
        }
        return result;
      }

      if (typeof filter === 'string') {
        // When the filter info is sent with loopback's syntax
        try {
          filter = JSON.parse(filter);
        } catch(err) {
          console.log('BaseModel.beforeRemote find JSON.parse() got error: invalid filter');
          var error = new Error('invalid filter');
          error.statusCode = error.status = 422;
          return next(error);
        }
      }

      // Adding default limit if none provided
      filter['limit'] = query.limit || filter.limit ? query.limit || filter.limit : 30;
      if (query.offset) {
        filter['skip'] = query.offset;
      }

      if (order) {
        tableName = connector.tableEscaped(model.modelName);
        if (typeof order === 'string') {
          order = fixOrder( order );
        } else {
          // Is an Array
          order.forEach(function(orderClause, index) {
            order[index] = fixOrder(orderClause);
          });
        }
        filter['order'] = order;
      }
      ctx.args.filter = filter;
      next();
    });

    /*
     if we have a result to return from find(), then
        1    wrap it in a root level object for Ember requirements
        2    add a meta object with available count, and current pagination offset
     */
    this.afterRemote('find', function(ctx, result, next) {
      if (ctx.result) {
        var filter = ctx.args.filter;
        model.count(filter.where ? filter.where : {}, function(err, count) {
          if (err) {
            console.log('BaseModel.afterRemote find count() got error ', err);
            return next(err);
          }
          ctx.result = {
            'meta' : {
              'limit'  : filter && filter.limit ? parseInt(filter.limit) : 0,
              'offset' : filter && filter.skip ? parseInt(filter.skip) : 0,
              'total'  : count
            }
          };
          ctx.result[model.pluralModelName] = result;
          next();
        });
      } else {
        next();
      }
    });

    /*
      if we have a result to return from findById(), then
          wrap it in a root level object for Ember requirements
     */
    this.afterRemote('findById', function(ctx, result, next) {
      if ( result ) {
        ctx.result = {};
        ctx.result[model.modelName] = result;
      }
      next();
    });

  }; // BaseModel.setup()

  BaseModel.setup();
};
