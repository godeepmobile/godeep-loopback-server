module.exports = function(UserRoleType) {
  /*
    if we have a result to return from find(), then
      1    wrap it in a root level object for Ember requirements
      2    add a meta object with available count, and current pagination offset
  */

  UserRoleType.afterRemote('find', function(ctx, result, next) {
    console.log('UserRoleType.afterRemote find');
    if (ctx.result) {
      console.log('UserRoleType.afterRemote find has result');
      var filter = ctx.args.filter;
      UserRoleType.count( (filter && filter.where) ? filter.where : {}, function(err,count) {
        if (err) {
          console.log("UserRoleType.afterRemote find UserRoleType.count() got error ", err);
          return next(err);
        }
        ctx.result = {
          'meta' : {
            'limit'  : filter && filter.limit ? parseInt(filter.limit) : 0,
            'offset' : filter && filter.skip ? parseInt(filter.skip) : 0,
            'total'  : count
          }
        };
        ctx.result[UserRoleType.pluralModelName] = result;
        next();
      });
    } else {
      next();
    }
  });

  /*
    if we have a result to return from findById(), then
      wrap it in a root level object for Ember requirements
  */
  UserRoleType.afterRemote('findById', function(ctx, result, next) {
    console.log('UserRoleType.afterRemote findById');
    if ( result ) {
      console.log('UserRoleType.afterRemote findById has result');
      ctx.result = {};
      ctx.result[UserRoleType.modelName] = result;
    }
    next();
  });
};
