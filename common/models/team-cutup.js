var loopback  = require('loopback');
var util      = require('util');
var sendEmail = require('../../server/middleware/sendEmail');

module.exports = function(TeamCutup) {

	TeamCutup.playsInfo = function(id, cb) {
		var requestQuery = 'SELECT * FROM godeepmobile.view_team_cutup_plays_by_platoon_type_count WHERE team_cutup_id = $1'
		  , connector    = TeamCutup.getDataSource().connector
		  , response;
		console.log('TeamCutup.playsInfo');
		connector.executeSQL(requestQuery, [id], function(err, info) {
			if (err) {
				console.log('TeamCutup.playsInfo, execute got error ', err);
				return cb(err);
			}
			console.log('TeamCutup.playsInfo execute done');
			if (info && info.length) {
				console.log('TeamCutup.playsInfo has team cutup plays');
				response = {
					numPlays               : parseInt(info[0].num_plays),
					offensePlays           : parseInt(info[0].offense_plays),
					defensePlays           : parseInt(info[0].defense_plays),
					specialTeamsPlays      : parseInt(info[0].special_teams_plays),
					unknownUnassignedPlays : parseInt(info[0].unknown_unassigned_plays)
				};
			} else {
				console.log('TeamCutup.playsInfo has no team cutup plays');
				response = {
					numPlays               : 0,
					offensePlays           : 0,
					defensePlays           : 0,
					specialTeamsPlays      : 0,
					unknownUnassignedPlays : 0
				};
			}
			console.log('TeamCutup.playsInfo done');
			cb(null, response);
		});
	};

	TeamCutup.playsData = function(id, data, cb) {
		var connector                = TeamCutup.getDataSource().connector
		  , TeamEvent                = TeamCutup.getDataSource().models.TeamEvent
		  , TeamPlatoonConfiguration = TeamCutup.getDataSource().models.TeamPlatoonConfiguration
		  , teamId                   = loopback.getCurrentContext().get('accessToken').teamId
		  , query                    = 'UPDATE godeepmobile.team_play_data SET game_down = %s, game_distance = %s, game_possession = %s, quarter = %s, yard_line = %s, gsis_participation = %s, data = \'_json_data_\' WHERE play_number = %s AND team_cutup_id = ' + id + ';'
		  , queryString              = ''
		  , headers
		  , changeHeaders
		  , playsData
		  , numRecords
		  , jsonData
		  , queryLine
		  , participatingPlayersColumn
		  , participatingPlayersJerseyNumbers
		  , participatingPlayersValue
		  , gamePossession
		  , teamCutupPlatoon
		  , playNumber
		  , down
		  , distance
		  , quarter
		  , yardLine
		  , error;

		console.log('TeamCutup.playsData');

		if (data && data.playsData && data.headers) {
			headers       = data.headers;
			changeHeaders = data.changeHeaders;
			playsData     = data.playsData;
			numRecords    = playsData.length;

			TeamCutup.findById(id, function(err, teamCutup) {
				if (err) {
					console.log('TeamCutup.playsData error while finding the cutup', err);
					return cb(err);
				}

				teamCutupPlatoon = teamCutup.whichPlatoon;

				TeamEvent.findById(teamCutup.teamEvent, function(err, teamEvent) {
					if (err) {
						console.log('TeamCutup.playsData error while finding the team event', err);
						return cb(err);
					}

					console.log('TeamCutup.playsData process data for %d records', numRecords);

					TeamPlatoonConfiguration.findOne({
						where: {
							and : [
								{team        : teamId},
								{platoonType : teamCutup.whichPlatoon}
							]
						}
					}, function(err, teamPlatoonConfiguration) {
						if (err) {
							console.log('TeamCutup.playsData error while finding the team platoon configuration', err);
							return cb(err);
						}

						if (teamCutupPlatoon === 1) {
							gamePossession = teamEvent.teamShortName || teamEvent.teamName;
						} else if (teamCutupPlatoon === 2) {
							gamePossession = teamEvent.opponentShortName || teamEvent.opponentName;
						}

						participatingPlayersColumn = teamPlatoonConfiguration.gsisParticipationField;

						playsData.every(function(record, index) {

							if (!record[headers.playNumber]) {
								console.log('TeamCutup.playsData got error, required field "play_number" was not provided for record ', record);
					            error = new Error('Play number was not provided for play ' + (index + 2));
					            return false;
							}

							if (isNaN(record[headers.playNumber])) {
								console.log('TeamCutup.playsData got error, required field "play_number" must be a number ', record);
					            error = new Error('Play number is not numeric for play ' + (index + 2));
					            return false;
							} 
							
							down = record[headers.down];

							if (down && down.trim()) {
								if (isNaN(down)) {
									console.log('TeamCutup.playsData got error, field "game_down" must be a number ', record);
					            	error = new Error('Down is not numeric for play ' + (index + 2));
					            	return false;
								}
							} else {
								down = 'null';
							}

							distance = record[headers.distance];

							if (distance && distance.trim()) {
								if (isNaN(distance)) {
									console.log('TeamCutup.playsData got error, field "game_distance" must be a number ', record);
					            	error = new Error('Distance is not numeric for play ' + (index + 2));
					            	return false;
								}
							} else {
								distance = 'null';
							}

							quarter = record[headers.quarter];

							if (quarter && quarter.trim()) {
								if (isNaN(quarter)) {
									console.log('TeamCutup.playsData got error, field "quarter" must be a number ', record);
					            	error = new Error('Quarter is not numeric for play ' + (index + 2));
					            	return false;
								}
							} else {
								quarter = 'null';
							}

							yardLine = record[headers.yardLine];

							if (yardLine && yardLine.trim()) {
								if (isNaN(yardLine)) {
									console.log('TeamCutup.playsData got error, field "yard_line" must be a number ', record);
					            	error = new Error('Field Pos is not numeric for play ' + (index + 2));
					            	return false;
								}
							} else {
								yardLine = 'null';
							}

							participatingPlayersValue = participatingPlayersColumn ? record[participatingPlayersColumn] : null;

							if (participatingPlayersValue) {
								console.log('TeamCutup.playsData found players column');

								participatingPlayersJerseyNumbers = participatingPlayersValue.split(',').filter(Number);

								if (participatingPlayersJerseyNumbers.length !== 11) {
									// The value on that column are not 11 numbers separated by commas
									console.log('TeamCutup.playsData players column value it\'s not a csv');
									participatingPlayersJerseyNumbers = participatingPlayersValue.split(/\[(.*?)\]/).filter(Number);

									if (participatingPlayersJerseyNumbers.length !== 11) {
										// The value on that column are not 11 numbers inside brackets, not considering it
										participatingPlayersJerseyNumbers = null;
										console.log('TeamCutup.playsData players column it\'s not a value between brackets');
									}
								}
							}

							queryLine = util.format(query, 
								down, 
								distance, 
								gamePossession ? ('\'' + gamePossession + '\'') : 'null', 
								quarter,
								yardLine,
								participatingPlayersJerseyNumbers ? 'ARRAY[' + participatingPlayersJerseyNumbers.join(',') + ']' : 'null',
								record[headers.playNumber]);

							// The data non related to the result/situation is stored on a json block
							delete record[headers.down];
							delete record[headers.distance];
							//delete record[headers.possession];
							delete record[headers.quarter];
							delete record[headers.yardLine];
							delete record[headers.playNumber];

							// Escaping single quotes
							jsonData = JSON.stringify(record).replace('\'', '\'\'');

							queryString += queryLine.replace('_json_data_', jsonData);

							return true;
						});

						if (error) {
							return cb(error);
						}

						connector.executeSQL(queryString, [], function(err) {
							if (err) {
								console.log('TeamCutup.playsData update query got error ', err);
								console.log('TeamCutup.playsData update query is ', queryString);
								return cb(err);
							}

							if (changeHeaders) {
								teamPlatoonConfiguration.updateAttribute('playDataFields', headers, function(err) {
									if (err) {
										console.log('TeamCutup.playsData got error when updating the team platoon configuration', err);
										return cb(err);
									}

									console.log('TeamCutup.playsData done');
									return cb();
								});

							} else {
								console.log('TeamCutup.playsData done');
								return cb();
							}
						});

					});

				});

			});

		} else {
			console.log('TeamCutup.playsData got error, no plays data or column headers were provided for cutup', id);
			return cb(new Error('No plays data or column headers were provided for cutup: ' + id));
		}
	};

	TeamCutup.remoteMethod('playsInfo', {
		description : 'Get information about the number of plays by platoon type',
		notes       : 'Includes the amount of plays by platoon type',
		http        : {path: '/:id/playsInfo', verb: 'get'},
		accepts     : [{
			arg: 'id',
			type: 'Number',
			required: true,
			description:"The team cutup id"
		}]
	});

	TeamCutup.remoteMethod('playsData', {
		description : 'Set the the data for each play on the cutup',
		notes       : 'Bulk save for play data',
		http        : {path: '/:id/playsData', verb: 'post'},
	    returns     : {arg: 'playsData', type: 'object'},
		accepts     : [{
			arg: 'id',
			type: 'Number',
			required: true,
			description:"The team cutup id"
		}, {
			arg: 'data',
			type: 'Object',
			required: true,
			description:"The play data",
			http: {source: 'body'}
		}]
	});

	// NOTE(carlos) this hook is no longer needed cause the number of plays is recovered on the store proc in charge of
	// create new plays from the team platoon configuration 

	/*TeamCutup.observe('before save', function(ctx, next) {
		var cutup  = ctx.instance
		  , teamId = loopback.getCurrentContext().get('accessToken').teamId;

		console.log('TeamCutup.observe before save, teamId is ',teamId);
		if (cutup) {
			console.log('TeamCutup.observe before save has an instance proceding to hard code the number of plays');
			// NOTE(carlos) harcoding for UAT, this would make the upload process simpler
			switch (cutup.whichPlatoon) {
				case 3:
					cutup.numPlays = process.env.NUM_PLAYS_ST_CUTUP || 50;
					break;
				case 4:
					cutup.numPlays = process.env.NUM_PLAYS_ALL_CUTUP || 300;
					break;
				default:
					cutup.numPlays = process.env.NUM_PLAYS_NORM_CUTUP || 150;
			}
			console.log('TeamCutup.observe before save the number of plays for the new cutup is ', cutup.numPlays);
			next();
		} else {
			console.log('TeamCutup.observe before save does not have an instance');
			next();
		}
	});*/

	TeamCutup.observe('after save', function(ctx, next) {
		var connector    = TeamCutup.getDataSource().connector
		  , requestQuery = 'SELECT godeepmobile.create_empty_plays_for_event_cutup($1,$2,$3)'
		  , cutup        = ctx.instance
		  , teamId       = loopback.getCurrentContext().get('accessToken').teamId;

		console.log('TeamCutup.observe after save, teamId is ',teamId);
		if (ctx.instance) {
			console.log('TeamCutup.observe after save has an instance');
			connector.executeSQL(requestQuery, [cutup.teamEvent, cutup.id, teamId], function(err) {
				if (err) {
					console.log('TeamCutup.observe after save create empty plays proc got error ', err);
					return next(err);
				}
				console.log('TeamCutup.observe after save execute done');
				next();
			});
		} else {
			console.log('TeamCutup.observe after save does not have an instance');
			next();
		}
	});

	TeamCutup.observe('before delete', function(ctx, next) {
		var connector            = TeamCutup.getDataSource().connector
		  , models               = TeamCutup.getDataSource().models
		  , TeamPlay             = models.TeamPlay
		  , TeamEventGradeStatus = models.TeamEventGradeStatus
		  , requestQuery         = 'SELECT COUNT(*) AS num_grades FROM godeepmobile.team_play_grade grade, godeepmobile.team_play_data play WHERE grade.team_play_data_id = play.id AND play.team_cutup_id = $1;'
		  , cutupId
		  , error;

		console.log('TeamCutup.observe before delete starts checking if cutup has grades');
		if (ctx.where && ctx.where.id) {
			cutupId = ctx.where.id;
			console.log('TeamCutup.observe before delete has an instance');
			connector.executeSQL(requestQuery, [cutupId], function(err, res) {
				if (err) {
					console.log('TeamCutup.observe before delete checking if cutup has grades got error ', err);
					return next(err);
				}

				if (res && res[0]) {
					if (parseInt(res[0].num_grades) > 0) {
						console.log('TeamCutup.observe before delete the cutup has grades');
						error = new Error('It\'s not possible to delete the cutup, there are grades entered for it');
          				error.statusCode = error.status = 422;
          				return next(error);
					}
				}

				// Deleting plays
				TeamPlay.deleteAll({teamCutup : cutupId}, function(err) {
					if (err) {
						console.log('TeamCutup.observe before delete deleting related plays got error ', err);
						return next(err);
					}

					// Deleting grade statuses
					TeamEventGradeStatus.deleteAll({teamCutup : cutupId}, function(err) {
						if (err) {
							console.log('TeamCutup.observe before delete deleting related grade statuses got error ', err);
							return next(err);
						}
						console.log('TeamCutup.observe before delete execute done');
						next();
					});
				});
			});
		} else {
			console.log('TeamCutup.observe before delete does not have an instance');
			next();
		}
	});

    /*
    * Notify players to see report.
    */
    TeamCutup.performanceReportByEvent = function(teamCutupId, teamEventId, cb) {
        console.log('TeamCutup.performanceReportByEvent');
        var requestQuery = 'SELECT player.email, player.first_name as "firstName", player.last_name as "lastName", event.name as "eventName", to_char(event.date, \'MM-DD-YYYY\') as "eventDate" \
                            FROM view_team_player_game_cutup_stat cutup, team_player player, team_event event \
                            WHERE cutup.team_player_id = player.id \
                                 AND player.email IS NOT NULL \
                                 AND cutup.go_team_id = $1 \
                                 AND cutup.team_cutup_id = $2 \
                                 AND event.id = $3;';
        var connector = TeamCutup.getDataSource().connector;
        var context   = loopback.getCurrentContext()
            , teamId  = context.get('accessToken').teamId;
        connector.executeSQL(requestQuery, [teamId, teamCutupId, teamEventId], function(err, players) {
            if (err) {
                console.log('TeamCutup.performanceReportByEvent, execute got error ', err);
                    return cb(err);
            } else {
                console.log('TeamCutup.performanceReportByEvent, execute done');
                if (players.length) {
                    var url = "http://app.godeepmobile.com";
                    var subject;
                    players.forEach(function(player) {
                        subject = 'Your Game Performance Report: '+player.eventName + ' - ' + player.eventDate;
                        var mailOptions = {
                            template: 'player-report-notification',
                            context : {
                                firstName: player.firstName,
                                url      : url
                            }
                        };
                        sendEmail(player.email, subject, mailOptions);
                    });
                    console.log('TeamCutup.performanceReportByEvent, Players notified: ', players);
                    return cb(null, 'Players notified');
                } else {
                    console.log('TeamCutup.performanceReportByEvent, No players to notify');
                    return cb(null, 'No players to notify');
                }
            }
        });
    };

    TeamCutup.remoteMethod(
        'performanceReportByEvent', // local function that implements the RESTful API method
        {
          description: "Send email to players invinting to see their performance reports",
          notes      : "Send email to players",
          accepts    : [
                        {arg: 'id', type: 'Number', required: true, description:"You must pass a teamCutup id."},
                        {arg: 'teamEventId', type: 'Number', required: true, description:"You must pass a teamEvent id."}
                        ],
          http       : {path: '/:id/performanceReportByEvent/:teamEventId/', verb: 'post'},
          returns    : {arg: 'message', type: 'string'}
        }
    );
};
