module.exports = function(TeamConfiguration) {

 TeamConfiguration.observe('before save', function(ctx, next) {
    var teamConf = ctx.data
      , Team     = TeamConfiguration.app.models.Team;

    if (teamConf) {
      console.log('TeamConfiguration.observe before save record is being updated');
      Team.findById(teamConf.team, function(err, team) {
        if (err) {
          console.log("TeamConfiguration.observe before save: error finding team by id: ", err);
          return next(err);
        } else {
          console.log("TeamConfiguration.observe before save: Team.findById() done");
          if (team.numPlayGrades > 0) {
            console.log("TeamConfiguration.observe before save: is not updatable");
            var error = new Error('Team Configuration is not updatable due to Grades have been entered');
            //http status: 405 Method Not Allowed
            error.statusCode = error.status = 405;
            return next(error);
          }
        }
      });
    }
    next();
  });

  TeamConfiguration.updateSettings = function(id, settings, cb) {
    var connector   = TeamConfiguration.getDataSource().connector
      , updateQuery = 'UPDATE team_configuration SET %%__new_settings__%% WHERE id = $1'
      , newSettings = '';

    if (settings && typeof settings.createCutupForEachPlatoon === 'boolean') {
      newSettings += 'create_cutup_for_each_platoon = ' + settings.createCutupForEachPlatoon
    }

    updateQuery = updateQuery.replace('%%__new_settings__%%', newSettings);

    connector.executeSQL(updateQuery, [id], function(err) {
      if (err) {
        console.log('TeamConfiguration.updateSettings, execute got error ', err);
        return cb(err);
      }

      cb();
    });
  };

  TeamConfiguration.remoteMethod('updateSettings', {
    description : 'Update some configuration settings which could be applied eve if the team has grades already. Fields supported: createCutupForEachPlatoon. The format to send the new settings: {createCutupForEachPlatoon : new_value, ...}',
    http        : {path: '/:id/updateSettings', verb: 'put'},
    accepts     : [{
      arg: 'id',
      type: 'Number',
      required: true,
      description:"The configuration id"
    }, {
      arg: 'settings',
      type: 'Object',
      required: true,
      description:"The new settings data",
      http: {source: 'body'}
    }]
  });
};