var assert = require('assert');

module.exports = function(TeamPositionGroup) {

  /*
  * Updates PositionTypes for TeamPositionGroup instance and
  * persist it into the data source.
  */
  TeamPositionGroup.teamPositionTypes = function(_id, data, cb) {
    console.log('TeamPositionGroup.teamPositionTypes');
    TeamPositionGroup.findById(_id, function(err, teamPositionGroup) {
      if (err) {
        console.log('TeamPositionGroup.teamPositionTypes findById() got error ', err);
        return cb(err);
      }
      if (teamPositionGroup) {
        console.log('TeamPositionGroup.teamPositionTypes TeamPositionGroup found');
        var TeamPositionType = TeamPositionGroup.app.models.TeamPositionType;
        assert(TeamPositionType,'TeamPositionType is defined');
        TeamPositionType.find({
              where: {
                  and: [
                      {teamPositionGroup: _id},
                      {endDate          : null},
                      {platoonType      : teamPositionGroup.platoonType}
                  ]
            }
          }, function(err, teamPositionTypes) {
            if (err) {
              console.log('TeamPositionGroup.teamPositionTypes TeamPositionType.find() got error ', err);
              return cb(err);
            }

            var positionTypesToBeDeleted = teamPositionTypes.filter(function(value) {
              return arrayObjectIndexOf(data, value.positionType, 'positionType') === -1;
            });
            var positionTypesToBeAdded = data.filter(function(value) {
              return arrayObjectIndexOf(teamPositionTypes, value.positionType, 'positionType') === -1;
            });
            var errors = [];
            positionTypesToBeDeleted.forEach(function(teamPositionType) {
              var endDate = new Date();
              teamPositionType.updateAttribute('endDate', endDate, function(err, obj) {
                if (err) {
                  console.log('TeamPositionGroup.teamPositionTypes teamPositionType.updateAttribute() got error ', err);
                  errors.push("TeamPositionType updateAttribute has error: " + err);
                }
              });
            });

            TeamPositionType.create(positionTypesToBeAdded, function(err, obj) {
              if (err) {
                console.log('TeamPositionGroup.teamPositionTypes TeamPositionType.create() got error ', err);
                errors.push("TeamPositionType create has error: " + err);
              }

              if (errors.length > 0) {
                return cb(errors.toString());
              } else {
                console.log('TeamPositionGroup.teamPositionTypes does not have errors');
                cb(null, data);
              }
            });
        });
      } else {
        console.log('TeamPositionGroup.teamPositionTypes TeamPositionGroup not found');
        return cb('TeamPositionGroup not found');
      }
    });
  };

  /*
  * Returns the index of the first occurrence of the specified element in array list,
  * or -1 if this list does not contain the element.
  */
  var arrayObjectIndexOf = function(array, value, property) {
    for(var i = 0, len = array.length; i < len; i++) {
        if (parseInt(array[i][property]) === parseInt(value))
          return i;
    }
    return -1;
  };

  /*
   * Deletes logically a teamPositionType and persist it into the data source.
   */
  TeamPositionGroup.delete = function(_id, cb) {
    console.log('TeamPositionGroup.delete');
    TeamPositionGroup.findById(_id, function(err, teamPositionGroup) {
      if (err) {
        console.log('TeamPositionGroup.delete findById() got error ', err);
        return cb(err);
      }
      if (teamPositionGroup) {
        var endDate = new Date();
        teamPositionGroup.updateAttribute('endDate', endDate, function(err, obj) {
          if (err) {
            console.log('TeamPositionGroup.delete updateAttribute() got error ', err);
            return cb(err);
          }
        });
      }
      console.log('TeamPositionGroup.delete done');
      return cb();
    });
  };

  TeamPositionGroup.remoteMethod(
    'teamPositionTypes', // local function that implements the RESTful API method
    {
      description: "Updates TeamPositionTypes for TeamPositionGroup instance and persist it into the data source.",
      notes      : "Updates TeamPositionGroup's TeamPositionTypes information.",
      accepts    : [
                    {arg: 'id', type: 'Number', required: true, description:"You must pass an TeamPositionGroup id."},
                    {arg: 'data', type: 'object', description: 'teamPositionTypes list', http: {source: 'body'}}
                    ],
      http       : {path: '/:id/teamPositionTypes', verb: 'put'},
      returns    : {arg: 'teamPositionTypes', type: 'object'}
    }
  );

  TeamPositionGroup.remoteMethod(
    'delete', // local function that implements the RESTful API method
    {
      description: "Delete a TeamPositionGroup by id from the data source",
      notes      : "Delete a TeamPositionGroup logically",
      accepts    : [
                    {arg: 'id', type: 'string', required: true, description:"You must pass a TeamPositionGroup id."}
                    ],
      http       : {path: '/:id/logicalDelete', verb: 'del'},
      returns    : {arg: 'teamPositionType', type: 'object'}
    }
  );
};
