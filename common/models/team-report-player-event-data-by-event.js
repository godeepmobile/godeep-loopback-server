module.exports = function(TeamReportPlayerEventDataByEvent) {
  var isStatic = true;
  TeamReportPlayerEventDataByEvent.disableRemoteMethod('create', isStatic);
  TeamReportPlayerEventDataByEvent.disableRemoteMethod('update', isStatic);
  TeamReportPlayerEventDataByEvent.disableRemoteMethod('updateAll', isStatic);
  TeamReportPlayerEventDataByEvent.disableRemoteMethod('upsert', isStatic);
  TeamReportPlayerEventDataByEvent.disableRemoteMethod('exists', isStatic);
  TeamReportPlayerEventDataByEvent.disableRemoteMethod('findById', isStatic);
  TeamReportPlayerEventDataByEvent.disableRemoteMethod('deleteById', isStatic);
  isStatic = false;
  TeamReportPlayerEventDataByEvent.disableRemoteMethod('updateAttribute', isStatic);
  TeamReportPlayerEventDataByEvent.disableRemoteMethod('updateAttributes', isStatic);

  TeamReportPlayerEventDataByEvent.observe('access', function dumpQuery(ctx, next) {
    console.log('TeamReportPlayerEventDataByEvent.access: query is ', ctx.query);
    next();
  }.bind(this)); // this.observe('access')


};
