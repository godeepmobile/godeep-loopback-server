module.exports = function(GoOrganization) {

	GoOrganization.afterRemote('create', function(ctx, result, next) {
		var Conference       = GoOrganization.getDataSource().models.Conference
		  , OrganizationType = GoOrganization.getDataSource().models.OrganizationType
		  , slackbot         = GoOrganization.app.slackbot;

		console.log('GoOrganization.afterRemote create, adding conference and organization type');

		if (result && result.organizationType && result.conference) {
			console.log("GoOrganization.afterRemote create: recovering organization type info");

			OrganizationType.findById(result.toJSON().organizationType, function(err, organizationType) {
				if (err) {
		        	console.log("GoOrganization.afterRemote create: error finding organization type by id: ", err);
		        	return next(err);
		        }

		        organizationType = organizationType.toJSON();
		        console.log("GoOrganization.afterRemote create: recovering conference info");

		        Conference.findById(result.toJSON().conference, function(err, conference) {
		        	if (err) {
			        	console.log("GoOrganization.afterRemote create: error finding conference by id: ", err);
			        	return next(err);
			        }

			        conference = conference.toJSON();

			        result.__data['conference'] = {
			        	id          : conference.id,
			        	name        : conference.name,
			        	level       : conference.level,
			        	description : conference.description
			        };
			        result.__data['organizationType'] = {
			        	id   : organizationType.id,
			        	name : organizationType.name
			        };

			        if (slackbot) {
				    	slackbot.sendNewOrganizationNotification(result.toJSON());
				    }

			        next();
		        });
			});
		}
	});
};
