var loopback  = require('loopback');

module.exports = function(TeamScoutingEvalCriteria) {
  TeamScoutingEvalCriteria.observe('before save', function(ctx, next) {
    var criteria = null;
    if (ctx.instance) {
      console.log('TeamScoutingEvalCriteria.observe before save: got ctx instance');
      var query = 'SELECT MAX(sort_index) AS sort_index_max \
                  FROM team_scouting_eval_criteria \
                  WHERE go_team_id = $1 \
                    AND go_position_type_id = $2 \
                    AND go_scouting_eval_criteria_type_id = $3 ;';
      var connector = TeamScoutingEvalCriteria.getDataSource().connector;
      var context = loopback.getCurrentContext();
      var accessToken = context.get('accessToken');
      var evalCriteria = ctx.instance;
      evalCriteria.importance =  evalCriteria.importance || 3; // 3 default importance value

      connector.executeSQL(query, [accessToken.teamId, evalCriteria.positionType, evalCriteria.scoutingEvalCriteriaType],
        function (err, result) {
        if (err) {
          console.log('TeamScoutingEvalCriteria.observe before save: execute has error ', err);
          return next(err);
        } else {
          if(result.length) {
            evalCriteria.sortIndex = result[0].sort_index_max + 1;
          }
          console.log('TeamScoutingEvalCriteria.observe before save: done ');
          next();
        }
      });
    } else {
      next();
    }
  });
};
