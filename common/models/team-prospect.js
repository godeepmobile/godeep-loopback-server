var util     = require('util');
var loopback = require('loopback');

module.exports = function(TeamProspect) {
  TeamProspect.observe('before save', function(ctx, next) {
    if (ctx.instance) {
      console.log('TeamProspect.observe before save has a new instance');
      ctx.instance.isProspect = true;
    }
    next();
  });

  /*
   * Deletes logically a team prospect and persist it into the data source.
   */
  TeamProspect.delete = function(_id, cb) {
    console.log('TeamProspect.delete');
    TeamProspect.findById(_id, function(err, teamProspect) {
      if (err) {
        console.log('TeamProspect.delete findById() got error ', err);
        return cb(err);
      }
      if ( teamProspect ) {
        console.log('TeamProspect.delete TeamProspect found');
        var endDate = new Date();
        teamProspect.updateAttribute('endDate', endDate, function(err, obj) {
          if (err) {
            console.log('TeamProspect.delete updateAttribute got error ', err);
            return cb(err);
          }
          console.log('TeamProspect.delete TeamProspect deleted');
        });
      }
      return cb();
    });
  };

  /*
   * Gets prospect stat data.
   */
  TeamProspect.evaluationStats = function(_id, cb) {
    var requestQuery = 'SELECT (SELECT row_to_json(career_stat.*) \
      FROM view_prospect_evaluation_career_stat career_stat \
      WHERE career_stat.team_prospect_id = $2 \
         AND career_stat.go_team_id = $1) as career_data, \
      array_to_json(array_agg(row_to_json(seasons))) as seasons_data \
      FROM (  SELECT \
      row_to_json(rank_overall.*) as season_data, \
      ( \
      SELECT array_to_json(array_agg(row_to_json(evaluation))) as evaluations_data \
      FROM ( \
        SELECT users.first_name, users.last_name, eval_stat.*, overall_at_position.go_position_type_id, overall_at_position.rank_at_post, overall_at_position.short_name \
          FROM view_prospect_evaluation_stat eval_stat, view_active_user_non_players users, view_prospect_rank_overall_at_position overall_at_position \
          WHERE eval_stat.season = rank_overall.season \
            AND overall_at_position.season = eval_stat.season \
            AND eval_stat.team_prospect_id = $2 \
            AND eval_stat.team_prospect_id = overall_at_position.team_prospect_id \
            AND eval_stat.go_team_id = $1 \
            AND eval_stat.go_team_id = overall_at_position.go_team_id \
            AND eval_stat.team_scouting_eval_score_group_id = overall_at_position.team_scouting_eval_score_group_id \
            AND users.go_team_id = eval_stat.go_team_id \
            AND users.id = eval_stat.go_user_id \
      ) evaluation \
      ) \
      FROM view_prospect_evaluation_season_stat rank_overall \
      WHERE rank_overall.team_prospect_id = $2 \
        AND rank_overall.go_team_id = $1 \
      GROUP BY rank_overall.season, rank_overall.* \
      ) seasons;';
    var connector = TeamProspect.getDataSource().connector
      , response;
    console.log('TeamProspect.evaluationStats');
    var context           = loopback.getCurrentContext()
      , teamId            = context.get('accessToken').teamId;
    connector.executeSQL(requestQuery, [teamId, _id], function(err, records) {
      if (err) {
        console.log('TeamProspect.evaluationStats, execute got error ', err);
        return cb(err);
      } else{
        console.log('TeamProspect.evaluationStats execute done');
        cb(null, records);
      }
    });
  };

  TeamProspect.remoteMethod(
    'delete', // local function that implements the RESTful API method
    {
      description: "Delete a team prospect by id from the data source",
      notes      : "Delete a team prospect logically",
      accepts    : [
                    {arg: 'id', type: 'string', required: true, description:"You must pass an team prospect id."}
                    ],
      http       : {path: '/:id/logicalDelete', verb: 'del'},
      returns    : {arg: 'teamProspect', type: 'object'}
    }
  );

  TeamProspect.upload = function(data, cb) {
    var connector          = TeamProspect.getDataSource().connector
      , TeamPositionType   = TeamProspect.app.models.TeamPositionType
      , teamId             = loopback.getCurrentContext().get('accessToken').teamId
      , positionTypesAbbrs = []
      , positionsMap       = {'T':'OT', 'CB':'DC', 'SE':'WR', 'G':'OG', 'C':'OC', 'PK':'K'} // used to map some positions that are the same but with different codes
      , currentDate        = new Date()
      , currentSeason      = currentDate.getMonth() < 2 ? currentDate.getFullYear() - 1 : currentDate.getFullYear()
      , error
      , recordNumber
      , positionST
      , message
      , queryString  = ''
      , query        = 'WITH prospect AS (INSERT INTO godeepmobile.team_player ( \
                          jersey_number, \
                          first_name, \
                          last_name, \
                          height, \
                          weight, \
                          team_position_type_id_pos_2, \
                          team_position_type_id_pos_1, \
                          high_school, \
                          go_level_id, \
                          team_position_type_id_pos_st, \
                          go_state_id, \
                          birth_date, \
                          is_prospect, \
                          go_team_id) \
                        VALUES ( \
                          %s, \
                          \'%s\', \
                          \'%s\', \
                          \'%s\', \
                          \'%s\', \
                          (SELECT id FROM godeepmobile.go_position_type WHERE short_name = \'%s\'),\
                          (SELECT id FROM godeepmobile.go_position_type WHERE short_name = \'%s\'),\
                          \'%s\',\
                          (SELECT id FROM godeepmobile.go_level WHERE short_name = \'%s\'), \
                          (SELECT id FROM godeepmobile.go_position_type WHERE short_name = %s), \
                          (SELECT id FROM godeepmobile.go_state WHERE name = %s), \
                          %s, \
                          true, \
                          \'' + teamId + '\') RETURNING id,go_team_id,jersey_number,team_position_type_id_pos_1,team_position_type_id_pos_2,team_position_type_id_pos_st, is_prospect) \
                        INSERT INTO godeepmobile.team_player_assignment (team_player_id,go_team_id,jersey_number,position_1,position_2,position_st,is_prospect,start_date) SELECT *,now() FROM prospect UNION SELECT *,\'' + (currentSeason+1) + '-03-01\' FROM prospect;';

    console.log('TeamProspect.upload');

    if (data && data.prospects && data.prospects.length > 0) {

      TeamPositionType.find(function(errFindPositionTypes, teamPositionTypes) {
        if (errFindPositionTypes) {
          console.log('TeamPlayer.upload got error, unable to find the team\'s position types', errFindPositionTypes);
          return cb(errFindPositionTypes);
        }

        teamPositionTypes.forEach(function(positionType) {
          positionTypesAbbrs.push(positionType.shortName);
        });

        data.prospects.every(function(prospect, index) {
          //Validating required fields
          recordNumber = index + 1;
          if (!prospect.firstName) {
            console.log('TeamProspect.upload got error, required field "first_name" was not provided for record %d', recordNumber);
            error = new Error('Required field "First Name" was not provided for prospect ' + recordNumber);
            return false;
          }

          if (!prospect.lastName) {
            console.log('TeamProspect.upload got error, required field "last_name" was not provided for record %d', recordNumber);
            error = new Error('Required field "Last Name" was not provided for prospect ' + recordNumber);
            return false;
          }

          if (!prospect.jerseyNumber) {
            console.log('TeamProspect.upload got error, required field "jersey_number" was not provided for record %d', recordNumber);
            error = new Error('Required field "Jersey No" was not provided for prospect ' + recordNumber);
            return false;
          } else {
            if (isNaN(prospect.jerseyNumber)) {
              if(index === 0) {
                message = 'The first record was discarded because we detected that it was a header row.';
              } else {
                console.log('TeamProspect.upload got error, field "jersey_number" must be a number on record %d', recordNumber);
                error = new Error('Field "Jersey No" is not a number for prospect ' + recordNumber);
                return false;
              }
            }
          }

          if (!prospect.height) {
            console.log('TeamProspect.upload got error, required field "height" was not provided for record %d', recordNumber);
            error = new Error('Required field "Height" was not provided for prospect ' + recordNumber);
            return false;
          }

          if (!prospect.weight) {
            console.log('TeamProspect.upload got error, required field "weight" was not provided for record %d', recordNumber);
            error = new Error('Required field "Weight" was not provided for prospect ' + recordNumber);
            return false;
          }

          if (!prospect.currentPosition) {
            console.log('TeamProspect.upload got error, required field "currentPosition" was not provided for record %d', recordNumber);
            error = new Error('Required field "Current Position" was not provided for prospect ' + recordNumber);
            return false;
          }

          if (positionTypesAbbrs.indexOf(prospect.currentPosition) === -1) {
            if (positionsMap[prospect.currentPosition]) {
              // need to be mapped
              prospect.currentPosition = positionsMap[prospect.currentPosition];
            } else {
              console.log('TeamPlayer.upload got error, field "currentPosition" has a wrong value ("%s") for record %d', prospect.currentPosition, recordNumber);
              error = new Error('Current Position uploaded in line ' + recordNumber + ' is not valid');
              return false;
            }
          }

          if (!prospect.projectedPosition) {
            console.log('TeamProspect.upload got error, required field "projectedPosition" was not provided for record %d', recordNumber);
            error = new Error('Required field "Projected Position" was not provided for prospect ' + recordNumber);
            return false;
          }

          if (positionTypesAbbrs.indexOf(prospect.projectedPosition) === -1) {
            if (positionsMap[prospect.projectedPosition]) {
              // need to be mapped
              prospect.projectedPosition = positionsMap[prospect.projectedPosition];
            } else {
              console.log('TeamPlayer.upload got error, field "projectedPosition" has a wrong value ("%s") for record %d', prospect.projectedPosition, recordNumber);
              error = new Error('Projected Position uploaded in line ' + recordNumber + ' is not valid');
              return false;
            }
          }

          if (!prospect.highSchool) {
            console.log('TeamProspect.upload got error, required field "highSchool" was not provided for record %d', recordNumber);
            error = new Error('Required field "School/Team" was not provided for prospect ' + recordNumber);
            return false;
          }

          if (!prospect.currentLevel) {
            console.log('TeamProspect.upload got error, required field "currentLevel" was not provided for record %d', recordNumber);
            error = new Error('Required field "Current Level" was not provided for prospect ' + recordNumber);
            return false;
          }

          if (!prospect.state) {
            console.log('TeamProspect.upload got error, required field "state" was not provided for record %d', recordNumber);
            error = new Error('Required field "State" was not provided for prospect ' + recordNumber);
            return false;
          }

          if (prospect.positionST && prospect.positionST.trim()) {

            if (positionTypesAbbrs.indexOf(prospect.positionST) === -1) {
              if (positionsMap[prospect.positionST]) {
                // need to be mapped
                prospect.positionST = positionsMap[prospect.positionST];
              } else {
                console.log('TeamPlayer.upload got error, field "positionST" has a wrong value ("%s") for record %d', prospect.positionST, recordNumber);
                error = new Error('ST Position uploaded in line ' + recordNumber + ' is not valid');
                return false;
              }
            }

            positionST = '\'' + prospect.positionST + '\'';
          } else {
            positionST = 'null';
          }

          if (index !== 0 || !message) {
            queryString += util.format(query,
              prospect.jerseyNumber,
              prospect.firstName.replace(/'/g, "''"),
              prospect.lastName.replace(/'/g, "''"),
              prospect.height.replace(/'/g, "''"),
              prospect.weight.replace(/'/g, "''"),
              prospect.currentPosition,
              prospect.projectedPosition,
              prospect.highSchool.replace(/'/g, "''"),
              prospect.currentLevel,
              positionST,
              prospect.state && prospect.state.trim() ? '\'' + prospect.state + '\'' : 'null',
              prospect.birthDate && prospect.birthDate.trim() ? '\'' + prospect.birthDate + '\'' : 'null'
            );
          }

          return true;
        });

        if (error) {
          return cb(error);
        }

        connector.executeSQL(queryString, [], function(err) {
          if (err) {
            console.log('TeamProspect.upload insert query got error ', err);
            console.log('TeamProspect.upload insert query is ', queryString);
            return cb(err);
          }

          if (message) {
            cb(null, {msg: message});
          } else {
            cb();
          }
        });

      });

    } else {
      console.log('TeamProspect.upload got error, no prospects data was provided');
      return cb(new Error('No prospects data was provided'));
    }
  };

  TeamProspect.remoteMethod('upload', {
    description : 'Add multiple prospects',
    notes       : 'Bulk save for team prospects',
    http        : {path: '/upload', verb: 'post'},
      returns     : {arg: 'upload', type: 'object'},
    accepts     : [{
      arg: 'data',
      type: 'Object',
      required: true,
      description:"The prospects data",
      http: {source: 'body'}
    }]
  });

  TeamProspect.remoteMethod(
    'evaluationStats', // local function that implements the RESTful API method
    {
      description : 'Gets prospect evaluation stats',
      notes       : 'player evaluation stats',
      accepts    : [
                    {arg: 'id', type: 'string', required: true, description:"You must pass an team prospect id."}
                    ],
      http       : {path: '/:id/evaluationStats', verb: 'get'},
      returns    : {arg: 'stat', type: 'object'}
    }
  );
};
