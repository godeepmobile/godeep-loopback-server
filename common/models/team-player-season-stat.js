var loopback = require('loopback');

module.exports = function(TeamPlayerSeasonStat) {
  TeamPlayerSeasonStat.updateSeasonStats = function(_season, cb) {
    var connector = TeamPlayerSeasonStat.getDataSource().connector;
    var ctx = loopback.getCurrentContext();
    var accessToken = ctx.get('accessToken');
    var requestQuery = 'SELECT godeepmobile.refresh_season_stats($1,$2)';

    console.log('TeamPlayerSeasonStat.updateSeasonStats');
    connector.executeSQL(requestQuery, [accessToken.teamId, _season ], function(err, records) {
      if (err) {
        console.log("TeamPlayerSeasonStat.updateSeasonStats: refresh_season_stats got error \nquery: %s \nhas error %s\n\n", requestQuery, JSON.stringify(err));
        return cb(err);
      }
      console.log('TeamPlayerSeasonStat.updateSeasonStats: refresh_season_stats execute done');
      return cb();
    }); // execute()
  }; // TeamPlayerSeasonStat.updateSeasonStats

  TeamPlayerSeasonStat.remoteMethod(
    'updateSeasonStats',
    {
      description: "Update the season statistics for all players for a season",
      accepts: [
        {arg: 'season', type: 'Number', required: true, description:"Season"}
      ],
      http: {path: '/updateSeasonStats', verb: 'post'}
    }
  );  // remoteMethod()

};
