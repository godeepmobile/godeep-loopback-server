module.exports = function(TeamEvent) {

	TeamEvent.playsInfo = function(id, cb) {
		var requestQuery = 'SELECT * FROM godeepmobile.view_team_event_plays_by_platoon_type_count WHERE team_event_id = $1'
		  , connector    = TeamEvent.getDataSource().connector
		  , response;

		connector.executeSQL(requestQuery, [id], function(err, info) {
			if (err) {
				console.log('TeamEvent.playsInfo execute got error ', err);
				return cb(err);
			}
			console.log('TeamEvent.playsInfo execute done');
			//TODO: add error checking here, info can be empty.
			response = {
				numPlays               : parseInt(info[0].num_plays),
				offensePlays           : parseInt(info[0].offense_plays),
				defensePlays           : parseInt(info[0].defense_plays),
				specialTeamsPlays      : parseInt(info[0].special_teams_plays),
				unknownUnassignedPlays : parseInt(info[0].unknown_unassigned_plays)
			};
			cb(null, response);
		});
	};

	TeamEvent.remoteMethod('playsInfo', {
		description : 'Get information about the number of plays by platoon type',
		notes       : 'Includes the amount of plays by platoon type',
		http        : {path: '/:id/playsInfo', verb: 'get'},
	    returns     : {arg: 'playsInfo', type: 'object'},
		accepts     : [{
			arg: 'id',
			type: 'Number',
			required: true,
			description:"The team event id"
		}]
	});
};
