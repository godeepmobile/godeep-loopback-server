module.exports = function(TeamReportData) {
  var isStatic = true;
  TeamReportData.disableRemoteMethod('create', isStatic);
  TeamReportData.disableRemoteMethod('update', isStatic);
  TeamReportData.disableRemoteMethod('updateAll', isStatic);
  TeamReportData.disableRemoteMethod('upsert', isStatic);
  TeamReportData.disableRemoteMethod('exists', isStatic);
  TeamReportData.disableRemoteMethod('findById', isStatic);
  TeamReportData.disableRemoteMethod('deleteById', isStatic);
  isStatic = false;
  TeamReportData.disableRemoteMethod('updateAttribute', isStatic);
  TeamReportData.disableRemoteMethod('updateAttributes', isStatic);
};
