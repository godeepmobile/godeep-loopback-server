var loopback = require('loopback');
var assert = require('assert');

module.exports = function(TeamPlayerGameStat) {
  TeamPlayerGameStat.observe('before save', function(ctx, next) {
    var modelName = 'TeamPlayerGameStat';
    if (ctx.instance) {
      console.log('TeamPlayerGameStat.observe before save, setup lastUpdate for new instance');
      ctx.instance.lastUpdate = new Date();
    } else {
      console.log('TeamPlayerGameStat.observe before save, setup lastUpdate for existing instance');
      ctx.data.lastUpdate = new Date();
    }

    next();
  });

  TeamPlayerGameStat.updateEventStats = function(_eventId, cb) {
    var connector = TeamPlayerGameStat.getDataSource().connector;
    var ctx = loopback.getCurrentContext();
    var accessToken = ctx.get('accessToken');
    var requestQuery = 'SELECT godeepmobile.refresh_event_stats($1)';

    console.log('TeamPlayerGameStat.updateEventStats');
    connector.executeSQL(requestQuery, [_eventId], function(err, records) {
      if (err) {
        console.log('TeamPlayerGameStat.updateEventStats refresh_event_stats execute got error ', err);
        return cb(err);
      }
      console.log('TeamPlayerGameStat.updateEventStats refresh_event_stats execute done');
      return cb();
    }); // execute()
  }; // TeamPlayerGameStat.updateEventStats

  TeamPlayerGameStat.remoteMethod(
    'updateEventStats',
    {
      description: "Update the event statistics for all players that participated in an event",
      accepts: [
        {arg: 'eventId', type: 'Number', required: true, description:"Team Event ID"}
      ],
      http: {path: '/updateEventStats', verb: 'post'}
    }
  );  // remoteMethod()
};
