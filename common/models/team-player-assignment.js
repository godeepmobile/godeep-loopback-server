var loopback  = require('loopback');

module.exports = function(TeamPlayerAssignment) {
	/*
   * Deletes logically a team player and persist it into the data source.
   */
	TeamPlayerAssignment.delete = function(_id, isPermanentDelete, cb) {

	    var query1 = isPermanentDelete ? 'UPDATE \
	                     godeepmobile.team_player_assignment \
	                  SET \
	                     end_date = now() \
	                  WHERE go_team_id = $2 \
	                    AND id = $1 \
	                    AND end_date IS NULL;' : 'UPDATE \
	                     godeepmobile.team_player_assignment \
	                  SET \
	                     end_date = now(), \
	                     temporarily_inactive = true \
	                  WHERE go_team_id = $2 \
	                    AND id = $1 \
	                    AND end_date IS NULL;',
	        query2 = 'UPDATE \
	                  go_user_team_assignment \
	                SET \
	                   end_date = now() \
	                FROM (SELECT go_user_team_assignment_id AS id \
	                FROM godeepmobile.team_player_assignment player \
	                WHERE player.id = $1 \
	                  AND player.go_team_id = $2 ) AS subquery \
	                WHERE go_user_team_assignment.id = subquery.id;',
			query3 = 'UPDATE \
	                  go_user \
	                SET \
	                   logical_delete = true \
	                FROM (SELECT assignment.go_user_id \
	                  FROM godeepmobile.team_player_assignment player, godeepmobile.go_user_team_assignment assignment \
	                  WHERE player.id = $1 \
	                    AND assignment.id = player.go_user_team_assignment_id \
	                    AND player.go_team_id = $2) AS subquery \
	                WHERE go_user.id = subquery.go_user_id;',
			connector = TeamPlayerAssignment.getDataSource().connector,
	    	response,
	        context   = loopback.getCurrentContext(),
	    	teamId    = context.get('accessToken').teamId;
	    
	    console.log('TeamPlayerAssignment.delete');

	    connector.executeSQL(query1, [_id, teamId], function(err, records) {
	      if (err) {
	        console.log('TeamPlayerAssignment.delete, execute got error ', err);
	        return cb(err);
	      } else{
	        console.log('TeamPlayerAssignment.delete execute done');
	        connector.executeSQL(query2, [_id, teamId], function(err, records) {
	          if (err) {
	            console.log('TeamPlayerAssignment.delete, delete assignment execute got error ', err);
	            return cb(err);
	          } else{
	            console.log('TeamPlayerAssignment.delete, delete assignment execute done');
	            connector.executeSQL(query3, [_id, teamId], function(err, records) {
	              if (err) {
	                console.log('TeamPlayerAssignment.delete, go_user logical delete execute got error ', err);
	                return cb(err);
	              } else{
	                console.log('TeamPlayerAssignment.delete, go_user logical delete execute done');
	                cb();
	              }
	            });
	          }
	        });
	      }
	    });
	};

	TeamPlayerAssignment.reactivate = function(_id, cb) {
		var query1 = 'UPDATE \
	                     godeepmobile.team_player_assignment \
	                  SET \
	                     end_date = null, \
	                     temporarily_inactive = false \
	                  WHERE go_team_id = $2 \
	                    AND id = $1',
	        query2 = 'UPDATE \
	                  go_user_team_assignment \
	                SET \
	                   end_date = null \
	                FROM (SELECT go_user_team_assignment_id AS id \
	                FROM godeepmobile.team_player_assignment player \
	                WHERE player.id = $1 \
	                  AND player.go_team_id = $2 ) AS subquery \
	                WHERE go_user_team_assignment.id = subquery.id;',
			query3 = 'UPDATE \
	                  go_user \
	                SET \
	                   logical_delete = false \
	                FROM (SELECT assignment.go_user_id \
	                  FROM godeepmobile.team_player_assignment player, godeepmobile.go_user_team_assignment assignment \
	                  WHERE player.id = $1 \
	                    AND assignment.id = player.go_user_team_assignment_id \
	                    AND player.go_team_id = $2) AS subquery \
	                WHERE go_user.id = subquery.go_user_id;',
			connector = TeamPlayerAssignment.getDataSource().connector,
	    	response,
	        context   = loopback.getCurrentContext(),
	    	teamId    = context.get('accessToken').teamId;
	    
	    console.log('TeamPlayerAssignment.reactivate');

	    connector.executeSQL(query1, [_id, teamId], function(err, records) {
	      if (err) {
	        console.log('TeamPlayerAssignment.reactivate, execute got error ', err);
	        return cb(err);
	      } else{
	        console.log('TeamPlayerAssignment.reactivate execute done');
	        connector.executeSQL(query2, [_id, teamId], function(err, records) {
	          if (err) {
	            console.log('TeamPlayerAssignment.reactivate, reactivate assignment execute got error ', err);
	            return cb(err);
	          } else{
	            console.log('TeamPlayerAssignment.reactivate, reactivate assignment execute done');
	            connector.executeSQL(query3, [_id, teamId], function(err, records) {
	              if (err) {
	                console.log('TeamPlayerAssignment.reactivate, go_user logical reactivate execute got error ', err);
	                return cb(err);
	              } else{
	                console.log('TeamPlayerAssignment.reactivate, go_user logical reactivate execute done');
	                cb();
	              }
	            });
	          }
	        });
	      }
	    });
	};

	/*
   * Gets players stat data.
   */
	TeamPlayerAssignment.evaluationStats = function(_id, cb) {
	    var requestQuery = 'SELECT (SELECT row_to_json(career_stat.*) \
	      FROM view_player_evaluation_career_stat career_stat \
	      WHERE career_stat.team_player_id = $2 \
	         AND career_stat.go_team_id = $1) as career_data, \
	      array_to_json(array_agg(row_to_json(seasons))) as seasons_data \
	      FROM (  SELECT \
	      row_to_json(rank_overall.*) as season_data, \
	      ( \
	      SELECT array_to_json(array_agg(row_to_json(evaluation))) as evaluations_data \
	      FROM ( \
	        SELECT users.first_name, users.last_name, eval_stat.*, overall_at_position.go_position_type_id, overall_at_position.rank_at_post, overall_at_position.short_name \
	          FROM view_player_evaluation_stat eval_stat, view_active_user_non_players users, view_player_rank_overall_at_position overall_at_position \
	          WHERE eval_stat.season = rank_overall.season \
	            AND overall_at_position.season = eval_stat.season \
	            AND eval_stat.team_player_id = $2 \
	            AND eval_stat.team_player_id = overall_at_position.team_player_id \
	            AND eval_stat.go_team_id = $1 \
	            AND eval_stat.go_team_id = overall_at_position.go_team_id \
	            AND eval_stat.team_scouting_eval_score_group_id = overall_at_position.team_scouting_eval_score_group_id \
	            AND users.go_team_id = eval_stat.go_team_id \
	            AND users.id = eval_stat.go_user_id \
	      ) evaluation \
	      ) \
	      FROM view_player_evaluation_season_stat rank_overall \
	      WHERE rank_overall.team_player_id = $2 \
	        AND rank_overall.go_team_id = $1 \
	      GROUP BY rank_overall.season, rank_overall.* \
	      ) seasons;';
	    var connector = TeamPlayerAssignment.getDataSource().connector
	      , response;
	    console.log('TeamPlayerAssignment.evaluationStats');
	    var context           = loopback.getCurrentContext()
	      , teamId            = context.get('accessToken').teamId;

	    TeamPlayerAssignment.findById(_id, function(err, teamPlayerAssignment) {
	    	if (err) {
	    		console.log('TeamPlayerAssignment.evaluationStats, find got error ', err);
	    		return cb(err);
	    	}

	    	connector.executeSQL(requestQuery, [teamId, teamPlayerAssignment.teamPlayer], function(err, records) {
			    if (err) {
			    	console.log('TeamPlayerAssignment.evaluationStats, execute got error ', err);
			        return cb(err);
			    } else{
			        console.log('TeamPlayerAssignment.evaluationStats execute done');
			        cb(null, records);
			    }
		    });
	    });
  	};

	TeamPlayerAssignment.remoteMethod(
    	'delete', // local function that implements the RESTful API method
    {
		description: "Delete a team player assignment by id from the data source",
		notes      : "Delete a team player assignment logically",
		accepts    : [
		            {arg: 'id', type: 'string', required: true, description:"You must pass an team player id."},
		            {arg: 'is_permanent', type: 'boolean', required: false}
		            ],
		http       : {path: '/:id/logicalDelete/:is_permanent', verb: 'del'},
		returns    : {arg: 'teamPlayerAssignment', type: 'object'}
    }
  	);

  	TeamPlayerAssignment.remoteMethod(
    	'reactivate', // local function that implements the RESTful API method
    {
		description: "Reactivates a team player assignment",
		notes      : "Reactivates a team player assignment logically",
		accepts    : [{arg: 'id', type: 'string', required: true, description:"You must pass an team player id."}],
		http       : {path: '/:id/reactivate/', verb: 'put'},
		returns    : {arg: 'teamPlayerAssignment', type: 'object'}
    }
  	);

  	TeamPlayerAssignment.remoteMethod(
	    'evaluationStats', // local function that implements the RESTful API method
	    {
	      description : 'Gets player evaluation stats',
	      notes       : 'player evaluation stats',
	      accepts    : [
	                    {arg: 'id', type: 'string', required: true, description:"You must pass an team player id."}
	                    ],
	      http       : {path: '/:id/evaluationStats', verb: 'get'},
	      returns    : {arg: 'stat', type: 'object'}
    });

    /*TeamPlayerAssignment.beforeRemote('delete', function(ctx, next) {
    	next();
    });*/
};
