var loopback = require('loopback');
var sendEmail = require('../../server/middleware/sendEmail');

module.exports = function(GoUser) {
  GoUser.profiles = function(_id, cb) {
    var connector = GoUser.getDataSource().connector;
    var ctx = loopback.getCurrentContext();
    var accessToken = ctx.get('accessToken');
    var Team = this.app.models.Team;
    var team = {};
    var filter = {
      where: {
        id: accessToken.teamId
      }
    };
    var params = [accessToken.userId, accessToken.teamId];
    //TODO move this into a stored proc or a view.
    var requestQuery =
      'SELECT  \
        1 as id\
        ,\
        array ( \
          with positionGroups as ( SELECT tupga.team_position_group_id FROM godeepmobile.team_user_position_group_assignment tupga WHERE tupga.end_date IS NULL AND tupga.go_user_id = $1)	\
          SELECT id FROM (SELECT pos_type.sort_index, tpids.id \
          FROM godeepmobile.go_position_type pos_type, godeepmobile.team_player tpl, \
            (SELECT distinct tp.id \
              FROM \
                godeepmobile.team_position_type tpt, \
                godeepmobile.go_position_type gpt, \
                godeepmobile.team_player tp, \
                positionGroups \
              WHERE \
                tp.go_team_id = $2 AND \
                tp.end_date IS NULL AND \
                tp.is_prospect = false AND \
                tpt.team_position_group_id in (select * from positionGroups) AND \
                tpt.go_position_type_id = gpt.id AND \
                ( \
                  gpt.id = tp.team_position_type_id_pos_1 or \
                  gpt.id = tp.team_position_type_id_pos_2 or \
                  gpt.id = tp.team_position_type_id_pos_3 or \
                  gpt.id = tp.team_position_type_id_pos_st \
                )) tpids \
           WHERE tpids.id = tpl.id AND tpl.team_position_type_id_pos_1 = pos_type.id \
           ORDER BY pos_type.sort_index ASC) x \
        ) \
        AS "teamPlayers"\
        ,\
        array ( \
          SELECT go_position_type_id \
            FROM team_position_type \
            WHERE go_team_id = $2 \
            AND team_position_group_id IN ( \
              SELECT tupga.team_position_group_id \
              FROM godeepmobile.team_user_position_group_assignment tupga \
              WHERE tupga.end_date IS NULL AND tupga.go_user_id = $1) \
        ) \
        AS "positionTypes"\
        ,\
        array(\
          SELECT\
            tupga.team_position_group_id\
          FROM\
            godeepmobile.team_user_position_group_assignment tupga\
          WHERE\
            tupga.go_user_id = $1\
        ) \
        AS "teamPositionGroups"\
        , \
        array(\
        (SELECT row_to_json(row) \
          FROM (\
            SELECT \
              id, \
              go_user_id as "user",\
              user_name as "userName",\
              role as "role",\
              last_submit_date as "lastSubmitDate"\
            FROM \
              godeepmobile.view_active_team_graders g\
            WHERE \
              g.go_team_id = $2 \
          ) row)) \
          AS "teamGraders" \
        ,  \
        array(\
        (SELECT row_to_json(row) \
          FROM (\
            SELECT \
              id, \
              full_name as "fullName",\
              role as "role"\
            FROM \
              godeepmobile.view_active_user_non_players g\
            WHERE \
              g.go_team_id = $2 \
          ) row)) \
          AS "teamUsers" \
        ,  \
        (SELECT row_to_json(row) \
          FROM (\
            SELECT \
              tc.id, tc.go_team_id as team,\
              tc.grade_cat1_label "gradeCat1Label", tc.grade_cat2_label "gradeCat2Label", \
              tc.grade_cat3_label "gradeCat3Label", tc.grade_base as "gradeBase", \
              tc.grade_increment "gradeIncrement", \
              tc.num_grade_categories "numGradeCategories", \
              tc.scout_eval_score_min "scoutEvalScoreMin", \
              tc.scout_eval_score_max "scoutEvalScoreMax", \
              tc.can_evaluate "canEvaluate", \
              tc.create_cutup_for_each_platoon "createCutupForEachPlatoon", \
              tc.players_can_see_ranks "playersCanSeeRanks", \
              tc.players_can_see_averages "playersCanSeeAverages", \
              tc.grade_fields "gradeFields" \
            FROM \
              godeepmobile.team_configuration tc\
            WHERE \
              tc.go_team_id = $2 \
          ) row) \
          AS "teamConfiguration" \
        ,  \
         array(\
            SELECT \
              pc.id \
            FROM \
              godeepmobile.team_platoon_configuration pc\
            WHERE \
              pc.go_team_id = $2 \
          ) \
          AS "teamPlatoonConfiguration" \
        , \
        array(SELECT row_to_json(y) FROM ( \
          SELECT id, name, description FROM go_user_role_type \
          WHERE id IN (SELECT uta.go_user_role_type_id \
            FROM go_user_team_assignment uta \
            WHERE uta.go_team_id = $2 AND uta.go_user_id = $1) \
        ) y) AS "teamRoles" \
        %%__player_assignments_query__%% \
        ;';

    /*
     (SELECT row_to_json(row) \
     FROM (\
     SELECT\
     gt.id, go.name, gt.short_name as "shortName", gt.mascot, gt.go_conference_id as "conference", gt.abbreviation as "abbreviation", \
     tc.grade_cat1_label "cat1Label", tc.grade_cat2_label "cat2Label", tc.grade_cat3_label "cat3Label", tc.grade_base as "gradeBase", tc.grade_increment "gradeIncrement",\
     gt.asset_base_url as "assetBaseUrl"\
     FROM\
     go_organization go, \
     go_team gt, \
     team_configuration tc \
     WHERE \
     gt.id = $2 AND \
     gt.go_organization_id = go.id AND \
     gt.id = tc.go_team_id \
     ) row) \
     AS "team" \
     , \

     */

    if (accessToken && accessToken.playerId) {
    // if the user is a player, his seasons on the team must be included, otherwise no
    requestQuery = requestQuery.replace('%%__player_assignments_query__%%', ',array(SELECT row_to_json(s) FROM (\
      SELECT \
        assignment.id, \
        assignment.jersey_number AS "jerseyNumber", \
        assignment.season, \
        assignment.position_1 AS "position1", \
        assignment.position_2 AS "position2", \
        assignment.position_3 AS "position3", \
        assignment.position_st AS "positionST", \
        assignment.team_player_id AS "teamPlayer", \
        assignment.temporarily_inactive AS "temporarilyInactive", \
        assignment.go_team_id AS "team", \
        assignment.is_special_teams AS "isSpecialTeams", \
        assignment.is_offense AS "isOffense", \
        assignment.is_defense AS "isDefense", \
        assignment.start_date AS "startDate", \
        assignment.end_date AS "endDate" \
      FROM \
        godeepmobile.view_team_player_assignment assignment\
      WHERE \
        assignment.go_team_id = $2 AND assignment.team_player_id = $3 \
      ) s) \
      AS "teamPlayerAssignments"');
      
      params.push(accessToken.playerId);
      
    } else {
      requestQuery = requestQuery.replace('%%__player_assignments_query__%%', '');
    }

    console.log("GoUser.profiles execute query");
    connector.executeSQL(requestQuery, params, function(err, records) {
      if (err) {
        console.log("GoUser.profiles \n****************\nquery: %s \n***************\nhas error %o\n****************\n", requestQuery, err);
        return cb(err);
      }
      console.log("GoUser.profiles execute done");
      if (!records || records.length < 1) {
        console.log('GoUser.profiles: no records returned. bailing.');
        return cb(new Error('no records returned for GoUser.profiles.'));
      }
      var response = records[0];
      Team.findById(accessToken.teamId, function(err, team) {
        if (err) {
          console.log("GoUser.profiles: error finding team by id: ", err);
          return cb(err);
        }
        console.log("GoUser.profiles Team.findById() done");
        response.team = team;
        cb(null, response);
      });
    });
  };

  GoUser.remoteMethod(
    'profiles', // local function that implements the RESTful API method
    {
      description: "Get profile information about current user",
      notes: "Profile information includes team configuration properties, and players in grading groups.",
      accepts: [{
        arg: 'id',
        type: 'Number',
        required: true,
        description: "You must pass an id, but it is currently ignored. pass 1 just to be safe."
      }],
      http: {
        path: '/profiles/:id',
        verb: 'get'
      },
      returns: {
        arg: 'profile',
        type: 'object'
      }
    }
  );

  GoUser.question = function(_q, cb) {
    var userId = loopback.getCurrentContext().get('accessToken').userId
      , teamId = loopback.getCurrentContext().get('accessToken').teamId
      , user
      , mailOptions
      , name
      , userEmail
      , teamName
      , team;

    if (_q && _q.text) {

      GoUser.findById(userId, function(err, user) {
        if (err) {
          return cb(err);
        }

        GoUser.getDataSource().models.Team.findById(teamId, function(err, team) {

          if (err) {
            return cb(err);
          }

          name      = user.firstName + ' ' + user.lastName;
          userEmail = user.email;
          teamName  = team.shortName + ' ' + team.mascot;
          mailOptions = {
            template: 'ask-question',
            context : {
              name     : name,
              team     : teamName,
              email    : userEmail,
              question : _q.text
            }
          };

          console.log('User %s asked a question', user.id);
          sendEmail('support@godeepmobile.com', 'Question from ' + name + ' - ' + teamName, mailOptions);

          cb();
        });
      });
    } else {
      return cb(new Error('No question was sent'));
    }

  };

  GoUser.remoteMethod('question', {
    description : 'Send a question from the user to the GodeepMobile team',
    notes       : 'The question is sent by email',
    http        : {path: '/question', verb: 'post'},
    returns     : {arg: 'question', type: 'object'},
    accepts     : [{
      arg: 'text',
      type: 'Object',
      required: true,
      description:"The question text",
      http: {source: 'body'}
    }]
  });


  /*
    if we have a result to return from find(), then
      1    wrap it in a root level object for Ember requirements
      2    add a meta object with available count, and current pagination offset
  */

  GoUser.afterRemote('find', function(ctx, result, next) {
    if (ctx.result) {
      console.log('GoUser.afterRemote.find: has result');
      var filter = ctx.args.filter;
      GoUser.count((filter && filter.where) ? filter.where : {}, function(err, count) {
        if (err) {
          console.log("GoUser.afterRemote.find: GoUser.count() got error ", err);
          return next(err);
        }
        console.log('GoUser.afterRemote.find: GoUser.count() done');
        ctx.result = {
          'meta': {
            'limit': filter && filter.limit ? parseInt(filter.limit) : 0,
            'offset': filter && filter.skip ? parseInt(filter.skip) : 0,
            'total': count
          }
        };
        ctx.result[GoUser.pluralModelName] = result;
        next();
      });
    } else {
      console.log('GoUser.afterRemote.find: does not have result');
      next();
    }
  });

  /*
    if we have a result to return from findById(), then
      wrap it in a root level object for Ember requirements
  */
  GoUser.afterRemote('findById', function(ctx, result, next) {
    console.log('GoUser.afterRemote findById');
    if (result) {
      console.log('GoUser.afterRemote findById has result');
      ctx.result = {};
      ctx.result[GoUser.modelName] = result;
    }
    next();
  });

  GoUser.observe('before save', function lowercaseUsername(ctx, next) {
    if (ctx.instance) {
      console.log('GoUser.observe before save has an instance');
      if (ctx.instance.username) {
        ctx.instance.username = ctx.instance.username.toLowerCase().trim();
      }
      if (ctx.instance.email) {
        ctx.instance.email    = ctx.instance.email.toLowerCase().trim();
      }
    } else {
      console.log('GoUser.observe before save has a ctx.data');
      if (ctx.data.username) {
        ctx.data.username = ctx.data.username.toLowerCase().trim();
      }
      if (ctx.data.email) {
        ctx.data.email = ctx.data.email.toLowerCase().trim();
      }
    }
    next();
  });

  GoUser.prototype.isSuperuser = function() {
    return this.superUser;
  };

  /*GoUser.afterRemote('login', function(ctx, result, next) {
    var slackbot = GoUser.app.slackbot
      , user     = result.toJSON().user;

    console.log('GODEEPBOT. Sending login notification..')
    if (slackbot) {
      slackbot.send(user.firstName + ' ' + user.lastName + ' (' + user.email + ') has logged in.');
      console.log('GODEEPBOT. Login notification sent')
    }

    next();
  });*/
};
