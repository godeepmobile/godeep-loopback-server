var loopback  = require('loopback');
var util      = require('util');

module.exports = function(TeamReport) {

  TeamReport.run = function(type, filter, criteria, criteriaRange, scope, scopeRange, condition, conditionRange, cb) {
    var response                       = {}
      , connector                      = TeamReport.getDataSource().connector
      , teamId                         = loopback.getCurrentContext().get('accessToken').teamId
      , jerseyNumberFromTeamAssignment = '(SELECT jersey_number FROM view_team_player_assignment WHERE season = %%__team_player_assignment_season__%% AND team_player_id = %%__team_player_assignment_player_id__%%) AS "jerseyNumber"'
      , positionFromTeamAssignment     = '(SELECT (SELECT short_name FROM go_position_type WHERE id=position_1) FROM view_team_player_assignment WHERE season = %%__team_player_assignment_season__%% AND team_player_id = %%__team_player_assignment_player_id__%%) AS "position"'
      , baseQuery                      = 'SELECT \
          (%%__graph_query__%%) AS "graphDetails", \
          row_to_json((SELECT z FROM (SELECT \
              ROUND(AVG(grade.cat_1_grade), 2) AS "avgCat1Grade", \
              ROUND(AVG(grade.cat_2_grade), 2) AS "avgCat2Grade", \
              ROUND(AVG(grade.cat_3_grade), 2) AS "avgCat3Grade", \
              ROUND(AVG(grade.cat_1_grade + COALESCE(grade.cat_2_grade, 0) + COALESCE(grade.cat_3_grade, 0)) / (SELECT num_grade_categories FROM team_configuration WHERE go_team_id = $1)::numeric, 2) AS "avgOverallGrade" \
            FROM \
              team_play_grade grade, team_play_data play, team_event event \
            WHERE \
              grade.team_play_data_id = play.id AND \
              play.team_event_id = event.id AND \
              %%__report_overalls_clause__%% \
          ) z)) AS overalls, \
          (SELECT array_to_json(array_agg(t ORDER BY (overalls->>\'avgOverallGrade\')::numeric DESC)) FROM \
          (SELECT \
            team_player.id, \
            team_player.first_name AS "firstName", \
            team_player.last_name AS "lastName", \
            ' + jerseyNumberFromTeamAssignment.replace('%%__team_player_assignment_player_id__%%', 'team_player.id') + ', \
            ' + positionFromTeamAssignment.replace('%%__team_player_assignment_player_id__%%', 'team_player.id') + ', \
            event_details.events, \
            row_to_json((SELECT z FROM (SELECT \
                COUNT(*) AS "numPlays", \
                ROUND(AVG(grade.cat_1_grade), 2) AS "avgCat1Grade", \
                ROUND(AVG(grade.cat_2_grade), 2) AS "avgCat2Grade", \
                ROUND(AVG(grade.cat_3_grade), 2) AS "avgCat3Grade", \
                ROUND(AVG(grade.cat_1_grade + COALESCE(grade.cat_2_grade, 0) + COALESCE(grade.cat_3_grade, 0)) / (SELECT num_grade_categories FROM team_configuration WHERE go_team_id = $1)::numeric, 2) AS "avgOverallGrade", \
                CASE WHEN (SELECT grade_fields->\'specialPlay\' FROM team_configuration WHERE go_team_id = $1) IS NOT NULL \
                  THEN replace((SELECT string_agg(val || \': \' || cant, \', \') FROM (SELECT custom_fields->\'specialPlay\' AS val, COUNT(*) AS cant FROM team_play_grade WHERE id = ANY(array_agg(grade.id)) GROUP BY val) sp WHERE val <> \'null\'), \'"\', \'\') \
                END AS "specialPlay" \
              FROM \
                team_play_grade grade, team_play_data play, team_event event \
              WHERE \
                grade.team_player_id = team_player.id AND \
                grade.team_play_data_id = play.id AND \
                play.team_event_id = event.id AND \
                %%__report_clauses__%% \
            ) z)) AS overalls \
          FROM \
            (SELECT  \
              grade_details.player_id, \
              json_agg((SELECT y FROM (SELECT grade_details."eventId", _event.name, _event.date, grade_details."overalls", grade_details.grades) y) ORDER BY _event.date) AS events \
            FROM \
              (SELECT  grade.team_player_id AS "player_id", \
                event.id AS "eventId", \
                row_to_json((SELECT o FROM (SELECT COUNT(grade) AS "numPlays", \
                ROUND(AVG(grade.cat_1_grade), 2) AS "avgCat1Grade", \
                ROUND(AVG(grade.cat_2_grade), 2) AS "avgCat2Grade", \
                ROUND(AVG(grade.cat_3_grade), 2) AS "avgCat3Grade", \
                ROUND(AVG(grade.cat_1_grade + COALESCE(grade.cat_2_grade, 0) + COALESCE(grade.cat_3_grade, 0)) / (SELECT num_grade_categories FROM team_configuration WHERE go_team_id = $1)::numeric, 2) AS "avgOverallGrade") o)) AS "overalls", \
                json_agg((SELECT x FROM ( \
                  SELECT play.play_number AS "playNumber", \
                  play.quarter, \
                  play.game_down || \'/\' || play.game_distance AS "dnDist", \
                  play.yard_line AS "yardLine", \
                  (SELECT name FROM go_play_result_type WHERE id=play.go_play_result_type_id) AS "resultType", \
                  (SELECT short_name FROM go_position_type WHERE id=grade.position_played) as "position", \
                  (SELECT name FROM go_platoon_type WHERE id=grade.go_platoon_type_id) as "platoon", \
                  grade.cat_1_grade AS "cat1Grade", \
                  grade.cat_2_grade AS "cat2Grade", \
                  grade.cat_3_grade AS "cat3Grade", \
                  round((grade.cat_1_grade + COALESCE(grade.cat_2_grade, 0) + COALESCE(grade.cat_3_grade, 0)) / (SELECT num_grade_categories FROM team_configuration WHERE go_team_id = $1)::numeric, 2) AS "overallGrade", \
                  (SELECT name FROM go_play_factor WHERE id=grade.go_play_factor_id) AS "factorInPlay", \
                  CASE WHEN (SELECT grade_fields->\'specialPlay\' FROM team_configuration WHERE go_team_id = $1) IS NOT NULL AND grade.custom_fields->\'specialPlay\' <> \'null\' \
                    THEN replace((grade.custom_fields->\'specialPlay\')::text, \'"\', \'\') \
                  END AS "specialPlay", \
                  %%__grade_notes__%% AS notes \
                ) x) ORDER BY play.play_number) AS grades \
              FROM \
                team_play_grade grade, team_play_data play, team_event event \
              WHERE \
                grade.team_play_data_id = play.id AND \
                play.team_event_id = event.id AND \
                %%__report_clauses__%% \
              GROUP BY "player_id", "eventId" \
              ORDER BY "player_id", event.date) grade_details, \
              team_player player, \
              team_event _event \
            WHERE \
              grade_details.player_id = player.id AND grade_details."eventId" = _event.id \
            GROUP BY grade_details.player_id) event_details, team_player \
          WHERE event_details.player_id = team_player.id) t) AS "tableDetails"'
      , queryByPlays = 'SELECT  \
          row_to_json((SELECT z FROM (SELECT \
              ROUND(AVG(grade.cat_1_grade), 2) AS "avgCat1Grade", \
              ROUND(AVG(grade.cat_2_grade), 2) AS "avgCat2Grade", \
              ROUND(AVG(grade.cat_3_grade), 2) AS "avgCat3Grade", \
              ROUND(AVG(grade.cat_1_grade + COALESCE(grade.cat_2_grade, 0) + COALESCE(grade.cat_3_grade, 0)) / (SELECT num_grade_categories FROM team_configuration WHERE go_team_id = $1)::numeric, 2) AS "avgOverallGrade" \
            FROM \
              team_play_grade grade, team_play_data play, team_event event \
            WHERE \
              grade.team_play_data_id = play.id AND \
              play.team_event_id = event.id AND \
              %%__report_clauses__%% \
          ) z)) AS overalls, \
          (SELECT array_to_json(array_agg(t)) FROM \
          (SELECT \
            play.play_number AS "playNumber", \
            play.quarter, \
            play.game_down AS "down", \
            play.game_distance AS "distance", \
            play.yard_line AS "yardLine", \
            play.game_possession AS "possession", \
            COUNT(grade.team_player_id) AS "numPlayers", \
            ROUND(AVG(grade.cat_1_grade), 2) AS "avgCat1Grade", \
            ROUND(AVG(grade.cat_2_grade), 2) AS "avgCat2Grade", \
            ROUND(AVG(grade.cat_3_grade), 2) AS "avgCat3Grade", \
            ROUND(AVG(grade.cat_1_grade + COALESCE(grade.cat_2_grade, 0) + COALESCE(grade.cat_3_grade, 0)) / (SELECT num_grade_categories FROM team_configuration WHERE go_team_id = $1)::numeric, 2) AS "avgOverallGrade", \
            json_agg((SELECT x FROM ( \
              SELECT player.first_name AS "firstName", \
              player.last_name AS "lastName", \
              ' + jerseyNumberFromTeamAssignment.replace('%%__team_player_assignment_player_id__%%', 'player.id') + ', \
              ' + positionFromTeamAssignment.replace('%%__team_player_assignment_player_id__%%', 'player.id') + ', \
              (SELECT short_name FROM go_position_type WHERE id=grade.position_played) as "positionPlayed", \
              (SELECT name FROM go_platoon_type WHERE id=grade.go_platoon_type_id) as "platoon", \
              grade.cat_1_grade AS "cat1Grade", \
              grade.cat_2_grade AS "cat2Grade", \
              grade.cat_3_grade AS "cat3Grade", \
              round((grade.cat_1_grade + COALESCE(grade.cat_2_grade, 0) + COALESCE(grade.cat_3_grade, 0)) / (SELECT num_grade_categories FROM team_configuration WHERE go_team_id = $1)::numeric, 2) AS "overallGrade", \
              (SELECT name FROM go_play_factor WHERE id=grade.go_play_factor_id) AS "factorInPlay", \
              CASE WHEN (SELECT grade_fields->\'specialPlay\' FROM team_configuration WHERE go_team_id = $1) IS NOT NULL AND grade.custom_fields->\'specialPlay\' <> \'null\' \
                THEN replace((grade.custom_fields->\'specialPlay\')::text, \'"\', \'\') \
              END AS "specialPlay", \
              %%__grade_notes__%% AS notes \
            ) x) ORDER BY round((grade.cat_1_grade + COALESCE(grade.cat_2_grade, 0) + COALESCE(grade.cat_3_grade, 0)) / (SELECT num_grade_categories FROM team_configuration WHERE go_team_id = $1)::numeric, 2) DESC) AS grades \
          FROM \
            team_play_grade grade, team_play_data play, team_event event, team_player player \
          WHERE \
            grade.team_play_data_id = play.id AND \
            play.team_event_id = event.id AND \
            grade.team_player_id = player.id AND \
            %%__report_clauses__%% \
          GROUP BY play.id \
          ORDER BY "playNumber") t) AS "tableDetails"'
      , graphQuery = 'SELECT array_to_json(array_agg(s)) FROM \
          (SELECT \
              ROW_NUMBER() OVER (ORDER BY event.date) AS "eventNumber", \
              event.date, \
              event.name, \
              event.id, \
              ROUND(AVG(grade.cat_1_grade), 2) AS "avgCat1Grade", \
              ROUND(AVG(grade.cat_2_grade), 2) AS "avgCat2Grade", \
              ROUND(AVG(grade.cat_3_grade), 2) AS "avgCat3Grade", \
              ROUND(AVG(grade.cat_1_grade + COALESCE(grade.cat_2_grade, 0) + COALESCE(grade.cat_3_grade, 0)) / (SELECT num_grade_categories FROM team_configuration WHERE go_team_id = $1)::numeric, 2) AS "avgOverallGrade", \
              COUNT(DISTINCT play.id) AS "numPlays" \
            FROM \
              team_play_grade grade, team_play_data play, team_event event \
            WHERE \
              grade.team_play_data_id = play.id AND \
              play.team_event_id = event.id AND \
              %%__report_clauses__%% \
            GROUP BY event.id \
            ORDER BY event.date \
          ) s'
      , graphQueryForSingleEvent = 'SELECT array_to_json(array_agg(s)) FROM \
          (SELECT \
              CASE \
                WHEN play.quarter = 1 THEN \'1st\' \
                WHEN play.quarter = 2 THEN \'2nd\' \
                WHEN play.quarter = 3 THEN \'3rd\' \
                WHEN play.quarter = 4 THEN \'4th\' \
                ELSE \'no\' \
              END || \' Qtr\' AS "category", \
              ROUND(AVG(grade.cat_1_grade), 2) AS "avgCat1Grade", \
              ROUND(AVG(grade.cat_2_grade), 2) AS "avgCat2Grade", \
              ROUND(AVG(grade.cat_3_grade), 2) AS "avgCat3Grade", \
              ROUND(AVG(grade.cat_1_grade + COALESCE(grade.cat_2_grade, 0) + COALESCE(grade.cat_3_grade, 0)) / (SELECT num_grade_categories FROM team_configuration WHERE go_team_id = $1)::numeric, 2) AS "avgOverallGrade", \
              COUNT(DISTINCT play.id) AS "numPlays" \
            FROM \
              team_play_grade grade, team_play_data play, team_event event \
            WHERE \
              grade.team_play_data_id = play.id AND \
              play.team_event_id = event.id AND \
              %%__report_clauses__%% \
            GROUP BY play.quarter \
            ORDER BY play.quarter \
          ) s'
      , graphQueryForPlayerComparison = 'SELECT row_to_json((SELECT gd FROM ( \
          SELECT \
          row_to_json (( \
            SELECT p FROM (SELECT \
              id, \
              first_name AS "firstName", \
              last_name AS "lastName", \
              ' + jerseyNumberFromTeamAssignment.replace('%%__team_player_assignment_player_id__%%', 'team_player.id') + ', \
              ' + positionFromTeamAssignment.replace('%%__team_player_assignment_player_id__%%', 'team_player.id') + ' \
            FROM team_player \
            WHERE id = %%__player_to_compare_id__%%) p)) AS "player", \
          %%__comparison_detail__%% , \
          (SELECT array_to_json(array_agg(s)) FROM ( \
          SELECT \
              ROW_NUMBER() OVER (ORDER BY event.date) AS "eventNumber", \
              event.name, \
              event.id, \
              ROUND(AVG(CASE WHEN grade.team_player_id = %%__player_to_compare_id__%% THEN grade.cat_1_grade + COALESCE(grade.cat_2_grade, 0) + COALESCE(grade.cat_3_grade, 0) END) / (SELECT num_grade_categories FROM team_configuration WHERE go_team_id = $1)::numeric, 2) AS "playerOverall", \
              COUNT(DISTINCT play.id) AS "numPlays", \
              %%__player_comparison__%% \
            FROM \
              team_play_grade grade, team_play_data play, team_event event \
            WHERE \
              grade.team_play_data_id = play.id AND \
              play.team_event_id = event.id AND \
              %%__report_clauses__%% \
            GROUP BY event.id \
            ORDER BY event.date \
            ) s) AS "data") gd))'
      , graphQueryForPlayerComparisonByQuarter = 'SELECT row_to_json((SELECT gd FROM ( \
          SELECT \
          row_to_json (( \
            SELECT p FROM (SELECT \
              id, \
              first_name AS "firstName", \
              last_name AS "lastName", \
              ' + jerseyNumberFromTeamAssignment.replace('%%__team_player_assignment_player_id__%%', 'team_player.id') + ', \
              ' + positionFromTeamAssignment.replace('%%__team_player_assignment_player_id__%%', 'team_player.id') + ' \
            FROM team_player \
            WHERE id = %%__player_to_compare_id__%%) p)) AS "player", \
          %%__comparison_detail__%% , \
          (SELECT array_to_json(array_agg(s)) FROM ( \
          SELECT \
              CASE \
                WHEN play.quarter = 1 THEN \'1st\' \
                WHEN play.quarter = 2 THEN \'2nd\' \
                WHEN play.quarter = 3 THEN \'3rd\' \
                WHEN play.quarter = 4 THEN \'4th\' \
                ELSE \'no\' \
              END || \' Qtr\' AS "category", \
              ROUND(AVG(CASE WHEN grade.team_player_id = %%__player_to_compare_id__%% THEN grade.cat_1_grade + COALESCE(grade.cat_2_grade, 0) + COALESCE(grade.cat_3_grade, 0) END) / (SELECT num_grade_categories FROM team_configuration WHERE go_team_id = $1)::numeric, 2) AS "playerOverall", \
              COUNT(DISTINCT play.id) AS "numPlays", \
              %%__player_comparison__%% \
            FROM \
              team_play_grade grade, team_play_data play, team_event event \
            WHERE \
              grade.team_play_data_id = play.id AND \
              play.team_event_id = event.id AND \
              %%__report_clauses__%% \
            GROUP BY play.quarter \
            ORDER BY play.quarter \
            ) s) AS "data") gd))'
      , reportClause           = 'grade.go_team_id = $1 AND event.go_event_type_id = $2 AND '
      , notesQuery             = 'CASE WHEN TRIM(grade.notes) IS NOT NULL THEN CONCAT(grade.notes,\' (\',(SELECT initials FROM view_go_user WHERE id = grade.go_user_id),\')\') END'
      , playerComparison       = 'ROUND(AVG(CASE WHEN grade.team_player_id != %%__player_to_compare_id__%% THEN grade.cat_1_grade + COALESCE(grade.cat_2_grade, 0) + COALESCE(grade.cat_3_grade, 0) END) / (SELECT num_grade_categories FROM team_configuration WHERE go_team_id = $1)::numeric, 2) AS "playersGroupOverall"'
      , playerGroupDetail      = 'row_to_json ((SELECT g FROM (SELECT name FROM team_position_group WHERE id = $3) g)) AS "playersGroup"'
      , playerPlatoonDetails   = 'row_to_json ((SELECT g FROM (SELECT name FROM go_platoon_type WHERE id = $3) g)) AS "playersGroup"'
      , individualPlayerDetail = []
      , avgByIndividualPlayer  = []
      , players                = []
      , clausesByPlayData      = []
      , queryParams            = [teamId, filter]
      , gameDistance
      , gameDistanceSplitted
      , clauseByPlayData
      , playDataFields
      , splittedField
      , dnDist
      , fieldPosition
      , playerComparisonDetail
      , paramIndex
      , query
      , periodDates
      , partGameParams
      , initFieldPosition
      , endFieldPosition
      , onTeamSide
      , onOpponentSide
      , teamPlayerAssignmentSeason
      , auxDate
      , auxMonth
      , auxYear;

    console.log('TeamReport.run building query...');

    switch (criteria) {
      case 'platoon' :
        reportClause += 'grade.go_platoon_type_id = $3 AND ';
        queryParams.push(criteriaRange);
        break;
      case 'position_group' :
        reportClause += 'grade.position_played IN ( \
          SELECT go_position_type_id FROM team_position_type WHERE go_team_id = $1 AND team_position_group_id = $3 \
        ) AND ';
        queryParams.push(criteriaRange);
        break;
      case 'players' :
        criteriaRange.split(',').forEach(function(playerId) {
          players.push('\'' + playerId + '\'');
        });

        if (condition === 'playerToCompare' && conditionRange) {
          players.push('\'' + conditionRange + '\'');
        }

        reportClause += 'grade.team_player_id IN (' + players.join(',') + ') AND ';
        break;
    }

    paramIndex = queryParams.length + 1;

    switch (scope) {
      case 'event' :
        reportClause += 'event.id = $' + paramIndex;
        graphQuery    = graphQueryForSingleEvent;
        queryParams.push(scopeRange);

        teamPlayerAssignmentSeason = '(SELECT \
            CASE \
              WHEN date_part(\'month\'::text,te.date)::integer <= 2 THEN date_part(\'year\'::text, te.date)::integer - 1 \
              ELSE date_part(\'year\'::text, te.date)::integer \
             END \
          FROM team_event te WHERE te.id = ' + scopeRange + ')';

        break;
      case 'season' :
        reportClause += 'event.season = $' + paramIndex;
        queryParams.push(scopeRange);

        teamPlayerAssignmentSeason = scopeRange;

        break;
      case 'period' :
        periodDates   = scopeRange.split(',');
        reportClause += 'event.date between \'' + periodDates[0] + '\' and \'' + periodDates[1] + '\'';
        auxDate       = periodDates[1].split('/');
        auxMonth      = auxDate[1];
        auxYear       = auxDate[2];

        teamPlayerAssignmentSeason = auxMonth <= 2 ? parseInt(auxYear) - 1 : auxYear;

        break;
      case 'part_game' :
        partGameParams = scopeRange.split(',');

        if (partGameParams.length === 3) {
          reportClause += 'event.id = $' + paramIndex;
          reportClause += ' AND play.play_number between ' + partGameParams[1] + ' and ' + partGameParams[2];
          graphQuery    = graphQueryForSingleEvent;
          queryParams.push(partGameParams[0]);

          teamPlayerAssignmentSeason = '(SELECT \
            CASE \
              WHEN date_part(\'month\'::text,te.date)::integer <= 2 THEN date_part(\'year\'::text, te.date)::integer - 1 \
              ELSE date_part(\'year\'::text, te.date)::integer \
             END \
          FROM team_event te WHERE te.id = ' + partGameParams[0] + ')';

        } else {
          console.log('The plays to review are invalid or the event id is missing');
          return cb(new Error('The plays to review are invalid or the event id is missing'));
        }
        break;
    }

    paramIndex = queryParams.length + 1;

    switch (type) {
      case 'gameGroupReview':
        baseQuery = queryByPlays;
        break;
      case 'playerComparison' :
        if (condition === 'playerToCompare' && conditionRange) {
          graphQuery = (scope === 'event' ? graphQueryForPlayerComparisonByQuarter : graphQueryForPlayerComparison).replace(/%%__player_to_compare_id__%%/g, '$' + paramIndex);
          queryParams.push(conditionRange);

          switch (criteria) {
            case 'players':
              if (players && players.length <= 4) {
                players.forEach(function(player, index) {
                  if (player !== ('\'' + conditionRange + '\'')) {
                    avgByIndividualPlayer.push('ROUND(AVG(CASE WHEN grade.team_player_id = ' + player + ' THEN grade.cat_1_grade + COALESCE(grade.cat_2_grade, 0) + COALESCE(grade.cat_3_grade, 0) END) / (SELECT num_grade_categories FROM team_configuration WHERE go_team_id = $1)::numeric, 2) AS "individualPlayerOverall_'+ (index+1) + '"');
                    individualPlayerDetail.push('row_to_json ((SELECT p FROM (SELECT id, first_name AS "firstName", last_name AS "lastName", ' + jerseyNumberFromTeamAssignment.replace('%%__team_player_assignment_player_id__%%', 'team_player.id') + ', ' + positionFromTeamAssignment.replace('%%__team_player_assignment_player_id__%%', 'team_player.id') + ' FROM team_player WHERE id = ' + player + ') p)) AS "individualPlayer_' + (index+1) + '"');
                  }
                });

                playerComparison = avgByIndividualPlayer.join(',');
                playerComparisonDetail = individualPlayerDetail.join(',');
              } else {
                playerComparisonDetail = '\'Specific Players\' AS "playersGroup"';
              }
              break;

            case 'position_group':
              playerComparisonDetail = playerGroupDetail;
              break;

            case 'platoon':
              playerComparisonDetail = playerPlatoonDetails;
              break;
          }

          playerComparison = playerComparison.replace(/%%__player_to_compare_id__%%/g, '$' + paramIndex);
        }
        break;
    }

    if (condition === 'includeCoachesOnlyNotes' && conditionRange !== 'true') {
      // false or undefined
      notesQuery = 'CASE WHEN grade.players_can_see_notes THEN ' + notesQuery + ' ELSE null END';
    }


    if (conditionRange) {
      switch (condition) {
        case 'downDistance':
          dnDist = conditionRange.split(',');
          paramIndex = queryParams.length + 1;

          if (dnDist[0] && !isNaN(dnDist[0])) {
            reportClause += ' AND play.game_down = $' + paramIndex;
            paramIndex += 1;
            queryParams.push(dnDist[0]);
          }

          if (dnDist[1] && dnDist[1] !== 'null') {
            gameDistance = dnDist[1];

            if (gameDistance.split(':').length > 1) {
              // it's a series
              gameDistanceSplitted = gameDistance.split(':');

              if (gameDistanceSplitted[0] !== '-') {
                reportClause += ' AND play.game_distance >= $' + paramIndex;

                queryParams.push(gameDistanceSplitted[0]);
                paramIndex += 1;
              }

              if (gameDistanceSplitted[1] !== '-') {
                reportClause += ' AND play.game_distance < $' + paramIndex;
                queryParams.push(gameDistanceSplitted[1]);
              }

            } else {
              // it's a single number
              reportClause += ' AND play.game_distance = $' + paramIndex;
              queryParams.push(dnDist[1]);
            }
          }
          break;

        case 'fieldPosition':
          fieldPosition     = conditionRange.split(',');
          initFieldPosition = parseFloat(fieldPosition[0]);
          endFieldPosition  = parseFloat(fieldPosition[1]);
          paramIndex        = queryParams.length + 1;
          onTeamSide        = initFieldPosition < 0 && endFieldPosition < 0;
          onOpponentSide    = initFieldPosition > 0 && endFieldPosition > 0;
          initFieldPosition = parseInt(initFieldPosition);
          endFieldPosition  = parseInt(endFieldPosition);

          if (onTeamSide || onOpponentSide) {
            reportClause += ' AND play.yard_line <= $' + paramIndex + ' AND play.yard_line >= $' + (paramIndex + 1);

            queryParams.push(initFieldPosition > endFieldPosition ? initFieldPosition : endFieldPosition);
            queryParams.push(initFieldPosition > endFieldPosition ? endFieldPosition : initFieldPosition);

            console.log('TeamReport.run querying fields on the same side...');

          } else {
            reportClause += ' AND (play.yard_line <= $' + paramIndex + ' OR play.yard_line >= $' + (paramIndex + 1) + ')';

            queryParams.push(initFieldPosition > endFieldPosition ? endFieldPosition : initFieldPosition);
            queryParams.push(initFieldPosition > endFieldPosition ? initFieldPosition : endFieldPosition);

            console.log('TeamReport.run querying fields on different sides...');
          }

          break;

        case 'playDataFields':
          playDataFields = conditionRange.split(',');
          paramIndex     = queryParams.length + 1;

          if (playDataFields.length > 0) {

            reportClause += ' AND (';

            playDataFields.forEach(function(field) {
              splittedField = field.split('=');

              switch (splittedField[0]) {
                case 'and':
                  clauseByPlayData = 'AND ';
                  break;

                case 'or':
                  clauseByPlayData = 'OR ';
                  break;

                default:
                  clauseByPlayData = '';
              }

              clauseByPlayData += 'play.data->>$' + paramIndex + ' = $' + (paramIndex + 1);

              clausesByPlayData.push(clauseByPlayData);
              queryParams.push(splittedField[1]);
              queryParams.push(splittedField[2]);

              paramIndex += 2;
            });

            reportClause += clausesByPlayData.join(' ') + ')';
          }

          break;
      }
    }

    query = baseQuery.replace(/%%__graph_query__%%/g, graphQuery);
    query = query.replace(/%%__grade_notes__%%/g, notesQuery);
    query = query.replace(/%%__player_comparison__%%/g, playerComparison);
    query = query.replace(/%%__comparison_detail__%%/g, playerComparisonDetail);

    query = query.replace(/%%__report_clauses__%%/g, reportClause);
    query = query.replace(/%%__report_overalls_clause__%%/g, condition === 'playerToCompare' && conditionRange ?
      reportClause + ' AND grade.team_player_id = \'' + conditionRange + '\'':
      reportClause);

    query = query.replace(/%%__team_player_assignment_season__%%/g, teamPlayerAssignmentSeason);

    console.log('TeamReport.run executing query...');

    connector.executeSQL(query, queryParams, function(err, result) {
      if (err) {
        console.log('Error: ', err);
        console.log('There was an error running the report, query: %s', query);
        return cb(err);
      }

      if (result && result[0] && result[0].tableDetails) {
        //response['header'] = {};
        response['data']   = result[0];
        console.log('TeamReport.run data found');
      }

       console.log('TeamReport.run returning data...');
      cb(null, response);
    });
  };

  TeamReport.remoteMethod('run', {
    description : 'Run a report with the provided parameters',
    notes       : 'This API is used to run reports which were not previously saved',
    http        : {path: '/run', verb: 'get'},
    returns     : {arg: 'report', type: 'object'},
    accepts     : [{
      arg: 'type',
      type: 'String',
      required: true,
      description:"The report type, expected values are 'gamePerformance', ",
    },
    {
      arg: 'filter',
      type: 'String',
      required: true,
      description:"The report filter, expected values are event types.",
    },
    {
      arg: 'criteria',
      type: 'String',
      required: true,
      description:"The report criteria, expected values are 'platoon', 'position_group', 'players'",
    },
    {
      arg: 'criteriaRange',
      type: 'String',
      required: true,
      description:"The report criteria range",
    },
    {
      arg: 'scope',
      type: 'String',
      required: true,
      description:"The report scope, expected values are 'event', 'season', 'period', 'part_game'",
    },
    {
      arg: 'scopeRange',
      type: 'String',
      required: true,
      description:"The scope range",
    },
    {
      arg: 'condition',
      type: 'String',
      required: false,
      description:"The report condition",
    },
    {
      arg: 'conditionRange',
      type: 'String',
      required: false,
      description:"The report condition range",
    }]
  });

  TeamReport.playData = function(cb) {
    var response  = []
      , connector = TeamReport.getDataSource().connector
      , teamId    = loopback.getCurrentContext().get('accessToken').teamId
      , query     = 'SELECT json_agg(t) AS "playData" FROM (SELECT \
                      play_data.key, \
                      (SELECT array_agg(d.value) FROM ( \
                        SELECT DISTINCT data->>play_data.key AS value \
                        FROM team_play_data \
                        WHERE data->>play_data.key <> \' \' AND \
                          data->>play_data.key <> \'\' AND \
                          go_team_id = $1 \
                      ) d) AS values \
                    FROM \
                    (SELECT DISTINCT json_object_keys(data::json) AS key \
                      FROM team_play_data \
                      WHERE go_team_id = $1 \
                      ORDER BY key) play_data  \
                      WHERE play_data.key <> \' \' AND play_data.key <> \'\') t WHERE array_length(values, 1) > 0';

    connector.executeSQL(query, [teamId], function(err, result) {

      if (err) {
        console.log('Error: ', err);
        console.log('There was an error recovering the play data fields, query: %s', query);
        return cb(err);
      }

      if (result && result[0]) {
        response = result[0].playData;
      }

      cb(null, response);

    });
  };

  TeamReport.remoteMethod('playData', {
    description : 'Returns all play data fields with their values',
    notes       : 'This API is used to return play data fields including their values',
    http        : {path: '/playData', verb: 'get'},
    returns     : {arg: 'data', type: 'object'}
  });

  TeamReport.observe('access', function(ctx, next) {
    var userId = loopback.getCurrentContext().get('accessToken').userId
     ,  query  = ctx.query;

    if (query.where && query.where.id) {
      query.where['user'] = userId;
    } else {
      query['where'] = {
        or: [
          {user     : userId},
          {isPublic : true}
        ]
      };
    }

    next();

  });

};
