var loopback = require('loopback');
var assert = require('assert');

module.exports = function(TeamPlayerGameCutupStat) {
  TeamPlayerGameCutupStat.observe('before save', function(ctx, next) {
    var modelName = 'TeamPlayerGameCutupStat';
    if (ctx.instance) {
      console.log('TeamPlayerGameCutupStat.observe before save, setup lastUpdate for new instance');
      ctx.instance.lastUpdate = new Date();
    } else {
      console.log('TeamPlayerGameCutupStat.observe before save, setup lastUpdate for existing instance');
      ctx.data.lastUpdate = new Date();
    }

    next();
  });

  /*
   */
  TeamPlayerGameCutupStat.updatePlayerCutupStats = function(_playerId, _cutupId, cb) {
    var connector = TeamPlayerGameCutupStat.getDataSource().connector;
    var ctx = loopback.getCurrentContext();
    var accessToken = ctx.get('accessToken');
    var requestQuery = 'SELECT  * from godeepmobile.view_team_player_event_grades_by_cutup where "teamPlayer" = $1 and "teamCutup" = $2';

    console.log('TeamPlayerGameCutupStat.updatePlayerCutupStats');
    connector.executeSQL(requestQuery, [_playerId, _cutupId], function(err, records) {
      if (err) {
        console.log('TeamPlayerGameCutupStat.updatePlayerCutupStats; select view_team_player_event_grades_by_cutup execute got error ', err);
        return cb(err);
      }
      console.log('TeamPlayerGameCutupStat.updatePlayerCutupStats: execute done');

      if (records && records.length > 0 && records[0] && records[0].teamPlayer) {
        var result = records[0];
        console.log('TeamPlayerGameCutupStat.updatePlayerCutupStats: execute query has grades');
        TeamPlayerGameCutupStat.upsert(result,function(err,obj) {
          if (err) {
            console.log('TeamPlayerGameCutupStat.updatePlayerCutupStats upsert() got error ', err);
            return cb(err);
          }
          console.log('TeamPlayerGameCutupStat.updatePlayerCutupStats upsert() done');
          cb(null, obj);
        });
      } else {
        // there are no grades for this player for this event, return empty object
        console.log('TeamPlayerGameCutupStat.updatePlayerCutupStats execute query has no grades');
        cb(null,{});
      }
    }); // execute()
  }; // TeamPlayerGameCutupStat.updatePlayerCutupStats

  /*
   */
  TeamPlayerGameCutupStat.remoteMethod(
    'updatePlayerCutupStats',
    {
      description: "Update the event statistics for a player",
      accepts: [
        {arg: 'playerId', type: 'String', required: true, description:"Team Player ID (uuid)"},
        {arg: 'cutupId', type: 'Number', required: true, description:"Team Cutup ID"}
      ],
      http: {path: '/updatePlayerCutupStats', verb: 'post'}
    }
  );  // remoteMethod()

  TeamPlayerGameCutupStat.updateCutupStats = function(_cutupId, cb) {
    var connector = TeamPlayerGameCutupStat.getDataSource().connector;
    var ctx = loopback.getCurrentContext();
    var accessToken = ctx.get('accessToken');
    var requestQuery = 'SELECT godeepmobile.refresh_cutup_stats($1)';

    console.log('TeamPlayerGameCutupStat.updateCutupStats');
    connector.executeSQL(requestQuery, [_cutupId], function(err, records) {
      if (err) {
        console.log('TeamPlayerGameCutupStat.updateCutupStats: refresh_cutup_stats execute got error ', err);
        return cb(err);
      }
      console.log('TeamPlayerGameCutupStat.updateCutupStats: refresh_cutup_stats execute done');
      return cb();
    }); // execute()
  }; // TeamPlayerGameCutupStat.updateCutupStats

  TeamPlayerGameCutupStat.remoteMethod(
    'updateCutupStats',
    {
      description: "Update the event statistics for all players that participated in an game cutup",
      accepts: [
        {arg: 'cutupId', type: 'Number', required: true, description:"Team Cutup ID"}
      ],
      http: {path: '/updateCutupStats', verb: 'post'}
    }
  );  // remoteMethod()
};
