module.exports = function(TeamReportPositionTypeEventDataByQuarter) {
  var isStatic = true;
  TeamReportPositionTypeEventDataByQuarter.disableRemoteMethod('create', isStatic);
  TeamReportPositionTypeEventDataByQuarter.disableRemoteMethod('update', isStatic);
  TeamReportPositionTypeEventDataByQuarter.disableRemoteMethod('updateAll', isStatic);
  TeamReportPositionTypeEventDataByQuarter.disableRemoteMethod('upsert', isStatic);
  TeamReportPositionTypeEventDataByQuarter.disableRemoteMethod('exists', isStatic);
  TeamReportPositionTypeEventDataByQuarter.disableRemoteMethod('findById', isStatic);
  TeamReportPositionTypeEventDataByQuarter.disableRemoteMethod('deleteById', isStatic);
  isStatic = false;
  TeamReportPositionTypeEventDataByQuarter.disableRemoteMethod('updateAttribute', isStatic);
  TeamReportPositionTypeEventDataByQuarter.disableRemoteMethod('updateAttributes', isStatic);
};
